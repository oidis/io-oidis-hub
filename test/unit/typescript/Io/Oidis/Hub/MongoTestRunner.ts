/*! ******************************************************************************************************** *
 *
 * Copyright 2020-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { UnitTestRunner } from "./UnitTestRunner.js";

import { BaseObject } from "@io-oidis-commons/Io/Oidis/Commons/Primitives/BaseObject.js";
import { JsonUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/JsonUtils.js";
import { LogIt } from "@io-oidis-commons/Io/Oidis/Commons/Utils/LogIt.js";
import { StringUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/StringUtils.js";
import { assert } from "@io-oidis-commons/test/unit/Io/Oidis/Commons/UnitTestEnvironment.js";
import { FileSystemHandler } from "@io-oidis-localhost/Io/Oidis/Localhost/Connectors/FileSystemHandler.js";
import { EnvironmentHelper } from "@io-oidis-localhost/Io/Oidis/Localhost/Utils/EnvironmentHelper.js";
import { GraphQL } from "../../../../../../source/typescript/Io/Oidis/Hub/DAO/GraphQL/GraphQL.js";
import { MongoDriver } from "../../../../../../source/typescript/Io/Oidis/Hub/DAO/Mongoose/MongoContext.js";
import { IExecutionResult } from "../../../../../../source/typescript/Io/Oidis/Hub/Interfaces/DAO/GraphQL/IExecutionResult.js";
import { Loader } from "../../../../../../source/typescript/Io/Oidis/Hub/Loader.js";

export class MongoDirectContext extends BaseObject {
    // https://mongoosejs.com/docs/api/connection.html
    private connection : any;

    constructor($connection : any) {
        super();
        this.connection = $connection;
    }

    public async Insert($collection : string, $items : any[]) : Promise<string[]> {
        return this.connection.collection($collection).insertMany($items)
            .then(async ($arg : any) : Promise<string[]> => {
                const ids : string[] = [];
                for (const item in $arg.insertedIds) {
                    if ($arg.insertedIds.hasOwnProperty(item)) {
                        ids.push($arg.insertedIds[item]);
                    }
                }
                LogIt.Debug("Inserted " + $items.length + " documents into " + $collection);
                return ids;
            })
            .catch(($error : any) : void => {
                LogIt.Warning("MongoDB insert into " + $collection + " failed: " + $error.stack);
            });
    }

    public async Update($collection : string, $items : any[]) : Promise<string[]> {
        const collection : any = this.connection.collection($collection);
        const promises : Array<Promise<any>> = [];
        $items.forEach(($item : any) : void => {
            promises.push(new Promise<any>(($resolve : any, $reject : any) : void => {
                collection.updateOne({_id: $item._id}, {$set: $item})
                    .then(() : void => {
                        $resolve();
                    })
                    .catch(($reason : any) : void => {
                        $reject($reason);
                    });
            }));
        });
        return Promise.all(promises)
            .then(() : string[] => {
                LogIt.Debug("Updated " + $items.length + " documents");
                const ids : string[] = [];
                for (let index : number = 0; index < $items.length; index++) {
                    if ($items.hasOwnProperty(index)) {
                        ids.push($items[index]._id);
                    }
                }
                return ids;
            });
    }

    public async DropDatabase() : Promise<void> {
        return this.connection.dropDatabase()
            .then(() : void => {
                LogIt.Debug("Database successfully dropped.");
            });
    }

    public async DropCollection($collection : string) : Promise<void> {
        return this.connection.dropCollection($collection)
            .then(() : void => {
                LogIt.Debug("Collection '{0}' successfully dropped.", $collection);
            });
    }

    public async getAll($collection : string, $queryObject : any = {}, $skip : number = 0, $limit : number = 0) : Promise<any[]> {
        return this.connection.collection($collection).find($queryObject, {skip: $skip, limit: $limit}).toArray();
    }

    public async getById($collection : string, $id : string) : Promise<any> {
        return this.connection.collection($collection).findOne({_id: $id})
            .then(($result : any) : void => {
                LogIt.Debug("Found 1 document with id = '{0}'.", $id);
                return $result;
            });
    }
}

export abstract class MongoTestRunner extends UnitTestRunner {
    protected mongoMemoryServer : any;
    protected context : MongoDirectContext;
    protected gqlContext : GraphQL;
    protected mongo : MongoDriver;

    protected async before() : Promise<void> {
        this.mongoMemoryServer = await (require("mongodb-memory-server")).MongoMemoryServer.create({
            binary: {
                downloadDir: this.getAbsoluteRoot() + "/../../" + (EnvironmentHelper.IsWindows() ?
                    "dependencies/nodejs/build/node_modules/.cache/mongodb-memory-server/mongodb-binaries" :
                    "dependencies/nodejs/build/patched-home/.cache/mongodb-binaries")
            }
        });
        const connectionString : string = await this.mongoMemoryServer.getUri();
        const connectionParts : string[] = connectionString.split(/[:\/]+/); // eslint-disable-line no-useless-escape
        this.mongo = <MongoDriver>Loader.getInstance().getDBDriver();
        await this.mongo.Open({
            dbName  : connectionParts[3],
            location: connectionParts[1],
            port    : StringUtils.ToInteger(connectionParts[2])
        });
        this.context = new MongoDirectContext(this.mongo.getDb().connection);

        Loader.getInstance().getGQLContext = () => {
            return this.getGraphQLInstance();
        };
        this.gqlContext = Loader.getInstance().getGQLContext();
        this.gqlContext.getComposer().clear();
        this.gqlContext.RegisterModels();

        LogIt.Debug("MongoDB memory server connected for testing...");
    }

    protected async after() : Promise<void> {
        await this.mongo.Close();
        this.mongoMemoryServer.stop();
    }

    protected async tearDown() : Promise<void> {
        try {
            await this.context.DropDatabase();
            LogIt.Debug("Db reset was successful.");
        } catch (ex) {
            LogIt.Warning("Db reset failed.");
            LogIt.Warning(ex.message);
        }
    }

    protected readData($testFileRelativePath : string) : string {
        const fileSystemHandler : FileSystemHandler = Loader.getInstance().getFileSystemHandler();
        this.readData = ($testFileRelativePath : string) : string => {
            return fileSystemHandler.Read("test/resource/data/Io/Oidis/Hub/" + $testFileRelativePath).toString();
        };
        return this.readData($testFileRelativePath);
    }

    protected filterKeys($actual : any, $expected : any) : void {
        const rmKeys : string[] = [];
        for (const key in $actual) {
            if ($actual.hasOwnProperty(key)) {
                if (!$expected.hasOwnProperty(key)) {
                    rmKeys.push(key);
                } else if ($expected[key] === "*") {
                    $actual[key] = "*";
                }
            }
        }
        for (const key of rmKeys) {
            delete $actual[key];
        }
    }

    protected getGraphQLInstance() : GraphQL {
        return new GraphQL();
    }

    protected async assertGraphQL($testCase : ITestCase) : Promise<IExecutionResult> {
        const result : IExecutionResult[] = await this.gqlContext.ExecuteAll($testCase.input);
        const lastResult : IExecutionResult = result[result.length - 1];
        assert.deepEqual(JsonUtils.Sort(lastResult.data), JsonUtils.Sort($testCase.expectedResult), JSON.stringify(lastResult.errors));
        return lastResult;
    }

    protected async assertGraphQLErrorMessage($testCase : ITestCase) : Promise<IExecutionResult> {
        const result : IExecutionResult[] = await this.gqlContext.ExecuteAll($testCase.input);
        const lastResult : IExecutionResult = result[result.length - 1];
        assert.equal(lastResult.errors.length, 1);
        assert.deepEqual(lastResult.errors[0].message, $testCase.expectedResult.errorMessage);
        return lastResult;
    }
}

interface ITestCase {
    input : string | string[];
    expectedResult : any;
}

/*! ******************************************************************************************************** *
 *
 * Copyright 2019 NXP
 * Copyright 2019-2024 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { BaseUnitTestRunner } from "@io-oidis-commons/test/unit/Io/Oidis/Commons/BaseUnitTestRunner.js";
import { EnvironmentArgs } from "@io-oidis-localhost/Io/Oidis/Localhost/EnvironmentArgs.js";
import { Loader } from "../../../../../../source/typescript/Io/Oidis/Hub/Loader.js";

export class UnitTestEnvironmentArgs extends EnvironmentArgs {
    public Load($appConfig : any, $handler : () => void) : void {
        super.Load($appConfig, $handler);
    }

    protected getConfigPaths() : string[] {
        return [];
    }
}

export class UnitTestLoader extends Loader {
    protected processProgramArgs() : boolean {
        // skip DB connectivity
        return true;
    }

    protected initEnvironment() : UnitTestEnvironmentArgs {
        return new UnitTestEnvironmentArgs();
    }
}

export abstract class UnitTestRunner extends BaseUnitTestRunner {
    protected initLoader($className? : any) : void {
        super.initLoader(ObjectValidator.IsEmptyOrNull($className) ? UnitTestLoader : $className);
    }
}

/*! ******************************************************************************************************** *
 *
 * Copyright 2020-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { UnitTestRunner } from "./UnitTestRunner.js";

import { HttpMethodType } from "@io-oidis-commons/Io/Oidis/Commons/Enums/HttpMethodType.js";
import { HttpRequestEventArgs } from "@io-oidis-commons/Io/Oidis/Commons/Events/Args/HttpRequestEventArgs.js";
import { JsonUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/JsonUtils.js";
import { LogIt } from "@io-oidis-commons/Io/Oidis/Commons/Utils/LogIt.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { Reflection } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Reflection.js";
import { StringUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/StringUtils.js";
import { IUnitTestRunnerPromise } from "@io-oidis-commons/test/unit/Io/Oidis/Commons/BaseUnitTestRunner.js";
import { assert } from "@io-oidis-commons/test/unit/Io/Oidis/Commons/UnitTestEnvironment.js";
import { FileSystemHandler } from "@io-oidis-localhost/Io/Oidis/Localhost/Connectors/FileSystemHandler.js";
import { HttpServerEventArgs } from "@io-oidis-localhost/Io/Oidis/Localhost/Events/Args/HttpServerEventArgs.js";
import { HttpRequestParser } from "@io-oidis-localhost/Io/Oidis/Localhost/HttpProcessor/HttpRequestParser.js";
import { IResponse } from "@io-oidis-localhost/Io/Oidis/Localhost/Interfaces/IResponse.js";
import { HttpServer } from "../../../../../../source/typescript/Io/Oidis/Hub/HttpProcessor/HttpServer.js";
import { Loader } from "../../../../../../source/typescript/Io/Oidis/Hub/Loader.js";

export abstract class RESTUnitTestRunner extends UnitTestRunner {
    protected onlineTest : boolean = false;
    protected controller : any;
    private fileSystem : FileSystemHandler;
    private serverLocation : string;
    private host : string;

    protected setServerLocation($value : string) : void {
        this.serverLocation = $value;
    }

    protected setUp() : void {
        if (!ObjectValidator.IsEmptyOrNull(this.controller)) {
            this.controller.ResponseAssert(null);
        }
    }

    protected before() : void | IUnitTestRunnerPromise {
        const config : any = Loader.getInstance().getAppConfiguration();
        config.reverseProxy = <any>{
            enabled: false
        };
        config.domain = <any>{
            location: "",
            ports   : {
                http: 0
            },
            ssl     : {
                enabled: false
            }
        };
        config.tunnels = <any>{
            server: ""
        };

        this.fileSystem = Loader.getInstance().getFileSystemHandler();

        const onComplete : any = () : void => {
            LogIt.Debug("> using server at: {0}", this.serverLocation);
            this.host = StringUtils.Remove(this.serverLocation, "https://", "http://");
        };
        if (ObjectValidator.IsEmptyOrNull(this.serverLocation)) {
            if (this.onlineTest) {
                const server : HttpServer = this.initHttpServer();
                server.Start();
                return ($done : () => void) : void => {
                    server.getEvents().OnStart(($eventArgs : HttpServerEventArgs) : void => {
                        this.serverLocation = "http://localhost:" + $eventArgs.Port();
                        onComplete();
                        $done();
                    });
                };
            } else {
                this.serverLocation = "http://localhost";
            }
        }
        onComplete();
    }

    protected integrityCheck() : string {
        return "";
    }

    protected getServerLocation() : string {
        return this.onlineTest ? this.host : this.serverLocation;
    }

    protected getDefaultHeaders() : any {
        return {
            "accept"         : "application/json",
            "accept-language": "en",
            "host"           : this.host
        };
    }

    protected makeRequest($url : string, $method? : HttpMethodType, $body? : any, $online? : boolean) : any;

    protected makeRequest($url : string, $method? : HttpMethodType, $body? : any, $headers? : any, $online? : boolean) : any;

    protected makeRequest($url : string, $method : HttpMethodType = HttpMethodType.GET, $body : any = {}, $headers : any | boolean = {},
                          $online : boolean = false) : any {
        if (ObjectValidator.IsBoolean($headers)) {
            $online = $headers;
            $headers = {};
        }
        $headers = JsonUtils.Extend(this.getDefaultHeaders(), $headers);
        return {
            Then: ($callback : ($headers : any, $body : string) => void) : void => {
                if (!ObjectValidator.IsString($body)) {
                    $body = JSON.stringify($body);
                }
                this.send({
                    body   : $body,
                    headers: $headers,
                    method : $method,
                    url    : $url
                }, ($headers : any, $body : string) : void => {
                    try {
                        $body = JSON.parse($body);
                    } catch (ex) {
                        // ignore json parse failure
                    }
                    $callback($headers, $body);
                }, $online);
            }
        };
    }

    protected send($requestArgs : any, $callback : ($headers : any, $body : string) => void, $online : boolean = false) : void {
        if (!StringUtils.Contains($requestArgs.url, "http://", "https://")) {
            $requestArgs.url = this.serverLocation + $requestArgs.url;
        }
        this.setUrl($requestArgs.url);
        if (!$online) {
            this.controller.ResponseAssert($callback);
            this.controller.RequestArgs($requestArgs);
            this.controller.Process();
        } else {
            $requestArgs.streamOutput = true;
            $requestArgs.verbose = false;
            this.fileSystem.Download($requestArgs, ($headers : any, $body : string) : void => {
                try {
                    $callback($headers, $body);
                } catch (ex) {
                    if (ex.message === "__ASYNC_ASSERT_FAIL__") {
                        console.log(ex.message); // eslint-disable-line no-console
                    } else {
                        throw ex;
                    }
                }
            }, ($httpResponse : any, $callback : IResponse) : boolean => {
                let streamBuffers : any[] = [];
                $httpResponse.on("data", ($chunk : string) : void => {
                    streamBuffers.push($chunk);
                });
                let completed : boolean = false;
                const onComplete : any = () : void => {
                    if (!completed) {
                        completed = true;
                        $callback.OnComplete($httpResponse.headers, Buffer.concat(streamBuffers).toString());
                    }
                };
                let connectionClosed : boolean = false;
                $httpResponse.on("close", () : void => {
                    connectionClosed = true;
                    streamBuffers = [];
                    onComplete();
                });
                $httpResponse.on("end", () : void => {
                    if (!connectionClosed) {
                        onComplete();
                    }
                });
                return false;
            });
        }
    }

    protected assertHeaders($actual : any, $expected : any, $isOnline : boolean = false) : void {
        if ($isOnline) {
            ["connection", "keep-alive", "transfer-encoding", "vary", "server", "date"].forEach(($ignoreKey : string) : void => {
                delete $actual[$ignoreKey];
            });
        }
        if (ObjectValidator.IsSet($actual.etag)) {
            $actual.etag = "*";
        }

        assert.deepEqual($actual, $expected);
    }

    protected normalizeResponseBody($data : any) : any {
        return $data;
    }

    protected assertBody($actual : any, $expected : any) : void {
        assert.deepEqual(this.normalizeResponseBody($actual), this.normalizeResponseBody($expected));
    }

    protected setController($instance : any) : void {
        this.controller = $instance;
        const $className : any = Reflection.getInstance().getClass(this.controller.getClassName());
        this.controller.responseCallback = null;
        this.controller.requestOwner = null;

        this.controller.ResponseAssert = function ($callback : ($headers : any, $body : any) => void) : void {
            this.responseCallback = $callback;
        };

        this.controller.RequestArgs = function ($args? : any) : HttpRequestEventArgs {
            if (ObjectValidator.IsObject($args)) {
                this.requestOwner = JsonUtils.Extend({
                    body   : "",
                    headers: {},
                    method : HttpMethodType.GET
                }, $args);
                const request : HttpRequestParser = this.getRequest();
                $args = new HttpRequestEventArgs(request.getUrl());

                this.getHttpManager().getResolverClassName(request.getHostUrl());
                $args.GET().Copy(request.getUrlArgs());
                $args.GET().Copy(this.getHttpManager().getResolverParameters());
                $args.POST().Add(this.requestOwner.body, "Protocol");
            }
            return $className.prototype.RequestArgs.apply(this, [$args]);
        };

        this.controller.getConnector = function () : any {
            if (ObjectValidator.IsEmptyOrNull(this.responseCallback)) {
                return $className.prototype.getConnector.apply(this, []);
            }
            return {
                Send: ($args : any) : void => {
                    if (ObjectValidator.IsObject($args)) {
                        this.responseCallback($args.headers, $args.body);
                    } else {
                        this.responseCallback($args);
                    }
                }
            };
        };

        this.controller.getRequest = function () : any {
            if (ObjectValidator.IsEmptyOrNull(this.responseCallback)) {
                return $className.prototype.getRequest.apply(this, []);
            }
            return new HttpRequestParser(this.requestOwner.url, this.requestOwner);
        };
    }

    protected initHttpServer() : HttpServer {
        return new HttpServer();
    }
}

/*! ******************************************************************************************************** *
 *
 * Copyright 2022-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { MongoTestRunner } from "../../MongoTestRunner.js";

import { JsonUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/JsonUtils.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { Test } from "@io-oidis-commons/test/unit/Io/Oidis/Commons/BaseUnitTestRunner.js";
import { assert } from "@io-oidis-commons/test/unit/Io/Oidis/Commons/UnitTestEnvironment.js";
import { BaseListToRefConverter } from "../../../../../../../../source/typescript/Io/Oidis/Hub/DAO/Converters/BaseListToRefConverter.js";
import { BaseToRefConverter } from "../../../../../../../../source/typescript/Io/Oidis/Hub/DAO/Converters/BaseToRefConverter.js";
import { BaseModel } from "../../../../../../../../source/typescript/Io/Oidis/Hub/DAO/Models/BaseModel.js";
import { IContext } from "../../../../../../../../source/typescript/Io/Oidis/Hub/Interfaces/DAO/IConverter.js";
import { Entity, Property } from "../../../../../../../../source/typescript/Io/Oidis/Hub/Primitives/Decorators.js";

export class PotMockSubDataToRefConverter extends BaseToRefConverter {
    public async from($value : any, $context : IContext) : Promise<any> {
        return PotMockSubData.FindById($value);
    }
}

export class PotMockDataToRefConverter extends BaseToRefConverter {
    public async from($value : any, $context : IContext) : Promise<any> {
        return PotMockData.FindById($value);
    }
}

export class PotMockDataListToRefConverter extends BaseListToRefConverter {
    public async from($value : any, $context : IContext) : Promise<any> {
        const items : PotMockData[] = [];
        for await (const item of $value) {
            items.push(await PotMockData.FindById(item));
        }
        return items;
    }
}

export class PotMockSubDataListToRefConverter extends BaseListToRefConverter {
    public async from($value : any, $context : IContext) : Promise<any> {
        const items : PotMockSubData[] = [];
        for await(const item of $value) {
            items.push(await PotMockSubData.FindById(item));
        }
        return items;
    }
}

@Entity()
export class PotMockSubData extends BaseModel<PotMockSubData> {
    @Property()
    public declare Name : string;

    @Property({noJSON: true})
    public declare Name2 : string;

    protected getSchema() : any {
        return super.getSchema()
            .add({
                name : {_id: false, type: String},
                name2: {_id: false, type: String}
            });
    }
}

@Entity()
export class PotMockData extends BaseModel<PotMockData> {
    @Property()
    public declare Name : string;

    @Property({converter: new PotMockSubDataListToRefConverter()})
    public declare SubData : PotMockSubData[];

    @Property({converter: new PotMockSubDataToRefConverter()})
    public declare SubDataSingle : PotMockSubData;

    constructor() {
        super();

        this.SubData = [];
    }

    protected getSchema() : any {
        return super.getSchema()
            .add({
                name         : {_id: false, type: String},
                subData      : [
                    {_id: false, type: String}
                ],
                subDataSingle: {_id: false, type: String}
            });
    }
}

@Entity()
export class PotMockRoot extends BaseModel<PotMockRoot> {
    @Property()
    public declare Name : string;

    @Property()
    public declare AuthorizedMethods : string[];

    @Property()
    public declare Hidden : boolean;

    @Property({converter: new PotMockDataListToRefConverter()})
    public declare Data : PotMockData[];

    // leave undefined
    @Property({converter: new PotMockDataListToRefConverter()})
    public declare Data1 : PotMockData[];

    @Property({converter: new PotMockDataListToRefConverter(), noResolve: true})
    public declare Data2 : PotMockData[];

    @Property({converter: new PotMockDataListToRefConverter(), noSerialize: true})
    public declare Data3 : PotMockData[];

    @Property({converter: new PotMockDataToRefConverter()})
    public declare DataSingle : PotMockData;

    // leave undefined
    @Property({converter: new PotMockDataToRefConverter()})
    public declare DataSingle1 : PotMockData;

    @Property({converter: new PotMockDataToRefConverter(), noResolve: true})
    public declare DataSingle2 : PotMockData;

    @Property({converter: new PotMockDataToRefConverter(), noSerialize: true})
    public declare DataSingle3 : PotMockData;

    constructor() {
        super();

        this.AuthorizedMethods = [];
        this.Hidden = false;
        this.Data = [];
    }

    protected getSchema() : any {
        return super.getSchema()
            .add({
                authorizedMethods: [
                    {_id: false, type: String}
                ],
                data             : [
                    {_id: false, type: String}
                ],
                data1            : [
                    {_id: false, type: String}
                ],
                data2            : [
                    {_id: false, type: String}
                ],
                data3            : [
                    {_id: false, type: String}
                ],
                dataSingle       : {_id: false, type: String},
                dataSingle1      : {_id: false, type: String},
                dataSingle2      : {_id: false, type: String},
                dataSingle3      : {_id: false, type: String},
                hidden           : {_id: false, required: false, type: Boolean},
                name             : {_id: false, required: true, type: String}
            });
    }
}

@Entity()
export class PotMockBuffer extends BaseModel<PotMockBuffer> {
    @Property({noResolve: false})
    public declare Data : Buffer;

    @Property()
    public declare Name : string;

    protected getSchema() : any {
        return super.getSchema()
            .add({
                data: {_id: false, required: false, type: Buffer},
                name: {_id: false, required: false, type: String}
            });
    }
}

export class PropertyOptionsTest extends MongoTestRunner {

    constructor() {
        super();

        this.setMethodFilter(
            // "testNormal"
            // "testNoResolve"
            // "testNoSerialize"
            // "testToJSON"
        );
    }

    @Test()
    public async Normal() : Promise<void> {
        let root : PotMockRoot[] = await this.context.getAll(PotMockRoot.getCollection());
        assert.ok(ObjectValidator.IsEmptyOrNull(root));

        const data : PotMockRoot = await this.prepareData();

        root = await this.context.getAll(PotMockRoot.getCollection());
        assert.ok(!ObjectValidator.IsEmptyOrNull(root));
        assert.ok(ObjectValidator.IsArray(root));
        assert.equal(root.length, 1);
        delete (<any>root[0]).__v;
        assert.deepEqual(root, [
            {
                _id              : data.Id,
                authorizedMethods: ["ma1", "ma2"],
                data             : [data.Data[0].Id, data.Data[1].Id],
                data1            : [],
                data2            : [],
                data3            : [],
                lastModified     : data.LastModified,
                created          : data.Created,
                className        : data.ClassName,
                deleted          : data.Deleted,
                name             : "mockRoot",
                hidden           : false,
                dataSingle       : data.DataSingle.Id
            }
        ]);

        // const newRoot : MockRoot[] = await MockRoot.Find({});
        // assert.ok(!ObjectValidator.IsEmptyOrNull(newRoot));
        // assert.ok(ObjectValidator.IsArray(newRoot));
        // assert.equal(newRoot.length, 1);

        const newData : PotMockData[] = await this.context.getAll(PotMockData.getCollection());
        assert.ok(!ObjectValidator.IsEmptyOrNull(newData));
        assert.ok(ObjectValidator.IsArray(newData));
        assert.equal(newData.length, 3);
        await this.removeProps(newData, ["__v", "_id", "lastModified", "created", "className"]);
        assert.deepEqual(newData, [
            {
                subData      : [data.Data[0].SubData[0].Id, data.Data[0].SubData[1].Id],
                name         : "n data A",
                subDataSingle: data.Data[0].SubDataSingle.Id,
                deleted      : false
            },
            {
                subData      : [data.Data[1].SubData[0].Id, data.Data[1].SubData[1].Id],
                name         : "m data B",
                subDataSingle: data.Data[1].SubDataSingle.Id,
                deleted      : false
            },
            {
                subData      : [data.DataSingle.SubData[0].Id, data.DataSingle.SubData[1].Id],
                name         : "b data single",
                subDataSingle: data.DataSingle.SubDataSingle.Id,
                deleted      : false
            }
        ]);

        const newSubData : PotMockSubData[] = await this.context.getAll(PotMockSubData.getCollection());
        assert.ok(!ObjectValidator.IsEmptyOrNull(newSubData));
        assert.ok(ObjectValidator.IsArray(newSubData));
        assert.equal(newSubData.length, 9);
        await this.removeProps(newSubData, ["__v", "_id", "lastModified", "created", "className", "deleted"]);
        assert.deepEqual(newSubData, [
            {
                name : "n data subdata A",
                name2: "n data subdata A jsonLess"
            },
            {
                name : "n data subdata B",
                name2: "n data subdata B jsonLess"
            },
            {
                name : "n data subdata single",
                name2: "n data subdata single jsonLess"
            },
            {
                name : "m data subdata A",
                name2: "m data subdata A jsonLess"
            },
            {
                name : "m data subdata B",
                name2: "m data subdata B jsonLess"
            },
            {
                name : "m data subdata single",
                name2: "m data subdata single jsonLess"
            },
            {
                name : "b subdata A",
                name2: "b subdata A jsonLess"
            },
            {
                name : "b subdata B",
                name2: "b subdata B jsonLess"
            },
            {
                name : "b subdata single",
                name2: "b subdata single jsonLess"
            }
        ]);

        // assert.equal(newRoot[0].Name, "mockRoot");
        // assert.deepEqual(newRoot[0].AuthorizedMethods, ["ma1", "ma2"]);
        // assert.deepEqual(newRoot[0].DataSingle.Name, "b data single");
        // assert.deepEqual(newRoot[0].DataSingle.SubDataSingle.Name, "b subdata single");
        // assert.deepEqual(newRoot[0].DataSingle.SubData[0].Name, "b subdata A");
        // assert.deepEqual(newRoot[0].DataSingle.SubData[1].Name, "b subdata B");
        //
        // assert.deepEqual(newRoot[0].Data[0].Name, "n data A");
        // assert.deepEqual(newRoot[0].Data[0].SubDataSingle.Name, "n data subdata single");
        // assert.deepEqual(newRoot[0].Data[0].SubData[0].Name, "n data subdata A");
        // assert.deepEqual(newRoot[0].Data[0].SubData[1].Name, "n data subdata B");
        //
        // assert.deepEqual(newRoot[0].Data[1].Name, "m data B");
        // assert.deepEqual(newRoot[0].Data[1].SubDataSingle.Name, "m data subdata single");
        // assert.deepEqual(newRoot[0].Data[1].SubData[0].Name, "m data subdata A");
        // assert.deepEqual(newRoot[0].Data[1].SubData[1].Name, "m data subdata B");
    }

    @Test()
    public async NoResolve() : Promise<void> {
        const root : PotMockRoot[] = await this.context.getAll(PotMockRoot.getCollection());
        assert.ok(ObjectValidator.IsEmptyOrNull(root));

        const data : PotMockRoot = await this.prepareData();
        let converted : any = await data.ToJSON();
        await this.removeProps(converted, ["Id", "LastModified", "Created", "ClassName", "Deleted"]);
        assert.deepEqual(JSON.stringify(converted),
            JSON.stringify({
                Name             : "mockRoot",
                AuthorizedMethods: ["ma1", "ma2"],
                Hidden           : false,
                Data             : [
                    {
                        Name         : "n data A",
                        SubData      : [
                            {
                                Name: "n data subdata A"
                            },
                            {
                                Name: "n data subdata B"
                            }
                        ],
                        SubDataSingle: {
                            Name: "n data subdata single"
                        }
                    },
                    {
                        Name         : "m data B",
                        SubData      : [
                            {
                                Name: "m data subdata A"
                            },
                            {
                                Name: "m data subdata B"
                            }
                        ],
                        SubDataSingle: {
                            Name: "m data subdata single"
                        }
                    }
                ],
                DataSingle       : {
                    Name         : "b data single",
                    SubData      : [
                        {
                            Name: "b subdata A"
                        },
                        {
                            Name: "b subdata B"
                        }
                    ],
                    SubDataSingle: {
                        Name: "b subdata single"
                    }
                }
            })
        );

        data.Data2 = [new PotMockData()];
        data.Data2[0].Name = "f data";
        data.Data2[0].SubDataSingle = new PotMockSubData();
        data.Data2[0].SubDataSingle.Name = "f data subdata single";
        data.Data2[0].SubDataSingle.Name2 = "f data subdata single jsonLess";

        data.DataSingle2 = new PotMockData();
        data.DataSingle2.Name = "f single data";

        await data.Save();

        // Data2 are with "noResolve" but they are just saved, so this is ok, look into next assert for fresh load
        assert.deepEqual(data.Data2[0].Name, "f data");
        converted = await data.ToJSON();
        await this.removeProps(converted, ["Id", "LastModified", "Created", "ClassName", "Deleted"]);
        assert.deepEqual(JSON.stringify(converted),
            JSON.stringify({
                Name             : "mockRoot",
                AuthorizedMethods: ["ma1", "ma2"],
                Hidden           : false,
                Data             : [
                    {
                        Name         : "n data A",
                        SubData      : [
                            {
                                Name: "n data subdata A"
                            },
                            {
                                Name: "n data subdata B"
                            }
                        ],
                        SubDataSingle: {
                            Name: "n data subdata single"
                        }
                    },
                    {
                        Name         : "m data B",
                        SubData      : [
                            {
                                Name: "m data subdata A"
                            },
                            {
                                Name: "m data subdata B"
                            }
                        ],
                        SubDataSingle: {
                            Name: "m data subdata single"
                        }
                    }
                ],
                Data2            : [{Name: "f data", SubData: [], SubDataSingle: {Name: "f data subdata single"}}],
                DataSingle       : {
                    Name         : "b data single",
                    SubData      : [
                        {
                            Name: "b subdata A"
                        },
                        {
                            Name: "b subdata B"
                        }
                    ],
                    SubDataSingle: {
                        Name: "b subdata single"
                    }
                },
                DataSingle2      : {Name: "f single data", SubData: []}
            })
        );

        // Data2 are with "noResolve" and just loaded
        const dump : PotMockRoot[] = await this.context.getAll(PotMockRoot.getCollection());
        assert.ok(!ObjectValidator.IsEmptyOrNull(dump));
        assert.ok(ObjectValidator.IsArray(dump));
        assert.equal(dump.length, 1);
        await this.removeProps(dump, ["__v"]);
        assert.deepEqual(dump, [
            {
                _id              : data.Id,
                authorizedMethods: ["ma1", "ma2"],
                data             : [data.Data[0].Id, data.Data[1].Id],
                data1            : [],
                data3            : [],
                lastModified     : data.LastModified,
                created          : data.Created,
                className        : data.ClassName,
                deleted          : data.Deleted,
                name             : "mockRoot",
                hidden           : false,
                dataSingle       : data.DataSingle.Id,
                data2            : [data.Data2[0].Id],
                dataSingle2      : data.DataSingle2.Id
            }
        ]);
        const newData : PotMockRoot = await PotMockRoot.FindById(data.Id);
        // await data.Refresh();
        assert.deepEqual(newData.Data2, [data.Data2[0].Id]);
        converted = await newData.ToJSON();
        await this.removeProps(converted, ["Id", "LastModified", "Created", "ClassName", "Deleted"]);
        assert.deepEqual(JSON.stringify(converted),
            JSON.stringify({
                Name             : "mockRoot",
                AuthorizedMethods: ["ma1", "ma2"],
                Hidden           : false,
                Data             : [
                    {
                        Name         : "n data A",
                        SubData      : [
                            {
                                Name: "n data subdata A"
                            },
                            {
                                Name: "n data subdata B"
                            }
                        ],
                        SubDataSingle: {
                            Name: "n data subdata single"
                        }
                    },
                    {
                        Name         : "m data B",
                        SubData      : [
                            {
                                Name: "m data subdata A"
                            },
                            {
                                Name: "m data subdata B"
                            }
                        ],
                        SubDataSingle: {
                            Name: "m data subdata single"
                        }
                    }
                ],
                Data1            : [],
                Data2            : [data.Data2[0].Id],
                Data3            : [],
                DataSingle       : {
                    Name         : "b data single",
                    SubData      : [
                        {
                            Name: "b subdata A"
                        },
                        {
                            Name: "b subdata B"
                        }
                    ],
                    SubDataSingle: {
                        Name: "b subdata single"
                    }
                },
                DataSingle2      : data.DataSingle2.Id
            })
        );
    }

    @Test()
    public async NoSerialize() : Promise<void> {
        const root : PotMockRoot[] = await this.context.getAll(PotMockRoot.getCollection());
        assert.ok(ObjectValidator.IsEmptyOrNull(root));

        const data : PotMockRoot = await this.prepareData();
        let converted : any = await data.ToJSON();
        await this.removeProps(converted, ["Id", "LastModified", "Created", "ClassName", "Deleted"]);
        assert.deepEqual(JSON.stringify(converted),
            JSON.stringify({
                Name             : "mockRoot",
                AuthorizedMethods: ["ma1", "ma2"],
                Hidden           : false,
                Data             : [
                    {
                        Name         : "n data A",
                        SubData      : [
                            {
                                Name: "n data subdata A"
                            },
                            {
                                Name: "n data subdata B"
                            }
                        ],
                        SubDataSingle: {
                            Name: "n data subdata single"
                        }
                    },
                    {
                        Name         : "m data B",
                        SubData      : [
                            {
                                Name: "m data subdata A"
                            },
                            {
                                Name: "m data subdata B"
                            }
                        ],
                        SubDataSingle: {
                            Name: "m data subdata single"
                        }
                    }
                ],
                DataSingle       : {
                    Name         : "b data single",
                    SubData      : [
                        {
                            Name: "b subdata A"
                        },
                        {
                            Name: "b subdata B"
                        }
                    ],
                    SubDataSingle: {
                        Name: "b subdata single"
                    }
                }
            })
        );

        data.Data3 = [new PotMockData()];
        data.Data3[0].Name = "g data";
        data.Data3[0].SubDataSingle = new PotMockSubData();
        data.Data3[0].SubDataSingle.Name = "g data subdata single";
        data.Data3[0].SubDataSingle.Name2 = "g data subdata single jsonLess";

        data.DataSingle3 = new PotMockData();
        data.DataSingle3.Name = "g single data";

        await data.Save();

        assert.deepEqual(data.Data3[0].Name, "g data");
        converted = await data.ToJSON();
        await this.removeProps(converted, ["Id", "LastModified", "Created", "ClassName", "Deleted"]);
        assert.deepEqual(JSON.stringify(converted),
            JSON.stringify({
                Name             : "mockRoot",
                AuthorizedMethods: ["ma1", "ma2"],
                Hidden           : false,
                Data             : [
                    {
                        Name         : "n data A",
                        SubData      : [
                            {
                                Name: "n data subdata A"
                            },
                            {
                                Name: "n data subdata B"
                            }
                        ],
                        SubDataSingle: {
                            Name: "n data subdata single"
                        }
                    },
                    {
                        Name         : "m data B",
                        SubData      : [
                            {
                                Name: "m data subdata A"
                            },
                            {
                                Name: "m data subdata B"
                            }
                        ],
                        SubDataSingle: {
                            Name: "m data subdata single"
                        }
                    }
                ],
                Data3            : [data.Data3[0].Id],
                DataSingle       : {
                    Name         : "b data single",
                    SubData      : [
                        {
                            Name: "b subdata A"
                        },
                        {
                            Name: "b subdata B"
                        }
                    ],
                    SubDataSingle: {
                        Name: "b subdata single"
                    }
                },
                DataSingle3      : data.DataSingle3.Id
            })
        );

        const dump : PotMockRoot[] = await this.context.getAll(PotMockRoot.getCollection());
        assert.ok(!ObjectValidator.IsEmptyOrNull(dump));
        assert.ok(ObjectValidator.IsArray(dump));
        assert.equal(dump.length, 1);
        await this.removeProps(dump, ["__v"]);
        assert.deepEqual(dump, [
            {
                _id              : data.Id,
                authorizedMethods: ["ma1", "ma2"],
                data             : [data.Data[0].Id, data.Data[1].Id],
                data1            : [],
                data2            : [],
                lastModified     : data.LastModified,
                created          : data.Created,
                className        : data.ClassName,
                deleted          : data.Deleted,
                name             : "mockRoot",
                hidden           : false,
                dataSingle       : data.DataSingle.Id,
                data3            : [data.Data3[0].Id],
                dataSingle3      : data.DataSingle3.Id
            }
        ]);
        const newData : PotMockRoot = await PotMockRoot.FindById(data.Id);
        // await data.Refresh();
        assert.ok(Array.isArray(newData.Data3));
        assert.equal(newData.Data3[0].Id, data.Data3[0].Id);
        converted = await newData.ToJSON();
        await this.removeProps(converted, ["Id", "LastModified", "Created", "ClassName", "Deleted"]);
        assert.deepEqual(JSON.stringify(converted),
            JSON.stringify({
                Name             : "mockRoot",
                AuthorizedMethods: ["ma1", "ma2"],
                Hidden           : false,
                Data             : [
                    {
                        Name         : "n data A",
                        SubData      : [
                            {
                                Name: "n data subdata A"
                            },
                            {
                                Name: "n data subdata B"
                            }
                        ],
                        SubDataSingle: {
                            Name: "n data subdata single"
                        }
                    },
                    {
                        Name         : "m data B",
                        SubData      : [
                            {
                                Name: "m data subdata A"
                            },
                            {
                                Name: "m data subdata B"
                            }
                        ],
                        SubDataSingle: {
                            Name: "m data subdata single"
                        }
                    }
                ],
                Data1            : [],
                Data2            : [],
                Data3            : [data.Data3[0].Id],
                DataSingle       : {
                    Name         : "b data single",
                    SubData      : [
                        {
                            Name: "b subdata A"
                        },
                        {
                            Name: "b subdata B"
                        }
                    ],
                    SubDataSingle: {
                        Name: "b subdata single"
                    }
                },
                DataSingle3      : data.DataSingle3.Id
            })
        );
    }

    @Test()
    public async ToJSON() : Promise<void> {
        const root : PotMockRoot[] = await this.context.getAll(PotMockRoot.getCollection());
        assert.ok(ObjectValidator.IsEmptyOrNull(root));

        const data : PotMockRoot = await this.prepareData();
        let converted : any = await data.ToJSON();
        await this.removeProps(converted, ["Id", "LastModified", "Created", "ClassName", "Deleted"]);
        assert.deepEqual(JSON.stringify(converted),
            JSON.stringify({
                Name             : "mockRoot",
                AuthorizedMethods: ["ma1", "ma2"],
                Hidden           : false,
                Data             : [
                    {
                        Name         : "n data A",
                        SubData      : [
                            {
                                Name: "n data subdata A"
                            },
                            {
                                Name: "n data subdata B"
                            }
                        ],
                        SubDataSingle: {
                            Name: "n data subdata single"
                        }
                    },
                    {
                        Name         : "m data B",
                        SubData      : [
                            {
                                Name: "m data subdata A"
                            },
                            {
                                Name: "m data subdata B"
                            }
                        ],
                        SubDataSingle: {
                            Name: "m data subdata single"
                        }
                    }
                ],
                DataSingle       : {
                    Name         : "b data single",
                    SubData      : [
                        {
                            Name: "b subdata A"
                        },
                        {
                            Name: "b subdata B"
                        }
                    ],
                    SubDataSingle: {
                        Name: "b subdata single"
                    }
                }
            })
        );

        data.Data2 = [new PotMockData()];
        data.Data2[0].Name = "f data";
        data.Data2[0].SubDataSingle = new PotMockSubData();
        data.Data2[0].SubDataSingle.Name = "f data subdata single";
        data.Data2[0].SubDataSingle.Name2 = "f data subdata single jsonLess";
        data.DataSingle2 = new PotMockData();
        data.DataSingle2.Name = "f single data";

        data.Data3 = [new PotMockData()];
        data.Data3[0].Name = "g data";
        data.Data3[0].SubDataSingle = new PotMockSubData();
        data.Data3[0].SubDataSingle.Name = "g data subdata single";
        data.Data3[0].SubDataSingle.Name2 = "g data subdata single jsonLess";
        data.DataSingle3 = new PotMockData();
        data.DataSingle3.Name = "g single data";

        await data.Save();

        // DEFAULT
        assert.deepEqual(data.Data3[0].Name, "g data");
        converted = await data.ToJSON();
        await this.removeProps(converted, ["Id", "LastModified", "Created", "ClassName", "Deleted"]);
        assert.deepEqual(JSON.stringify(converted),
            JSON.stringify({
                Name             : "mockRoot",
                AuthorizedMethods: ["ma1", "ma2"],
                Hidden           : false,
                Data             : [
                    {
                        Name         : "n data A",
                        SubData      : [
                            {
                                Name: "n data subdata A"
                            },
                            {
                                Name: "n data subdata B"
                            }
                        ],
                        SubDataSingle: {
                            Name: "n data subdata single"
                        }
                    },
                    {
                        Name         : "m data B",
                        SubData      : [
                            {
                                Name: "m data subdata A"
                            },
                            {
                                Name: "m data subdata B"
                            }
                        ],
                        SubDataSingle: {
                            Name: "m data subdata single"
                        }
                    }
                ],
                Data2            : [
                    {
                        Name: "f data", SubData: [],
                        SubDataSingle          : {Name: "f data subdata single"}
                    }
                ],
                Data3            : [data.Data3[0].Id],
                DataSingle       : {
                    Name         : "b data single",
                    SubData      : [
                        {Name: "b subdata A"},
                        {Name: "b subdata B"}
                    ],
                    SubDataSingle: {
                        Name: "b subdata single"
                    }
                },
                DataSingle2      : {
                    Name: "f single data", SubData: []
                },
                DataSingle3      : data.DataSingle3.Id
            })
        );

        // FORCE ALL
        assert.deepEqual(data.Data3[0].Name, "g data");
        converted = await data.ToJSON({expanded: true});
        await this.removeProps(converted, ["Id", "LastModified", "Created", "ClassName", "Deleted"]);
        assert.deepEqual(JSON.stringify(converted),
            JSON.stringify({
                Name             : "mockRoot",
                AuthorizedMethods: ["ma1", "ma2"],
                Hidden           : false,
                Data             : [
                    {
                        Name         : "n data A",
                        SubData      : [
                            {
                                Name: "n data subdata A"
                            },
                            {
                                Name: "n data subdata B"
                            }
                        ],
                        SubDataSingle: {
                            Name: "n data subdata single"
                        }
                    },
                    {
                        Name         : "m data B",
                        SubData      : [
                            {
                                Name: "m data subdata A"
                            },
                            {
                                Name: "m data subdata B"
                            }
                        ],
                        SubDataSingle: {
                            Name: "m data subdata single"
                        }
                    }
                ],
                Data2            : [
                    {
                        Name: "f data", SubData: [],
                        SubDataSingle          : {Name: "f data subdata single"}
                    }
                ],
                Data3            : [{Name: "g data", SubData: [], SubDataSingle: {Name: "g data subdata single"}}],
                DataSingle       : {
                    Name         : "b data single",
                    SubData      : [
                        {Name: "b subdata A"},
                        {Name: "b subdata B"}
                    ],
                    SubDataSingle: {
                        Name: "b subdata single"
                    }
                },
                DataSingle2      : {
                    Name: "f single data", SubData: []
                },
                DataSingle3      : {Name: "g single data", SubData: []}
            })
        );

        // FORCE NO CHILD
        assert.deepEqual(data.Data3[0].Name, "g data");
        converted = await data.ToJSON({idsOnly: true});
        await this.removeProps(converted, ["Id", "LastModified", "Created", "ClassName", "Deleted"]);
        assert.deepEqual(JSON.stringify(converted),
            JSON.stringify({
                Name             : "mockRoot",
                AuthorizedMethods: [],
                Hidden           : false,
                Data             : [data.Data[0].Id, data.Data[1].Id],
                Data2            : [data.Data2[0].Id],
                Data3            : [data.Data3[0].Id],
                DataSingle       : data.DataSingle.Id,
                DataSingle2      : data.DataSingle2.Id,
                DataSingle3      : data.DataSingle3.Id
            })
        );
    }

    // TODO(mkelnar) findById and find could be merged or replaced by one and options resolve could be tested
    //  by mocked "object.fromModel", while Refresh is almost covered by this object.fromModel internally.
    //  But for now it is the fastest solution before release
    @Test()
    public async FindById() : Promise<void> {
        const root : PotMockRoot[] = await this.context.getAll(PotMockRoot.getCollection());
        assert.ok(ObjectValidator.IsEmptyOrNull(root));

        const data : PotMockRoot = await this.prepareData();
        data.Data2 = [new PotMockData()];
        data.Data2[0].Name = "f data";
        data.Data2[0].SubDataSingle = new PotMockSubData();
        data.Data2[0].SubDataSingle.Name = "f data subdata single";
        data.Data2[0].SubDataSingle.Name2 = "f data subdata single jsonLess";
        data.DataSingle2 = new PotMockData();
        data.DataSingle2.Name = "f single data";

        data.Data3 = [new PotMockData()];
        data.Data3[0].Name = "g data";
        data.Data3[0].SubDataSingle = new PotMockSubData();
        data.Data3[0].SubDataSingle.Name = "g data subdata single";
        data.Data3[0].SubDataSingle.Name2 = "g data subdata single jsonLess";
        data.DataSingle3 = new PotMockData();
        data.DataSingle3.Name = "g single data";

        await data.Save();

        // DEFAULT (defined by decorators option)
        let md : PotMockRoot = await PotMockRoot.FindById(data.Id);
        assert.equal(md.Id, data.Id);
        assert.equal(md.Name, data.Name);

        assert.ok(Array.isArray(md.Data));
        assert.ok(Array.isArray(md.Data1));
        assert.ok(Array.isArray(md.Data2));
        assert.ok(Array.isArray(md.Data3));
        assert.equal(md.Data.length, 2);
        assert.equal(md.Data1.length, 0);
        assert.equal(md.Data2.length, 1);
        assert.equal(md.Data3.length, 1);
        assert.ok(ObjectValidator.IsObject(md.Data[0]));
        assert.ok(ObjectValidator.IsString(md.Data2[0]));
        assert.ok(ObjectValidator.IsObject(md.Data3[0]));
        assert.ok(md.Data[0].Id, data.Data[0].Id);
        assert.ok(md.Data2[0], data.Data2[0].Id);
        assert.ok(md.Data3[0].Id, data.Data3[0].Id);

        assert.ok(ObjectValidator.IsObject(md.DataSingle) && !ObjectValidator.IsEmptyOrNull(md.DataSingle));
        assert.ok(ObjectValidator.IsEmptyOrNull(md.DataSingle1));
        assert.ok(ObjectValidator.IsString(md.DataSingle2) && !ObjectValidator.IsEmptyOrNull(md.DataSingle2));
        assert.ok(ObjectValidator.IsObject(md.DataSingle3) && !ObjectValidator.IsEmptyOrNull(md.DataSingle3));
        assert.ok(md.DataSingle.Id, data.DataSingle.Id);
        assert.ok(md.DataSingle2, data.DataSingle2.Id);
        assert.ok(md.DataSingle3.Id, data.DataSingle3.Id);

        // FORCE NO REF
        md = await PotMockRoot.FindById(data.Id, {partial: true});
        assert.equal(md.Id, data.Id);
        assert.equal(md.Name, data.Name);

        assert.ok(Array.isArray(md.Data));
        assert.ok(Array.isArray(md.Data1));
        assert.ok(Array.isArray(md.Data2));
        assert.ok(Array.isArray(md.Data3));
        assert.equal(md.Data.length, 2);
        assert.equal(md.Data1.length, 0);
        assert.equal(md.Data2.length, 1);
        assert.equal(md.Data3.length, 1);
        assert.ok(ObjectValidator.IsString(md.Data[0]));
        assert.ok(ObjectValidator.IsString(md.Data2[0]));
        assert.ok(ObjectValidator.IsString(md.Data3[0]));
        assert.ok(md.Data[0], data.Data[0].Id);
        assert.ok(md.Data2[0], data.Data2[0].Id);
        assert.ok(md.Data3[0], data.Data3[0].Id);

        assert.ok(ObjectValidator.IsString(md.DataSingle) && !ObjectValidator.IsEmptyOrNull(md.DataSingle));
        assert.ok(ObjectValidator.IsEmptyOrNull(md.DataSingle1));
        assert.ok(ObjectValidator.IsString(md.DataSingle2) && !ObjectValidator.IsEmptyOrNull(md.DataSingle2));
        assert.ok(ObjectValidator.IsString(md.DataSingle3) && !ObjectValidator.IsEmptyOrNull(md.DataSingle3));
        assert.ok(md.DataSingle, data.DataSingle.Id);
        assert.ok(md.DataSingle2, data.DataSingle2.Id);
        assert.ok(md.DataSingle3, data.DataSingle3.Id);

        // FORCE FLAT
        md = await PotMockRoot.FindById(data.Id, {expanded: true});
        assert.equal(md.Id, data.Id);
        assert.equal(md.Name, data.Name);

        assert.ok(Array.isArray(md.Data));
        assert.ok(Array.isArray(md.Data1));
        assert.ok(Array.isArray(md.Data2));
        assert.ok(Array.isArray(md.Data3));
        assert.equal(md.Data.length, 2);
        assert.equal(md.Data1.length, 0);
        assert.equal(md.Data2.length, 1);
        assert.equal(md.Data3.length, 1);
        assert.ok(ObjectValidator.IsObject(md.Data[0]));
        assert.ok(ObjectValidator.IsObject(md.Data2[0]));
        assert.ok(ObjectValidator.IsObject(md.Data3[0]));
        assert.ok(md.Data[0].Id, data.Data[0].Id);
        assert.ok(md.Data2[0].Id, data.Data2[0].Id);
        assert.ok(md.Data3[0].Id, data.Data3[0].Id);

        assert.ok(ObjectValidator.IsObject(md.DataSingle) && !ObjectValidator.IsEmptyOrNull(md.DataSingle));
        assert.ok(ObjectValidator.IsEmptyOrNull(md.DataSingle1));
        assert.ok(ObjectValidator.IsObject(md.DataSingle2) && !ObjectValidator.IsEmptyOrNull(md.DataSingle2));
        assert.ok(ObjectValidator.IsObject(md.DataSingle3) && !ObjectValidator.IsEmptyOrNull(md.DataSingle3));
        assert.ok(md.DataSingle.Id, data.DataSingle.Id);
        assert.ok(md.DataSingle2.Id, data.DataSingle2.Id);
        assert.ok(md.DataSingle3.Id, data.DataSingle3.Id);
    }

    @Test()
    public async Find() : Promise<void> {
        const root : PotMockRoot[] = await this.context.getAll(PotMockRoot.getCollection());
        assert.ok(ObjectValidator.IsEmptyOrNull(root));

        const data : PotMockRoot = await this.prepareData();
        data.Data2 = [new PotMockData()];
        data.Data2[0].Name = "f data";
        data.Data2[0].SubDataSingle = new PotMockSubData();
        data.Data2[0].SubDataSingle.Name = "f data subdata single";
        data.Data2[0].SubDataSingle.Name2 = "f data subdata single jsonLess";
        data.DataSingle2 = new PotMockData();
        data.DataSingle2.Name = "f single data";

        data.Data3 = [new PotMockData()];
        data.Data3[0].Name = "g data";
        data.Data3[0].SubDataSingle = new PotMockSubData();
        data.Data3[0].SubDataSingle.Name = "g data subdata single";
        data.Data3[0].SubDataSingle.Name2 = "g data subdata single jsonLess";
        data.DataSingle3 = new PotMockData();
        data.DataSingle3.Name = "g single data";

        await data.Save();

        // DEFAULT (defined by decorators option)
        let md : PotMockRoot = (await PotMockRoot.Find<PotMockRoot>({name: data.Name}))[0];
        assert.equal(md.Id, data.Id);
        assert.equal(md.Name, data.Name);

        assert.ok(Array.isArray(md.Data));
        assert.ok(Array.isArray(md.Data1));
        assert.ok(Array.isArray(md.Data2));
        assert.ok(Array.isArray(md.Data3));
        assert.equal(md.Data.length, 2);
        assert.equal(md.Data1.length, 0);
        assert.equal(md.Data2.length, 1);
        assert.equal(md.Data3.length, 1);
        assert.ok(ObjectValidator.IsObject(md.Data[0]));
        assert.ok(ObjectValidator.IsString(md.Data2[0]));
        assert.ok(ObjectValidator.IsObject(md.Data3[0]));
        assert.ok(md.Data[0].Id, data.Data[0].Id);
        assert.ok(md.Data2[0], data.Data2[0].Id);
        assert.ok(md.Data3[0].Id, data.Data3[0].Id);

        assert.ok(ObjectValidator.IsObject(md.DataSingle) && !ObjectValidator.IsEmptyOrNull(md.DataSingle));
        assert.ok(ObjectValidator.IsEmptyOrNull(md.DataSingle1));
        assert.ok(ObjectValidator.IsString(md.DataSingle2) && !ObjectValidator.IsEmptyOrNull(md.DataSingle2));
        assert.ok(ObjectValidator.IsObject(md.DataSingle3) && !ObjectValidator.IsEmptyOrNull(md.DataSingle3));
        assert.ok(md.DataSingle.Id, data.DataSingle.Id);
        assert.ok(md.DataSingle2, data.DataSingle2.Id);
        assert.ok(md.DataSingle3.Id, data.DataSingle3.Id);

        // FORCE NO REF
        md = (await PotMockRoot.Find<PotMockRoot>({name: data.Name}, {partial: true}))[0];
        assert.equal(md.Id, data.Id);
        assert.equal(md.Name, data.Name);

        assert.ok(Array.isArray(md.Data));
        assert.ok(Array.isArray(md.Data1));
        assert.ok(Array.isArray(md.Data2));
        assert.ok(Array.isArray(md.Data3));
        assert.equal(md.Data.length, 2);
        assert.equal(md.Data1.length, 0);
        assert.equal(md.Data2.length, 1);
        assert.equal(md.Data3.length, 1);
        assert.ok(ObjectValidator.IsString(md.Data[0]));
        assert.ok(ObjectValidator.IsString(md.Data2[0]));
        assert.ok(ObjectValidator.IsString(md.Data3[0]));
        assert.ok(md.Data[0], data.Data[0].Id);
        assert.ok(md.Data2[0], data.Data2[0].Id);
        assert.ok(md.Data3[0], data.Data3[0].Id);

        assert.ok(ObjectValidator.IsString(md.DataSingle) && !ObjectValidator.IsEmptyOrNull(md.DataSingle));
        assert.ok(ObjectValidator.IsEmptyOrNull(md.DataSingle1));
        assert.ok(ObjectValidator.IsString(md.DataSingle2) && !ObjectValidator.IsEmptyOrNull(md.DataSingle2));
        assert.ok(ObjectValidator.IsString(md.DataSingle3) && !ObjectValidator.IsEmptyOrNull(md.DataSingle3));
        assert.ok(md.DataSingle, data.DataSingle.Id);
        assert.ok(md.DataSingle2, data.DataSingle2.Id);
        assert.ok(md.DataSingle3, data.DataSingle3.Id);

        // FORCE FLAT
        md = (await PotMockRoot.Find<PotMockRoot>({name: data.Name}, {expanded: true}))[0];
        assert.equal(md.Id, data.Id);
        assert.equal(md.Name, data.Name);

        assert.ok(Array.isArray(md.Data));
        assert.ok(Array.isArray(md.Data1));
        assert.ok(Array.isArray(md.Data2));
        assert.ok(Array.isArray(md.Data3));
        assert.equal(md.Data.length, 2);
        assert.equal(md.Data1.length, 0);
        assert.equal(md.Data2.length, 1);
        assert.equal(md.Data3.length, 1);
        assert.ok(ObjectValidator.IsObject(md.Data[0]));
        assert.ok(ObjectValidator.IsObject(md.Data2[0]));
        assert.ok(ObjectValidator.IsObject(md.Data3[0]));
        assert.ok(md.Data[0].Id, data.Data[0].Id);
        assert.ok(md.Data2[0].Id, data.Data2[0].Id);
        assert.ok(md.Data3[0].Id, data.Data3[0].Id);

        assert.ok(ObjectValidator.IsObject(md.DataSingle) && !ObjectValidator.IsEmptyOrNull(md.DataSingle));
        assert.ok(ObjectValidator.IsEmptyOrNull(md.DataSingle1));
        assert.ok(ObjectValidator.IsObject(md.DataSingle2) && !ObjectValidator.IsEmptyOrNull(md.DataSingle2));
        assert.ok(ObjectValidator.IsObject(md.DataSingle3) && !ObjectValidator.IsEmptyOrNull(md.DataSingle3));
        assert.ok(md.DataSingle.Id, data.DataSingle.Id);
        assert.ok(md.DataSingle2.Id, data.DataSingle2.Id);
        assert.ok(md.DataSingle3.Id, data.DataSingle3.Id);
    }

    @Test()
    public async BufferData() : Promise<void> {
        let data : PotMockBuffer[] = await this.context.getAll(PotMockBuffer.getCollection());
        assert.ok(ObjectValidator.IsEmptyOrNull(data));

        const buff : PotMockBuffer = new PotMockBuffer();
        buff.Name = "buff1";
        buff.Data = Buffer.from([1, 3, 5, 7]);
        await buff.Save();

        data = await this.context.getAll(PotMockBuffer.getCollection());
        assert.ok(!ObjectValidator.IsEmptyOrNull(data));
        assert.ok(ObjectValidator.IsArray(data));
        assert.equal(data.length, 1);
        assert.deepEqual(JSON.stringify(JsonUtils.Sort(data)), JSON.stringify([
            {
                __v         : 0,
                _id         : buff.Id,
                className   : buff.ClassName,
                created     : buff.Created,
                data        : {buffer: {type: "Buffer", data: [1, 3, 5, 7]}, position: 4, sub_type: 0},
                deleted     : buff.Deleted,
                lastModified: buff.LastModified,
                name        : "buff1"
            }
        ]));

        buff.Data = Buffer.from([2, 4, 6, 8]);
        buff.Name = "buff2";
        await buff.Save();

        data = await this.context.getAll(PotMockBuffer.getCollection());
        assert.ok(!ObjectValidator.IsEmptyOrNull(data));
        assert.ok(ObjectValidator.IsArray(data));
        assert.equal(data.length, 1);
        assert.deepEqual(JSON.stringify(JsonUtils.Sort(data)), JSON.stringify([
            {
                __v         : 0,
                _id         : buff.Id,
                className   : buff.ClassName,
                created     : buff.Created,
                data        : {buffer: {type: "Buffer", data: [2, 4, 6, 8]}, position: 4, sub_type: 0},
                deleted     : buff.Deleted,
                lastModified: buff.LastModified,
                name        : "buff2"
            }
        ]));
    }

    protected async prepareData() : Promise<PotMockRoot> {
        const data : PotMockRoot = new PotMockRoot();
        data.Name = "mockRoot";
        data.Hidden = false;
        data.AuthorizedMethods = ["ma1", "ma2"];
        data.Data = [
            new PotMockData(),
            new PotMockData()
        ];
        data.DataSingle = new PotMockData();
        data.DataSingle.Name = "b data single";
        data.DataSingle.SubDataSingle = new PotMockSubData();
        data.DataSingle.SubDataSingle.Name = "b subdata single";
        data.DataSingle.SubDataSingle.Name2 = "b subdata single jsonLess";
        data.DataSingle.SubData = [new PotMockSubData(), new PotMockSubData()];
        data.DataSingle.SubData[0].Name = "b subdata A";
        data.DataSingle.SubData[0].Name2 = "b subdata A jsonLess";
        data.DataSingle.SubData[1].Name = "b subdata B";
        data.DataSingle.SubData[1].Name2 = "b subdata B jsonLess";

        data.Data[0].Name = "n data A";
        data.Data[0].SubDataSingle = new PotMockSubData();
        data.Data[0].SubDataSingle.Name = "n data subdata single";
        data.Data[0].SubDataSingle.Name2 = "n data subdata single jsonLess";
        data.Data[0].SubData = [new PotMockSubData(), new PotMockSubData()];
        data.Data[0].SubData[0].Name = "n data subdata A";
        data.Data[0].SubData[0].Name2 = "n data subdata A jsonLess";
        data.Data[0].SubData[1].Name = "n data subdata B";
        data.Data[0].SubData[1].Name2 = "n data subdata B jsonLess";

        data.Data[1].Name = "m data B";
        data.Data[1].SubDataSingle = new PotMockSubData();
        data.Data[1].SubDataSingle.Name = "m data subdata single";
        data.Data[1].SubDataSingle.Name2 = "m data subdata single jsonLess";
        data.Data[1].SubData = [new PotMockSubData(), new PotMockSubData()];
        data.Data[1].SubData[0].Name = "m data subdata A";
        data.Data[1].SubData[0].Name2 = "m data subdata A jsonLess";
        data.Data[1].SubData[1].Name = "m data subdata B";
        data.Data[1].SubData[1].Name2 = "m data subdata B jsonLess";

        await data.Save();

        return data;
    }

    private async removeProps($obj : any, $keys : string[]) : Promise<void> {
        if (Array.isArray($obj)) {
            for await(const item of $obj) {
                await this.removeProps(item, $keys);
            }
        } else if (typeof $obj === "object" && $obj != null) {
            for await(const key of Object.getOwnPropertyNames($obj)) {
                if ($keys.indexOf(key) !== -1) {
                    delete $obj[key];
                } else {
                    await this.removeProps($obj[key], $keys);
                }
            }
        }
    }
}

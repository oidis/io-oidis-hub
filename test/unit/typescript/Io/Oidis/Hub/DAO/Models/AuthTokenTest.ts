/*! ******************************************************************************************************** *
 *
 * Copyright 2019-2024 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { MongoTestRunner } from "../../MongoTestRunner.js";

import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { StringUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/StringUtils.js";
import { Test } from "@io-oidis-commons/test/unit/Io/Oidis/Commons/BaseUnitTestRunner.js";
import { assert } from "@io-oidis-commons/test/unit/Io/Oidis/Commons/UnitTestEnvironment.js";
import { AuthToken } from "../../../../../../../../source/typescript/Io/Oidis/Hub/DAO/Models/AuthToken.js";

export class AuthTokenTest extends MongoTestRunner {

    constructor() {
        super();

        // this.setMethodFilter("testCreate");
    }

    @Test()
    public async ID() : Promise<void> {
        let tokens : AuthToken[] = await this.context.getAll(AuthToken.getCollection());
        assert.ok(ObjectValidator.IsEmptyOrNull(tokens));

        const data : AuthToken = new AuthToken();
        data.Token = StringUtils.getSha1("test name");
        data.Expire = 11;
        data.Date = 156;
        const id : string = data.Id;
        await data.Save();

        tokens = await this.context.getAll(AuthToken.getCollection());
        assert.ok(!ObjectValidator.IsEmptyOrNull(tokens));
        assert.ok(ObjectValidator.IsArray(tokens));
        assert.equal(tokens.length, 1);
        delete (<any>tokens[0]).__v;
        assert.deepEqual(tokens, [
            {
                _id         : id,
                className   : data.ClassName,
                created     : data.Created,
                date        : 156,
                deleted     : false,
                expire      : 11,
                lastModified: data.LastModified,
                token       : StringUtils.getSha1("test name")
            }
        ]);
    }

    @Test()
    public async Create() : Promise<void> {
        let tokens : AuthToken[] = await this.context.getAll(AuthToken.getCollection());
        assert.ok(ObjectValidator.IsEmptyOrNull(tokens));

        let data : AuthToken = new AuthToken();
        data.Token = StringUtils.getSha1("test name");
        data.Expire = 11;
        data.Date = 156;
        await data.Save();

        tokens = await this.context.getAll(AuthToken.getCollection());
        assert.ok(!ObjectValidator.IsEmptyOrNull(tokens));
        assert.ok(ObjectValidator.IsArray(tokens));
        assert.equal(tokens.length, 1);
        delete (<any>tokens[0])._id;
        delete (<any>tokens[0]).__v;
        assert.deepEqual(tokens, [
            {
                className   : data.ClassName,
                created     : data.Created,
                date        : 156,
                deleted     : false,
                expire      : 11,
                lastModified: data.LastModified,
                token       : StringUtils.getSha1("test name")
            }
        ]);

        data = new AuthToken();
        data.Token = StringUtils.getSha1("test name 2");
        data.Expire = 33;
        data.Date = 567;
        await data.Save();

        tokens = await this.context.getAll(AuthToken.getCollection());
        assert.ok(!ObjectValidator.IsEmptyOrNull(tokens));
        assert.ok(ObjectValidator.IsArray(tokens));
        assert.equal(tokens.length, 2);
        tokens.forEach(($value : any) : any => {
            delete $value._id;
            delete $value.__v;
            delete $value.className;
            delete $value.created;
            delete $value.lastModified;
            return $value;
        });
        assert.deepEqual(tokens, [
            {
                date   : 156,
                deleted: false,
                expire : 11,
                token  : StringUtils.getSha1("test name")
            },
            {
                date  : 567,
                deleted: false,
                expire: 33,
                token : StringUtils.getSha1("test name 2")
            }
        ]);
    }

    @Test()
    public async FindById() : Promise<void> {
        let tokens : AuthToken[] = await this.context.getAll(AuthToken.getCollection());
        assert.ok(ObjectValidator.IsEmptyOrNull(tokens));
        let data : AuthToken = new AuthToken();
        data.Token = StringUtils.getSha1("test name");
        data.Expire = 11;
        data.Date = 156;
        const id : string = data.Id;
        await data.Save();

        tokens = await this.context.getAll(AuthToken.getCollection());
        assert.ok(!ObjectValidator.IsEmptyOrNull(tokens));
        assert.ok(ObjectValidator.IsArray(tokens));
        assert.equal(tokens.length, 1);
        delete (<any>tokens[0]).__v;
        assert.deepEqual(tokens, [
            {
                _id         : id,
                className   : data.ClassName,
                created     : data.Created,
                date        : 156,
                deleted     : false,
                expire      : 11,
                lastModified: data.LastModified,
                token       : StringUtils.getSha1("test name")
            }
        ]);

        data = await AuthToken.FindById(id);
        assert.ok(!ObjectValidator.IsEmptyOrNull(data));
        assert.equal(data.Date, 156);
        assert.equal(data.Expire, 11);
        assert.equal(data.Token, StringUtils.getSha1("test name"));
        assert.equal(data.Id, id);
    }

    @Test()
    public async Find() : Promise<void> {
        let tokens : AuthToken[] = await this.context.getAll(AuthToken.getCollection());
        assert.ok(ObjectValidator.IsEmptyOrNull(tokens));

        let data : AuthToken = new AuthToken();
        data.Token = StringUtils.getSha1("test name 1");
        data.Expire = 11;
        data.Date = 123;
        await data.Save();

        data = new AuthToken();
        data.Token = StringUtils.getSha1("test name 2");
        data.Expire = 22;
        data.Date = 234;
        await data.Save();

        data = new AuthToken();
        data.Token = StringUtils.getSha1("test name 3");
        data.Expire = 33;
        data.Date = 123;
        await data.Save();

        tokens = await this.context.getAll(AuthToken.getCollection());
        assert.ok(!ObjectValidator.IsEmptyOrNull(tokens));
        assert.ok(ObjectValidator.IsArray(tokens));
        assert.equal(tokens.length, 3);

        tokens = await AuthToken.Find({});
        assert.ok(!ObjectValidator.IsEmptyOrNull(tokens));
        assert.ok(ObjectValidator.IsArray(tokens));
        assert.equal(tokens.length, 3);
        tokens.forEach(($value : any) : any => {
            delete $value._id;
            delete $value.__v;
            delete $value.deleted;
            delete $value.created;
            delete $value.lastModified;
            return $value;
        });

        assert.equal(tokens[0].Token, StringUtils.getSha1("test name 1"));
        assert.equal(tokens[0].Expire, 11);
        assert.equal(tokens[0].Date, 123);

        assert.equal(tokens[1].Token, StringUtils.getSha1("test name 2"));
        assert.equal(tokens[1].Expire, 22);
        assert.equal(tokens[1].Date, 234);

        assert.equal(tokens[2].Token, StringUtils.getSha1("test name 3"));
        assert.equal(tokens[2].Expire, 33);
        assert.equal(tokens[2].Date, 123);

        tokens = await AuthToken.Find({date: 123});
        assert.ok(!ObjectValidator.IsEmptyOrNull(tokens));
        assert.ok(ObjectValidator.IsArray(tokens));
        assert.equal(tokens.length, 2);
        tokens.forEach(($value : any) : any => {
            delete $value._id;
            delete $value.__v;
            return $value;
        });

        assert.equal(tokens[0].Token, StringUtils.getSha1("test name 1"));
        assert.equal(tokens[0].Expire, 11);
        assert.equal(tokens[0].Date, 123);

        assert.equal(tokens[1].Token, StringUtils.getSha1("test name 3"));
        assert.equal(tokens[1].Expire, 33);
        assert.equal(tokens[1].Date, 123);
    }

    @Test()
    public async Delete() : Promise<void> {
        let tokens : AuthToken[] = await this.context.getAll(AuthToken.getCollection());
        assert.ok(ObjectValidator.IsEmptyOrNull(tokens));

        let data : AuthToken = new AuthToken();
        data.Token = StringUtils.getSha1("test name");
        data.Expire = 11;
        data.Date = 156;
        const id : string = data.Id;
        await data.Save();

        tokens = await this.context.getAll(AuthToken.getCollection());
        assert.ok(!ObjectValidator.IsEmptyOrNull(tokens));
        assert.ok(ObjectValidator.IsArray(tokens));
        assert.equal(tokens.length, 1);
        delete (<any>tokens[0]).__v;
        assert.deepEqual(tokens, [
            {
                _id         : id,
                className   : data.ClassName,
                created     : data.Created,
                date        : 156,
                deleted     : false,
                expire      : 11,
                lastModified: data.LastModified,
                token       : StringUtils.getSha1("test name")
            }
        ]);

        data = await AuthToken.FindById(id);
        assert.ok(!ObjectValidator.IsEmptyOrNull(data));
        assert.equal(data.Date, 156);
        assert.equal(data.Expire, 11);
        assert.equal(data.Token, StringUtils.getSha1("test name"));
        assert.equal(data.Id, id);
        await data.Remove();

        tokens = await this.context.getAll(AuthToken.getCollection());
        assert.ok(ObjectValidator.IsEmptyOrNull(tokens));
    }
}

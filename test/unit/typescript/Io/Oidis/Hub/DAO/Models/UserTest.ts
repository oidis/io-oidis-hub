/*! ******************************************************************************************************** *
 *
 * Copyright 2019-2024 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { MongoTestRunner } from "../../MongoTestRunner.js";

import { JsonUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/JsonUtils.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { StringUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/StringUtils.js";
import { Test } from "@io-oidis-commons/test/unit/Io/Oidis/Commons/BaseUnitTestRunner.js";
import { assert } from "@io-oidis-commons/test/unit/Io/Oidis/Commons/UnitTestEnvironment.js";
import { BaseListToRefConverter } from "../../../../../../../../source/typescript/Io/Oidis/Hub/DAO/Converters/BaseListToRefConverter.js";
import { GroupListToRefConverter } from "../../../../../../../../source/typescript/Io/Oidis/Hub/DAO/Converters/GroupListToRefConverter.js";
import { GroupToRefConverter } from "../../../../../../../../source/typescript/Io/Oidis/Hub/DAO/Converters/GroupToRefConverter.js";
import { AuthToken } from "../../../../../../../../source/typescript/Io/Oidis/Hub/DAO/Models/AuthToken.js";
import { BaseModel } from "../../../../../../../../source/typescript/Io/Oidis/Hub/DAO/Models/BaseModel.js";
import { Group } from "../../../../../../../../source/typescript/Io/Oidis/Hub/DAO/Models/Group.js";
import { User } from "../../../../../../../../source/typescript/Io/Oidis/Hub/DAO/Models/User.js";
import { IContext } from "../../../../../../../../source/typescript/Io/Oidis/Hub/Interfaces/DAO/IConverter.js";
import { Entity, Property } from "../../../../../../../../source/typescript/Io/Oidis/Hub/Primitives/Decorators.js";

export class MockGroupListToRefConverter extends GroupListToRefConverter {
    public async from($value : any, $context : IContext) : Promise<any> {
        const items : MockGroup[] = [];
        for await(const item of $value) {
            items.push(await MockGroup.FindById(item));
        }
        return items;
    }
}

export class MockGroupToRefConverter extends GroupToRefConverter {
    public async from($value : any, $context : IContext) : Promise<any> {
        return MockGroup.FindById($value);
    }
}

export class MockDataListToRefConverter extends BaseListToRefConverter {
    public async from($value : any, $context : IContext) : Promise<any> {
        const items : MockData[] = [];
        for await (const item of $value) {
            items.push(await MockData.FindById(item));
        }
        return items;
    }
}

export class MockSubDataListToRefConverter extends BaseListToRefConverter {
    public async from($value : any, $context : IContext) : Promise<any> {
        const items : MockSubData[] = [];
        for await(const item of $value) {
            items.push(await MockSubData.FindById(item));
        }
        return items;
    }
}

@Entity("subData")
export class MockSubData extends BaseModel<MockSubData> {
    @Property({type: String, required: false})
    public declare Name : string;
}

@Entity("data")
export class MockData extends BaseModel<MockData> {
    @Property({type: [MockSubData], default: [], required: false, converter: new MockSubDataListToRefConverter()})
    public declare SubData : MockSubData[];
}

@Entity("group")
export class MockGroup extends Group {
    @Property({type: [MockData], default: [], required: false, converter: new MockDataListToRefConverter()})
    public declare Data : MockData[];
}

@Entity("user")
export class MockUser extends User {
    @Property({type: MockGroup, converter: new MockGroupToRefConverter()})
    public declare DefaultGroup : MockGroup;

    @Property({type: [MockGroup], converter: new MockGroupListToRefConverter()})
    public declare Groups : MockGroup[];
}

export class UserTest extends MongoTestRunner {

    constructor() {
        super();

        // this.setMethodFilter("testToJSON");
    }

    @Test()
    public async ID() : Promise<void> {
        let users : MockUser[] = await this.context.getAll(MockUser.getCollection());
        assert.ok(ObjectValidator.IsEmptyOrNull(users));

        const user : MockUser = new MockUser();
        user.UserName = "test name";
        user.Password = "test pass";
        user.Email = "this.is.test@mail.oid";
        user.DefaultGroup = new MockGroup();
        user.DefaultGroup.Name = "name";
        await user.DefaultGroup.Save();
        const id : string = user.Id;
        await user.Save();

        users = await this.context.getAll(MockUser.getCollection());
        assert.ok(!ObjectValidator.IsEmptyOrNull(users));
        assert.ok(ObjectValidator.IsArray(users));
        assert.equal(users.length, 1);
        assert.deepEqual(JsonUtils.Sort(users), [
            {
                __v         : 0,
                _id         : id,
                activated   : true,
                altEmails   : [],
                auditLog    : {
                    _id         : user.AuditLog.Id,
                    className   : user.AuditLog.ClassName,
                    created     : user.AuditLog.Created,
                    deleted     : user.AuditLog.Deleted,
                    lastModified: user.AuditLog.LastModified
                },
                authTokens  : [],
                className   : user.ClassName,
                created     : user.Created,
                defaultGroup: user.DefaultGroup.Id,
                deleted     : user.Deleted,
                email       : "this.is.test@mail.oid",
                groups      : [],
                lastModified: user.LastModified,
                password    : "test pass",
                ssoTokens   : {
                    _id         : user.SSOTokens.Id,
                    className   : user.SSOTokens.ClassName,
                    created     : user.SSOTokens.Created,
                    deleted     : user.SSOTokens.Deleted,
                    lastModified: user.SSOTokens.LastModified
                },
                userName    : "test name"
            }
        ]);
    }

    @Test()
    public async Create() : Promise<void> {
        let users : MockUser[] = await this.context.getAll(MockUser.getCollection());
        assert.ok(ObjectValidator.IsEmptyOrNull(users));

        let user : MockUser = new MockUser();
        user.UserName = "test name";
        user.Password = "test pass";
        user.Email = "this.is.test@mail.oid";
        user.DefaultGroup = new MockGroup();
        user.DefaultGroup.Name = "name";
        await user.DefaultGroup.Save();
        await user.Save();
        const user1 : User = user;

        users = await this.context.getAll(MockUser.getCollection());
        assert.ok(!ObjectValidator.IsEmptyOrNull(users));
        assert.ok(ObjectValidator.IsArray(users));
        assert.equal(users.length, 1);
        assert.deepEqual(JsonUtils.Sort(users), JsonUtils.Sort([
            {
                __v         : 0,
                _id         : user1.Id,
                activated   : true,
                altEmails   : [],
                auditLog    : {
                    _id         : user1.AuditLog.Id,
                    className   : user1.AuditLog.ClassName,
                    created     : user1.AuditLog.Created,
                    deleted     : user1.AuditLog.Deleted,
                    lastModified: user1.AuditLog.LastModified
                },
                authTokens  : [],
                className   : user1.ClassName,
                created     : user1.Created,
                defaultGroup: user1.DefaultGroup.Id,
                deleted     : user1.Deleted,
                email       : "this.is.test@mail.oid",
                groups      : [],
                lastModified: user1.LastModified,
                password    : "test pass",
                ssoTokens   : {
                    _id         : user1.SSOTokens.Id,
                    className   : user1.SSOTokens.ClassName,
                    created     : user1.SSOTokens.Created,
                    deleted     : user1.SSOTokens.Deleted,
                    lastModified: user1.SSOTokens.LastModified
                },
                userName    : "test name"
            }
        ]));

        user = new MockUser();
        user.UserName = "test name 2";
        user.Password = "test pass 2";
        user.Email = "this.is.second.test@mail.oid";
        user.DefaultGroup = new MockGroup();
        user.DefaultGroup.Name = "name 2";
        await user.DefaultGroup.Save();
        const group : MockGroup = new MockGroup();
        group.Name = "test";
        group.AuthorizedMethods = ["authMethod1", "authMethod2"];
        await group.Save();
        user.Groups = [group];
        await user.Save();

        users = await this.context.getAll(MockUser.getCollection());
        assert.ok(!ObjectValidator.IsEmptyOrNull(users));
        assert.ok(ObjectValidator.IsArray(users));
        assert.equal(users.length, 2);
        assert.deepEqual(JsonUtils.Sort(users), JsonUtils.Sort([
            {
                __v         : 0,
                _id         : user1.Id,
                activated   : true,
                altEmails   : [],
                auditLog    : {
                    _id         : user1.AuditLog.Id,
                    className   : user1.AuditLog.ClassName,
                    created     : user1.AuditLog.Created,
                    deleted     : user1.AuditLog.Deleted,
                    lastModified: user1.AuditLog.LastModified
                },
                authTokens  : [],
                className   : user1.ClassName,
                created     : user1.Created,
                defaultGroup: user1.DefaultGroup.Id,
                deleted     : user1.Deleted,
                email       : "this.is.test@mail.oid",
                groups      : [],
                lastModified: user1.LastModified,
                password    : "test pass",
                ssoTokens   : {
                    _id         : user1.SSOTokens.Id,
                    className   : user1.SSOTokens.ClassName,
                    created     : user1.SSOTokens.Created,
                    deleted     : user1.SSOTokens.Deleted,
                    lastModified: user1.SSOTokens.LastModified
                },
                userName    : "test name"
            },
            {
                __v         : 0,
                _id         : user.Id,
                activated   : true,
                altEmails   : [],
                auditLog    : {
                    _id         : user.AuditLog.Id,
                    className   : user.AuditLog.ClassName,
                    created     : user.AuditLog.Created,
                    deleted     : user.AuditLog.Deleted,
                    lastModified: user.AuditLog.LastModified
                },
                authTokens  : [],
                className   : user.ClassName,
                created     : user.Created,
                defaultGroup: user.DefaultGroup.Id,
                deleted     : user.Deleted,
                email       : "this.is.second.test@mail.oid",
                groups      : [group.Id],
                lastModified: user.LastModified,
                password    : "test pass 2",
                ssoTokens   : {
                    _id         : user.SSOTokens.Id,
                    className   : user.SSOTokens.ClassName,
                    created     : user.SSOTokens.Created,
                    deleted     : user.SSOTokens.Deleted,
                    lastModified: user.SSOTokens.LastModified
                },
                userName    : "test name 2"
            }
        ]));
        const groups : any[] = await this.context.getAll(MockGroup.getCollection());
        assert.ok(!ObjectValidator.IsEmptyOrNull(groups));
        assert.equal(groups.length, 3);
        assert.deepEqual(JsonUtils.Sort(groups), JsonUtils.Sort([
            {
                __v              : 0,
                _id              : groups[0]._id,
                authorizedMethods: [],
                className        : groups[0].className,
                created          : groups[0].created,
                data             : [],
                deleted          : groups[0].deleted,
                hidden           : false,
                lastModified     : groups[0].lastModified,
                name             : "name"
            },
            {
                __v              : 0,
                _id              : groups[1]._id,
                authorizedMethods: [],
                className        : groups[1].className,
                created          : groups[1].created,
                data             : [],
                deleted          : groups[1].deleted,
                hidden           : false,
                lastModified     : groups[1].lastModified,
                name             : "name 2"
            },
            {
                __v              : 0,
                _id              : groups[2]._id,
                authorizedMethods: ["authMethod1", "authMethod2"],
                className        : groups[2].className,
                created          : groups[2].created,
                data             : [],
                deleted          : groups[2].deleted,
                hidden           : false,
                lastModified     : groups[2].lastModified,
                name             : "test"
            }
        ]));
    }

    @Test()
    public async FindById() : Promise<void> {
        let users : MockUser[] = await this.context.getAll(MockUser.getCollection());
        assert.ok(ObjectValidator.IsEmptyOrNull(users));

        let user : MockUser = new MockUser();
        user.UserName = "name";
        user.Password = "pass";
        user.Email = "this.is.test@mail.oid";
        user.DefaultGroup = new MockGroup();
        user.DefaultGroup.Name = "name";
        await user.DefaultGroup.Save();
        const id : string = user.Id;
        const groupId : string = user.DefaultGroup.Id;
        await user.Save();

        users = await this.context.getAll(MockUser.getCollection());
        assert.ok(!ObjectValidator.IsEmptyOrNull(users));
        assert.ok(ObjectValidator.IsArray(users));
        assert.equal(users.length, 1);
        assert.deepEqual(users, [
            {
                __v         : 0,
                _id         : id,
                activated   : true,
                altEmails   : [],
                auditLog    : {
                    _id         : user.AuditLog.Id,
                    className   : user.AuditLog.ClassName,
                    created     : user.AuditLog.Created,
                    deleted     : user.AuditLog.Deleted,
                    lastModified: user.AuditLog.LastModified
                },
                authTokens  : [],
                className   : user.ClassName,
                created     : user.Created,
                defaultGroup: groupId,
                deleted     : user.Deleted,
                email       : "this.is.test@mail.oid",
                groups      : [],
                lastModified: user.LastModified,
                password    : "pass",
                ssoTokens   : {
                    _id         : user.SSOTokens.Id,
                    className   : user.SSOTokens.ClassName,
                    created     : user.SSOTokens.Created,
                    deleted     : user.SSOTokens.Deleted,
                    lastModified: user.SSOTokens.LastModified
                },
                userName    : "name"
            }
        ]);

        user = await MockUser.FindById(id);
        assert.ok(!ObjectValidator.IsEmptyOrNull(user));
        assert.deepEqual(user.Groups, []);
        assert.equal(user.Email, "this.is.test@mail.oid");
        assert.equal(user.Password, "pass");
        assert.equal(user.UserName, "name");
        assert.equal(user.Id, id);
        assert.equal(user.DefaultGroup.Id, groupId);
    }

    @Test()
    public async Find() : Promise<void> {
        let users : MockUser[] = await this.context.getAll(MockUser.getCollection());
        assert.ok(ObjectValidator.IsEmptyOrNull(users));

        let user : MockUser = new MockUser();
        user.UserName = "name";
        user.Password = "pass";
        user.Email = "testA@mail.oid";
        user.DefaultGroup = new MockGroup();
        user.DefaultGroup.Name = "name";
        await user.DefaultGroup.Save();
        await user.Save();

        user = new MockUser();
        user.UserName = "name2";
        user.Password = "pass2";
        user.Email = "testB@mail.oid";
        user.DefaultGroup = new MockGroup();
        user.DefaultGroup.Name = "name";
        await user.DefaultGroup.Save();
        const group : MockGroup = new MockGroup();
        group.Name = "test";
        group.AuthorizedMethods = ["testAuthMethod1", "testAuthMethod2"];
        await group.Save();
        user.Groups = [group];
        await user.Save();

        user = new MockUser();
        user.UserName = "name3";
        user.Password = "pass3";
        user.Email = "testA@mail.oid";
        user.DefaultGroup = new MockGroup();
        user.DefaultGroup.Name = "name";
        await user.DefaultGroup.Save();
        await user.Save();

        users = await this.context.getAll(MockUser.getCollection());
        assert.ok(!ObjectValidator.IsEmptyOrNull(users));
        assert.ok(ObjectValidator.IsArray(users));
        assert.equal(users.length, 3);

        users = await MockUser.Find({});
        assert.ok(!ObjectValidator.IsEmptyOrNull(users));
        assert.ok(ObjectValidator.IsArray(users));
        assert.equal(users.length, 3);
        assert.deepEqual(users[0].Groups, []);
        assert.equal(users[0].Email, "testA@mail.oid");
        assert.equal(users[0].Password, "pass");
        assert.equal(users[0].UserName, "name");

        assert.equal(users[1].Groups.length, 1);
        assert.equal(users[1].Groups[0].Name, group.Name);
        assert.deepEqual(users[1].Groups[0].AuthorizedMethods, group.AuthorizedMethods);
        assert.equal(users[1].Email, "testB@mail.oid");
        assert.equal(users[1].Password, "pass2");
        assert.equal(users[1].UserName, "name2");

        assert.deepEqual(users[2].Groups, []);
        assert.equal(users[2].Email, "testA@mail.oid");
        assert.equal(users[2].Password, "pass3");
        assert.equal(users[2].UserName, "name3");

        users = await MockUser.Find({email: "testA@mail.oid"});
        assert.ok(!ObjectValidator.IsEmptyOrNull(users));
        assert.ok(ObjectValidator.IsArray(users));
        assert.equal(users.length, 2);
        assert.deepEqual(users[0].Groups, []);
        assert.equal(users[0].Email, "testA@mail.oid");
        assert.equal(users[0].Password, "pass");
        assert.equal(users[0].UserName, "name");

        assert.deepEqual(users[1].Groups, []);
        assert.equal(users[1].Email, "testA@mail.oid");
        assert.equal(users[1].Password, "pass3");
        assert.equal(users[1].UserName, "name3");
    }

    @Test()
    public async Update() : Promise<void> {
        let users : MockUser[] = await this.context.getAll(MockUser.getCollection());
        assert.ok(ObjectValidator.IsEmptyOrNull(users));

        let user : MockUser = new MockUser();
        user.UserName = "name";
        user.Password = "pass";
        user.Email = "this.is.test@mail.oid";
        user.DefaultGroup = new MockGroup();
        user.DefaultGroup.Name = "name";
        await user.DefaultGroup.Save();
        const id : string = user.Id;
        await user.Save();

        users = await this.context.getAll(MockUser.getCollection());
        assert.ok(!ObjectValidator.IsEmptyOrNull(users));
        assert.ok(ObjectValidator.IsArray(users));
        assert.equal(users.length, 1);
        assert.deepEqual(users, [
            {
                __v         : 0,
                _id         : id,
                activated   : true,
                altEmails   : [],
                auditLog    : {
                    _id         : user.AuditLog.Id,
                    className   : user.AuditLog.ClassName,
                    created     : user.AuditLog.Created,
                    deleted     : user.AuditLog.Deleted,
                    lastModified: user.AuditLog.LastModified
                },
                authTokens  : [],
                className   : user.ClassName,
                created     : user.Created,
                defaultGroup: user.DefaultGroup.Id,
                deleted     : user.Deleted,
                email       : "this.is.test@mail.oid",
                groups      : [],
                lastModified: user.LastModified,
                password    : "pass",
                ssoTokens   : {
                    _id         : user.SSOTokens.Id,
                    className   : user.SSOTokens.ClassName,
                    created     : user.SSOTokens.Created,
                    deleted     : user.SSOTokens.Deleted,
                    lastModified: user.SSOTokens.LastModified
                },
                userName    : "name"
            }
        ]);

        user = await MockUser.FindById(id);
        assert.ok(!ObjectValidator.IsEmptyOrNull(user));
        assert.deepEqual(user.Groups, []);
        assert.equal(user.Email, "this.is.test@mail.oid");
        assert.equal(user.Password, "pass");
        assert.equal(user.UserName, "name");
        assert.equal(user.Id, id);
        user.Email = "this.is.updated.test@mail.oid";
        user.Password = "pass2";
        await user.Save();

        users = await this.context.getAll(MockUser.getCollection());
        assert.ok(!ObjectValidator.IsEmptyOrNull(users));
        assert.ok(ObjectValidator.IsArray(users));
        assert.equal(users.length, 1);
        assert.deepEqual(users, [
            {
                __v         : 0,
                _id         : id,
                activated   : true,
                altEmails   : [],
                auditLog    : {
                    _id         : user.AuditLog.Id,
                    className   : user.AuditLog.ClassName,
                    created     : user.AuditLog.Created,
                    deleted     : user.AuditLog.Deleted,
                    lastModified: user.AuditLog.LastModified
                },
                authTokens  : [],
                className   : user.ClassName,
                created     : user.Created,
                defaultGroup: user.DefaultGroup.Id,
                deleted     : user.Deleted,
                email       : "this.is.updated.test@mail.oid",
                groups      : [],
                lastModified: user.LastModified,
                password    : "pass2",
                ssoTokens   : {
                    _id         : user.SSOTokens.Id,
                    className   : user.SSOTokens.ClassName,
                    created     : user.SSOTokens.Created,
                    deleted     : user.SSOTokens.Deleted,
                    lastModified: user.SSOTokens.LastModified
                },
                userName    : "name"
            }
        ]);

        user = await MockUser.FindById(id);
        assert.ok(!ObjectValidator.IsEmptyOrNull(user));
        assert.deepEqual(user.Groups, []);
        assert.equal(user.Email, "this.is.updated.test@mail.oid");
        assert.equal(user.Password, "pass2");
        assert.equal(user.UserName, "name");
        assert.equal(user.Id, id);
    }

    @Test()
    public async Refresh() : Promise<void> {
        let users : MockUser[] = await this.context.getAll(MockUser.getCollection());
        assert.ok(ObjectValidator.IsEmptyOrNull(users));

        let user : MockUser = new MockUser();
        user.UserName = "name";
        user.Password = "pass";
        user.Email = "this.is.test@mail.oid";
        user.DefaultGroup = new MockGroup();
        user.DefaultGroup.Name = "name";
        await user.DefaultGroup.Save();
        const id : string = user.Id;
        await user.Save();

        users = await this.context.getAll(MockUser.getCollection());
        assert.ok(!ObjectValidator.IsEmptyOrNull(users));
        assert.ok(ObjectValidator.IsArray(users));
        assert.equal(users.length, 1);
        assert.deepEqual(users, [
            {
                __v         : 0,
                _id         : id,
                activated   : true,
                altEmails   : [],
                auditLog    : {
                    _id         : user.AuditLog.Id,
                    className   : user.AuditLog.ClassName,
                    created     : user.AuditLog.Created,
                    deleted     : user.AuditLog.Deleted,
                    lastModified: user.AuditLog.LastModified
                },
                authTokens  : [],
                className   : user.ClassName,
                created     : user.Created,
                defaultGroup: user.DefaultGroup.Id,
                deleted     : user.Deleted,
                email       : "this.is.test@mail.oid",
                groups      : [],
                lastModified: user.LastModified,
                password    : "pass",
                ssoTokens   : {
                    _id         : user.SSOTokens.Id,
                    className   : user.SSOTokens.ClassName,
                    created     : user.SSOTokens.Created,
                    deleted     : user.SSOTokens.Deleted,
                    lastModified: user.SSOTokens.LastModified
                },
                userName    : "name"
            }
        ]);

        const userNew : MockUser = await MockUser.FindById(id);
        assert.ok(!ObjectValidator.IsEmptyOrNull(userNew));
        userNew.Email = "this.is.updated.test@mail.oidNew";
        userNew.Password = "passUpdated";
        userNew.UserName = "nameUpdated";
        await userNew.Save();

        users = await this.context.getAll(MockUser.getCollection());
        assert.ok(!ObjectValidator.IsEmptyOrNull(users));
        assert.ok(ObjectValidator.IsArray(users));
        assert.equal(users.length, 1);
        assert.deepEqual(users, [
            {
                __v         : 0,
                _id         : id,
                activated   : true,
                altEmails   : [],
                auditLog    : {
                    _id         : userNew.AuditLog.Id,
                    className   : userNew.AuditLog.ClassName,
                    created     : userNew.AuditLog.Created,
                    deleted     : userNew.AuditLog.Deleted,
                    lastModified: userNew.AuditLog.LastModified
                },
                authTokens  : [],
                className   : userNew.ClassName,
                created     : userNew.Created,
                defaultGroup: userNew.DefaultGroup.Id,
                deleted     : userNew.Deleted,
                email       : "this.is.updated.test@mail.oidNew",
                groups      : [],
                lastModified: userNew.LastModified,
                password    : "passUpdated",
                ssoTokens   : {
                    _id         : userNew.SSOTokens.Id,
                    className   : userNew.SSOTokens.ClassName,
                    created     : userNew.SSOTokens.Created,
                    deleted     : userNew.SSOTokens.Deleted,
                    lastModified: userNew.SSOTokens.LastModified
                },
                userName    : "nameUpdated"
            }
        ]);
        assert.ok(user.LastModified.getTime() < userNew.LastModified.getTime());

        user.UserName = "nameToReinsert";
        let err : string = "";
        try {
            await user.Save();
        } catch (e) {
            err = e.message;
        }
        assert.equal(err, "");

        user = await MockUser.FindById(id);
        assert.ok(!ObjectValidator.IsEmptyOrNull(user));
        assert.deepEqual(user.Groups, []);
        assert.equal(user.Email, "this.is.updated.test@mail.oidNew");
        assert.equal(user.Password, "passUpdated");
        assert.equal(user.UserName, "nameToReinsert");
        assert.equal(user.Id, id);

        assert.equal(userNew.UserName, "nameUpdated");
        await userNew.Refresh();
        assert.equal(userNew.UserName, "nameToReinsert");
    }

    @Test()
    public async Delete() : Promise<void> {
        let users : MockUser[] = await this.context.getAll(MockUser.getCollection());
        assert.ok(ObjectValidator.IsEmptyOrNull(users));

        let user : MockUser = new MockUser();
        user.UserName = "name";
        user.Password = "pass";
        user.Email = "this.is.test@mail.oid";
        user.DefaultGroup = new MockGroup();
        user.DefaultGroup.Name = "name";
        await user.DefaultGroup.Save();
        const id : string = user.Id;
        await user.Save();

        users = await this.context.getAll(MockUser.getCollection());
        assert.ok(!ObjectValidator.IsEmptyOrNull(users));
        assert.ok(ObjectValidator.IsArray(users));
        assert.equal(users.length, 1);
        assert.deepEqual(JsonUtils.Sort(users), [
            {
                __v         : 0,
                _id         : id,
                activated   : true,
                altEmails   : [],
                auditLog    : {
                    _id         : user.AuditLog.Id,
                    className   : user.AuditLog.ClassName,
                    created     : user.AuditLog.Created,
                    deleted     : user.AuditLog.Deleted,
                    lastModified: user.AuditLog.LastModified
                },
                authTokens  : [],
                className   : user.ClassName,
                created     : user.Created,
                defaultGroup: user.DefaultGroup.Id,
                deleted     : user.Deleted,
                email       : "this.is.test@mail.oid",
                groups      : [],
                lastModified: user.LastModified,
                password    : "pass",
                ssoTokens   : {
                    _id         : user.SSOTokens.Id,
                    className   : user.SSOTokens.ClassName,
                    created     : user.SSOTokens.Created,
                    deleted     : user.SSOTokens.Deleted,
                    lastModified: user.SSOTokens.LastModified
                },
                userName    : "name"
            }
        ]);

        user = await MockUser.FindById(id);
        assert.ok(!ObjectValidator.IsEmptyOrNull(user));
        assert.deepEqual(user.Groups, []);
        assert.equal(user.Email, "this.is.test@mail.oid");
        assert.equal(user.Password, "pass");
        assert.equal(user.UserName, "name");
        assert.equal(user.Id, id);
        await user.Remove();

        users = await this.context.getAll(MockUser.getCollection());
        assert.ok(ObjectValidator.IsEmptyOrNull(users));
    }

    @Test()
    public async WithReference() : Promise<void> {
        const user : MockUser = new MockUser();
        user.UserName = "name";
        user.Password = "pass";
        user.Email = "this.is.test@mail.oid";
        user.DefaultGroup = new MockGroup();
        user.DefaultGroup.Name = "name";
        await user.DefaultGroup.Save();
        const id : string = user.Id;
        const group : MockGroup = new MockGroup();
        group.Name = "test";
        group.AuthorizedMethods = ["ns1.methodA", "ns2.methodB", "ns3.methodA"];
        await group.Save();
        user.Groups = [group];
        user.AuthTokens.push(Object.assign(new AuthToken(), {
            Date  : 2012,
            Expire: 666,
            Token : StringUtils.getSha1("this is token A")
        }));
        user.AuthTokens.push(Object.assign(new AuthToken(), {
            Date  : 1415,
            Expire: 999,
            Token : StringUtils.getSha1("this is token B")
        }));

        let users : MockUser[] = await this.context.getAll(MockUser.getCollection());
        assert.ok(ObjectValidator.IsEmptyOrNull(users));
        await user.Save();

        let tokens : AuthToken[] = await this.context.getAll(AuthToken.getCollection());
        assert.ok(!ObjectValidator.IsEmptyOrNull(tokens));
        assert.ok(ObjectValidator.IsArray(tokens));
        assert.equal(tokens.length, 2);
        assert.deepEqual(tokens, [
            {
                __v         : 0,
                _id         : user.AuthTokens[0].Id,
                className   : user.AuthTokens[0].ClassName,
                created     : user.AuthTokens[0].Created,
                date        : 2012,
                deleted     : user.AuthTokens[0].Deleted,
                expire      : 666,
                lastModified: user.AuthTokens[0].LastModified,
                token       : StringUtils.getSha1("this is token A")
            },
            {
                __v         : 0,
                _id         : user.AuthTokens[1].Id,
                className   : user.AuthTokens[1].ClassName,
                created     : user.AuthTokens[1].Created,
                date        : 1415,
                deleted     : user.AuthTokens[1].Deleted,
                expire      : 999,
                lastModified: user.AuthTokens[1].LastModified,
                token       : StringUtils.getSha1("this is token B")
            }
        ]);

        users = await this.context.getAll(MockUser.getCollection());
        assert.ok(!ObjectValidator.IsEmptyOrNull(users));
        assert.ok(ObjectValidator.IsArray(users));
        assert.equal(users.length, 1);
        const references : string[] = (<any>users[0]).authTokens;
        delete (<any>users[0]).authTokens;
        assert.deepEqual(users, [
            {
                __v         : 0,
                _id         : id,
                activated   : true,
                altEmails   : [],
                auditLog    : {
                    _id         : user.AuditLog.Id,
                    className   : user.AuditLog.ClassName,
                    created     : user.AuditLog.Created,
                    deleted     : user.AuditLog.Deleted,
                    lastModified: user.AuditLog.LastModified
                },
                className   : user.ClassName,
                created     : user.Created,
                defaultGroup: user.DefaultGroup.Id,
                deleted     : user.Deleted,
                email       : "this.is.test@mail.oid",
                groups      : [group.Id],
                lastModified: user.LastModified,
                password    : "pass",
                ssoTokens   : {
                    _id         : user.SSOTokens.Id,
                    className   : user.SSOTokens.ClassName,
                    created     : user.SSOTokens.Created,
                    deleted     : user.SSOTokens.Deleted,
                    lastModified: user.SSOTokens.LastModified
                },
                userName    : "name"
            }
        ]);

        tokens = await AuthToken.Find({_id: {$in: references}});
        assert.ok(!ObjectValidator.IsEmptyOrNull(tokens));
        assert.ok(ObjectValidator.IsArray(tokens));
        assert.equal(tokens.length, 2);
        assert.equal(tokens[0].Date, 2012);
        assert.equal(tokens[0].Expire, 666);
        assert.equal(tokens[0].Token, StringUtils.getSha1("this is token A"));
        assert.equal(tokens[1].Date, 1415);
        assert.equal(tokens[1].Expire, 999);
        assert.equal(tokens[1].Token, StringUtils.getSha1("this is token B"));

        const userFind : MockUser = await MockUser.FindById(id);
        assert.equal(userFind.Email, user.Email);
        assert.equal(userFind.Password, user.Password);
        assert.equal(userFind.UserName, user.UserName);
        assert.equal(userFind.Id, user.Id);
        assert.equal(userFind.Groups.length, user.Groups.length);
        for (let i : number = 0; i < userFind.Groups.length; i++) {
            const act : Group = userFind.Groups[i];
            const exp : Group = user.Groups[i];
            assert.equal(act.Name, exp.Name);
            assert.deepEqual(act.AuthorizedMethods, exp.AuthorizedMethods);
        }
        assert.equal(userFind.AuthTokens.length, user.AuthTokens.length);
        for (let i : number = 0; i < userFind.AuthTokens.length; i++) {
            const act : AuthToken = userFind.AuthTokens[i];
            const exp : AuthToken = user.AuthTokens[i];
            assert.equal(act.Date, exp.Date);
            assert.equal(act.Expire, exp.Expire);
            assert.equal(act.Token, exp.Token);
        }
    }

    @Test()
    public async ToJSON() : Promise<void> {
        const user : MockUser = new MockUser();
        user.UserName = "name";
        user.Password = "pass";
        user.Email = "this.is.test@mail.oid";
        user.DefaultGroup = new MockGroup();
        user.DefaultGroup.Name = "name";
        await user.DefaultGroup.Save();
        const group : MockGroup = new MockGroup();
        group.Name = "test";
        group.AuthorizedMethods = ["ns1.methodA", "ns2.methodB", "ns3.methodA"];
        await group.Save();
        user.Groups = [group];
        user.AuthTokens.push(Object.assign(new AuthToken(), {
            Date  : 2012,
            Expire: 666,
            Token : StringUtils.getSha1("this is token A")
        }));
        user.AuthTokens.push(Object.assign(new AuthToken(), {
            Date  : 1415,
            Expire: 999,
            Token : StringUtils.getSha1("this is token B")
        }));

        assert.ok(ObjectValidator.IsEmptyOrNull(await this.context.getAll(MockUser.getCollection())));
        await user.Save();

        assert.equal(JSON.stringify(JsonUtils.Sort(await user.ToJSON())), JSON.stringify(({
            Activated   : true,
            AltEmails   : [],
            AuditLog    : {
                ClassName   : user.AuditLog.ClassName,
                Created     : user.AuditLog.Created,
                Deleted     : user.AuditLog.Deleted,
                Id          : user.AuditLog.Id,
                LastModified: user.AuditLog.LastModified
            },
            AuthTokens  : [
                {
                    ClassName   : user.AuthTokens[0].ClassName,
                    Created     : user.AuthTokens[0].Created,
                    Date        : 2012,
                    Deleted     : user.AuthTokens[0].Deleted,
                    Expire      : 666,
                    Id          : user.AuthTokens[0].Id,
                    LastModified: user.AuthTokens[0].LastModified,
                    Token       : "4cb026a2383900dee62e1b674b1f583733dc0496"
                },
                {
                    ClassName   : user.AuthTokens[1].ClassName,
                    Created     : user.AuthTokens[1].Created,
                    Date        : 1415,
                    Deleted     : user.AuthTokens[1].Deleted,
                    Expire      : 999,
                    Id          : user.AuthTokens[1].Id,
                    LastModified: user.AuthTokens[1].LastModified,
                    Token       : "a8346e9355d9b7236cc5fcadaeb0b55a57ead09d"
                }
            ],
            ClassName   : user.ClassName,
            Created     : user.Created,
            DefaultGroup: {
                AuthorizedMethods: [],
                ClassName        : user.DefaultGroup.ClassName,
                Created          : user.DefaultGroup.Created,
                Data             : [],
                Deleted          : user.DefaultGroup.Deleted,
                Hidden           : false,
                Id               : user.DefaultGroup.Id,
                LastModified     : user.DefaultGroup.LastModified,
                Name             : "name"
            },
            Deleted     : user.Deleted,
            Email       : "this.is.test@mail.oid",
            Groups      : [
                {
                    AuthorizedMethods: [
                        "ns1.methodA",
                        "ns2.methodB",
                        "ns3.methodA"
                    ],
                    ClassName        : user.Groups[0].ClassName,
                    Created          : user.Groups[0].Created,
                    Data             : [],
                    Deleted          : user.Groups[0].Deleted,
                    Hidden           : false,
                    Id               : group.Id,
                    LastModified     : user.Groups[0].LastModified,
                    Name             : "test"
                }
            ],
            Id          : user.Id,
            LastModified: user.LastModified,
            Password    : "pass",
            SSOTokens   : {
                ClassName   : user.SSOTokens.ClassName,
                Created     : user.SSOTokens.Created,
                Deleted     : user.SSOTokens.Deleted,
                Id          : user.SSOTokens.Id,
                LastModified: user.SSOTokens.LastModified
            },
            UserName    : "name"
        })));
    }

    @Test()
    public async ToInterface() : Promise<void> {
        const user : MockUser = new MockUser();
        user.UserName = "name";
        user.Password = "pass";
        user.DefaultGroup = new MockGroup();
        user.DefaultGroup.Name = "name";
        const group : MockGroup = new MockGroup();
        group.Name = "test";
        group.AuthorizedMethods = ["ns1.methodA", "ns2.methodB", "ns3.methodA"];
        user.Groups = [group];
        user.AuthTokens.push(Object.assign(new AuthToken(), {
            Date  : 2012,
            Expire: 666,
            Token : StringUtils.getSha1("this is token A")
        }));
        user.AuthTokens.push(Object.assign(new AuthToken(), {
            Date  : 1415,
            Expire: 999,
            Token : StringUtils.getSha1("this is token B")
        }));
        /// TODO: ToInterface should not populate Id and props not saved to DB, so this test respect it and is not calling Save
        // await user.Save();
        const data : any = user.ToInterface();
        const original : any = {
            Activated   : true,
            AltEmails   : [],
            AuditLog    : {
                ClassName: user.AuditLog.ClassName,
                Created  : user.AuditLog.Created,
                Deleted  : user.AuditLog.Deleted,
                // Id          : user.AuditLog.Id,
                LastModified: user.AuditLog.LastModified
            },
            AuthTokens  : [
                {
                    ClassName: user.AuthTokens[0].ClassName,
                    Created  : user.AuthTokens[0].Created,
                    Date     : 2012,
                    Deleted  : user.AuthTokens[0].Deleted,
                    Expire   : 666,
                    // Id          : user.AuthTokens[0].Id,
                    LastModified: user.AuthTokens[0].LastModified,
                    Token       : "4cb026a2383900dee62e1b674b1f583733dc0496"
                },
                {
                    ClassName: user.AuthTokens[1].ClassName,
                    Created  : user.AuthTokens[1].Created,
                    Date     : 1415,
                    Deleted  : user.AuthTokens[1].Deleted,
                    Expire   : 999,
                    // Id          : user.AuthTokens[1].Id,
                    LastModified: user.AuthTokens[1].LastModified,
                    Token       : "a8346e9355d9b7236cc5fcadaeb0b55a57ead09d"
                }
            ],
            ClassName   : user.ClassName,
            Created     : user.Created,
            DefaultGroup: {
                AuthorizedMethods: [],
                ClassName        : user.DefaultGroup.ClassName,
                Created          : user.DefaultGroup.Created,
                Data             : [],
                Deleted          : user.DefaultGroup.Deleted,
                Hidden           : false,
                // Id               : user.DefaultGroup.Id,
                LastModified: user.DefaultGroup.LastModified,
                Name        : "name"
            },
            Deleted     : user.Deleted,
            Groups      : [
                {
                    AuthorizedMethods: [
                        "ns1.methodA",
                        "ns2.methodB",
                        "ns3.methodA"
                    ],
                    ClassName        : user.Groups[0].ClassName,
                    Created          : user.Groups[0].Created,
                    Data             : [],
                    Deleted          : user.Groups[0].Deleted,
                    Hidden           : false,
                    // Id               : group.Id,
                    LastModified: user.Groups[0].LastModified,
                    Name        : "test"
                }
            ],
            // Id          : user.Id,
            LastModified: user.LastModified,
            Password    : "pass",
            SSOTokens   : {
                ClassName: user.SSOTokens.ClassName,
                Created  : user.SSOTokens.Created,
                Deleted  : user.SSOTokens.Deleted,
                // Id          : user.SSOTokens.Id,
                LastModified: user.SSOTokens.LastModified
            },
            UserName    : "name"
        };
        assert.equal(JSON.stringify(JsonUtils.Sort(data)), JSON.stringify(original));
        // hacked stringify-parse - stuff because it is sorted by different way than original and interface generator
        // is called internally (implicitly) and we do not include sort inside (performance, not-needed outside tests)
        assert.equal(JSON.stringify(JsonUtils.Sort(JSON.parse(JSON.stringify(user)))), JSON.stringify(original));
    }

    // all sub elements are saved before top-most save
    @Test()
    public async ContinuousSave() : Promise<void> {
        const sub1 : MockSubData = new MockSubData();
        sub1.Name = "sub1";
        await sub1.Save();
        const sub2 : MockSubData = new MockSubData();
        sub2.Name = "sub2";
        await sub2.Save();
        const sub3 : MockSubData = new MockSubData();
        sub3.Name = "sub3";
        await sub3.Save();

        const md1 : MockData = new MockData();
        md1.SubData = [sub1, sub3];
        await md1.Save();

        const md2 : MockData = new MockData();
        md2.SubData = [sub2, sub3];
        await md2.Save();

        const md3 : MockData = new MockData();
        md3.SubData = [sub1];
        await md3.Save();

        const group1 : MockGroup = new MockGroup();
        group1.Name = "Group1";
        group1.AuthorizedMethods = ["some1", "some2"];
        group1.Data = [md2, md1];
        await group1.Save();

        const group2 : MockGroup = new MockGroup();
        group2.Name = "Group2";
        group2.AuthorizedMethods = ["someA", "someB"];
        group2.Data = [md1, md3];
        await group2.Save();

        const user1 : MockUser = new MockUser();
        user1.UserName = "name1";
        user1.Password = "pass1";
        user1.Email = "this.is.test1@mail.oid";
        user1.Groups = [group1];
        user1.DefaultGroup = group2;

        const user2 : MockUser = new MockUser();
        user2.UserName = "name2";
        user2.Password = "pass2";
        user2.Email = "this.is.test2@mail.oid";
        user2.Groups = [group1, group2];
        user2.DefaultGroup = group1;

        let users : MockUser[] = await this.context.getAll(MockUser.getCollection());
        assert.ok(ObjectValidator.IsEmptyOrNull(users));
        await user1.Save();
        group2.AuthorizedMethods.push("someC");
        await group2.Save();
        await user2.Save();

        users = await MockUser.Find({userName: "name1"});

        assert.ok(!ObjectValidator.IsEmptyOrNull(users));
        assert.ok(ObjectValidator.IsArray(users));
        assert.equal(users.length, 1);

        let err : string = "";
        try {
            await users[0].Save();
        } catch (e) {
            err = e.message;
        }
        assert.equal(err, "");

        const usersData : any[] = (await Promise.all((
            await MockUser.Find({})).map(($user) => $user.ToJSON()))).map(($value : any) : any => {
            delete $value.DefaultGroup;
            delete $value.SSOTokens;
            delete $value.AuditLog;
            return $value;
        });
        await this.removeProps(usersData, ["Created", "ClassName", "LastModified", "Deleted"]);
        assert.ok(!ObjectValidator.IsEmptyOrNull(usersData));
        assert.ok(ObjectValidator.IsArray(usersData));
        assert.equal(usersData.length, 2);
        // assert.deepEqual(JsonUtils.Sort(usersData), JsonUtils.Sort([
        assert.deepEqual(JSON.stringify(JsonUtils.Sort(usersData)), JSON.stringify([
            {
                Activated : true,
                AltEmails : [],
                AuthTokens: [],
                Email     : user1.Email,
                Groups    : [
                    {
                        AuthorizedMethods: ["some1", "some2"],
                        Data             : [
                            {
                                Id     : md2.Id,
                                SubData: [
                                    {Id: sub2.Id, Name: sub2.Name},
                                    {Id: sub3.Id, Name: sub3.Name}
                                ]
                            },
                            {
                                Id     : md1.Id,
                                SubData: [
                                    {Id: sub1.Id, Name: sub1.Name},
                                    {Id: sub3.Id, Name: sub3.Name}
                                ]
                            }
                        ],
                        Hidden           : false,
                        Id               : group1.Id,
                        Name             : group1.Name
                    }
                ],
                Id        : user1.Id,
                Password  : user1.Password,
                UserName  : user1.UserName
            },
            {
                Activated : true,
                AltEmails : [],
                AuthTokens: [],
                Email     : user2.Email,
                Groups    : [
                    {
                        AuthorizedMethods: ["some1", "some2"],
                        Data             : [
                            {
                                Id     : md2.Id,
                                SubData: [
                                    {Id: sub2.Id, Name: sub2.Name},
                                    {Id: sub3.Id, Name: sub3.Name}
                                ]
                            },
                            {
                                Id     : md1.Id,
                                SubData: [
                                    {Id: sub1.Id, Name: sub1.Name},
                                    {Id: sub3.Id, Name: sub3.Name}
                                ]
                            }
                        ],
                        Hidden           : false,
                        Id               : group1.Id,
                        Name             : group1.Name
                    },
                    {
                        AuthorizedMethods: ["someA", "someB", "someC"],
                        Data             : [
                            {
                                Id     : md1.Id,
                                SubData: [
                                    {Id: sub1.Id, Name: sub1.Name},
                                    {Id: sub3.Id, Name: sub3.Name}
                                ]
                            },
                            {
                                Id     : md3.Id,
                                SubData: [
                                    {Id: sub1.Id, Name: sub1.Name}
                                ]
                            }
                        ],
                        Hidden           : false,
                        Id               : group2.Id,
                        Name             : group2.Name
                    }
                ],
                Id        : user2.Id,
                Password  : user2.Password,
                UserName  : user2.UserName
            }
        ]));
    }

    // NO sub element is saved before top-most save so everything is saved at once
    @Test()
    public async MasterSave() : Promise<void> {
        const sub1 : MockSubData = new MockSubData();
        sub1.Name = "sub1";
        const sub2 : MockSubData = new MockSubData();
        sub2.Name = "sub2";
        const sub3 : MockSubData = new MockSubData();
        sub3.Name = "sub3";

        const md1 : MockData = new MockData();
        md1.SubData = [sub1, sub3];
        const md2 : MockData = new MockData();
        md2.SubData = [sub2, sub3];
        const md3 : MockData = new MockData();
        md3.SubData = [sub1];

        const group1 : MockGroup = new MockGroup();
        group1.Name = "Group1";
        group1.AuthorizedMethods = ["some1", "some2"];
        group1.Data = [md2, md1];

        const group2 : MockGroup = new MockGroup();
        group2.Name = "Group2";
        group2.AuthorizedMethods = ["someA", "someB"];
        group2.Data = [md1, md3];

        const user1 : MockUser = new MockUser();
        user1.UserName = "name1";
        user1.Password = "pass1";
        user1.Email = "this.is.test1@mail.oid";
        user1.Groups = [group1];
        user1.DefaultGroup = group2;

        const user2 : MockUser = new MockUser();
        user2.UserName = "name2";
        user2.Password = "pass2";
        user2.Email = "this.is.test2@mail.oid";
        user2.Groups = [group1, group2];
        user2.DefaultGroup = group1;

        // now save of user starting
        let users : MockUser[] = await this.context.getAll(MockUser.getCollection());
        assert.ok(ObjectValidator.IsEmptyOrNull(users));
        await user1.Save();
        // update previously referenced group in user1 before user2 save to test proper update of referenced object
        // group2.AuthorizedMethods.push("someC");
        await user2.Save();

        users = await MockUser.Find({userName: "name1"});

        assert.ok(!ObjectValidator.IsEmptyOrNull(users));
        assert.ok(ObjectValidator.IsArray(users));
        assert.equal(users.length, 1);

        let err : string = "";
        try {
            await users[0].Save();
        } catch (e) {
            err = e.message;
        }
        assert.equal(err, "");

        const usersData : any[] = (await Promise.all((
            await MockUser.Find({})).map(($user) => $user.ToJSON()))).map(($value : any) : any => {
            delete $value.DefaultGroup;
            delete $value.SSOTokens;
            delete $value.AuditLog;
            return $value;
        });
        await this.removeProps(usersData, ["Created", "ClassName", "LastModified", "Deleted"]);
        assert.ok(!ObjectValidator.IsEmptyOrNull(usersData));
        assert.ok(ObjectValidator.IsArray(usersData));
        assert.equal(usersData.length, 2);
        // assert.deepEqual(usersData, ([
        assert.deepEqual(JSON.stringify(JsonUtils.Sort(usersData)), JSON.stringify([
            {
                Activated : true,
                AltEmails : [],
                AuthTokens: [],
                Email     : user1.Email,
                Groups    : [
                    {
                        AuthorizedMethods: ["some1", "some2"],
                        Data             : [
                            {
                                Id     : md2.Id,
                                SubData: [
                                    {Id: sub2.Id, Name: sub2.Name},
                                    {Id: sub3.Id, Name: sub3.Name}
                                ]
                            },
                            {
                                Id     : md1.Id,
                                SubData: [
                                    {Id: sub1.Id, Name: sub1.Name},
                                    {Id: sub3.Id, Name: sub3.Name}
                                ]
                            }
                        ],
                        Hidden           : false,
                        Id               : group1.Id,
                        Name             : group1.Name
                    }
                ],
                Id        : user1.Id,
                Password  : user1.Password,
                UserName  : user1.UserName
            },
            {
                Activated : true,
                AltEmails : [],
                AuthTokens: [],
                Email     : user2.Email,
                Groups    : [
                    {
                        AuthorizedMethods: ["some1", "some2"],
                        Data             : [
                            {
                                Id     : md2.Id,
                                SubData: [
                                    {Id: sub2.Id, Name: sub2.Name},
                                    {Id: sub3.Id, Name: sub3.Name}
                                ]
                            },
                            {
                                Id     : md1.Id,
                                SubData: [
                                    {Id: sub1.Id, Name: sub1.Name},
                                    {Id: sub3.Id, Name: sub3.Name}
                                ]
                            }
                        ],
                        Hidden           : false,
                        Id               : group1.Id,
                        Name             : group1.Name
                    },
                    {
                        AuthorizedMethods: ["someA", "someB"],
                        Data             : [
                            {
                                Id     : md1.Id,
                                SubData: [
                                    {Id: sub1.Id, Name: sub1.Name},
                                    {Id: sub3.Id, Name: sub3.Name}
                                ]
                            },
                            {
                                Id     : md3.Id,
                                SubData: [
                                    {Id: sub1.Id, Name: sub1.Name}
                                ]
                            }
                        ],
                        Hidden           : false,
                        Id               : group2.Id,
                        Name             : group2.Name
                    }
                ],
                Id        : user2.Id,
                Password  : user2.Password,
                UserName  : user2.UserName
            }
        ]));
    }

    // remove and re-insert group data without refresh to fail
    @Test()
    public async MultiSave() : Promise<void> {
        const sub1 : MockSubData = new MockSubData();
        sub1.Name = "sub1";
        const sub2 : MockSubData = new MockSubData();
        sub2.Name = "sub2";
        const sub3 : MockSubData = new MockSubData();
        sub3.Name = "sub3";

        const md1 : MockData = new MockData();
        md1.SubData = [sub1, sub3];
        const md2 : MockData = new MockData();
        md2.SubData = [sub2, sub3];
        const md3 : MockData = new MockData();
        md3.SubData = [sub1];

        const group1 : MockGroup = new MockGroup();
        group1.Name = "Group1";
        group1.AuthorizedMethods = ["some1", "some2"];
        group1.Data = [md2, md1];

        const group2 : MockGroup = new MockGroup();
        group2.Name = "Group2";
        group2.AuthorizedMethods = ["someA", "someB"];
        group2.Data = [md1, md3];

        const user1 : MockUser = new MockUser();
        user1.UserName = "name1";
        user1.Password = "pass1";
        user1.Email = "this.is.test1@mail.oid";
        user1.Groups = [group1];
        user1.DefaultGroup = group2;

        const user2 : MockUser = new MockUser();
        user2.UserName = "name2";
        user2.Password = "pass2";
        user2.Email = "this.is.test2@mail.oid";
        user2.Groups = [group1, group2];
        user2.DefaultGroup = group1;

        // now user1 only to check simple ref-save ant user2 will be processed later on
        let users : MockUser[] = await this.context.getAll(MockUser.getCollection());
        assert.ok(ObjectValidator.IsEmptyOrNull(users));
        await user1.Save();

        let subData : MockSubData[] = await this.context.getAll(MockSubData.getCollection());
        await this.removeProps(subData, ["created", "className", "lastModified", "deleted"]);
        assert.deepEqual(subData, [
            {_id: sub2.Id, name: "sub2", __v: 0},
            {_id: sub3.Id, name: "sub3", __v: 0},
            {_id: sub1.Id, name: "sub1", __v: 0}
        ]);

        let data : MockData[] = await this.context.getAll(MockData.getCollection());
        await this.removeProps(data, ["created", "className", "lastModified", "deleted"]);
        assert.deepEqual(data, [
            {_id: md2.Id, subData: [sub2.Id, sub3.Id], __v: 0},
            {_id: md1.Id, subData: [sub1.Id, sub3.Id], __v: 0},
            {_id: md3.Id, subData: [sub1.Id], __v: 0}
        ]);

        let groups : MockGroup[] = await this.context.getAll(MockGroup.getCollection());
        await this.removeProps(groups, ["created", "className", "lastModified", "deleted"]);
        assert.deepEqual(groups, [
            {
                __v              : 0,
                _id              : group1.Id,
                authorizedMethods: ["some1", "some2"],
                data             : [md2.Id, md1.Id],
                hidden           : false,
                name             : "Group1"
            },
            {
                __v              : 0,
                _id              : group2.Id,
                authorizedMethods: ["someA", "someB"],
                data             : [md1.Id, md3.Id],
                hidden           : false,
                name             : "Group2"
            }
        ]);

        let usersA : MockUser[] = await this.context.getAll(MockUser.getCollection());
        await this.removeProps(usersA, ["created", "className", "lastModified", "deleted"]);
        assert.deepEqual(JsonUtils.Sort(usersA), [
            {
                __v         : 0,
                _id         : user1.Id,
                activated   : true,
                altEmails   : [],
                auditLog    : {_id: user1.AuditLog.Id},
                authTokens  : [],
                defaultGroup: group2.Id,
                email       : "this.is.test1@mail.oid",
                groups      : [group1.Id],
                password    : "pass1",
                ssoTokens   : {_id: user1.SSOTokens.Id},
                userName    : "name1"
            }
        ]);

        // another save without any changes to handle blocked save of internal document version property
        await user1.Save();
        await user1.Save();

        subData = await this.context.getAll(MockSubData.getCollection());
        await this.removeProps(subData, ["created", "className", "lastModified", "deleted"]);
        assert.deepEqual(subData, [
            {_id: sub2.Id, name: "sub2", __v: 0},
            {_id: sub3.Id, name: "sub3", __v: 0},
            {_id: sub1.Id, name: "sub1", __v: 0}
        ]);

        data = await this.context.getAll(MockData.getCollection());
        await this.removeProps(data, ["created", "className", "lastModified", "deleted"]);
        assert.deepEqual(data, [
            {_id: md2.Id, subData: [sub2.Id, sub3.Id], __v: 0},
            {_id: md1.Id, subData: [sub1.Id, sub3.Id], __v: 0},
            {_id: md3.Id, subData: [sub1.Id], __v: 0}
        ]);

        groups = await this.context.getAll(MockGroup.getCollection());
        await this.removeProps(groups, ["created", "className", "lastModified", "deleted"]);
        assert.deepEqual(groups, [
            {
                __v              : 0,
                _id              : group1.Id,
                authorizedMethods: ["some1", "some2"],
                data             : [md2.Id, md1.Id],
                hidden           : false,
                name             : "Group1"
            },
            {
                __v              : 0,
                _id              : group2.Id,
                authorizedMethods: ["someA", "someB"],
                data             : [md1.Id, md3.Id],
                hidden           : false,
                name             : "Group2"
            }
        ]);

        usersA = await this.context.getAll(MockUser.getCollection());
        await this.removeProps(usersA, ["created", "className", "lastModified", "deleted"]);
        assert.deepEqual(usersA, [
            {
                __v         : 0,
                _id         : user1.Id,
                activated   : true,
                altEmails   : [],
                auditLog    : {_id: user1.AuditLog.Id},
                authTokens  : [],
                defaultGroup: group2.Id,
                email       : "this.is.test1@mail.oid",
                groups      : [group1.Id],
                password    : "pass1",
                ssoTokens   : {_id: user1.SSOTokens.Id},
                userName    : "name1"
            }
        ]);

        // now save another use and check that groups and data/subdata has no modification of doc version
        await user2.Save();

        subData = await this.context.getAll(MockSubData.getCollection());
        await this.removeProps(subData, ["created", "className", "lastModified", "deleted"]);
        assert.deepEqual(subData, [
            {_id: sub2.Id, name: "sub2", __v: 0},
            {_id: sub3.Id, name: "sub3", __v: 0},
            {_id: sub1.Id, name: "sub1", __v: 0}
        ]);

        data = await this.context.getAll(MockData.getCollection());
        await this.removeProps(data, ["created", "className", "lastModified", "deleted"]);
        assert.deepEqual(data, [
            {_id: md2.Id, subData: [sub2.Id, sub3.Id], __v: 0},
            {_id: md1.Id, subData: [sub1.Id, sub3.Id], __v: 0},
            {_id: md3.Id, subData: [sub1.Id], __v: 0}
        ]);

        groups = await this.context.getAll(MockGroup.getCollection());
        await this.removeProps(groups, ["created", "className", "lastModified", "deleted"]);
        assert.deepEqual(groups, ([
            {
                __v              : 0,
                _id              : group1.Id,
                authorizedMethods: ["some1", "some2"],
                data             : [md2.Id, md1.Id],
                hidden           : false,
                name             : "Group1"
            },
            {
                __v              : 0,
                _id              : group2.Id,
                authorizedMethods: ["someA", "someB"],
                data             : [md1.Id, md3.Id],
                hidden           : false,
                name             : "Group2"
            }
        ]));

        usersA = await this.context.getAll(MockUser.getCollection());
        await this.removeProps(usersA, ["created", "className", "lastModified", "deleted"]);
        assert.deepEqual(usersA, [
            {
                __v         : 0,
                _id         : user1.Id,
                activated   : true,
                altEmails   : [],
                auditLog    : {_id: user1.AuditLog.Id},
                authTokens  : [],
                defaultGroup: group2.Id,
                email       : "this.is.test1@mail.oid",
                groups      : [group1.Id],
                password    : "pass1",
                ssoTokens   : {_id: user1.SSOTokens.Id},
                userName    : "name1"
            },
            {
                __v         : 0,
                _id         : user2.Id,
                activated   : true,
                altEmails   : [],
                auditLog    : {_id: user2.AuditLog.Id},
                authTokens  : [],
                defaultGroup: group1.Id,
                email       : "this.is.test2@mail.oid",
                groups      : [group1.Id, group2.Id],
                password    : "pass2",
                ssoTokens   : {_id: user2.SSOTokens.Id},
                userName    : "name2"
            }
        ]);

        users = await MockUser.Find({userName: "name1"});

        assert.ok(!ObjectValidator.IsEmptyOrNull(users));
        assert.ok(ObjectValidator.IsArray(users));
        assert.equal(users.length, 1);

        await users[0].Save();

        // now all users has proper versions of referenced objects (__v === 0)
        subData = await this.context.getAll(MockSubData.getCollection());
        await this.removeProps(subData, ["created", "className", "lastModified", "deleted"]);
        assert.deepEqual(subData, [
            {_id: sub2.Id, name: "sub2", __v: 0},
            {_id: sub3.Id, name: "sub3", __v: 0},
            {_id: sub1.Id, name: "sub1", __v: 0}
        ]);

        data = await this.context.getAll(MockData.getCollection());
        await this.removeProps(data, ["created", "className", "lastModified", "deleted"]);
        assert.deepEqual(data, [
            {_id: md2.Id, subData: [sub2.Id, sub3.Id], __v: 0},
            {_id: md1.Id, subData: [sub1.Id, sub3.Id], __v: 0},
            {_id: md3.Id, subData: [sub1.Id], __v: 0}
        ]);

        groups = await this.context.getAll(MockGroup.getCollection());
        await this.removeProps(groups, ["created", "className", "lastModified", "deleted"]);
        assert.deepEqual(groups, [
            {
                __v              : 0,
                _id              : group1.Id,
                authorizedMethods: ["some1", "some2"],
                data             : [md2.Id, md1.Id],
                hidden           : false,
                name             : "Group1"
            },
            {
                __v              : 0,
                _id              : group2.Id,
                authorizedMethods: ["someA", "someB"],
                data             : [md1.Id, md3.Id],
                hidden           : false,
                name             : "Group2"
            }
        ]);

        usersA = await this.context.getAll(MockUser.getCollection());
        await this.removeProps(usersA, ["created", "className", "lastModified", "deleted"]);
        assert.deepEqual(usersA, [
            {
                __v         : 0,
                _id         : user1.Id,
                activated   : true,
                altEmails   : [],
                auditLog    : {_id: user1.AuditLog.Id},
                authTokens  : [],
                defaultGroup: group2.Id,
                email       : "this.is.test1@mail.oid",
                groups      : [group1.Id],
                password    : "pass1",
                ssoTokens   : {_id: user1.SSOTokens.Id},
                userName    : "name1"
            },
            {
                __v         : 0,
                _id         : user2.Id,
                activated   : true,
                altEmails   : [],
                auditLog    : {_id: user2.AuditLog.Id},
                authTokens  : [],
                defaultGroup: group1.Id,
                email       : "this.is.test2@mail.oid",
                groups      : [group1.Id, group2.Id],
                password    : "pass2",
                ssoTokens   : {_id: user2.SSOTokens.Id},
                userName    : "name2"
            }
        ]);

        // now change data on several places and save them explicitly
        // !! nothing except this values can be changed (also ids and versions are the same)
        users[0].DefaultGroup.Name = "changed group";
        await users[0].DefaultGroup.Save();
        users[0].Groups[0].Data[0].SubData[0].Name = "changed subdata";
        await users[0].Groups[0].Data[0].SubData[0].Save();
        users[0].UserName = "changed username";
        await users[0].Save();

        subData = await this.context.getAll(MockSubData.getCollection());
        await this.removeProps(subData, ["created", "className", "lastModified", "deleted"]);
        assert.deepEqual(subData, [
            {_id: sub2.Id, name: "changed subdata", __v: 0},
            {_id: sub3.Id, name: "sub3", __v: 0},
            {_id: sub1.Id, name: "sub1", __v: 0}
        ]);

        data = await this.context.getAll(MockData.getCollection());
        await this.removeProps(data, ["created", "className", "lastModified", "deleted"]);
        assert.deepEqual(data, [
            {_id: md2.Id, subData: [sub2.Id, sub3.Id], __v: 0},
            {_id: md1.Id, subData: [sub1.Id, sub3.Id], __v: 0},
            {_id: md3.Id, subData: [sub1.Id], __v: 0}
        ]);

        groups = await this.context.getAll(MockGroup.getCollection());
        await this.removeProps(groups, ["created", "className", "lastModified", "deleted"]);
        assert.deepEqual(groups, [
            {
                __v              : 0,
                _id              : group1.Id,
                authorizedMethods: ["some1", "some2"],
                data             : [md2.Id, md1.Id],
                hidden           : false,
                name             : "Group1"
            },
            {
                __v              : 0,
                _id              : group2.Id,
                authorizedMethods: ["someA", "someB"],
                data             : [md1.Id, md3.Id],
                hidden           : false,
                name             : "changed group"
            }
        ]);

        usersA = await this.context.getAll(MockUser.getCollection());
        await this.removeProps(usersA, ["created", "className", "lastModified", "deleted"]);
        assert.deepEqual(usersA, [
            {
                __v         : 0,
                _id         : user1.Id,
                activated   : true,
                altEmails   : [],
                auditLog    : {_id: user1.AuditLog.Id},
                authTokens  : [],
                defaultGroup: group2.Id,
                email       : "this.is.test1@mail.oid",
                groups      : [group1.Id],
                password    : "pass1",
                ssoTokens   : {_id: user1.SSOTokens.Id},
                userName    : "changed username"
            },
            {
                __v         : 0,
                _id         : user2.Id,
                activated   : true,
                altEmails   : [],
                auditLog    : {_id: user2.AuditLog.Id},
                authTokens  : [],
                defaultGroup: group1.Id,
                email       : "this.is.test2@mail.oid",
                groups      : [group1.Id, group2.Id],
                password    : "pass2",
                ssoTokens   : {_id: user2.SSOTokens.Id},
                userName    : "name2"
            }
        ]);

        // now change data on several places and save them implicitly (no explicit .Save() on sub elements)
        // !! nothing except this values can be changed (also ids and versions are the same)
        users[0].DefaultGroup.Name = "changed group - implicit";
        users[0].Groups[0].Data[0].SubData[0].Name = "changed subdata - implicit";
        users[0].UserName = "changed username - implicit";
        await users[0].Save();

        subData = await this.context.getAll(MockSubData.getCollection());
        await this.removeProps(subData, ["created", "className", "lastModified", "deleted"]);
        assert.deepEqual(subData, [
            {_id: sub2.Id, name: "changed subdata - implicit", __v: 0},
            {_id: sub3.Id, name: "sub3", __v: 0},
            {_id: sub1.Id, name: "sub1", __v: 0}
        ]);

        data = await this.context.getAll(MockData.getCollection());
        await this.removeProps(data, ["created", "className", "lastModified", "deleted"]);
        assert.deepEqual(data, [
            {_id: md2.Id, subData: [sub2.Id, sub3.Id], __v: 0},
            {_id: md1.Id, subData: [sub1.Id, sub3.Id], __v: 0},
            {_id: md3.Id, subData: [sub1.Id], __v: 0}
        ]);

        groups = await this.context.getAll(MockGroup.getCollection());
        await this.removeProps(groups, ["created", "className", "lastModified", "deleted"]);
        assert.deepEqual(groups, [
            {
                __v              : 0,
                _id              : group1.Id,
                authorizedMethods: ["some1", "some2"],
                data             : [md2.Id, md1.Id],
                hidden           : false,
                name             : "Group1"
            },
            {
                __v              : 0,
                _id              : group2.Id,
                authorizedMethods: ["someA", "someB"],
                data             : [md1.Id, md3.Id],
                hidden           : false,
                name             : "changed group - implicit"
            }
        ]);

        usersA = await this.context.getAll(MockUser.getCollection());
        await this.removeProps(usersA, ["created", "className", "lastModified", "deleted"]);
        assert.deepEqual(usersA, [
            {
                __v         : 0,
                _id         : user1.Id,
                activated   : true,
                altEmails   : [],
                auditLog    : {_id: user1.AuditLog.Id},
                authTokens  : [],
                defaultGroup: group2.Id,
                email       : "this.is.test1@mail.oid",
                groups      : [group1.Id],
                password    : "pass1",
                ssoTokens   : {_id: user1.SSOTokens.Id},
                userName    : "changed username - implicit"
            },
            {
                __v         : 0,
                _id         : user2.Id,
                activated   : true,
                altEmails   : [],
                auditLog    : {_id: user2.AuditLog.Id},
                authTokens  : [],
                defaultGroup: group1.Id,
                email       : "this.is.test2@mail.oid",
                groups      : [group1.Id, group2.Id],
                password    : "pass2",
                ssoTokens   : {_id: user2.SSOTokens.Id},
                userName    : "name2"
            }
        ]);
    }

    // remove and re-insert group data without refresh
    @Test()
    public async ComplexReference_remove_reinsert_no_refresh() : Promise<void> {
        const sub1 : MockSubData = new MockSubData();
        sub1.Name = "sub1";
        // await sub1.Save();
        const sub2 : MockSubData = new MockSubData();
        sub2.Name = "sub2";
        // await sub2.Save();
        const sub3 : MockSubData = new MockSubData();
        sub3.Name = "sub3";
        // await sub3.Save();

        const md1 : MockData = new MockData();
        md1.SubData = [sub1, sub3];
        // await md1.Save();
        const md2 : MockData = new MockData();
        md2.SubData = [sub2, sub3];
        // await md2.Save();
        const md3 : MockData = new MockData();
        md3.SubData = [sub1];
        // await md3.Save();

        const group1 : MockGroup = new MockGroup();
        group1.Name = "Group1";
        group1.AuthorizedMethods = ["some1", "some2"];
        group1.Data = [md2, md1];
        // await group1.Save();

        const group2 : MockGroup = new MockGroup();
        group2.Name = "Group2";
        group2.AuthorizedMethods = ["someA", "someB"];
        group2.Data = [md1, md3];
        // await group2.Save();

        const user1 : MockUser = new MockUser();
        user1.UserName = "name1";
        user1.Password = "pass1";
        user1.Email = "this.is.test1@mail.oid";
        user1.Groups = [group1];
        user1.DefaultGroup = group2;

        const user2 : MockUser = new MockUser();
        user2.UserName = "name2";
        user2.Password = "pass2";
        user2.Email = "this.is.test2@mail.oid";
        user2.Groups = [group1, group2];
        user2.DefaultGroup = group1;

        // now save of user starting
        let users : MockUser[] = await this.context.getAll(MockUser.getCollection());
        assert.ok(ObjectValidator.IsEmptyOrNull(users));
        await user1.Save();
        await user2.Save();

        users = await MockUser.Find({userName: "name1"});

        assert.ok(!ObjectValidator.IsEmptyOrNull(users));
        assert.ok(ObjectValidator.IsArray(users));
        assert.equal(users.length, 1);

        let err : string = "";
        try {
            await users[0].Save();
        } catch (e) {
            err = e.message;
        }
        assert.equal(err, "");

        const usersData : any[] = (await Promise.all((
            await MockUser.Find({})).map(($user) => $user.ToJSON()))).map(($value : any) : any => {
            delete $value.DefaultGroup;
            delete $value.SSOTokens;
            delete $value.AuditLog;
            return $value;
        });
        await this.removeProps(usersData, ["Created", "ClassName", "LastModified", "Deleted"]);
        assert.ok(!ObjectValidator.IsEmptyOrNull(usersData));
        assert.ok(ObjectValidator.IsArray(usersData));
        assert.equal(usersData.length, 2);

        // assert.deepEqual(JsonUtils.Sort(usersData), ([
        assert.deepEqual(JSON.stringify(JsonUtils.Sort(usersData)), JSON.stringify([
            {
                Activated : true,
                AltEmails : [],
                AuthTokens: [],
                Email     : user1.Email,
                Groups    : [
                    {
                        AuthorizedMethods: ["some1", "some2"],
                        Data             : [
                            {
                                Id     : md2.Id,
                                SubData: [
                                    {Id: sub2.Id, Name: sub2.Name},
                                    {Id: sub3.Id, Name: sub3.Name}
                                ]
                            },
                            {
                                Id     : md1.Id,
                                SubData: [
                                    {Id: sub1.Id, Name: sub1.Name},
                                    {Id: sub3.Id, Name: sub3.Name}
                                ]
                            }
                        ],
                        Hidden           : false,
                        Id               : group1.Id,
                        Name             : group1.Name
                    }
                ],
                Id        : user1.Id,
                Password  : user1.Password,
                UserName  : user1.UserName
            },
            {
                Activated : true,
                AltEmails : [],
                AuthTokens: [],
                Email     : user2.Email,
                Groups    : [
                    {
                        AuthorizedMethods: ["some1", "some2"],
                        Data             : [
                            {
                                Id     : md2.Id,
                                SubData: [
                                    {Id: sub2.Id, Name: sub2.Name},
                                    {Id: sub3.Id, Name: sub3.Name}
                                ]
                            },
                            {
                                Id     : md1.Id,
                                SubData: [
                                    {Id: sub1.Id, Name: sub1.Name},
                                    {Id: sub3.Id, Name: sub3.Name}
                                ]
                            }
                        ],
                        Hidden           : false,
                        Id               : group1.Id,
                        Name             : group1.Name
                    },
                    {
                        AuthorizedMethods: ["someA", "someB"],
                        Data             : [
                            {
                                Id     : md1.Id,
                                SubData: [
                                    {Id: sub1.Id, Name: sub1.Name},
                                    {Id: sub3.Id, Name: sub3.Name}
                                ]
                            },
                            {
                                Id     : md3.Id,
                                SubData: [
                                    {Id: sub1.Id, Name: sub1.Name}
                                ]
                            }
                        ],
                        Hidden           : false,
                        Id               : group2.Id,
                        Name             : group2.Name
                    }
                ],
                Id        : user2.Id,
                Password  : user2.Password,
                UserName  : user2.UserName
            }
        ]));

        let user : any = await (await MockUser.FindById(user1.Id)).ToJSON();
        delete user.DefaultGroup;
        delete user.SSOTokens;
        delete user.AuditLog;
        await this.removeProps(user, ["Created", "ClassName", "LastModified", "Deleted"]);
        // assert.deepEqual(JsonUtils.Sort(user), ({
        assert.deepEqual(JSON.stringify(JsonUtils.Sort(user)), JSON.stringify({
            Activated : true,
            AltEmails : [],
            AuthTokens: [],
            Email     : user1.Email,
            Groups    : [
                {
                    AuthorizedMethods: ["some1", "some2"],
                    Data             : [
                        {
                            Id     : md2.Id,
                            SubData: [
                                {Id: sub2.Id, Name: sub2.Name},
                                {Id: sub3.Id, Name: sub3.Name}
                            ]
                        },
                        {
                            Id     : md1.Id,
                            SubData: [
                                {Id: sub1.Id, Name: sub1.Name},
                                {Id: sub3.Id, Name: sub3.Name}
                            ]
                        }
                    ],
                    Hidden           : false,
                    Id               : group1.Id,
                    Name             : group1.Name
                }
            ],
            Id        : user1.Id,
            Password  : user1.Password,
            UserName  : user1.UserName
        }));

        (user1.Groups[0]).Data = [md3];
        err = "";
        try {
            await user1.Save();
        } catch (e) {
            err = e.message;
        }
        assert.equal(err, "");

        user = await (await MockUser.FindById(user1.Id)).ToJSON();
        delete user.DefaultGroup;
        delete user.SSOTokens;
        delete user.AuditLog;
        await this.removeProps(user, ["Created", "ClassName", "LastModified", "Deleted"]);
        // assert.deepEqual(JsonUtils.Sort(user), ({
        assert.deepEqual(JSON.stringify(JsonUtils.Sort(user)), JSON.stringify({
            Activated : true,
            AltEmails : [],
            AuthTokens: [],
            Email     : user1.Email,
            Groups    : [
                {
                    AuthorizedMethods: ["some1", "some2"],
                    Data             : [
                        {
                            Id     : md3.Id,
                            SubData: [
                                {Id: sub1.Id, Name: sub1.Name}
                            ]
                        }
                    ],
                    Hidden           : false,
                    Id               : group1.Id,
                    Name             : group1.Name
                }
            ],
            Id        : user1.Id,
            Password  : user1.Password,
            UserName  : user1.UserName
        }));
    }

    // remove and re-insert group data with refresh
    @Test()
    public async ComplexReference_remove_reinsert() : Promise<void> {
        const sub1 : MockSubData = new MockSubData();
        sub1.Name = "sub1";
        const sub2 : MockSubData = new MockSubData();
        sub2.Name = "sub2";
        const sub3 : MockSubData = new MockSubData();
        sub3.Name = "sub3";

        const md1 : MockData = new MockData();
        md1.SubData = [sub1, sub3];
        const md2 : MockData = new MockData();
        md2.SubData = [sub2, sub3];
        const md3 : MockData = new MockData();
        md3.SubData = [sub1];

        const group1 : MockGroup = new MockGroup();
        group1.Name = "Group1";
        group1.AuthorizedMethods = ["some1", "some2"];
        group1.Data = [md2, md1];

        const group2 : MockGroup = new MockGroup();
        group2.Name = "Group2";
        group2.AuthorizedMethods = ["someA", "someB"];
        group2.Data = [md1, md3];

        const user1 : MockUser = new MockUser();
        user1.UserName = "name1";
        user1.Password = "pass1";
        user1.Email = "this.is.test1@mail.oid";
        user1.Groups = [group1];
        user1.DefaultGroup = group2;

        const user2 : MockUser = new MockUser();
        user2.UserName = "name2";
        user2.Password = "pass2";
        user2.Email = "this.is.test2@mail.oid";
        user2.Groups = [group1, group2];
        user2.DefaultGroup = group1;

        // now save of user starting
        let users : MockUser[] = await this.context.getAll(MockUser.getCollection());
        assert.ok(ObjectValidator.IsEmptyOrNull(users));
        await user1.Save();
        await user2.Save();

        users = await MockUser.Find({userName: "name1"});

        assert.ok(!ObjectValidator.IsEmptyOrNull(users));
        assert.ok(ObjectValidator.IsArray(users));
        assert.equal(users.length, 1);

        let err : string = "";
        try {
            await users[0].Save();
        } catch (e) {
            err = e.message;
        }
        assert.equal(err, "");

        const usersData : any[] = (await Promise.all((
            await MockUser.Find({})).map(($user) => $user.ToJSON()))).map(($value : any) : any => {
            delete $value.DefaultGroup;
            delete $value.SSOTokens;
            delete $value.AuditLog;
            return $value;
        });
        await this.removeProps(usersData, ["Created", "ClassName", "LastModified", "Deleted"]);
        assert.ok(!ObjectValidator.IsEmptyOrNull(usersData));
        assert.ok(ObjectValidator.IsArray(usersData));
        assert.equal(usersData.length, 2);
        // assert.deepEqual(JsonUtils.Sort(usersData), ([
        assert.deepEqual(JSON.stringify(JsonUtils.Sort(usersData)), JSON.stringify([
            {
                Activated : true,
                AltEmails : [],
                AuthTokens: [],
                Email     : user1.Email,
                Groups    : [
                    {
                        AuthorizedMethods: ["some1", "some2"],
                        Data             : [
                            {
                                Id     : md2.Id,
                                SubData: [
                                    {Id: sub2.Id, Name: sub2.Name},
                                    {Id: sub3.Id, Name: sub3.Name}
                                ]
                            },
                            {
                                Id     : md1.Id,
                                SubData: [
                                    {Id: sub1.Id, Name: sub1.Name},
                                    {Id: sub3.Id, Name: sub3.Name}
                                ]
                            }
                        ],
                        Hidden           : false,
                        Id               : group1.Id,
                        Name             : group1.Name
                    }
                ],
                Id        : user1.Id,
                Password  : user1.Password,
                UserName  : user1.UserName
            },
            {
                Activated : true,
                AltEmails : [],
                AuthTokens: [],
                Email     : user2.Email,
                Groups    : [
                    {
                        AuthorizedMethods: ["some1", "some2"],
                        Data             : [
                            {
                                Id     : md2.Id,
                                SubData: [
                                    {Id: sub2.Id, Name: sub2.Name},
                                    {Id: sub3.Id, Name: sub3.Name}
                                ]
                            },
                            {
                                Id     : md1.Id,
                                SubData: [
                                    {Id: sub1.Id, Name: sub1.Name},
                                    {Id: sub3.Id, Name: sub3.Name}
                                ]
                            }
                        ],
                        Hidden           : false,
                        Id               : group1.Id,
                        Name             : group1.Name
                    },
                    {
                        AuthorizedMethods: ["someA", "someB"],
                        Data             : [
                            {
                                Id     : md1.Id,
                                SubData: [
                                    {Id: sub1.Id, Name: sub1.Name},
                                    {Id: sub3.Id, Name: sub3.Name}
                                ]
                            },
                            {
                                Id     : md3.Id,
                                SubData: [
                                    {Id: sub1.Id, Name: sub1.Name}
                                ]
                            }
                        ],
                        Hidden           : false,
                        Id               : group2.Id,
                        Name             : group2.Name
                    }
                ],
                Id        : user2.Id,
                Password  : user2.Password,
                UserName  : user2.UserName
            }
        ]));

        let user : any = await (await MockUser.FindById(user1.Id)).ToJSON();
        delete user.DefaultGroup;
        delete user.SSOTokens;
        delete user.AuditLog;
        await this.removeProps(user, ["Created", "ClassName", "LastModified", "Deleted"]);
        // assert.deepEqual(JsonUtils.Sort(user), ({
        assert.deepEqual(JSON.stringify(JsonUtils.Sort(user)), JSON.stringify({
            Activated : true,
            AltEmails : [],
            AuthTokens: [],
            Email     : user1.Email,
            Groups    : [
                {
                    AuthorizedMethods: ["some1", "some2"],
                    Data             : [
                        {
                            Id     : md2.Id,
                            SubData: [
                                {Id: sub2.Id, Name: sub2.Name},
                                {Id: sub3.Id, Name: sub3.Name}
                            ]
                        },
                        {
                            Id     : md1.Id,
                            SubData: [
                                {Id: sub1.Id, Name: sub1.Name},
                                {Id: sub3.Id, Name: sub3.Name}
                            ]
                        }
                    ],
                    Hidden           : false,
                    Id               : group1.Id,
                    Name             : group1.Name
                }
            ],
            Id        : user1.Id,
            Password  : user1.Password,
            UserName  : user1.UserName
        }));

        await user1.Refresh();
        (user1.Groups[0]).Data = [md3];
        err = "";
        try {
            await user1.Save();
        } catch (e) {
            err = e.message;
        }
        assert.equal(err, "");

        user = await (await MockUser.FindById(user1.Id)).ToJSON();
        delete user.DefaultGroup;
        delete user.SSOTokens;
        delete user.AuditLog;
        await this.removeProps(user, ["Created", "ClassName", "LastModified", "Deleted"]);
        // assert.deepEqual(JsonUtils.Sort(user), ({
        assert.deepEqual(JSON.stringify(JsonUtils.Sort(user)), JSON.stringify({
            Activated : true,
            AltEmails : [],
            AuthTokens: [],
            Email     : user1.Email,
            Groups    : [
                {
                    AuthorizedMethods: ["some1", "some2"],
                    Data             : [
                        {
                            Id     : md3.Id,
                            SubData: [
                                {Id: sub1.Id, Name: sub1.Name}
                            ]
                        }
                    ],
                    Hidden           : false,
                    Id               : group1.Id,
                    Name             : group1.Name
                }
            ],
            Id        : user1.Id,
            Password  : user1.Password,
            UserName  : user1.UserName
        }));
    }

    private async removeProps($obj : any, $keys : string[]) : Promise<void> {
        if (Array.isArray($obj)) {
            for await(const item of $obj) {
                await this.removeProps(item, $keys);
            }
        } else if (typeof $obj === "object" && $obj != null) {
            for await(const key of Object.getOwnPropertyNames($obj)) {
                if ($keys.indexOf(key) !== -1) {
                    delete $obj[key];
                } else {
                    await this.removeProps($obj[key], $keys);
                }
            }
        }
    }
}

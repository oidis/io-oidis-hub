/*! ******************************************************************************************************** *
 *
 * Copyright 2020-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { MongoTestRunner } from "../../MongoTestRunner.js";

import { JsonUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/JsonUtils.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { Test } from "@io-oidis-commons/test/unit/Io/Oidis/Commons/BaseUnitTestRunner.js";
import { assert } from "@io-oidis-commons/test/unit/Io/Oidis/Commons/UnitTestEnvironment.js";
import { ArrayConverter } from "../../../../../../../../source/typescript/Io/Oidis/Hub/DAO/Converters/ArrayConverter.js";
import { AuthTokenToRefConverter } from "../../../../../../../../source/typescript/Io/Oidis/Hub/DAO/Converters/AuthTokenToRefConverter.js";
import { BaseConverter } from "../../../../../../../../source/typescript/Io/Oidis/Hub/DAO/Converters/BaseConverter.js";
import { BaseInclusionConverter } from "../../../../../../../../source/typescript/Io/Oidis/Hub/DAO/Converters/BaseInclusionConverter.js";
import { BaseToRefConverter } from "../../../../../../../../source/typescript/Io/Oidis/Hub/DAO/Converters/BaseToRefConverter.js";
import { AuthToken } from "../../../../../../../../source/typescript/Io/Oidis/Hub/DAO/Models/AuthToken.js";
import { BaseModel } from "../../../../../../../../source/typescript/Io/Oidis/Hub/DAO/Models/BaseModel.js";
import { IContext } from "../../../../../../../../source/typescript/Io/Oidis/Hub/Interfaces/DAO/IConverter.js";
import { Entity, Property } from "../../../../../../../../source/typescript/Io/Oidis/Hub/Primitives/Decorators.js";

export class MockChildListToRefConverter extends AuthTokenToRefConverter {
    public async from($value : any, $context : IContext) : Promise<any> {
        const items : AuthToken[] = [];
        for await (const item of $value) {
            const instance : any = await MockChild.FindById(item);
            items.push(instance);
        }
        return items;
    }
}

export class MockChildToRefConverter extends BaseToRefConverter {
    public async from($value : any, $context : IContext) : Promise<any> {
        return MockChild.FindById($value);
    }
}

export class MockChildListInclusionConverter extends BaseConverter {
    public async from($value : any, $context : IContext) : Promise<any> {
        const items : MockChild[] = [];
        if (!ObjectValidator.IsEmptyOrNull($value)) {
            for await (const item of $value.toObject()) {
                const instance : any = new MockChild();
                instance.document = Object.assign(instance.document, item);
                await instance.fromModel();

                items.push(instance);
            }
        }
        return items;
    }

    public async to($value : any, $context : IContext) : Promise<any> {
        const objects : any[] = [];
        for await (const item of $value) {
            await item.toModel();
            await item.document.validate();  // call validate manually to rise potential errors directly
            objects.push(item.document.toJSON());
        }
        return objects;
    }
}

export class MockChildInclusionConverter extends BaseInclusionConverter {
    protected getInclusionInstance() : MockChild {
        return new MockChild();
    }
}

@Entity("mockChild")
export class MockChild extends BaseModel<MockChild> {
    @Property()
    public declare PropertyNumber : number;

    @Property()
    public declare PropertyString : string;

    @Property()
    public declare PropertyBool : boolean;

    @Property()
    public declare PropertyDate : Date;

    protected getSchema() : any {
        return super.getSchema()
            .add({
                propertyBool  : {_id: false, required: true, type: Boolean},
                propertyDate  : {_id: false, required: true, type: Date},
                propertyNumber: {_id: false, required: true, type: Number},
                propertyString: {_id: false, required: true, type: String}
            });
    }
}

@Entity("mockParent")
export class MockParent extends BaseModel<MockParent> {
    @Property()
    public declare PropertyNumber : number;

    @Property({converter: new ArrayConverter()})
    public declare PropertyArray : string[];

    @Property({converter: new MockChildListToRefConverter()})
    public declare PropertyListRef : MockChild[];

    @Property({converter: new MockChildToRefConverter()})
    public declare PropertyRef : MockChild;

    @Property({converter: new MockChildListInclusionConverter()})
    public declare PropertyListInclusion : MockChild[];

    @Property({converter: new MockChildInclusionConverter()})
    public declare PropertyInclusion : MockChild;

    constructor() {
        super();
        this.PropertyListRef = [];
        this.PropertyListInclusion = [];
    }

    protected getSchema() : any {
        return super.getSchema()
            .add({
                propertyArray        : [
                    {_id: false, required: false, type: String}
                ],
                propertyInclusion    : (<any>new MockChild()).getSchema(),
                propertyListInclusion: [
                    (<any>new MockChild()).getSchema()
                ],
                propertyListRef      : [
                    {_id: false, type: String}
                ],
                propertyNumber       : {_id: false, required: true, type: Number},
                propertyRef          : {_id: false, type: String}
            });
    }
}

export class ModelInclusionTest extends MongoTestRunner {

    constructor() {
        super();

        // this.setMethodFilter("testReferenceAndInjection");
    }

    @Test()
    public async ReferenceAndInjection() : Promise<void> {
        const parent : MockParent = new MockParent();
        const id : string = parent.Id;
        const childIds : string[] = [];
        let singleChildId : string = "";
        parent.PropertyNumber = 123;
        parent.PropertyArray = ["array.item.0", "array.item.1", "array.item.2"];
        parent.PropertyRef = Object.assign(new MockChild(), {
            PropertyBool  : false,
            PropertyDate  : new Date(236),
            PropertyNumber: 879,
            PropertyString: "ref.single"
        });
        singleChildId = parent.PropertyRef.Id;
        parent.PropertyListRef.push(Object.assign(new MockChild(), {
            PropertyBool  : true,
            PropertyDate  : new Date(789),
            PropertyNumber: 242,
            PropertyString: "ref.item.0"
        }));
        parent.PropertyListRef.push(Object.assign(new MockChild(), {
            PropertyBool  : false,
            PropertyDate  : new Date(254),
            PropertyNumber: 636,
            PropertyString: "ref.item.1"
        }));
        parent.PropertyInclusion = Object.assign(new MockChild(), {
            PropertyBool  : true,
            PropertyDate  : new Date(854),
            PropertyNumber: 632,
            PropertyString: "included.single"
        });
        parent.PropertyListInclusion.push(Object.assign(new MockChild(), {
            PropertyBool  : false,
            PropertyDate  : new Date(345),
            PropertyNumber: 878,
            PropertyString: "included.item.0"
        }));
        parent.PropertyListInclusion.push(Object.assign(new MockChild(), {
            PropertyBool  : true,
            PropertyDate  : new Date(756),
            PropertyNumber: 563,
            PropertyString: "included.item.1"
        }));

        let parents : MockParent[] = await this.context.getAll(MockParent.getCollection());
        assert.ok(ObjectValidator.IsEmptyOrNull(parents));
        await parent.Save();

        parents = await this.context.getAll(MockChild.getCollection());
        assert.ok(!ObjectValidator.IsEmptyOrNull(parents));
        assert.ok(ObjectValidator.IsArray(parents));
        assert.equal(parents.length, 3);
        parents.forEach(($value : any) : any => {
            childIds.push($value._id);
            delete $value._id;
            delete $value.__v;
            delete $value.created;
            delete $value.className;
            delete $value.lastModified;
            return $value;
        });
        assert.deepEqual(parents, [
            {
                deleted       : false,
                propertyBool  : true,
                propertyDate  : new Date(789),
                propertyNumber: 242,
                propertyString: "ref.item.0"
            },
            {
                deleted       : false,
                propertyBool  : false,
                propertyDate  : new Date(254),
                propertyNumber: 636,
                propertyString: "ref.item.1"
            },
            {
                deleted       : false,
                propertyBool  : false,
                propertyDate  : new Date(236),
                propertyNumber: 879,
                propertyString: "ref.single"
            }
        ]);

        parents = await this.context.getAll(MockParent.getCollection());
        assert.ok(!ObjectValidator.IsEmptyOrNull(parents));
        assert.ok(ObjectValidator.IsArray(parents));
        assert.equal(parents.length, 1);
        delete (<any>parents[0]).__v;
        delete (<any>parents[0]).propertyInclusion._id;
        for (const item of (<any>parents[0]).propertyListInclusion) {
            delete item._id;
        }
        assert.deepEqual(JsonUtils.Sort(parents), JsonUtils.Sort([
            {
                _id                  : id,
                className            : parent.ClassName,
                created              : parent.Created,
                deleted              : parent.Deleted,
                lastModified         : parent.LastModified,
                propertyArray        : ["array.item.0", "array.item.1", "array.item.2"],
                propertyInclusion    : {
                    className     : parent.PropertyInclusion.ClassName,
                    created       : parent.PropertyInclusion.Created,
                    deleted       : parent.PropertyInclusion.Deleted,
                    lastModified  : parent.PropertyInclusion.LastModified,
                    propertyBool  : true,
                    propertyDate  : new Date(854),
                    propertyNumber: 632,
                    propertyString: "included.single"
                },
                propertyListInclusion: [
                    {
                        className     : parent.PropertyListInclusion[0].ClassName,
                        created       : parent.PropertyListInclusion[0].Created,
                        deleted       : parent.PropertyListInclusion[0].Deleted,
                        lastModified  : parent.PropertyListInclusion[0].LastModified,
                        propertyBool  : false,
                        propertyDate  : new Date(345),
                        propertyNumber: 878,
                        propertyString: "included.item.0"
                    },
                    {
                        className     : parent.PropertyListInclusion[1].ClassName,
                        created       : parent.PropertyListInclusion[1].Created,
                        deleted       : parent.PropertyListInclusion[1].Deleted,
                        lastModified  : parent.PropertyListInclusion[1].LastModified,
                        propertyBool  : true,
                        propertyDate  : new Date(756),
                        propertyNumber: 563,
                        propertyString: "included.item.1"
                    }
                ],
                propertyListRef      : [childIds[0], childIds[1]],
                propertyNumber       : 123,
                propertyRef          : singleChildId
            }
        ]));

        const mockParent : MockParent = await MockParent.FindById(id);
        assert.deepEqual(mockParent.PropertyNumber, parent.PropertyNumber);
        assert.deepEqual(mockParent.PropertyArray, parent.PropertyArray);
        assert.ok(!ObjectValidator.IsEmptyOrNull(mockParent.PropertyRef));
        assert.equal(mockParent.PropertyRef.PropertyBool, false);
        assert.equal(mockParent.PropertyRef.PropertyNumber, 879);
        assert.equal(mockParent.PropertyRef.PropertyString, "ref.single");
        assert.equal(mockParent.PropertyRef.PropertyDate.toISOString(), new Date(236).toISOString());
        assert.equal(mockParent.PropertyListRef.length, parent.PropertyListRef.length);
        for (let i : number = 0; i < mockParent.PropertyListRef.length; i++) {
            const act : MockChild = mockParent.PropertyListRef[i];
            const exp : MockChild = parent.PropertyListRef[i];
            assert.equal(act.PropertyBool, exp.PropertyBool);
            assert.equal(act.PropertyNumber, exp.PropertyNumber);
            assert.equal(act.PropertyString, exp.PropertyString);
            assert.equal(act.PropertyDate.toISOString(), exp.PropertyDate.toISOString());
        }

        assert.ok(!ObjectValidator.IsEmptyOrNull(mockParent.PropertyInclusion));
        assert.equal(mockParent.PropertyInclusion.PropertyBool, true);
        assert.equal(mockParent.PropertyInclusion.PropertyNumber, 632);
        assert.equal(mockParent.PropertyInclusion.PropertyString, "included.single");
        assert.equal(mockParent.PropertyInclusion.PropertyDate.toISOString(), new Date(854).toISOString());
        assert.equal(mockParent.PropertyListInclusion.length, parent.PropertyListInclusion.length);
        for (let i : number = 0; i < mockParent.PropertyListRef.length; i++) {
            const act : MockChild = mockParent.PropertyListInclusion[i];
            const exp : MockChild = parent.PropertyListInclusion[i];
            assert.equal(act.PropertyBool, exp.PropertyBool);
            assert.equal(act.PropertyNumber, exp.PropertyNumber);
            assert.equal(act.PropertyString, exp.PropertyString);
            assert.equal(act.PropertyDate.toISOString(), exp.PropertyDate.toISOString());
        }
    }

    @Test()
    public async InvalidValues() : Promise<void> {
        assert.ok(ObjectValidator.IsEmptyOrNull(await this.context.getAll(MockParent.getCollection())));

        let parent : MockParent = new MockParent();
        (<any>parent).PropertyNumber = "INVALID";
        let msg : string = "";
        try {
            await parent.Save();
        } catch (e) {
            msg = e.message;
        }
        assert.equal(msg, "IoOidisHubDAOModelsMockParent validation failed: propertyNumber: Cast to Number failed for " +
            "value \"INVALID\" (type string) at path \"propertyNumber\"");
        assert.ok(ObjectValidator.IsEmptyOrNull(await this.context.getAll(MockParent.getCollection())));

        parent = new MockParent();
        msg = "";
        parent.PropertyRef = Object.assign(new MockChild(), {
            PropertyBool  : "INVALID",
            PropertyDate  : new Date(236),
            PropertyNumber: 123,
            PropertyString: "ref.single"
        });
        try {
            await parent.Save();
        } catch (e) {
            msg = e.message;
        }
        assert.equal(msg, "IoOidisHubDAOModelsMockChild validation failed: propertyBool: Cast to Boolean failed for " +
            "value \"INVALID\" (type string) at path \"propertyBool\" because of \"CastError\"");
        assert.ok(ObjectValidator.IsEmptyOrNull(await this.context.getAll(MockParent.getCollection())));

        parent = new MockParent();
        msg = "";
        parent.PropertyListRef.push(Object.assign(new MockChild(), {
            PropertyBool  : true,
            PropertyDate  : new Date(789),
            PropertyNumber: "INVALID-IN-LIST",
            PropertyString: "ref.item.0"
        }));
        try {
            await parent.Save();
        } catch (e) {
            msg = e.message;
        }
        assert.equal(msg, "IoOidisHubDAOModelsMockChild validation failed: propertyNumber: Cast to Number failed for " +
            "value \"INVALID-IN-LIST\" (type string) at path \"propertyNumber\"");
        assert.ok(ObjectValidator.IsEmptyOrNull(await this.context.getAll(MockParent.getCollection())));

        parent = new MockParent();
        parent.PropertyNumber = 456;
        msg = "";
        parent.PropertyInclusion = Object.assign(new MockChild(), {
            PropertyBool  : "INVALID-INCLUDED",
            PropertyDate  : new Date(854),
            PropertyNumber: 632,
            PropertyString: "included.single"
        });
        try {
            await parent.Save();
        } catch (e) {
            msg = e.message;
        }
        assert.equal(msg, "IoOidisHubDAOModelsMockChild validation failed: propertyBool: Cast to Boolean failed for " +
            "value \"INVALID-INCLUDED\" (type string) at path \"propertyBool\" because of \"CastError\"");
        assert.ok(ObjectValidator.IsEmptyOrNull(await this.context.getAll(MockParent.getCollection())));

        parent = new MockParent();
        parent.PropertyNumber = 456;
        msg = "";
        parent.PropertyListInclusion.push(Object.assign(new MockChild(), {
            PropertyBool  : false,
            PropertyDate  : new Date(345),
            PropertyNumber: "INVALID-LIST-INCLUSION",
            PropertyString: "included.item.0"
        }));
        try {
            await parent.Save();
        } catch (e) {
            msg = e.message;
        }
        assert.equal(msg, "IoOidisHubDAOModelsMockChild validation failed: propertyNumber: Cast to Number failed for " +
            "value \"INVALID-LIST-INCLUSION\" (type string) at path \"propertyNumber\"");
        assert.ok(ObjectValidator.IsEmptyOrNull(await this.context.getAll(MockParent.getCollection())));
    }
}

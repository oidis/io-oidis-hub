/*! ******************************************************************************************************** *
 *
 * Copyright 2023-2024 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { MongoTestRunner } from "../MongoTestRunner.js";

import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { Test } from "@io-oidis-commons/test/unit/Io/Oidis/Commons/BaseUnitTestRunner.js";
import { assert } from "@io-oidis-commons/test/unit/Io/Oidis/Commons/UnitTestEnvironment.js";
import { ArrayInclusion } from "../../../../../../../source/typescript/Io/Oidis/Hub/DAO/Converters/ArrayInclusion.js";
import { InclusionConverter } from "../../../../../../../source/typescript/Io/Oidis/Hub/DAO/Converters/InclusionConverter.js";
import { BaseModel } from "../../../../../../../source/typescript/Io/Oidis/Hub/DAO/Models/BaseModel.js";
import { MongoContext } from "../../../../../../../source/typescript/Io/Oidis/Hub/DAO/Mongoose/MongoContext.js";
import { Entity, Property } from "../../../../../../../source/typescript/Io/Oidis/Hub/Primitives/Decorators.js";

export class MockBaseModel<T> extends BaseModel<T> {
    public get Schema() {
        return this.getSchema();
    }
}

@Entity("dataExp")
export class MockDataExp extends MockBaseModel<MockDataExp> {
    @Property()
    public declare PropBool : boolean;

    @Property()
    public declare PropBuffer : Buffer;

    @Property()
    public declare PropDate : Date;

    @Property()
    public declare PropNumb : number;

    @Property()
    public declare PropStr : string;

    constructor() {
        super();
        this.PropStr = "hello";
        this.PropNumb = 11;
        this.PropDate = new Date("October 8, 2023 00:01:02");
        this.PropBool = true;
        this.PropBuffer = Buffer.from("hello Oidis");
    }

    protected getSchema() : any {
        return super.getSchema()
            .add({
                propBool  : {required: true, _id: false, type: Boolean},
                propBuffer: {required: true, _id: false, type: Buffer},
                propDate  : {required: true, _id: false, type: Date},
                propNumb  : {required: true, _id: false, type: Number},
                propStr   : {required: true, _id: false, type: String}
            });
    }
}

@Entity("dataDec")
export class MockDataDec extends MockBaseModel<MockDataDec> {
    @Property({type: Boolean, default: true})
    public declare PropBool : boolean;

    @Property({type: Buffer, default: Buffer.from("hello Oidis")})
    public declare PropBuffer : Buffer;

    @Property({type: Date, default: new Date("October 8, 2023 00:01:02")})
    public declare PropDate : Date;

    @Property({type: Number, default: 11})
    public declare PropNumb : number;

    @Property({type: String, default: "hello"})
    public declare PropStr : string;
}

@Entity("dataMixed")
export class MockDataMixed extends MockBaseModel<MockDataDec> {
    @Property({type: String, default: "good bye", required: false})
    public declare PropStr : string;

    @Property({type: Number, default: 23})
    public declare PropNumb : number;

    @Property({type: Date, default: new Date("October 9, 2023 00:01:02")})
    public declare PropDate : Date;

    @Property()
    public declare PropBool : boolean;

    @Property()
    public declare PropBuffer : Buffer;

    constructor() {
        super();
        this.PropBool = true;
        this.PropBuffer = Buffer.from("hello Oidis");
    }

    protected getSchema() : any {
        return super.getSchema().add({
            propBool  : {required: false, _id: false, type: Boolean},
            propBuffer: {required: true, _id: false, type: Buffer},
            propStr   : {required: false, _id: false, type: String}
        });
    }
}

@Entity("dataDerivedExp")
export class MockDataDerivedExp extends MockDataExp {
    @Property()
    public declare PropDerivedString : string;

    protected getSchema() : any {
        return super.getSchema()
            .add({
                propDerivedString: {required: true, _id: false, type: String}
            });
    }
}

@Entity("dataDerivedDec")
export class MockDataDerivedDec extends MockDataDec {
    @Property({type: String})
    public declare PropDerivedString : string;
}

@Entity("dataArrayExp")
export class MockDataArrayExp extends MockBaseModel<MockDataArrayExp> {

    @Property()
    public declare PropBoolArray : boolean[];

    @Property()
    public declare PropBufferArray : Buffer[];

    @Property()
    public declare PropDateArray : Date[];

    @Property()
    public declare PropNumbArray : number[];

    @Property()
    public declare PropStrArray : string[];

    constructor() {
        super();
        this.PropStrArray = ["hello"];
        this.PropNumbArray = [11];
        this.PropDateArray = [new Date("October 8, 2023 00:01:02")];
        this.PropBoolArray = [true];
        this.PropBufferArray = [Buffer.from("hello Oidis")];
    }

    protected getSchema() : any {
        return super.getSchema()
            .add({
                propBoolArray  : [{_id: false, required: true, type: Boolean}],
                propBufferArray: [{_id: false, required: true, type: Buffer}],
                propDateArray  : [{_id: false, required: true, type: Date}],
                propNumbArray  : [{_id: false, required: true, type: Number}],
                propStrArray   : [{_id: false, required: true, type: String}]
            });
    }
}

@Entity("dataArrayDec")
export class MockDataArrayDec extends MockBaseModel<MockDataArrayDec> {

    @Property({type: [Boolean], default: [true]})
    public declare PropBoolArray : boolean[];

    @Property({type: [Buffer], default: [Buffer.from("hello Oidis")]})
    public declare PropBufferArray : Buffer[];

    @Property({type: [Date], default: [new Date("October 8, 2023 00:01:02")]})
    public declare PropDateArray : Date[];

    @Property({type: [Number], default: [11]})
    public declare PropNumbArray : number[];

    @Property({type: [String], default: ["hello"]})
    public declare PropStrArray : string[];
}

@Entity("dataArrayMixed")
export class MockDataArrayMixed extends MockBaseModel<MockDataArrayMixed> {
    @Property({type: [String], default: ["hello", "world"]})
    public declare PropStrArray : string[];

    @Property({type: [Number], default: [16]})
    public declare PropNumbArray : number[];

    @Property({type: [Date], default: [new Date("October 10, 2023 00:01:02")]})
    public declare PropDateArray : Date[];

    @Property()
    public declare PropBoolArray : boolean[];

    @Property()
    public declare PropBufferArray : Buffer[];

    constructor() {
        super();
        this.PropBoolArray = [true, false];
        this.PropBufferArray = [Buffer.from("hello Oidis")];
    }

    protected getSchema() : any {
        return super.getSchema()
            .add({
                propBoolArray  : [{_id: false, required: false, type: Boolean}],
                propBufferArray: [{_id: false, required: true, type: Buffer}],
                propStrArray   : [{_id: false, required: false, type: String}]
            });
    }
}

@Entity("dataReferenceExp")
export class MockDataReferenceExp extends MockBaseModel<MockDataReferenceExp> {
    @Property()
    public declare PropRef : MockDataExp;

    @Property()
    public declare PropRefArray : MockDataExp[];

    constructor() {
        super();
        this.PropRefArray = [];
    }

    protected getSchema() : any {
        return super.getSchema()
            .add({
                propRef     : {required: true, _id: false, type: String},
                propRefArray: [{_id: false, required: false, type: String}]
            });
    }
}

@Entity("dataReferenceDec")
export class MockDataReferenceDec extends MockBaseModel<MockDataReferenceDec> {
    @Property({type: MockDataDec, default: null})
    public declare PropRef : MockDataDec;

    @Property({type: [MockDataDec], required: false, default: []})
    public declare PropRefArray : MockDataDec[];
}

@Entity("dataInclusionExp")
export class MockDataInclusionExp extends MockBaseModel<MockDataInclusionExp> {
    @Property({converter: new InclusionConverter(MockDataExp)})
    public declare PropInc : MockDataExp;

    @Property({converter: new ArrayInclusion(MockDataExp)})
    public declare PropIncArray : MockDataExp[];

    constructor() {
        super();
        this.PropIncArray = [];
    }

    protected getSchema() : any {
        return super.getSchema()
            .add({
                propInc     : (<any>new MockDataExp()).getSchema(),
                propIncArray: [(<any>new MockDataExp()).getSchema()]
            });
    }
}

@Entity("dataInclusionDec")
export class MockDataInclusionDec extends MockBaseModel<MockDataInclusionDec> {
    @Property({type: MockDataDec, default: null, converter: new InclusionConverter(MockDataDec)})
    public declare PropInc : MockDataDec;

    @Property({type: [MockDataDec], required: false, converter: new ArrayInclusion(MockDataDec), default: []})
    public declare PropIncArray : MockDataDec[];
}

export class DbSchemaTest extends MongoTestRunner {

    constructor() {
        super();

        // this.setMethodFilter("testSchemaInclusion");
    }

    @Test()
    public async Schema() : Promise<void> {
        const modelDec : MockDataDec = new MockDataDec();
        const modelExp : MockDataExp = new MockDataExp();

        const schemaDec : any = modelDec.Schema.paths;
        const schemaExp : any = modelExp.Schema.paths;

        assert.equal(JSON.stringify(Object.keys(schemaDec).sort()), JSON.stringify(Object.keys(schemaExp).sort()));

        for (const itemKey of Object.keys(schemaExp)) {
            if (itemKey !== "__v" && itemKey !== "className") {
                const expected = schemaExp[itemKey];
                const actual = schemaDec[itemKey];
                assert.equal(JSON.stringify(actual), JSON.stringify(expected), "Schema for '" + itemKey + "' not match");
            }
        }

        modelDec.Id = modelExp.Id = "normalised";
        assert.equal(modelDec.Created.getUTCDate(), modelExp.Created.getUTCDate());
        assert.equal(modelDec.LastModified.getUTCDate(), modelExp.LastModified.getUTCDate());
        modelDec.Created = modelExp.Created;  // normalize ms if at least seconds are precise
        modelDec.LastModified = modelExp.LastModified;
        modelDec.ClassName = modelExp.ClassName;
        assert.equal(JSON.stringify(await modelDec.ToJSON()), JSON.stringify(await modelExp.ToJSON()));
    }

    @Test()
    public async SchemaMerge() : Promise<void> {
        const model : MockDataMixed = new MockDataMixed();
        const schemaActual : any = model.Schema.paths;
        const schemaExpected : any = (new MongoContext(this.mongo)).CreateDocument("expected_schema_merge",
            new (this.mongo.getDb()).Schema({
                _id         : {required: true, alias: "id", default: require("uuid").v1, type: String},
                className   : {required: true, _id: false, type: String},
                created     : {required: true, _id: false, type: Date},
                deleted     : {required: false, _id: false, type: Boolean},
                lastModified: {required: true, _id: false, type: Date},
                propBool    : {required: false, _id: false, type: Boolean},
                propBuffer  : {required: true, _id: false, type: Buffer},
                propDate    : {required: true, _id: false, type: Date},
                propNumb    : {required: true, _id: false, type: Number},
                propStr     : {required: false, _id: false, type: String}
            }), "expected_schema_merge").schema.paths;
        delete schemaExpected.__v;

        assert.equal(JSON.stringify(Object.keys(schemaActual).sort()), JSON.stringify(Object.keys(schemaExpected).sort()));

        for (const itemKey of Object.keys(schemaExpected)) {
            if (itemKey !== "__v" && itemKey !== "className") {
                const expected = schemaExpected[itemKey];
                const actual = schemaActual[itemKey];
                assert.equal(JSON.stringify(actual), JSON.stringify(expected), "Schema for '" + itemKey + "' not match");
            }
        }
    }

    @Test()
    public async SchemaDerived() : Promise<void> {
        const modelDec : MockDataDerivedDec = new MockDataDerivedDec();
        const modelExp : MockDataDerivedExp = new MockDataDerivedExp();

        const schemaDec : any = modelDec.Schema.paths;
        const schemaExp : any = modelExp.Schema.paths;

        assert.equal(JSON.stringify(Object.keys(schemaDec).sort()), JSON.stringify(Object.keys(schemaExp).sort()));

        for (const itemKey of Object.keys(schemaExp)) {
            if (itemKey !== "__v" && itemKey !== "className") {
                const expected = schemaExp[itemKey];
                const actual = schemaDec[itemKey];
                assert.equal(JSON.stringify(actual), JSON.stringify(expected), "Schema for '" + itemKey + "' not match");
            }
        }

        modelDec.Id = modelExp.Id = "normalised";
        assert.equal(modelDec.Created.getUTCDate(), modelExp.Created.getUTCDate());
        assert.equal(modelDec.LastModified.getUTCDate(), modelExp.LastModified.getUTCDate());
        modelDec.Created = modelExp.Created;  // normalize ms if at least seconds are precise
        modelDec.LastModified = modelExp.LastModified;
        modelDec.ClassName = modelExp.ClassName;
        assert.equal(JSON.stringify(await modelDec.ToJSON()), JSON.stringify(await modelExp.ToJSON()));
    }

    @Test()
    public async SchemaArray() : Promise<void> {
        const modelDec : MockDataArrayDec = new MockDataArrayDec();
        const modelExp : MockDataArrayExp = new MockDataArrayExp();

        const schemaDec : any = modelDec.Schema.paths;
        const schemaExp : any = modelExp.Schema.paths;

        assert.equal(JSON.stringify(Object.keys(schemaDec).sort()), JSON.stringify(Object.keys(schemaExp).sort()));

        for (const itemKey of Object.keys(schemaExp)) {
            if (itemKey !== "__v" && itemKey !== "className") {
                const expected = schemaExp[itemKey];
                const actual = schemaDec[itemKey];
                assert.equal(JSON.stringify(actual), JSON.stringify(expected), "Schema for '" + itemKey + "' not match");
            }
        }

        modelDec.Id = modelExp.Id = "normalised";
        assert.equal(modelDec.Created.getUTCDate(), modelExp.Created.getUTCDate());
        assert.equal(modelDec.LastModified.getUTCDate(), modelExp.LastModified.getUTCDate());
        modelDec.Created = modelExp.Created;  // normalize ms if at least seconds are precise
        modelDec.LastModified = modelExp.LastModified;
        modelDec.ClassName = modelExp.ClassName;
        assert.equal(JSON.stringify(await modelDec.ToJSON()), JSON.stringify(await modelExp.ToJSON()));
    }

    @Test()
    public async SchemaArrayMerge() : Promise<void> {
        const model : MockDataArrayMixed = new MockDataArrayMixed();
        const schemaActual : any = model.Schema.paths;
        const schemaExpected : any = (new MongoContext(this.mongo)).CreateDocument("expected_schema_array_merge",
            new (this.mongo.getDb()).Schema({
                _id            : {required: true, alias: "id", default: require("uuid").v1, type: String},
                className      : {required: true, _id: false, type: String},
                created        : {required: true, _id: false, type: Date},
                deleted        : {required: false, _id: false, type: Boolean},
                lastModified   : {required: true, _id: false, type: Date},
                propBoolArray  : [{_id: false, required: false, type: Boolean}],
                propBufferArray: [{_id: false, required: true, type: Buffer}],
                propDateArray  : [{_id: false, required: true, type: Date}],
                propNumbArray  : [{_id: false, required: true, type: Number}],
                propStrArray   : [{_id: false, required: false, type: String}]
            }), "expected_schema_array_merge").schema.paths;
        delete schemaExpected.__v;

        assert.equal(JSON.stringify(Object.keys(schemaActual).sort()), JSON.stringify(Object.keys(schemaExpected).sort()));

        for (const itemKey of Object.keys(schemaExpected)) {
            if (itemKey !== "__v" && itemKey !== "className") {
                const expected = schemaExpected[itemKey];
                if (ObjectValidator.IsSet(expected.schemaOptions)) {
                    // TODO: this allows to create plurals of collection names - should be ok to not validate?
                    delete expected.schemaOptions.pluralization;
                }

                const actual = schemaActual[itemKey];
                assert.equal(JSON.stringify(actual), JSON.stringify(expected), "Schema for '" + itemKey + "' not match");
            }
        }
    }

    @Test()
    public async SchemaReferenced() : Promise<void> {
        const modelDec : MockDataReferenceDec = new MockDataReferenceDec();
        const modelExp : MockDataReferenceExp = new MockDataReferenceExp();

        const schemaDec : any = modelDec.Schema.paths;
        const schemaExp : any = modelExp.Schema.paths;

        assert.equal(JSON.stringify(Object.keys(schemaDec).sort()), JSON.stringify(Object.keys(schemaExp).sort()));

        for (const itemKey of Object.keys(schemaExp)) {
            if (itemKey !== "__v" && itemKey !== "className") {
                const expected = schemaExp[itemKey];
                const actual = schemaDec[itemKey];
                assert.equal(JSON.stringify(actual), JSON.stringify(expected), "Schema for '" + itemKey + "' not match");
            }
        }

        modelDec.Id = modelExp.Id = "normalised";
        assert.equal(modelDec.Created.getUTCDate(), modelExp.Created.getUTCDate());
        assert.equal(modelDec.LastModified.getUTCDate(), modelExp.LastModified.getUTCDate());
        modelDec.Created = modelExp.Created;  // normalize ms if at least seconds are precise
        modelDec.LastModified = modelExp.LastModified;
        modelDec.ClassName = modelExp.ClassName;
        assert.equal(JSON.stringify(await modelDec.ToJSON()), JSON.stringify(await modelExp.ToJSON()));
    }

    @Test()
    public async SchemaInclusion() : Promise<void> {
        const modelDec : MockDataInclusionDec = new MockDataInclusionDec();
        const modelExp : MockDataInclusionExp = new MockDataInclusionExp();

        const schemaDec : any = modelDec.Schema.paths;
        const schemaExp : any = modelExp.Schema.paths;

        assert.equal(JSON.stringify(Object.keys(schemaDec).sort()), JSON.stringify(Object.keys(schemaExp).sort()));

        for await (const itemKey of Object.keys(schemaExp)) {
            if (itemKey !== "__v" && itemKey !== "className") {
                // is it enough to control only schema for included model (validated on DocumentArrayPath)
                let expected = schemaExp[itemKey];
                let actual = schemaDec[itemKey];

                if (expected.hasOwnProperty("instance") && (expected.instance === "Embedded" || expected.instance === "Array")) {
                    actual = actual.schema.paths;
                    expected = expected.schema.paths;
                    await this.removeProps(actual, ["$parentSchemaDocArray"]);
                    await this.removeProps(expected, ["$parentSchemaDocArray"]);
                }

                assert.equal(JSON.stringify(actual), JSON.stringify(expected), "Schema for '" + itemKey + "' not match");
            }
        }

        modelDec.Id = modelExp.Id = "normalised";
        assert.equal(modelDec.Created.getUTCDate(), modelExp.Created.getUTCDate());
        assert.equal(modelDec.LastModified.getUTCDate(), modelExp.LastModified.getUTCDate());
        modelDec.Created = modelExp.Created;  // normalize ms if at least seconds are precise
        modelDec.LastModified = modelExp.LastModified;
        modelDec.ClassName = modelExp.ClassName;
        assert.equal(JSON.stringify(await modelDec.ToJSON()), JSON.stringify(await modelExp.ToJSON()));
    }

    private async removeProps($obj : any, $keys : string[]) : Promise<void> {
        if (Array.isArray($obj)) {
            for await (const item of $obj) {
                await this.removeProps(item, $keys);
            }
        } else if (typeof $obj === "object" && $obj != null) {
            for await (const key of Object.getOwnPropertyNames($obj)) {
                if ($keys.indexOf(key) !== -1) {
                    delete $obj[key];
                } else {
                    await this.removeProps($obj[key], $keys);
                }
            }
        }
    }
}

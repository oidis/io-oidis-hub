/*! ******************************************************************************************************** *
 *
 * Copyright 2019-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { MongoTestRunner } from "../MongoTestRunner.js";

import { JsonUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/JsonUtils.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { Test } from "@io-oidis-commons/test/unit/Io/Oidis/Commons/BaseUnitTestRunner.js";
import { assert } from "@io-oidis-commons/test/unit/Io/Oidis/Commons/UnitTestEnvironment.js";
import { AuthToken } from "../../../../../../../source/typescript/Io/Oidis/Hub/DAO/Models/AuthToken.js";
import { User } from "../../../../../../../source/typescript/Io/Oidis/Hub/DAO/Models/User.js";
import { IExecutionResult } from "../../../../../../../source/typescript/Io/Oidis/Hub/Interfaces/DAO/GraphQL/IExecutionResult.js";

export class UserSchemaTest extends MongoTestRunner {

    @Test(true)
    public async GetAllUsers_CollectionNotExist() : Promise<void> {
        await this.assertGraphQL({
            expectedResult: {userIds: null},
            input         : "query GetUsers { userIds: userById(_id: \"\") { _id } }"
        });
    }

    @Test(true)
    public GetUsersByIds_AllFields() : Promise<void> {
        return this.getAllUsers("TestCase/GetUser_byIds.json");
    }

    @Test(true)
    public GetAllUsers_AllFields() : Promise<void> {
        return this.getAllUsers("TestCase/GetAllUsers_AllFields.json");
    }

    @Test()
    public GetAllUsers_OnlyIds() : Promise<void> {
        return this.getAllUsers("TestCase/GetAllUsers_OnlyIds.json");
    }

    @Test(true)
    public GetUser_AllFields() : Promise<void> {
        return this.getAllUsers("TestCase/GetUser_AllFields.json");
    }

    @Test(true)
    public GetUser_NotFound() : Promise<void> {
        return this.getAllUsers("TestCase/GetUser_NotFound.json");
    }

    @Test(true)
    public async GetUser_CollectionNotExist() : Promise<void> {
        await this.assertGraphQL({
            expectedResult: {user: null},
            input         : "query GetOneUser { user(_id: \"6dccdc40-7cc3-11e9-b5c7-051afe35e28c\") { _id } }"
        });
    }

    @Test(true)
    public CreateUser_AllFields() : Promise<void> {
        return this.createUser("TestCase/CreateUser_AllFields.json");
    }

    @Test(true)
    public CreateUser_OnlyId() : Promise<void> {
        return this.createUser("TestCase/CreateUser_OnlyId.json");
    }

    @Test(true)
    public UpdateUser_AllFields() : Promise<void> {
        return this.updateUser("TestCase/UpdateUser_AllFields.json");
    }

    @Test(true)
    public async UpdateUser_NotFound() : Promise<void> {
        await this.assertGraphQL(JSON.parse(this.readData("TestCase/UpdateUser_NotFound.json")));
    }

    private async updateUser($testCaseRelativeFilePath : string) : Promise<void> {
        const expectedUpdatedUser : any = {
            _id     : "*",
            userName: "*",
            password: "*",
            // todo : string utils could handle escaped quotes to work with numbers as well
            __v: 1
        };
        const users : any[] = JSON.parse(this.readData("InitScripts/UserCollectionInit.json"));
        const testCase : ITestCase = JSON.parse(this.readData($testCaseRelativeFilePath));

        await this.context.Insert(User.getCollection(), users);
        const result : IExecutionResult = await this.assertGraphQL(testCase);

        const updatedUser : any = await this.context.getById(User.getCollection(), result.data?.updatedUser.record.id);
        this.filterKeys(updatedUser, expectedUpdatedUser);

        assert.patternEqual(JSON.stringify(JsonUtils.Sort(updatedUser)), JSON.stringify(JsonUtils.Sort(expectedUpdatedUser)));
    }

    private async createUser($testCaseRelativeFilePath : string) : Promise<void> {
        const testCaseData : string = this.readData($testCaseRelativeFilePath);
        const testCase : ITestCase = JSON.parse(testCaseData);
        const initCategoryScript : string = this.readData("InitScripts/AuthTokenCollectionInit.json");
        let tokens : AuthToken[] = JSON.parse(initCategoryScript);

        for await (const item of tokens) {
            await this.saveTokens([item]);
        }

        const result : IExecutionResult[] = await this.gqlContext.ExecuteAll(testCase.input);
        const resultLast : IExecutionResult = result[result.length - 1];
        assert.patternEqual(JSON.stringify(resultLast.data), JSON.stringify(testCase.expectedResult),
            JSON.stringify(resultLast.errors));

        const newUser : any = await this.context.getById(User.getCollection(), resultLast.data?.createdUser.record._id);
        delete newUser.__v;
        tokens = [];
        for await (const item of newUser.authTokens) {
            const token : any = await this.context.getById(AuthToken.getCollection(), item);
            delete token._id;
            tokens.unshift(JsonUtils.Sort(token));
        }
        if (!ObjectValidator.IsEmptyOrNull(testCase.expectedResult.createdUser.record.authTokens)) {
            assert.deepEqual(tokens, testCase.expectedResult.createdUser.record.authTokens);
            newUser.authTokens = [];
            testCase.expectedResult.createdUser.record.authTokens = [];
        }
        this.filterKeys(newUser, testCase.expectedResult.createdUser.record);

        assert.patternEqual(
            JSON.stringify(JsonUtils.Sort(newUser)),
            JSON.stringify(JsonUtils.Sort(testCase.expectedResult.createdUser.record)));
    }

    private async saveTokens($data : any[]) : Promise<string[]> {
        return this.context.Insert(AuthToken.getCollection(), $data);
    }

    private async getAllUsers($testCaseRelativeFilePath : string) : Promise<void> {
        const initCategoryScript : string = this.readData("InitScripts/UserCollectionInit.json");
        const users : any[] = JSON.parse(initCategoryScript);
        const testCaseData : string = this.readData($testCaseRelativeFilePath);
        const testCase : ITestCase = JSON.parse(testCaseData);

        for await (const item of users) {
            item.authTokens = await this.saveTokens(item.authTokens);
        }
        await this.context.Insert(User.getCollection(), users);
        await this.assertGraphQL(testCase);
    }
}

interface ITestCase {
    input : string | string[];
    expectedResult : any;
}

/*! ******************************************************************************************************** *
 *
 * Copyright 2024 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { MongoTestRunner } from "../MongoTestRunner.js";

import { Property } from "@io-oidis-commons/Io/Oidis/Commons/Primitives/Decorators.js";
import { Test } from "@io-oidis-commons/test/unit/Io/Oidis/Commons/BaseUnitTestRunner.js";
import { User } from "../../../../../../../source/typescript/Io/Oidis/Hub/DAO/Models/User.js";
import { Loader } from "../../../../../../../source/typescript/Io/Oidis/Hub/Loader.js";
import { Model, Mutation, Query } from "../../../../../../../source/typescript/Io/Oidis/Hub/Primitives/Decorators.js";

@Model("TestInnerObject")
export class TestInnerObject {
    @Property({type: String})
    public declare PropInStr : string;
}

@Model("TestObject")
export class TestObject {
    @Property({type: Number})
    public declare PropNumber : number;

    @Property({type: String})
    public declare PropString : string;

    @Property({type: Boolean})
    public declare PropBoolean : boolean;

    @Property({type: TestInnerObject})
    public declare PropInnerObj : TestInnerObject;
}

export class GQLSchemaTest extends MongoTestRunner {

    constructor() {
        super();
        // this.setMethodFilter("query_getUsers");
    }

    @Test()
    public async getHelpQueries() : Promise<void> {
        await this.assertGraphQL({
            expectedResult: {
                __schema: {
                    queryType: {
                        fields: [
                            {name: "getUsers", description: "ddd"},
                            {name: "getUserById", description: null},
                            {name: "getString", description: null},
                            {name: "IoOidisHubGuiModelsPrintAll", description: null},
                            {name: "authCodeAll", description: null},
                            {name: "testModelAll", description: null},
                            {name: "blobsAll", description: null},
                            {name: "filesAll", description: null},
                            {name: "configsAll", description: null},
                            {name: "groupAll", description: null},
                            {name: "organizationAll", description: null},
                            {name: "authTokenAll", description: null},
                            {name: "ssoTokensAll", description: null},
                            {name: "userAuditLogAll", description: null},
                            {name: "userAll", description: null},
                            {name: "buildPackagesAll", description: null},
                            {name: "logEntryAll", description: null},
                            {name: "webhookRecordAll", description: null},
                            {name: "authorizedMethodsAll", description: null},
                            {name: "authEntryAll", description: null},
                            {name: "sendmailAll", description: null},
                            {name: "appSettingsAll", description: null},
                            {name: "emailRecordAll", description: null},
                            {name: "tokenPayloadAll", description: null}
                        ]
                    }
                }
            },
            input         : "query {__schema{queryType{fields{name description}}}}"
        });
    }

    @Test()
    public async getHelpMutations() : Promise<void> {
        await this.assertGraphQL({
            expectedResult: {
                __schema: {
                    mutationType: {
                        fields: [
                            {name: "createUser", description: null},
                            {name: "createObj", description: null}
                        ]
                    }
                }
            },
            input         : "query {__schema{mutationType{fields{name description}}}}"
        });
    }

    @Test()
    public async query_getUsers() : Promise<void> {
        await this.assertGraphQL({
            expectedResult: {getUsers: [{Id: "WHO CARES"}]},
            input         : "query symbolicName{getUsers(user:{Deleted:false}){Id}}"
        });
    }

    @Test()
    public async query_getUserById() : Promise<void> {
        await this.assertGraphQL({
            expectedResult: {getUserById: {Id: "WHO CARES: nobody know"}},
            input         : `query symbolicName{getUserById(id:"nobody know"){Id}}`
        });
    }

    @Test()
    public async mutation_createUser() : Promise<void> {
        await this.assertGraphQL({
            expectedResult: {createUser: {Id: "who cares", Firstname: "Timmy", Lastname: "Shu", UserName: "wtf"}},
            input         : `mutation {createUser(user:{Firstname:"Timmy",Lastname:"Shu",UserName:"wtf"}){Id Firstname Lastname UserName}}`
        });
    }

    protected async before() : Promise<void> {
        await super.before();
        (<any>Loader.getInstance().getAuthManager()).defaultAccessibleMethods.push(this.getClassName() + ".*");
    }

    @Query({returnType: [User], args: [{name: "user", type: User}], description: "ddd"})
    private async getUsers($user : User) : Promise<User[]> {
        const user : User = new User();
        user.Firstname = "MOCKED";
        user.Id = "WHO CARES";
        return [user];
    }

    @Query({returnType: User, args: [{name: "id", type: String}]})
    private async getUserById($id : string) : Promise<User> {
        const user : User = new User();
        user.Firstname = "MOCKED";
        user.Id = "WHO CARES: " + $id;
        return user;
    }

    @Query({returnType: String, args: [{name: "arg1", type: Number}, {name: "arg2", type: Number}]})
    private async getString($arg1 : number, $arg2 : number) : Promise<string> {
        return $arg1 + "+" + $arg2 + "=" + ($arg1 + $arg2);
    }

    @Mutation({returnType: User, args: [{name: "user", type: User}]})
    private async createUser($user : User) : Promise<User> {
        const u : User = new User();
        u.Firstname = $user.Firstname;
        u.Lastname = $user.Lastname;
        u.UserName = $user.UserName;
        u.Id = "who cares";
        return u;
    }

    @Mutation({returnType: TestObject, args: [{name: "obj", type: TestObject}]})
    private async createObj($obj : TestObject) : Promise<TestObject> {
        const retVal : TestObject = new TestObject();
        retVal.PropBoolean = true;
        retVal.PropString = "modified: " + $obj.PropString;
        retVal.PropNumber = $obj.PropNumber * $obj.PropNumber;
        retVal.PropInnerObj = new TestInnerObject();
        retVal.PropInnerObj.PropInStr = "inner: " + $obj.PropInnerObj.PropInStr;
        return retVal;
    }
}

#!/bin/bash
# * ********************************************************************************************************* *
# *
# * Copyright 2024 Oidis
# *
# * SPDX-License-Identifier: BSD-3-Clause
# * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
# * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
# *
# * ********************************************************************************************************* *

host="http://localhost"
user=""
pass=""

for i in "$@"; do
  case $i in
  -h | --help)
    echo "Requests generator script."
    echo "  --help      Prints help."
    echo "  --user=*    Test user name."
    echo "  --pass=*    Test user pass."
    echo "  --host=*    Server location with protocol and port."
    exit 0
    ;;
  --user=*)
    user="${i#*=}"
    shift
    ;;
  --pass=*)
    pass="${i#*=}"
    shift
    ;;
  -*)
    echo "Unknown option $i" 1>&2
    exit 1
    ;;
  *) ;;
  esac
done

echo "login"
bearer=$(curl -X GET --silent --location "$host/api/login?user=$user&pass=$pass" -H "Accept: application/json")
if [ $? -ne 0 ]; then
  echo "Failed to login"
  exit 1
fi
bearer=$(echo "$bearer" | tr -d '"')

for i in {1..1000}; do
  ret=$(curl -X POST --silent --location "$host/api/v1/gobalik/notification" \
      -H "Accept: application/json" \
      -H "Content-Type: application/json" \
      -H "Authorization: Bearer $bearer" \
      -d '{
            "ownerId": "3774f110-d1fb-11ec-904e-2159b5aad466",
            "event": "gobalik:create",
            "eventCreated": "2019-01-08T15:13:39+0100",
            "eventInstance": "'$hash'"
          }')
done

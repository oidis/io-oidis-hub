/*! ******************************************************************************************************** *
 *
 * Copyright 2021-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { UnitTestRunner } from "../UnitTestRunner.js";

import { JsonUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/JsonUtils.js";
import { Property } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Property.js";
import { StringUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/StringUtils.js";
import { Test } from "@io-oidis-commons/test/unit/Io/Oidis/Commons/BaseUnitTestRunner.js";
import { assert } from "@io-oidis-commons/test/unit/Io/Oidis/Commons/UnitTestEnvironment.js";
import { FileSystemHandler } from "@io-oidis-localhost/Io/Oidis/Localhost/Connectors/FileSystemHandler.js";
import { Loader } from "../../../../../../../source/typescript/Io/Oidis/Hub/Loader.js";
import { MailEngine } from "../../../../../../../source/typescript/Io/Oidis/Hub/Utils/MailEngine.js";

class MockMailEngine extends MailEngine {
    private simulator : boolean;

    constructor() {
        super();
        this.simulator = true;
        (<any>this).domain = "http://localhost.test";
    }

    public Simulator($value? : boolean) : boolean {
        return this.simulator = Property.Boolean(this.simulator, $value);
    }

    protected async sendMail($email : string, $url : string, $type : string, $environment? : any) : Promise<any> {
        let status : boolean = true;
        $environment = JsonUtils.Extend({
            email: $email,
            url  : $url
        }, $environment);
        if (!this.simulator) {
            status = await super.sendMail($email, $url, $type, $environment);
        }
        try {
            return {
                body : (await this.getTemplate($type, {
                    env: $environment,
                    // beautify: true,
                    minify: true
                })).body,
                email: $email,
                status
            };
        } catch (ex) {
            return {
                body  : ex.stack,
                email : $email,
                status: false
            };
        }
    }
}

export class MailEngineTest extends UnitTestRunner {
    private engine : MockMailEngine;
    private readonly withMailSend : boolean;
    private filesystem : FileSystemHandler;

    constructor() {
        super();

        this.withMailSend = false;
        this.setMethodFilter(
            // "testRegister"
            // "testResetPassword",
            // "testResetPasswordConfirm",
            // "testInvitation",
            // "testActivation",
            // "testDeactivation",
            // "testRemoved",
            // "testRestored"
        );
    }

    @Test(true)
    public async Register() : Promise<void> {
        assert.deepEqual(await this.engine.Register("test@oidis.io", "some token"), {
            body  : this.getBodyData("Register"),
            email : "test@oidis.io",
            status: true
        });
    }

    @Test(true)
    public async ResetPassword() : Promise<void> {
        assert.deepEqual(await this.engine.ResetPassword("test@oidis.io", "some token"), {
            body  : this.getBodyData("ResetPassword"),
            email : "test@oidis.io",
            status: true
        });
    }

    @Test(true)
    public async ResetPasswordConfirm() : Promise<void> {
        assert.deepEqual(await this.engine.ResetPasswordConfirm("test@oidis.io"), {
            body  : this.getBodyData("ResetPasswordConfirm"),
            email : "test@oidis.io",
            status: true
        });
    }

    @Test(true)
    public async Invitation() : Promise<void> {
        assert.deepEqual(
            await this.engine.Invitation("test@oidis.io", "test group", "info@oidis.io", "some token"),
            {
                body  : this.getBodyData("Invitation"),
                email : "test@oidis.io",
                status: true
            });
    }

    @Test(true)
    public async Activation() : Promise<void> {
        assert.deepEqual(
            await this.engine.Activation("test@oidis.io", "some token", 10),
            {
                body  : this.getBodyData("Activation"),
                email : "test@oidis.io",
                status: true
            });
    }

    @Test(true)
    public async Deactivation() : Promise<void> {
        assert.deepEqual(
            await this.engine.Deactivation("test@oidis.io", "some token"),
            {
                body  : this.getBodyData("Deactivation"),
                email : "test@oidis.io",
                status: true
            });
    }

    @Test(true)
    public async Removed() : Promise<void> {
        assert.deepEqual(
            await this.engine.Removed("test@oidis.io"),
            {
                body  : this.getBodyData("Removed"),
                email : "test@oidis.io",
                status: true
            });
    }

    @Test(true)
    public async Restored() : Promise<void> {
        assert.deepEqual(
            await this.engine.Restored("test@oidis.io"),
            {
                body  : this.getBodyData("Restored"),
                email : "test@oidis.io",
                status: true
            });
    }

    protected before() : void {
        this.engine = new MockMailEngine();
        this.engine.Simulator(!this.withMailSend);
        this.filesystem = Loader.getInstance().getFileSystemHandler();
    }

    private getBodyData($for : string) : string {
        let data : string = this.filesystem.Read(
            this.getAbsoluteRoot() + "/test/resource/data/Io/Oidis/Hub/Utils/MailEngine/" + $for + ".html").toString("utf8");
        data = StringUtils.Replace(data, "\r\n", "\n");
        data = StringUtils.Replace(data, "\\n", "\n");
        return data;
    }
}

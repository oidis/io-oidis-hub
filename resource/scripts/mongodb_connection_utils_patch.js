/* ********************************************************************************************************* *
 *
 * Copyright 2021 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
'use strict';

const require_optional = require('optional-require')(require);

function debugOptions(debugFields, options) {
    const finaloptions = {};
    debugFields.forEach(function (n) {
        finaloptions[n] = options[n];
    });

    return finaloptions;
}

function retrieveBSON() {
    const BSON = require('bson');
    BSON.native = false;
    return BSON;
}

// Throw an error if an attempt to use Snappy is made when Snappy is not installed
function noSnappyWarning() {
    throw new Error(
        'Attempted to use Snappy compression, but Snappy is not installed. Install or disable Snappy compression and try again.'
    );
}

// Facilitate loading Snappy optionally
function retrieveSnappy() {
    return {
        compress: noSnappyWarning,
        uncompress: noSnappyWarning,
        compressSync: noSnappyWarning,
        uncompressSync: noSnappyWarning
    };
}

module.exports = {
    debugOptions,
    retrieveBSON,
    retrieveSnappy
};

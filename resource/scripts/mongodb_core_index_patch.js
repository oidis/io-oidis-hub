/* ********************************************************************************************************* *
 *
 * Copyright 2021 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
'use strict';

let BSON = require('bson');
const require_optional = require('optional-require')(require);
const EJSON = require('./utils').retrieveEJSON();

module.exports = {
    // Errors
    MongoError: require('./error').MongoError,
    MongoNetworkError: require('./error').MongoNetworkError,
    MongoParseError: require('./error').MongoParseError,
    MongoTimeoutError: require('./error').MongoTimeoutError,
    MongoServerSelectionError: require('./error').MongoServerSelectionError,
    MongoWriteConcernError: require('./error').MongoWriteConcernError,
    // Core
    Connection: require('./connection/connection'),
    Server: require('./topologies/server'),
    ReplSet: require('./topologies/replset'),
    Mongos: require('./topologies/mongos'),
    Logger: require('./connection/logger'),
    Cursor: require('./cursor').CoreCursor,
    ReadPreference: require('./topologies/read_preference'),
    Sessions: require('./sessions'),
    BSON: BSON,
    EJSON: EJSON,
    Topology: require('./sdam/topology').Topology,
    // Raw operations
    Query: require('./connection/commands').Query,
    // Auth mechanisms
    MongoCredentials: require('./auth/mongo_credentials').MongoCredentials,
    defaultAuthProviders: require('./auth/defaultAuthProviders').defaultAuthProviders,
    MongoCR: require('./auth/mongocr'),
    X509: require('./auth/x509'),
    Plain: require('./auth/plain'),
    GSSAPI: require('./auth/gssapi'),
    ScramSHA1: require('./auth/scram').ScramSHA1,
    ScramSHA256: require('./auth/scram').ScramSHA256,
    // Utilities
    parseConnectionString: require('./uri_parser')
};

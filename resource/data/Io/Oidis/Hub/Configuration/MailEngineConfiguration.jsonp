/* ********************************************************************************************************* *
 *
 * Copyright 2021-2024 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
Io.Oidis.Services.DAO.Resources.Data({
    version: "2022.3.0",
    template: "resource/data/Io/Oidis/Hub/Templates/MailEngine.mjml",
    logo: "/resource/graphics/Io/Oidis/Hub/Gui/LogoSymbol.png",
    favicon: "<? @var domain ?>/resource/graphics/icon.ico",
    accountsAddress: "accounts@oidis.io",
    contents: {
        register: {
            subject: "Welcome to Oidis",
            header: "You have successfully registered",
            htmlBody: "" +
                "Now you need to activate your account here:<br>" +
                "<? @var link ?>" +
                "<br><br>" +
                "If you do not activate your account within 30 days, it will be automatically deleted.",
            navigateText: "Activate",
            plainBody: ""
        },
        activation: {
            subject: "Activate Oidis account",
            header: "You have successfully registered byt account activation was not finished",
            htmlBody: "" +
                "Now you need to activate your account here:<br>" +
                "<? @var link ?>" +
                "<br><br>" +
                "If you do not activate your account within <? @var days ?> days, it will be automatically deleted.",
            navigateText: "Activate",
            plainBody: ""
        },
        deactivation: {
            subject: "Oidis account deactivated",
            header: "Your account was deactivated",
            htmlBody: "" +
                "Your account was not activated in past 30 days and was moved to archive. You can still try to activate your account here:<br>" +
                "<? @var link ?>" +
                "<br><br>" +
                "We do not garantie account exitence so you may need to create new account.",
            navigateText: "Activate",
            plainBody: ""
        },
        invitation: {
            subject: "Invitation to the group",
            header: "You have been invited to join the group",
            htmlBody: "" +
                "We are delivering an invitation to the group: <? @var group ?> owned by the user: <? @var owner ?><br>" +
                "<br>" +
                "You can accept the invitation here:<br>" +
                "<? @var link ?>",
            navigateText: "Accept",
            plainBody: ""
        },
        resetPassword: {
            subject: "Password recovery",
            header: "Received email password reset request: <? @var email ?>",
            htmlBody: "" +
                "You can reset your password here:<br>" +
                "<? @var link ?>" +
                "<br><br>" +
                "The password reset link will be valid for 14 days, after which you must issue a new request " +
                "or log in with the correct login information.",
            navigateText: "Restore",
            plainBody: ""
        },
        resetPasswordConfirm: {
            subject: "Password changed",
            header: "The password for your account has been changed",
            htmlBody: "" +
                "This is to confirm that you have changed the password for your email account:<br>" +
                "<? @var email ?>" +
                "<br><br>" +
                "If you did not make this change, please notify us immediately at accounts@oidis.io.",
            navigateText: "Report",
            plainBody: ""
        },
        restored: {
            subject: "Oidis account activated",
            header: "Welcome back",
            htmlBody: "You have successfully restored your account from archive.",
            navigateText: "SignIn",
            plainBody: ""
        },
        removed: {
            subject: "Oidis account removed",
            header: "Your account was removed",
            htmlBody: "" +
                "Your account was removed from archive and mernamently deleted from system. You can create new account here:<br>" +
                "<? @var link ?>",
            navigateText: "Sing Up",
            plainBody: ""
        },
        reportFailure:{
            subject: "Crash report detected",
            header: "New system crash has been detected",
            htmlBody: "" +
                "<? @var message ?>",
            navigateText: "Logs",
            plainBody: ""
        },
        feedback: {
            template: "resource/data/Io/Oidis/Hub/Templates/MailEngineSimple.mjml",
            subject: "New feedback from Oidis Hub",
            header: "<? @var category ?>",
            htmlBody: "" +
                "From: <? @var clientMail ?><br>" +
                "UUID: <? @var uuid ?><br>" +
                "<? @var message ?>"
        },
        publicFeedback: {
            template: "resource/data/Io/Oidis/Hub/Templates/MailEngineSimple.mjml",
            subject: "New feedback from Oidis Hub [NOT AUTHORIZED]",
            header: "<? @var category ?>",
            htmlBody: "" +
                "From: <? @var clientMail ?><br>" +
                "UUID: <? @var uuid ?><br>" +
                "<? @var message ?>"
        },
        newAuthCode: {
            subject: "Oidis: personal authentication codee",
            header: "New personal authentication code created",
            htmlBody: "" +
                "You can authorize by new personal authentication code now at: <? @var link ?>." +
                "<br><br>Code is:<br>" +
                "<strong style=\"font-size: 14px\"><? @var code ?></strong>",
            navigateText: "Login",
            plainBody: "New authorization code for <? @var link ?> is: <? @var code ?>"
        },
        authCodeResend: {
            subject: "Oidis: personal authentication code [RENEW]",
            header: "New personal authentication code created",
            htmlBody: "" +
                "Your authentication code has been updated. For authorization at <? @var link ?> you can use new code:<br>" +
                "<strong style=\"font-size: 14px\"><? @var code ?></strong>",
            navigateText: "Login",
            plainBody: "New authorization code for <? @var link ?> is: <? @var code ?>"
        }
    }
});

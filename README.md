# io-oidis-hub

> Oidis Framework synchronization hub

## Requirements

This library does not have any special requirements but it depends on the 
[Oidis Builder](https://gitlab.com/oidis/io-oidis-builder). See the Builder requirements before you build this project.

## Project build

The project build is fully automated. For more information about the project build, see the 
[Oidis Builder](https://gitlab.com/oidis/io-oidis-builder) documentation.

## Documentation

This project provides automatically-generated documentation in [TypeDoc](http://typedoc.org/) from the TypeScript source by running the 
`oidis docs` command from the {projectRoot} folder.

## History

### v2022.2.0
Updated docker distro setup. Refactored REST API support. Several other minor fixes and sync with dependencies.
### v2022.1.1
Updated and enhanced registry page features. Fixed some minor bugs.
### v2022.1.0
Updated registry page and all system around it. Extended agents registration and agents page view. 
Fixed pkg build time replaced by upload time. Modules cleanup based on integrity check results.
### v2022.0.0
A lot of changes connected with DB, agents, authentication and users/groups management has been done. Added base support for REST API.
### v2020.1.0
Added options to enable unsecured connection to SMTP server with default sender specification. Fixed wrong crash report time calculation.
### v2020.0.0
Identity update. Change of configuration files format.
### v2019.3.0
Refactoring of namespaces. Migration to gitlab. Initial update of identity for project fork. 
Added ability to notify updates upload to slack chanel. Implemented better GUI for Hub register page. Added full support for OSX.
Bug fixes for memory leaks and SSL configurations. Update of Live Content Protocol required by auth methods execution.  
### v2019.1.0
Usage of updater and reverse proxy for Hub deploy. Usage of typedefs.
### v2019.0.2
Usage of new app loader. Updated docker configuration.
### v2019.0.1
Enable to specify request patterns externally. Usage of extended default.confing. Updates in docker image.
### v2019.0.0
Added basic registry page. Added ability to check agents status. Integration of letencrypt, metrics and reverse proxy. 
Fixed compatibility with selfextractor. Fixed embedded mode for linux. Updated docker files.
### v2018.3.0
Added implementation based on REST services.
### v2018.2.0
Initial release.

## License

This software is owned or controlled by Oidis.
The use of this software is governed by the BSD-3-Clause Licence distributed with this material.
 
See the `LICENSE.txt` file for more details.

---

Author Jakub Cieslar, 
Copyright 2017-2019 [NXP](http://nxp.com/),
Copyright 2019-2025 [Oidis](https://www.oidis.io/)

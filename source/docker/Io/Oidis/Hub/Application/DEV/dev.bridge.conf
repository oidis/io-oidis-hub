# * ********************************************************************************************************* *
# *
# * Copyright 2022 Oidis
# *
# * SPDX-License-Identifier: BSD-3-Clause
# * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
# * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
# *
# * ********************************************************************************************************* *

server {
    server_name hub.dev.oidis.io www.hub.dev.oidis.io;
    listen 80;

    location / {
        set $hub_dev_oidis "oidis-dev-hub";
        proxy_pass http://$hub_dev_oidis:80$request_uri;

        proxy_buffering off;
        proxy_redirect off;
        proxy_set_header Host $host;
        proxy_set_header Upgrade $http_upgrade;
        proxy_set_header Connection $connection_upgrade;
        proxy_headers_hash_max_size 512;

        if (-f /var/oidis/hub.dev.oidis.io/mnton.txt) {
            return 503;
        }
    }

    include /var/oidis/maintenance.page/*.conf;
}

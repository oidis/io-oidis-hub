# install deps for managing pkgs
sudo apt-get install -y software-properties-common apt-transport-https

# enable webmin registry
sudo wget -q http://www.webmin.com/jcameron-key.asc -O- | sudo apt-key add -

sudo add-apt-repository "deb [arch=amd64] http://download.webmin.com/download/repository sarge contrib"

# install
sudo apt-get install -y webmin

# webmin root configuration; use same password as for oidis root user
sudo /usr/share/webmin/changepass.pl /etc/webmin/ root <password>

# change ssl requirement in file below and modify "ssl=1" to "ssl=0"
sudo nano /etc/webmin/miniserv.conf

# add webprefixnoredir=1
sudo nano /etc/webmin/config

# start/stop/restart using prepared scripts (they only wraps systemctl [start/stop] webmin)
sudo /etc/webmin/[start|stop|restart]

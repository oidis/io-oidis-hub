#!/bin/bash
# * ********************************************************************************************************* *
# *
# * Copyright 2020-2022 Oidis
# *
# * SPDX-License-Identifier: BSD-3-Clause
# * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
# * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
# *
# * ********************************************************************************************************* *

sourceFile="${BASH_SOURCE[0]}"
while [ -h "$sourceFile" ]; do
    sourceLink="$(readlink "$sourceFile")"
    if [[ $sourceLink == /* ]]; then
        sourceFile="$sourceLink"
    else
        sourceDir="$( dirname "$sourceFile" )"
        sourceFile="$sourceDir/$sourceLink"
    fi
done
binBase="$( cd -P "$( dirname "$sourceFile" )" && pwd )"

systemctl stop services-oidis-io.service
sudo docker-compose down

mkdir -p /var/oidis/services-oidis-io/hub

cd /var/oidis/services-oidis-io/hub

curl -fsSL --compressed "https://hub.oidis.io/Update/io-oidis-hub/null/shared-linux-nodejs" > hub.tar.gz
tar xf hub.tar.gz
cd io-oidis-hub-*
cp -r . ../
cd ..
rm -rf io-oidis-hub-*
rm hub.tar.gz

cp $binBase/OidisHub.config.jsonp /var/oidis/services-oidis-io/hub/OidisHub.config.jsonp

cd ..
sudo docker-compose up -d

cp $binBase/services-oidis-io.service /etc/systemd/system/services-oidis-io.service
systemctl enable services-oidis-io.service
systemctl start services-oidis-io.service

#!/bin/bash
# * ********************************************************************************************************* *
# *
# * Copyright 2020 Oidis
# *
# * SPDX-License-Identifier: BSD-3-Clause
# * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
# * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
# *
# * ********************************************************************************************************* *

cd /var/oidis/services-oidis-io
docker-compose down
docker-compose up -d
systemctl start services-oidis-io.service

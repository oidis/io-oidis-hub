#!/bin/bash
# * ********************************************************************************************************* *
# *
# * Copyright 2020 Oidis
# *
# * SPDX-License-Identifier: BSD-3-Clause
# * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
# * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
# *
# * ********************************************************************************************************* *

systemctl stop services-oidis-io.service
/etc/init.d/apache2 stop

letsencrypt certonly --standalone -d services.oidis.io
letsencrypt certonly --standalone -d services.pasaja.cz
letsencrypt certonly --standalone -d services.wuiframework.com

rm /var/lib/oidis/services-oidis-io/fullchain.pem
rm /var/lib/oidis/services-oidis-io/privkey.pem
cp -L /etc/letsencrypt/live/services.oidis.io/fullchain.pem /var/lib/oidis/services-oidis-io/fullchain.pem
cp -L /etc/letsencrypt/live/services.oidis.io/privkey.pem /var/lib/oidis/services-oidis-io/privkey.pem

rm /var/lib/oidis/services-pasaja-cz/fullchain.pem
rm /var/lib/oidis/services-pasaja-cz/privkey.pem
cp -L /etc/letsencrypt/live/services.pasaja.cz/fullchain.pem /var/lib/oidis/services-pasaja-cz/fullchain.pem
cp -L /etc/letsencrypt/live/services.pasaja.cz/privkey.pem /var/lib/oidis/services-pasaja-cz/privkey.pem

rm /var/lib/oidis/services-wuiframework-com/fullchain.pem
rm /var/lib/oidis/services-wuiframework-com/privkey.pem
cp -L /etc/letsencrypt/live/services.wuiframework.com/fullchain.pem /var/lib/oidis/services-wuiframework-com/fullchain.pem
cp -L /etc/letsencrypt/live/services.wuiframework.com/privkey.pem /var/lib/oidis/services-wuiframework-com/privkey.pem

/etc/init.d/apache2 start
systemctl start services-oidis-io.service

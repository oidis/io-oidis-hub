#!/bin/bash
# * ********************************************************************************************************* *
# *
# * Copyright 2020-2022 Oidis
# *
# * SPDX-License-Identifier: BSD-3-Clause
# * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
# * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
# *
# * ********************************************************************************************************* *

docker exec -d oidis-services /var/oidis/io-oidis-hub/cmd/OidisHub.sh start
docker exec -d pasaja-services /var/oidis/io-oidis-hub/cmd/OidisHub.sh start
docker exec -d wui-services /var/oidis/io-oidis-hub/cmd/OidisHub.sh start

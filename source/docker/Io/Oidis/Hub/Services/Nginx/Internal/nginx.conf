# * ********************************************************************************************************* *
# *
# * Copyright 2021-2022 Oidis
# *
# * SPDX-License-Identifier: BSD-3-Clause
# * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
# * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
# *
# * ********************************************************************************************************* *

user  nginx;
worker_processes  auto;

events {
    worker_connections  1024;
}

http {
    include       /etc/nginx/mime.types;
    default_type  application/octet-stream;

    log_format  main  '$remote_addr - $remote_user [$time_local] "$request" '
                      '$status $body_bytes_sent "$http_referer" '
                      '"$http_user_agent" "$http_x_forwarded_for"';

    access_log  /var/log/nginx/access.log  main;

    sendfile        on;
    #tcp_nopush     on;

    keepalive_timeout  65;

    #gzip  on;

    map $http_upgrade $connection_upgrade {
        default Upgrade;
        '' close;
    }

    # global override to suppress warnings
    proxy_headers_hash_bucket_size 128;

    # include all virtual servers from their own configs
    include /var/oidis/*/*.bridge.conf;

    # add default response for 443 or default server 80, something wrong or more specific that proxy config in applications is not OK

    # default server for certutil used when endpoint not exists yet
    # run cert-update.sh for new domain before *.nginx.conf will be coppied or rename it to *.nginx.conf.backup
    # just for first run or comment out SSL server config portion inside this file
#     server {
#         server_name _;
#         listen 80 default_server;
#
#         include /etc/nginx/utils/certutil.conf;

#         location /test.html {
#             root /var/oidis/hub-dev-oidis-io;
#         }
#     }
}

#!/bin/bash
# * ********************************************************************************************************* *
# *
# * Copyright 2022 Oidis
# *
# * SPDX-License-Identifier: BSD-3-Clause
# * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
# * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
# *
# * ********************************************************************************************************* *

# cron timing: recommended by letsecrypt and all registered domains should be processed by single plan
# do not forget to register cert-update.sh to /usr/bin
# sudo ln -sf $(pwd)/cert-update.sh /usr/bin
#  0 */12 * * * cert-update.sh
cd /var/oidis/main-proxy || return

logfile=cert-update.log

log () {
  echo "$1"
  echo "$1" >> $logfile
}

logError () {
  echo "$1" >>/dev/stderr
  echo "ERROR: $1" >> $logfile
}

for i in "$@"; do
  case $i in
  -h | --help)
    echo "This is simple wrapper for certbot."
    echo "  -d=*, --domain=*    Domain for which certificate should be initialized or renewed"
    echo "  -e=*, --email=*     Email used for letsencrypt notifications"
    echo "  --init              Init certificate for selected specified domain (--domain=* is mandatory), renew certificate otherwise."
    echo "                      Renewing will be requested without this flag, and all maintained certificates will be renewed when domain "
    echo "                      is not specified"
    echo "  --force             Forces proxy container restart (config reload is invoked otherwise)"
    exit 0
    ;;
  -d=* | --domain=*)
    DOMAIN="${i#*=}"
    shift
    ;;
  -e=* | --email=*)
    EMAIL="${i#*=}"
    shift
    ;;
  --init)
    IS_INIT=1
    shift
    ;;
  --force)
    IS_FORCE=1
    shift
    ;;
  -*)
    logError "Unknown option $i"
    exit 1
    ;;
  *) ;;
  esac
done

echo "" >> $logfile
date >> $logfile

if [[ -z "${EMAIL}" ]]; then
  EMAIL="info@oidis.io"
fi

if [[ $IS_INIT == 1 ]]; then
  if [[ ! -f "/var/www/certbot" ]]; then
    sudo mkdir -p /var/www/certbot
  fi

  if [[ -z "${DOMAIN}" ]]; then
    logError "Domain needs to be specified for initialization, please use '--domain=<domain>' attribute."
    exit 1
  else
    log "Certificate initialization for: $DOMAIN"
    sudo certbot certonly --agree-tos -m "$EMAIL" --webroot --webroot-path /var/www/certbot -d "$DOMAIN"
  fi
else
  if [[ -z "${DOMAIN}" ]]; then
    log "Renewing all certificates..."
    sudo certbot renew --quiet
  else
    log "Renewing certificate for: $DOMAIN"
    sudo certbot renew --cert-name "$DOMAIN"
  fi
fi

sudo nginx -s reload

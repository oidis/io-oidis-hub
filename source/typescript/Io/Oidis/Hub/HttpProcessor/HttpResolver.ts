/*! ******************************************************************************************************** *
 *
 * Copyright 2010-2013 Jakub Cieslar
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2018 NXP
 * Copyright 2019-2024 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { IHttpResolverContext } from "@io-oidis-commons/Io/Oidis/Commons/HttpProcessor/HttpResolver.js";
import { HttpResolver as Parent } from "@io-oidis-localhost/Io/Oidis/Localhost/HttpProcessor/HttpResolver.js";

export class HttpResolver extends Parent {

    protected async getStartupResolvers($context : IHttpResolverContext) : Promise<any> {
        if ($context.isBrowser) {
            if (!$context.isProd) {
                await this.registerResolver(async () => (await import("../Index.js")).Index,
                    "/", "/index", "/index.html", "/web/");
            }
            return super.getStartupResolvers($context);
        }

        await this.registerResolver(async () => (await import("./Resolvers/GraphiQLLoader.js")).GraphiQLLoader,
            "/graphiql");
        await this.registerResolver(async () => (await import("./Resolvers/NotAuthorizedResolver.js")).NotAuthorizedResolver,
            "/ServerError/Http/NotAuthorized");

        await this.registerResolver(async () => (await import("./Resolvers/FrontEndLoader.js")).FrontEndLoader,
            "/");
        await this.registerResolver(async () => (await import("./Resolvers/GraphQLResolver.js")).GraphQLResolver,
            "/graphql");
        await this.registerResolver(async () => (await import("./Resolvers/LiveContentResolver.js")).LiveContentResolver,
            "/liveContent/");
        await this.registerResolver(async () => (await import("./Resolvers/PingResolver.js")).PingResolver,
            "/ping.txt", "/ping.png");

        await this.registerResolver(async () => (await import("./Resolvers/FileRequestResolver.js")).FileRequestResolver,
            "/robots.txt",
            "/google*.html",
            "/sitemap.xml",
            "/manifest.json",
            "/Download/{fileId}",
            "/Configuration/{appName}/{configName}", "/Configuration/{appName}/{configName}/{version}",
            "/Update/{appName}", "/Update/{appName}/{releaseName}",
            "/Update/{appName}/{releaseName}/{platform}", "/Update/{appName}/{releaseName}/{platform}/{version}");

        await this.registerResolver(async () => (await import("./Resolvers/FileUploadResolver.js")).FileUploadResolver,
            "/Upload", "/Upload/{type}", "/Upload/{type}/{clientId}");
        await this.registerResolver(async () => (await import("./Resolvers/ImageRequestResolver.js")).ImageRequestResolver,
            "/DynamicImage/{type}/{source}/{settings}/{version}");

        await this.registerResolver(async () => (await import("./Resolvers/ForwardingResolver.js")).ForwardingResolver,
            "/forward", "/forward/continue");
        await this.registerResolver(async () => (await import("./Resolvers/PluginsRepository.js")).PluginsRepository,
            "/Plugins/{ideName}", "/Plugins/{ideName}/{repoDescriptor}", "/Plugins/{ideName}/{repoDescriptor}/{pluginName}");

        await this.registerResolver(async () => (await import("./Resolvers/AgentStatusResolver.js")).AgentStatusResolver,
            "/Status/Agent/{name}");
        await this.registerResolver(async () => (await import("./Resolvers/RESTResolver.js")).RESTResolver,
            "/api/{section}/{subsection1}/{subsection2}/{subsection3}/{subsection4}/{subsection5}",
            "/api/{section}/{subsection1}/{subsection2}/{subsection3}/{subsection4}",
            "/api/{section}/{subsection1}/{subsection2}/{subsection3}",
            "/api/{section}/{subsection1}/{subsection2}",
            "/api/{section}/{subsection1}",
            "/api/{section}");

        return super.getStartupResolvers($context);
    }
}

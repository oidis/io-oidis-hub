/*! ******************************************************************************************************** *
 *
 * Copyright 2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2018 NXP
 * Copyright 2019-2024 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { ArrayList } from "@io-oidis-commons/Io/Oidis/Commons/Primitives/ArrayList.js";
import { LogIt } from "@io-oidis-commons/Io/Oidis/Commons/Utils/LogIt.js";
import { ObjectDecoder } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectDecoder.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { StringUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/StringUtils.js";
import { ConfigFile } from "@io-oidis-hub-gui/Io/Oidis/Hub/Gui/Models/ConfigFile.js";
import { SelfupdatePackage } from "@io-oidis-hub-gui/Io/Oidis/Hub/Gui/Models/SelfupdatePackage.js";
import {
    FileRequestResolver as Parent,
    IFilePath
} from "@io-oidis-localhost/Io/Oidis/Localhost/HttpProcessor/Resolvers/FileRequestResolver.js";
import { Loader } from "../../Loader.js";
import { ConfigurationsManager } from "../../Utils/ConfigurationsManager.js";
import { SelfupdatesManager } from "../../Utils/SelfupdatesManager.js";
import { FileUploadResolver } from "./FileUploadResolver.js";

export class FileRequestResolver extends Parent {
    private config : ConfigFile;
    private package : SelfupdatePackage;
    private fileId : string;

    protected argsHandler($GET : ArrayList<string>, $POST : ArrayList<any>) : void {
        super.argsHandler($GET, $POST);
        this.config = null;
        this.package = null;
        this.fileId = null;
        if ($GET.KeyExists("appName")) {
            if (StringUtils.ContainsIgnoreCase(this.getRequest().getUrl(), "/Configuration/")) {
                this.config = new ConfigFile();
                this.config.name = $GET.getItem("appName");
                this.config.configName = $GET.getItem("configName");
                this.config.version = $GET.getItem("version");
            } else {
                this.package = new SelfupdatePackage();
                this.package.name = $GET.getItem("appName");
                this.package.release = $GET.getItem("releaseName");
                this.package.platform = $GET.getItem("platform");
                this.package.version = $GET.getItem("version");
            }
        }
        if ($GET.KeyExists("fileId")) {
            this.fileId = ObjectDecoder.Base64($GET.getItem("fileId"));
        }
    }

    protected async filePathResolver() : Promise<IFilePath> {
        if (!ObjectValidator.IsEmptyOrNull(this.config)) {
            if (ObjectValidator.IsEmptyOrNull(this.config.version)) {
                this.config.version = null;
            }
            const manager : ConfigurationsManager = ConfigurationsManager.getInstance();
            const packageInfo : IFilePath =
                await manager.getFileDescriptor(this.config.name, this.config.configName, this.config.version);
            packageInfo.url = this.getRequest().getUrl();
            return packageInfo;
        } else if (!ObjectValidator.IsEmptyOrNull(this.package)) {
            const manager : SelfupdatesManager = SelfupdatesManager.getInstance();
            const packageInfo : IFilePath =
                await manager.getFileDescriptor(this.package.name, this.package.release, this.package.platform, this.package.version);
            packageInfo.url = this.getRequest().getUrl();
            return packageInfo;
        } else if (StringUtils.ContainsIgnoreCase(this.getRequest().getUrl(), "/Download") &&
            !ObjectValidator.IsEmptyOrNull(this.fileId)) {
            const path : any = require("path");
            const name = path.basename(ObjectDecoder.Url(this.fileId));
            let extension = name;
            extension = StringUtils.Substring(extension,
                StringUtils.IndexOf(name, ".", false) + 1, StringUtils.Length(name));
            extension = StringUtils.ToLowerCase(extension);
            const filePath : string = path.normalize(Loader.getInstance().getProgramArgs().AppDataPath() +
                "/resource/data/" + StringUtils.Replace(FileUploadResolver.ClassName(), ".", "/") + "/" + name);

            return {
                extension,
                name,
                path: filePath,
                url : this.getRequest().getUrl()
            };
        } else {
            return super.filePathResolver();
        }
    }

    protected getAllowedPatterns() : string[] {
        return super.getAllowedPatterns().concat([
            "/Configuration/*",
            "/Update/*",
            "/Download/*",
            "/robots.txt",
            "/google*.html",
            "/sitemap.xml",
            "/manifest.json"
        ]);
    }

    protected resolver() : void {
        if (StringUtils.Contains(this.fileId, "http://", "https://")) {
            LogIt.Debug("init proxy stream for: {0}", this.fileId);
            const options : any = require("url").parse(this.fileId);
            const httpRequest : any = (options.protocol === "https:" ? require("https") : require("http"))
                .request(options, ($httpResponse : any) : void => {
                    $httpResponse.headers["content-encoding"] = "identity";
                    this.getConnector().Send({
                        body   : $httpResponse,
                        headers: $httpResponse.headers,
                        status : $httpResponse.statusCode
                    });
                });
            httpRequest.on("error", ($ex : Error) : void => {
                LogIt.Warning($ex.stack);
            });
            httpRequest.end();
        } else {
            super.resolver();
        }
    }
}

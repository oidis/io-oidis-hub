/*! ******************************************************************************************************** *
 *
 * Copyright 2021-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { HttpStatusType } from "@io-oidis-commons/Io/Oidis/Commons/Enums/HttpStatusType.js";
import { LogIt } from "@io-oidis-commons/Io/Oidis/Commons/Utils/LogIt.js";
import { BaseHttpResolver } from "@io-oidis-localhost/Io/Oidis/Localhost/HttpProcessor/Resolvers/BaseHttpResolver.js";

export class NotAuthorizedResolver extends BaseHttpResolver {

    public Process() : void {
        this.resolver();
    }

    protected resolver() : void {
        if (this.getEnvironmentArgs().HtmlOutputAllowed()) {
            this.getHttpManager().ReloadTo("/login", this.RequestArgs().POST());
        } else {
            LogIt.Warning("Not authorized request in headless mode.");
            this.getConnector().Send({
                body  : "Not authorized request.",
                status: HttpStatusType.NOT_AUTHORIZED
            });
        }
    }
}

/*! ******************************************************************************************************** *
 *
 * Copyright 2022-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { HttpMethodType } from "@io-oidis-commons/Io/Oidis/Commons/Enums/HttpMethodType.js";
import { HttpStatusType } from "@io-oidis-commons/Io/Oidis/Commons/Enums/HttpStatusType.js";
import { LogIt } from "@io-oidis-commons/Io/Oidis/Commons/Utils/LogIt.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { IAuthPromise } from "@io-oidis-localhost/Io/Oidis/Localhost/Interfaces/IResponse.js";
import { Loader } from "../../Loader.js";
import { GraphQLResponse } from "../ResponseApi/GraphQLResponse.js";
import { RESTResolver } from "./RESTResolver.js";

export class GraphQLResolver extends RESTResolver {
    private readonly graphQLSchema : any;
    private isAuthorized : boolean;

    constructor() {
        super();
        this.graphQLSchema = Loader.getInstance().getGQLContext().getSchema();
    }

    protected async accessValidator() : Promise<boolean> {
        this.isAuthorized = await super.accessValidator();
        return this.isAuthorized;
    }

    protected getConnector() : GraphQLResponse {
        return new GraphQLResponse(super.getConnector());
    }

    protected async resolver() : Promise<void> {
        const response : GraphQLResponse = this.getConnector();
        const {
            parse,
            validate,
            execute,
            formatError,
            validateSchema,
            GraphQLFieldResolver,
            GraphQLTypeResolver,
            GraphQLError,
            getOperationAST
        } = require("graphql");

        if (!this.isAuthorized) {
            response.SendError(new GraphQLError("Not authorized"), HttpStatusType.NOT_AUTHORIZED);
            return;
        }
        response.setOptions(this.options);

        if (this.options.method !== HttpMethodType.GET && this.options.method !== HttpMethodType.POST) {
            response.SendError("Unsupported HttpMethod type: " + this.options.method,
                HttpStatusType.NOT_ALLOWED, {Allow: "GET, POST"});
            return;
        }

        let request : IGraphQLRequest;
        try {
            request = await this.formatRequest();
            if (ObjectValidator.IsString(request) && !ObjectValidator.IsEmptyOrNull(request)) {
                try {
                    request = this.normalizeRequestBody(JSON.parse(<any>request));
                } catch (ex) {
                    LogIt.Warning(ex.stack);
                }
            }
        } catch ($e) {
            response.SendError($e, <any>400);
            return;
        }
        // TODO(mkelnar) this could be done only once per session, will se how much time this will take
        const schemaError : any[] = validateSchema(this.graphQLSchema);
        if (schemaError.length > 0) {
            response.SendError(schemaError, HttpStatusType.ERROR);
            return;
        }

        if (ObjectValidator.IsEmptyOrNull(request.query)) {
            response.SendError("Query syntax error: query must be defined", HttpStatusType.ERROR);
            return;
        }

        let ast;
        try {
            ast = parse(request.query);
        } catch ($e) {
            LogIt.Warning($e.stack);
            response.SendError("Query syntax error", HttpStatusType.ERROR);
            return;
        }

        const astError = validate(this.graphQLSchema, ast);
        if (astError.length > 0) {
            response.SendError(astError, HttpStatusType.ERROR);
            return;
        }

        if (this.options.method === HttpMethodType.GET) {
            const operationAST = getOperationAST(ast, request.operationName);
            if (operationAST && operationAST.operation !== "query") {
                response.SendError(
                    "Only query operation is allowed from a GET request, use POST for " + operationAST.operation,
                    HttpStatusType.NOT_ALLOWED);
                return;
            }
        }

        let result;
        try {
            result = await execute({
                contextValue  : {
                    invoker: async ($entity : any, $args : any[], $method : any) : Promise<any> => {
                        const promise : IAuthPromise = $method.apply($entity, $args);
                        if (await this.isAuthorizedFor(this.getCurrentToken(), promise.auth)) {
                            return promise.resolve.apply($entity, [this.getCurrentToken()]);
                        } else {
                            throw new Error("Not authorized for " + promise.auth);
                        }
                    },
                    token  : this.getCurrentToken()
                },
                document      : ast,
                fieldResolver : GraphQLFieldResolver,
                operationName : request.operationName,
                rootValue     : null,
                schema        : this.graphQLSchema,
                typeResolver  : GraphQLTypeResolver,
                variableValues: request.variables
            });
        } catch ($e) {
            response.SendError($e, HttpStatusType.ERROR);
            return;
        }
        const formattedResult = {...result, errors: result.errors?.map(formatError)};
        response.Send(formattedResult);
    }
}

export interface IGraphQLRequest {
    query : string;
    variables : any;
    operationName : string;
    raw : boolean;
}

// generated-code-start
export const IGraphQLRequest = globalThis.RegisterInterface(["query", "variables", "operationName", "raw"]);
// generated-code-end

/*! ******************************************************************************************************** *
 *
 * Copyright 2018 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { HttpStatusType } from "@io-oidis-commons/Io/Oidis/Commons/Enums/HttpStatusType.js";
import { ArrayList } from "@io-oidis-commons/Io/Oidis/Commons/Primitives/ArrayList.js";
import { Convert } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Convert.js";
import { ObjectDecoder } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectDecoder.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { BaseHttpResolver } from "@io-oidis-localhost/Io/Oidis/Localhost/HttpProcessor/Resolvers/BaseHttpResolver.js";
import { IAgentInfo } from "@io-oidis-services/Io/Oidis/Services/Connectors/AgentsRegisterConnector.js";
import { AgentsRegister } from "../../Primitives/AgentsRegister.js";

export class AgentStatusResolver extends BaseHttpResolver {
    private name : string;

    protected argsHandler($GET : ArrayList<string>, $POST : ArrayList<any>) : void {
        super.argsHandler($GET, $POST);
        this.name = null;
        if ($GET.KeyExists("name")) {
            this.name = ObjectDecoder.Url($GET.getItem("name"));
        }
    }

    protected resolver() : void {
        if (!ObjectValidator.IsEmptyOrNull(this.name)) {
            const agents : IAgentInfo[] = AgentsRegister.getInstance<AgentsRegister>().getAgentsList();
            let agent : IAgentInfo = null;
            agents.forEach(($agent : IAgentInfo) : void => {
                if ($agent.name === this.name) {
                    agent = $agent;
                }
            });
            if (!ObjectValidator.IsEmptyOrNull(agent)) {
                this.getConnector().Send({
                    body  : "Agent \"" + this.name + "\" uptime: " +
                        Convert.TimeToLongString(new Date().getTime() - agent.startTime.getTime()),
                    status: HttpStatusType.SUCCESS
                });
            } else {
                this.getConnector().Send({
                    body  : "Required agent \"" + this.name + "\" has not been found.",
                    status: HttpStatusType.NOT_FOUND
                });
            }
        } else {
            this.getConnector().Send({
                body  : "Agent name must be specified.",
                status: HttpStatusType.ERROR
            });
        }
    }
}

/*! ******************************************************************************************************** *
 *
 * Copyright 2010-2013 Jakub Cieslar
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2018 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { FileRequestResolver } from "@io-oidis-localhost/Io/Oidis/Localhost/HttpProcessor/Resolvers/FileRequestResolver.js";

export class ImageRequestResolver extends FileRequestResolver {

    protected resolver() : void {
        /// TODO: reuse UserControls implementation with back-end canvas support
        super.resolver();
    }
}

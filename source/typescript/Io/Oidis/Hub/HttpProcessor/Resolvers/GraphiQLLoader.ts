/*! ******************************************************************************************************** *
 *
 * Copyright 2022-2024 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { ArrayList } from "@io-oidis-commons/Io/Oidis/Commons/Primitives/ArrayList.js";
import { Convert } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Convert.js";
import { LogIt } from "@io-oidis-commons/Io/Oidis/Commons/Utils/LogIt.js";
import { ObjectEncoder } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectEncoder.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { Property } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Property.js";
import { StringUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/StringUtils.js";
import { PageContentBundles, PageContentBundleType } from "@io-oidis-gui/Io/Oidis/Gui/Utils/PageContentBundles.js";
import { StaticPageContentManager } from "@io-oidis-gui/Io/Oidis/Gui/Utils/StaticPageContentManager.js";
import { ISystemGroups } from "@io-oidis-hub-gui/Io/Oidis/Hub/Gui/Connectors/AuthManagerConnector.js";
import { AuthHttpResolver } from "@io-oidis-services/Io/Oidis/Services/ErrorPages/AuthHttpResolver.js";
import { Loader } from "../../Loader.js";
import { FrontEndLoader } from "./FrontEndLoader.js";

export class GraphiQLLoader extends FrontEndLoader {
    protected graphQLLocation : string;
    protected graphiQLLocation : string;

    constructor() {
        super();

        this.graphQLLocation = "/graphql";
        this.graphiQLLocation = "/graphiql";
    }

    public Process() : void {
        if (!this.getEnvironmentArgs().HtmlOutputAllowed()) {
            this.accessValidator().then(($status : boolean) : void => {
                if ($status) {
                    super.Process();
                } else {
                    Loader.getInstance().getAuthManager().IsAuthenticated(this.getCurrentToken())
                        .then(($status : boolean) : void => {
                            if ($status) {
                                this.getConnector().Send({
                                    body   : "",
                                    headers: {
                                        Location: this.getHostUrl() + "/#/ServerError/Http/Forbidden"
                                    },
                                    status : 302
                                });
                            } else {
                                const time : number = Property.Time(new Date().getTime(), "+60s");
                                this.getConnector().Send({
                                    body   : "",
                                    headers: {
                                        "Location"  : this.getHostUrl() + "/#/ServerError/Http/NotAuthorized",
                                        "Set-Cookie": "Referer=" + ObjectEncoder.Base64(
                                                this.getHostUrl() + this.graphiQLLocation, true) + "; " +
                                            "Expires=" + Convert.TimeToGMTformat(time) + ";"
                                    },
                                    status : 302
                                });
                            }
                        });
                }
            });
        } else {
            LogIt.Error("GraphiQLLoader is suitable only for back-end calls.");
        }
    }

    protected async accessValidator() : Promise<boolean> {
        const token : string = this.getCurrentToken();
        if (!ObjectValidator.IsEmptyOrNull(token)) {
            const groups : ISystemGroups = await Loader.getInstance().getAuthManager().getSystemGroups();
            return Loader.getInstance().getAuthManager().HasGroup(groups.Admin, token);
        }
        return false;
    }

    protected getCurrentToken() : string {
        return this.getRequest().getCookie(AuthHttpResolver.getTokenName());
    }

    protected getHostUrl() : string {
        const headers : ArrayList<string> = this.getRequest().getHeaders();
        if (headers.KeyExists("host")) {
            let address : string = headers.getItem("host");
            let scheme : string = this.getRequest().getScheme();
            if (!ObjectValidator.IsEmptyOrNull(headers.getItem("referer"))) {
                scheme = StringUtils.Contains(headers.getItem("referer"), "https://") ? "https://" : "http://";
            }
            if (!ObjectValidator.IsEmptyOrNull(headers.getItem("x-forwarded-host"))) {
                address = headers.getItem("x-forwarded-host");
            }
            let host : string = scheme + address;
            if (StringUtils.EndsWith(host, "/")) {
                host = host.slice(0, -1);
            }
            return host.trim();
        }
        return this.getHttpManager().getRequest().getHostUrl();
    }

    protected setContent() {
        const graphiQlContainer : string = this.getClassName() + ".graphiqlContainer";
        const tokenKey : string = AuthHttpResolver.getTokenName();
        const buildTime : number = new Date(this.getEnvironmentArgs().getBuildTime()).getTime();
        const serverUrl : string = this.getHostUrl();

        StaticPageContentManager.Title(this.getEnvironmentArgs().getProjectName() + " - GraphiQL");
        StaticPageContentManager.FaviconSource("resource/graphics/icon.ico");
        StaticPageContentManager.License(this.getLicense());
        PageContentBundles.getInstance().Apply(PageContentBundleType.REACT);

        StaticPageContentManager
            .BodyAppend("<script src=\"resource/libs/graphiql/graphiql.min.js?v=" + buildTime + "\"></script>");
        StaticPageContentManager.HeadLinkAppend("resource/libs/graphiql/graphiql.min.css?v=" + buildTime);
        StaticPageContentManager.BodyEventsEnabled(false);
        StaticPageContentManager.BodyAppend(this.getNoScript());
        StaticPageContentManager.BodyAppend(`
        <style>
            body {margin: 0 !important; padding: 0 !important;}
        </style>
        <div data-oidis-bind="${graphiQlContainer}" style="width: 100%; height: 100%;">Loading GraphiQL...</div>
        <script type="module">
            let token = "";
            try {
                token = localStorage.getItem("${tokenKey}");
            } catch(e) {
                console.warn(e.message);
            }
            if (token === "") {
                document.cookie.split(";").forEach(($item) => {
                    const [key,value] = $item.split("=");
                    if (key === "${tokenKey}") {
                        token = value;
                    }
                });
            }
            ReactDOM.render(
                React.createElement(GraphiQL, {
                    fetcher: GraphiQL.createFetcher({
                        url: "${serverUrl}${this.graphQLLocation}",
                        headers: {
                            Authorization: "Bearer " + token
                        }
                    }),
                    defaultEditorToolsVisibility: true,
                }),
                document.querySelector("[data-oidis-bind=\\"${graphiQlContainer}\\"]")
            );
        </script>`);
    }
}

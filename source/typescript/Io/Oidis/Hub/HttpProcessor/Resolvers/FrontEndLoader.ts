/*! ******************************************************************************************************** *
 *
 * Copyright 2019-2024 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { HttpStatusType } from "@io-oidis-commons/Io/Oidis/Commons/Enums/HttpStatusType.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { StringUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/StringUtils.js";
import { PageContentBundles, PageContentBundleType } from "@io-oidis-gui/Io/Oidis/Gui/Utils/PageContentBundles.js";
import { StaticPageContentManager } from "@io-oidis-gui/Io/Oidis/Gui/Utils/StaticPageContentManager.js";
import { FrontEndLoader as Parent } from "@io-oidis-localhost/Io/Oidis/Localhost/HttpProcessor/Resolvers/FrontEndLoader.js";
import { Loader } from "../../Loader.js";

export class FrontEndLoader extends Parent {

    protected setContent() {
        super.setContent();
        PageContentBundles.getInstance().Apply(PageContentBundleType.BOOTSTRAP);
        PageContentBundles.getInstance().Apply(PageContentBundleType.QRCODE);

        if (!ObjectValidator.IsEmptyOrNull(Loader.getInstance().getAppConfiguration().target.ga4ID)) {
            const ga4Id : string = Loader.getInstance().getAppConfiguration().target.ga4ID;
            const gaRegister : HTMLScriptElement = document.createElement("script");
            gaRegister.async = true;
            gaRegister.src = "https://www.googletagmanager.com/gtag/js?id=" + ga4Id;
            StaticPageContentManager.HeadScriptAppend(gaRegister);
            const gaLoader : HTMLScriptElement = document.createElement("script");
            gaLoader.text = `
                window.dataLayer = window.dataLayer || [];
                function gtag(){dataLayer.push(arguments);}
                gtag('js', new Date());
                gtag('config', '${ga4Id}');
                `;
            StaticPageContentManager.HeadScriptAppend(gaLoader);
        }
        if (Loader.getInstance().getAppConfiguration().target.pwaEnabled) {
            const link : HTMLLinkElement = document.createElement("link");
            link.href = "/manifest.json";
            link.rel = "manifest";
            StaticPageContentManager.HeadLinkAppend(link);
        }
    }

    protected getLicense() : string {
        return `
<!--

Copyright 2017-2019 NXP
Copyright ${StringUtils.YearFrom(2019)} Oidis

SPDX-License-Identifier: BSD-3-Clause
The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution

or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText

-->
<!-- version: ${this.getEnvironmentArgs().getProjectVersion()}, build: ${this.getEnvironmentArgs().getBuildTime()} -->
`;
    }

    protected getPageLoader() : string {
        return `
            <div class="container-fluid d-flex flex-column h-100 PageLoader">
                <div class="row align-items-center" style="text-align: center;height: 100%;">
                    <div class="col Logo"><img class="img-fluid d-xl-flex" src="resource/graphics/Io/Oidis/Hub/Gui/LogoSymbol.png" style="margin: auto;" /></div>
                </div>
            </div>
            `;
    }

    protected getResponseStatus() : number {
        return Loader.getInstance().getStatePool().State() !== Loader.getInstance().getStatePool().EndState() ?
            503 : HttpStatusType.SUCCESS;
    }
}

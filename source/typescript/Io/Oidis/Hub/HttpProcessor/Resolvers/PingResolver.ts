/*! ******************************************************************************************************** *
 *
 * Copyright 2018 NXP
 * Copyright 2019-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { HttpStatusType } from "@io-oidis-commons/Io/Oidis/Commons/Enums/HttpStatusType.js";
import { StringUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/StringUtils.js";
import { BaseHttpResolver } from "@io-oidis-localhost/Io/Oidis/Localhost/HttpProcessor/Resolvers/BaseHttpResolver.js";

export class PingResolver extends BaseHttpResolver {

    public Process() : void {
        this.resolver();
    }

    protected resolver() : void {
        let url : string = this.getRequest().getUrl();
        if (StringUtils.Contains(url, "?")) {
            url = StringUtils.Substring(url, 0, StringUtils.IndexOf(url, "?"));
        }
        switch (url) {
        case "/ping.txt":
            this.getConnector().Send({
                body   : "",
                headers: {
                    "content-length": 0,
                    "content-type"  : "text/plain"
                },
                status : HttpStatusType.SUCCESS
            });
            break;
        case  "/ping.png":
            const Readable : any = require("stream").Readable; // eslint-disable-line no-case-declarations
            const body : any = new Readable(); // eslint-disable-line no-case-declarations
            body._read = () : void => {
                // mock _read
            };
            body.push(Buffer.from(
                "iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAIAAACQd1PeAAAACXBIWXMAAA7DAAAOwwHHb6hkAAAAB3RJTUUH4gUHBDgSIUDCjwAAAAxJREFUCNdj+P" +
                "//PwAF/gL+3MxZ5wAAAABJRU5ErkJggg==", "base64"));
            body.push(null);
            this.getConnector().Send({
                body,
                headers: {
                    "content-length": 118,
                    "content-type"  : "image/png"
                },
                status : HttpStatusType.SUCCESS
            });
            break;
        default:
            this.getConnector().Send({
                body  : "Unrecognized ping request.",
                status: HttpStatusType.NOT_FOUND
            });
            break;
        }
    }
}

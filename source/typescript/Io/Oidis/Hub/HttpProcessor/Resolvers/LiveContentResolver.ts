/*! ******************************************************************************************************** *
 *
 * Copyright 2010-2013 Jakub Cieslar
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2019 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { LiveContentResolver as Parent } from "@io-oidis-localhost/Io/Oidis/Localhost/HttpProcessor/Resolvers/LiveContentResolver.js";
import { Loader } from "../../Loader.js";

export class LiveContentResolver extends Parent {

    protected isAuthorizedFor($token : string, $method : string, $callback : ($status : boolean) => void) : void {
        Loader.getInstance().getAuthManager().IsAuthorizedFor($method, $token)
            .then(($status : boolean) : void => {
                $callback($status);
            })
            .catch(() : void => {
                $callback(false);
            });
    }
}

/*! ******************************************************************************************************** *
 *
 * Copyright 2020-2023 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { RESTResolver as Parent } from "@io-oidis-localhost/Io/Oidis/Localhost/HttpProcessor/Resolvers/RESTResolver.js";
import { User } from "../../DAO/Models/User.js";
import { Loader } from "../../Loader.js";
import { AuthManager } from "../../Utils/AuthManager.js";

// TODO(mkelnar) BaseBearerResolver should be replaced by plug-in able driver to allow also other auth methods
//  into REST api, like basic, digest, ...; sou rest resolver will then ask this driver in accessValidator() based on selected
//  auth method type
// TODO(mkelnar) there should be some validation for HTTPS protocol only because using authenticated REST over unsecured
//  protocol doesn't make sense (only for testing purposes)
export class RESTResolver extends Parent {
    private user : User;
    private readonly auth : AuthManager;

    constructor() {
        super();
        this.auth = Loader.getInstance().getAuthManager();
    }

    protected async accessValidator() : Promise<boolean> {
        this.user = null;
        if (await super.accessValidator()) {
            const token : string = this.getCurrentToken();
            if (await this.auth.IsAuthenticated(token)) {
                this.user = await this.auth.getUser(token);
            } else {
                const serviceAccount : User = Loader.getInstance().getServiceAccount();
                const tempToken : string = await this.auth.Authenticate(serviceAccount.Id, token);
                if (!ObjectValidator.IsEmptyOrNull(tempToken)) {
                    this.user = serviceAccount;
                    this.getCurrentToken = () : string => {
                        return tempToken;
                    };
                }
            }
            return true;
        }
        return false;
    }

    protected async isAuthorizedFor($token : string, $method : string) : Promise<boolean> {
        return this.auth.IsAuthorizedFor($method, $token);
    }

    protected getCurrentUser() : User {
        return this.user;
    }
}

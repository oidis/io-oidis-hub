/*! ******************************************************************************************************** *
 *
 * Copyright 2018-2019 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { StringUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/StringUtils.js";
import { HttpServer as Parent } from "@io-oidis-localhost/Io/Oidis/Localhost/HttpProcessor/HttpServer.js";
import { IRequestConnector } from "@io-oidis-localhost/Io/Oidis/Localhost/Interfaces/IRequestConnector.js";
import { IProject } from "../Interfaces/IProject.js";

export class HttpServer extends Parent {

    protected isRequestAllowed($request : any, $hostName : string, $protocol : string, $port : number, $ip? : string) : boolean {
        if (StringUtils.Contains($request.url, "/.git/", "/.svn", "/webdav/") ||
            StringUtils.EndsWith($request.url, ".jsp") ||
            StringUtils.EndsWith($request.url, ".cgi")) {
            return false;
        }
        return super.isRequestAllowed($request, $hostName, $protocol, $port, $ip);
    }

    protected resolveRequest($url : string, $data : any, $connector : IRequestConnector, $request : any, $keepAlive : boolean) : void {
        if (!ObjectValidator.IsEmptyOrNull($request.headers) &&
            !ObjectValidator.IsEmptyOrNull(this.getAppConfiguration().tunnels.server) &&
            ($request.headers.hasOwnProperty("host") || $request.headers.hasOwnProperty("x-forwarded-host"))) {
            let host : string =
                $request.headers.hasOwnProperty("host") ? $request.headers.host : $request.headers["x-forwarded-host"];
            if (StringUtils.ContainsIgnoreCase(host, ":")) {
                host = StringUtils.Substring(host, 0, StringUtils.IndexOf(host, ":"));
            }
            if (this.getAppConfiguration().tunnels.server.hasOwnProperty(host)) {
                if ($url !== "/") {
                    $url = "/forward/continue";
                } else {
                    $url = "/forward";
                }
            }
        }
        super.resolveRequest($url, $data, $connector, $request, $keepAlive);
    }

    protected getAppConfiguration() : IProject {
        return <IProject>super.getAppConfiguration();
    }
}

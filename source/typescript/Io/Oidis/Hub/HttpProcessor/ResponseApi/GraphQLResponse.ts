/*! ******************************************************************************************************** *
 *
 * Copyright 2022-2023 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { HttpStatusType } from "@io-oidis-commons/Io/Oidis/Commons/Enums/HttpStatusType.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { RESTResponse } from "@io-oidis-localhost/Io/Oidis/Localhost/HttpProcessor/ResponseApi/Handlers/RESTResponse.js";

export class GraphQLResponse extends RESTResponse {

    public SendError($data : string | Error | Error[], $status : HttpStatusType, $headers? : any) {
        const {GraphQLError} = require("graphql");

        let errors : any[];
        if (!ObjectValidator.IsArray($data)) {
            let message : string;
            if (!ObjectValidator.IsString($data) && $data.hasOwnProperty("stack")) {
                message = (<Error>$data).message;
            } else if (ObjectValidator.IsString($data)) {
                message = <string>$data;
            }
            if (!ObjectValidator.IsEmptyOrNull(message)) {
                errors = [new GraphQLError(message)];
            } else {
                errors = [$data];
            }
        } else {
            errors = <any[]>$data;
        }
        const response : any = {
            body  : JSON.stringify({errors}),
            status: $status
        };
        if (!ObjectValidator.IsEmptyOrNull($headers)) {
            response.headers = $headers;
        }
        this.owner.Send(response);
    }

    public OnError($message : string | Error, ...$args) {
        this.SendError($message, HttpStatusType.ERROR);
    }
}

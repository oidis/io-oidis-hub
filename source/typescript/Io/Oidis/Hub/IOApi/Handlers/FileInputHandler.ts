/*! ******************************************************************************************************** *
 *
 * Copyright 2023 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { IFileTransferProtocol } from "@io-oidis-gui/Io/Oidis/Gui/Interfaces/Components/IFileUpload.js";
import { BaseConnector } from "@io-oidis-localhost/Io/Oidis/Localhost/Primitives/BaseConnector.js";
import { Extern } from "@io-oidis-localhost/Io/Oidis/Localhost/Primitives/Decorators.js";

export class FileInputHandler extends BaseConnector {
    private static uploadedData : any = {};

    constructor() {
        super();
    }

    @Extern()
    public async Upload($data : IFileTransferProtocol) : Promise<boolean> {
        if (ObjectValidator.IsString($data.data)) {
            $data.data = <any>Buffer.from($data.data, "base64");
        }
        if (!FileInputHandler.uploadedData.hasOwnProperty($data.path)) {
            FileInputHandler.uploadedData[$data.path] = [];
        }
        if ($data.index === 0) {
            FileInputHandler.uploadedData[$data.path] = [];
        }
        FileInputHandler.uploadedData[$data.path].push(<any>$data.data);
        return true;
    }

    public getData($path : string, $once : boolean = true) : Buffer {
        if (FileInputHandler.uploadedData.hasOwnProperty($path)) {
            const data : Buffer = Buffer.concat(FileInputHandler.uploadedData[$path]);
            if ($once) {
                delete FileInputHandler.uploadedData[$path];
            }
            return data;
        }
        return null;
    }
}

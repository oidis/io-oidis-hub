/*! ******************************************************************************************************** *
 *
 * Copyright 2023-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { BaseObject } from "@io-oidis-commons/Io/Oidis/Commons/Primitives/BaseObject.js";
import { JsonUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/JsonUtils.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { StringUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/StringUtils.js";
import { IExportStatus } from "@io-oidis-hub-gui/Io/Oidis/Hub/Gui/Interfaces/IExportStatus.js";
import { FileSystemHandler } from "@io-oidis-localhost/Io/Oidis/Localhost/Connectors/FileSystemHandler.js";
import { IFetchProgress } from "@io-oidis-services/Io/Oidis/Services/Primitives/BaseConnector.js";
import { Loader } from "../../Loader.js";

export class CSVHandler extends BaseObject {
    private filesystem : FileSystemHandler;
    private csv : any;
    private iconv : any;

    constructor() {
        super();
        this.filesystem = Loader.getInstance().getFileSystemHandler();
        this.csv = require("csv");
        this.iconv = require("iconv-lite");
    }

    public async Import($pathOrData : string | Buffer, $options? : ICSVImportOptions) : Promise<any[]> {
        if (ObjectValidator.IsString($pathOrData) && !this.filesystem.Exists(<string>$pathOrData)) {
            throw new Error("Source file has not been found");
        }
        $options = JsonUtils.Extend(<ICSVImportOptions>{
            progress        : null,
            encoding        : "win1250",
            delimiter       : ";",
            skipEmptyLines  : true,
            relaxColumnCount: true
        }, $options);
        return (new Promise<any>(
            ($resolve : ($data : any) => void, $reject : ($error : Error) => void) : void => {
                if (ObjectValidator.IsString($pathOrData)) {
                    $pathOrData = this.filesystem.Read(<string>$pathOrData);
                }
                const lines : number = !ObjectValidator.IsEmptyOrNull($options.progress) ?
                    StringUtils.OccurrenceCount($pathOrData.toString(), "\n") - 1 : 0;
                this.csv.parse(this.iconv.decode($pathOrData, $options.encoding), {
                    columns           : true,
                    delimiter         : $options.delimiter,
                    onRecord          : ($record : any, $context : any) : any => {
                        if (!ObjectValidator.IsEmptyOrNull($options.progress)) {
                            $options.progress({index: $context.records, from: lines});
                        }
                        return $record;
                    },
                    skip_empty_lines  : $options.skipEmptyLines,
                    relax_column_count: $options.relaxColumnCount
                }, ($error : Error, $data : any) : void => {
                    if (ObjectValidator.IsEmptyOrNull($error)) {
                        $resolve($data);
                    } else {
                        $reject($error);
                    }
                });
            }));
    }

    public async Export($data : ICSVExportData, $progress : ($data : IFetchProgress) => void,
                        $asStream : boolean = true) : Promise<IExportStatus> {
        const retVal : IExportStatus = {id: "", isPartial: false};
        if ($data.rows.length > 0) {
            const exportId : string = StringUtils.getSha1(new Date().getTime().toString());
            if (!ObjectValidator.IsEmptyOrNull($data.isPartial)) {
                retVal.isPartial = $data.isPartial;
            }
            retVal.id = exportId;
            retVal.data = {
                asStream: $asStream,
                data    : await (new Promise<Buffer>(
                    ($resolve : ($data : any) => void, $reject : ($error : Error) => void) : void => {
                        this.csv.stringify($data.rows, {
                            columns  : $data.columns,
                            delimiter: ";",
                            header   : true
                        }, ($error : Error, $data : string) : void => {
                            if (ObjectValidator.IsEmptyOrNull($error)) {
                                $resolve(this.iconv.encode($data, "win1250"));
                            } else {
                                $reject($error);
                            }
                        });
                    })),
                encoding: "windows-1250",
                format  : "csv",
                name    : exportId + ".csv"
            };
            if ($asStream) {
                retVal.data.data = retVal.data.data.toString("base64");
            }
        }
        return retVal;
    }
}

export interface ICSVExportData {
    rows : any[];
    columns : string[];
    isPartial? : boolean;
}

export interface ICSVImportOptions {
    progress? : ($data : IFetchProgress) => void;
    delimiter? : string;
    skipEmptyLines? : boolean;
    relaxColumnCount? : boolean;
    encoding? : string;
}

// generated-code-start
export const ICSVExportData = globalThis.RegisterInterface(["rows", "columns", "isPartial"]);
export const ICSVImportOptions = globalThis.RegisterInterface(["progress", "delimiter", "skipEmptyLines", "relaxColumnCount", "encoding"]);
// generated-code-end

/*! ******************************************************************************************************** *
 *
 * Copyright 2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2018 NXP
 * Copyright 2019-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { HttpStatusType } from "@io-oidis-commons/Io/Oidis/Commons/Enums/HttpStatusType.js";
import { Convert } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Convert.js";
import { JsonUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/JsonUtils.js";
import { LogIt } from "@io-oidis-commons/Io/Oidis/Commons/Utils/LogIt.js";
import { ObjectEncoder } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectEncoder.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { StringUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/StringUtils.js";
import { IFileTransferProtocol, IRemoteFileTransferProtocol } from "@io-oidis-gui/Io/Oidis/Gui/Interfaces/Components/IFileUpload.js";
import { IRegistryItem } from "@io-oidis-hub-gui/Io/Oidis/Hub/Gui/Interfaces/IRegistryContent.js";
import { SelfupdatePackage } from "@io-oidis-hub-gui/Io/Oidis/Hub/Gui/Models/SelfupdatePackage.js";
import { FileSystemHandler } from "@io-oidis-localhost/Io/Oidis/Localhost/Connectors/FileSystemHandler.js";
import { FileTransferHandler } from "@io-oidis-localhost/Io/Oidis/Localhost/Connectors/FileTransferHandler.js";
import { HttpManager } from "@io-oidis-localhost/Io/Oidis/Localhost/HttpProcessor/HttpManager.js";
import { IFilePath } from "@io-oidis-localhost/Io/Oidis/Localhost/HttpProcessor/Resolvers/FileRequestResolver.js";
import { ResponseFactory } from "@io-oidis-localhost/Io/Oidis/Localhost/HttpProcessor/ResponseApi/ResponseFactory.js";
import { ExternResponseType } from "@io-oidis-localhost/Io/Oidis/Localhost/Interfaces/IExternOptions.js";
import { IFileInfo, IRemoteFileInfo } from "@io-oidis-localhost/Io/Oidis/Localhost/Interfaces/IFileInfo.js";
import { IResponse } from "@io-oidis-localhost/Io/Oidis/Localhost/Interfaces/IResponse.js";
import { Extern } from "@io-oidis-localhost/Io/Oidis/Localhost/Primitives/Decorators.js";
import { IModelListResult } from "@io-oidis-services/Io/Oidis/Services/Connectors/AuthManagerConnector.js";
import { SlackConnector } from "../Connectors/SlackConnector.js";
import { BuildPackage } from "../DAO/Models/BuildPackage.js";
import { Loader } from "../Loader.js";
import { BaseRegister } from "../Primitives/BaseRegister.js";

export class SelfupdatesManager extends BaseRegister<SelfupdatePackage> {
    private transferHandler : FileTransferHandler;

    @Extern(ExternResponseType.FULL_CONTROL)
    public static Download($appName : string, $releaseName : string = "", $platform : string = "", $version : string = null,
                           $callback? : IResponse) : void {
        if (!ObjectValidator.IsString($releaseName)) {
            $callback = <any>$releaseName;
            $releaseName = "";
            $platform = "";
            $version = "";
        } else if (!ObjectValidator.IsString($platform)) {
            $callback = <any>$platform;
            $platform = "";
            $version = "";
        } else if (!ObjectValidator.IsString($version)) {
            $callback = <any>$version;
            $version = "";
        }
        const fileSystem : FileSystemHandler = Loader.getInstance().getFileSystemHandler();
        this.getInstance<SelfupdatesManager>().getFileDescriptor($appName, $releaseName, $platform, $version)
            .then(($descriptor : ISelfupdatePackage) : void => {
                const filePath : string = $descriptor.path;
                if (!ObjectValidator.IsEmptyOrNull(filePath) && fileSystem.Exists(filePath)) {
                    ResponseFactory.getResponse($callback).Send(true, fileSystem.Read(filePath).toString());
                } else {
                    ResponseFactory.getResponse($callback).Send(false, "");
                }
            });
    }

    /**
     * @deprecated To be removed soon, please use Download() instead
     * This methods sends data without LiveContentProtocol.
     * @param {string} $appName Application name
     * @param {string} $releaseName Application release type
     * @param {string} $platform Application platform
     * @param {string} $version Application version
     * @param {Function} $callback Callback for LCP communication.
     * @returns {void}
     */
    @Extern(ExternResponseType.FULL_CONTROL)
    public static DownloadRaw($appName : string, $releaseName : string = "", $platform : string = "", $version : string = null,
                              $callback? : any) : void {
        if (!ObjectValidator.IsString($releaseName)) {
            $callback = $releaseName;
            $releaseName = "";
            $platform = "";
            $version = "";
        } else if (!ObjectValidator.IsString($platform)) {
            $callback = $platform;
            $platform = "";
            $version = "";
        } else if (!ObjectValidator.IsString($version)) {
            $callback = $version;
            $version = "";
        }
        const fileSystem : FileSystemHandler = Loader.getInstance().getFileSystemHandler();
        this.getInstance<SelfupdatesManager>().getFileDescriptor($appName, $releaseName, $platform, $version)
            .then((filePath : ISelfupdatePackage) : void => {
                if (!ObjectValidator.IsEmptyOrNull(filePath.path) && fileSystem.Exists(filePath.path)) {
                    const fs : any = require("fs");
                    const stats : any = fs.statSync(filePath.path);
                    const fileSize : number = stats.size;
                    const fileName : string = ObjectEncoder.Url(filePath.name);
                    const headers : any = {
                        "Pragma"                   : "public",
                        "Expires"                  : 0,
                        "Last-Modified"            : Convert.TimeToGMTformat(filePath.modified),
                        "Cache-Control"            : "private",
                        "Accept-Ranges"            : "bytes",
                        "Content-Encoding"         : "identity",
                        "Content-Transfer-Encoding": "binary",
                        "Content-Description"      : "File Transfer",
                        "Content-Length"           : fileSize,
                        "Content-Range"            : "0-" + (fileSize - 1) + "/" + fileSize,
                        "Content-Disposition"      : "attachment; filename=\"" + fileName + "\"",
                        "Content-Type"             : "application/octet-stream",
                        "Content-Version"          : filePath.version,
                        "x-content-version"        : filePath.version
                    };
                    $callback.owner.Send({
                        body     : require("fs").createReadStream(filePath.path),
                        headers,
                        normalize: false,
                        status   : HttpStatusType.SUCCESS
                    });
                } else {
                    $callback.owner.Send({
                        body  : "Unable to locate: " + (ObjectValidator.IsEmptyOrNull(filePath.path) ? filePath.url : filePath.path),
                        status: HttpStatusType.NOT_FOUND
                    });
                }
            });
    }

    constructor() {
        super();
        this.transferHandler = new FileTransferHandler();
    }

    @Extern(ExternResponseType.FULL_CONTROL)
    public Upload($data : IFileTransferProtocol, $callback? : ($status : boolean) => void | IResponse) : void {
        const protocol : IRemoteFileTransferProtocol = <IRemoteFileTransferProtocol>$data;
        protocol.cwd = this.getStoragePath();
        this.transferHandler.UploadFile(protocol, $callback);
    }

    @Extern(ExternResponseType.FULL_CONTROL)
    public Download($fileInfo : IFileInfo, $callback? : IResponse) : void {
        const info : IRemoteFileInfo = <IRemoteFileInfo>$fileInfo;
        info.cwd = this.getStoragePath();
        this.transferHandler.DownloadFile(info, $callback);
    }

    @Extern()
    public AcknowledgeChunk($chunkId : string) : void {
        this.transferHandler.AcknowledgeChunk($chunkId);
    }

    @Extern()
    public async Register($appName : string, $releaseName : string, $platform : string, $version : string, $buildTime : number,
                          $fileName : string, $type : string) : Promise<boolean> {
        $releaseName = ObjectValidator.IsEmptyOrNull($releaseName) || $releaseName === "null" ? null : $releaseName;
        $platform = ObjectValidator.IsEmptyOrNull($platform) || $platform === "null" ? null : $platform;
        $type = ObjectValidator.IsEmptyOrNull($type) || $type === "null" ? "zip" : $type;
        const fileSystem : FileSystemHandler = Loader.getInstance().getFileSystemHandler();
        const newPackage : BuildPackage = new BuildPackage();
        newPackage.Name = $appName;
        newPackage.Release = $releaseName;
        newPackage.Platform = $platform;
        newPackage.Version = $version;
        /// TODO: build time should be marked as deprecated -> API update later
        $buildTime = new Date().getTime();
        newPackage.BuildTime = $buildTime;
        newPackage.FileName = $fileName;
        newPackage.Type = $type;
        try {
            const models : BuildPackage[] = await BuildPackage.Find({
                name    : newPackage.Name,
                platform: newPackage.Platform,
                release : newPackage.Release,
                version : newPackage.Version
            });
            let packageExists : boolean = false;
            if (!ObjectValidator.IsEmptyOrNull(models)) {
                for await (const model of models) {
                    fileSystem.Delete(this.getStoragePath() + "/" + model.FileName);
                    model.BuildTime = newPackage.BuildTime;
                    model.FileName = newPackage.FileName;
                    model.Type = newPackage.Type;
                    await model.Save();
                    packageExists = true;
                }
            }
            if (!packageExists) {
                await newPackage.Save();
            }

            LogIt.Debug("Register selfupdate package {0}(\"{1}\",{2}) {3} {4}",
                $appName, $releaseName, $platform, $version, $buildTime);
            let productName : string = $appName;
            if (!ObjectValidator.IsEmptyOrNull($releaseName) && $releaseName !== "null") {
                productName += "[" + $releaseName + "]";
            }
            productName += "(" + $version + ")";
            const slackChannel : string = Loader.getInstance().getAppConfiguration().deploy.slackChannel;
            if (!ObjectValidator.IsEmptyOrNull(slackChannel)) {
                new SlackConnector().Notify(slackChannel, "New product *" + productName + "* has been released for: " + $platform);
            }
            return true;
        } catch (ex) {
            LogIt.Error(ex);
        }
        return false;
    }

    @Extern()
    public async UpdateExists($appName : string, $releaseName : string, $platform : string, $version : string, $buildTime : number,
                              $fixedVersion : boolean = false) : Promise<boolean> {
        $releaseName = ObjectValidator.IsEmptyOrNull($releaseName) || $releaseName === "null" ? null : $releaseName;
        $platform = ObjectValidator.IsEmptyOrNull($platform) || $platform === "null" ? null : $platform;
        LogIt.Debug("Validate selfupdate package {0}(\"{1}\",{2}) {3} {4}",
            $appName, $releaseName, $platform, $version, $buildTime);
        if ($fixedVersion) {
            LogIt.Warning("Required validation of selfupdate against fixed version.");
        }
        const models : BuildPackage[] = await BuildPackage.Find({
            name    : $appName,
            platform: $platform === "installer" ? "win32" : $platform,
            release : $releaseName
        });
        let exists : boolean = false;
        if (!ObjectValidator.IsEmptyOrNull(models)) {
            models.forEach(($package : BuildPackage) : boolean => {
                if ($package.Version === $version) {
                    exists = $package.BuildTime > $buildTime;
                } else if (!$fixedVersion && StringUtils.VersionIsLower($version, $package.Version)) {
                    exists = true;
                }
                return !exists;
            });
        }
        LogIt.Debug("Update exists: {0}", exists);
        return exists;
    }

    public async getFileDescriptor($appName : string, $releaseName : string = "", $platform : string = "",
                                   $version : string = null) : Promise<ISelfupdatePackage> {
        if (!ObjectValidator.IsString($releaseName) || ObjectValidator.IsEmptyOrNull($releaseName) || $releaseName === "null") {
            $releaseName = null;
        }
        if (!ObjectValidator.IsString($platform) || ObjectValidator.IsEmptyOrNull($platform) || $platform === "null") {
            $platform = null;
        }
        if (!ObjectValidator.IsString($version) || ObjectValidator.IsEmptyOrNull($version) || $version === "null") {
            $version = null;
        }
        LogIt.Debug("Download selfupdate package for {0}(\"{1}\",{2})", $appName, $releaseName, $platform);
        if (!ObjectValidator.IsEmptyOrNull($version)) {
            LogIt.Warning("Required download of selfupdate package for specific version \"" + $version + "\"");
        }
        const fileSystem : FileSystemHandler = Loader.getInstance().getFileSystemHandler();
        let filePath : string = "";
        let fileName : string = "";
        let type : string = "zip";
        let lastVersion : string = "";
        let lastModified : number = 0;
        const query : any = {
            name    : $appName,
            platform: $platform === "installer" ? "win32" : $platform,
            release : $releaseName
        };
        if (!ObjectValidator.IsEmptyOrNull($version)) {
            query.version = $version;
        }
        const models : BuildPackage[] = await BuildPackage.Find(query);
        if (!ObjectValidator.IsEmptyOrNull(models)) {
            models.forEach(($package : BuildPackage) : void => {
                if (ObjectValidator.IsEmptyOrNull(lastVersion) || StringUtils.VersionIsLower(lastVersion, $package.Version)) {
                    filePath = this.getStoragePath() + "/" + $package.FileName;
                    if (!ObjectValidator.IsEmptyOrNull($package.Type)) {
                        type = $package.Type;
                    }
                    fileName = $package.Name + "-" + StringUtils.Replace($package.Version, ".", "-") + "." + type;
                    lastVersion = $package.Version;
                    lastModified = $package.BuildTime;
                }
            });
        }
        if (ObjectValidator.IsString(lastModified)) {
            lastModified = StringUtils.ToInteger(<any>lastModified);
        }
        if (fileSystem.Exists(filePath)) {
            const fileSize : number = require("fs").statSync(filePath).size;
            LogIt.Debug("Transferring file \"{0}\" with size: {1}", filePath, fileSize + "");
            return {
                extension: type,
                modified : lastModified,
                name     : fileName,
                path     : filePath,
                url      : "",
                version  : lastVersion
            };
        } else {
            if (ObjectValidator.IsEmptyOrNull(filePath)) {
                LogIt.Debug("Required package does not exist.");
            } else {
                LogIt.Debug("Package file \"{0}\" does not exist.", filePath);
            }
            return {
                extension: "zip",
                modified : lastModified,
                name     : "",
                path     : "",
                url      : "",
                version  : lastVersion
            };
        }
    }

    public async getRegistryItems() : Promise<IRegistryItem[]> {
        const manager : HttpManager = Loader.getInstance().getHttpManager();
        const links : IRegistryItem[] = [];
        const models : BuildPackage[] = await BuildPackage.Find({});
        if (!ObjectValidator.IsEmptyOrNull(models)) {
            models.forEach(($package : BuildPackage) : void => {
                const platform : string = ObjectValidator.IsEmptyOrNull($package.Platform) ? "null" : $package.Platform;
                const release : string = ObjectValidator.IsEmptyOrNull($package.Release) ? "null" : $package.Release;
                let buildTime : any = $package.BuildTime;
                if (ObjectValidator.IsString(buildTime)) {
                    buildTime = StringUtils.ToInteger(buildTime);
                }
                const type : any = ObjectValidator.IsEmptyOrNull($package.Type) ? "zip" : $package.Type;

                links.push({
                    info    : "[" + Convert.TimeToGMTformat(buildTime) + ", " + type + "]",
                    link    : manager.CreateLink(
                        "/Update/" + $package.Name + "/" + release + "/" + platform + "/" + $package.Version),
                    name    : $package.Name,
                    platform: $package.Platform,
                    version : $package.Version
                });
            });
        }
        return links;
    }

    @Extern()
    public async FindPackages($options : IPackageFindOptions = null) : Promise<IModelListResult> {
        $options = JsonUtils.Extend({
            buildTime: null,
            limit    : -1,
            name     : "",
            offset   : 0,
            platform : "",
            release  : "",
            version  : ""
        }, $options);
        if (!ObjectValidator.IsEmptyOrNull($options.id)) {
            const model : BuildPackage = await BuildPackage.FindById($options.id);
            if (ObjectValidator.IsEmptyOrNull(model)) {
                throw new Error("Package with id: " + $options.id + " not found");
            }
            return {
                data  : [model],
                limit : $options.limit,
                offset: $options.offset,
                size  : 1
            };
        }
        const filter : any = {};
        if (!StringUtils.IsEmpty($options.name)) {
            filter.name = new RegExp($options.name);
        }
        if (!StringUtils.IsEmpty($options.release)) {
            filter.release = new RegExp($options.release);
        }
        if (!StringUtils.IsEmpty($options.platform)) {
            filter.platform = new RegExp($options.platform);
        }
        if (!StringUtils.IsEmpty($options.version)) {
            filter.version = new RegExp($options.version);
        }
        if (!ObjectValidator.IsEmptyOrNull($options.buildTime)) {
            if (ObjectValidator.IsString($options.buildTime)) {
                $options.buildTime = new Date($options.buildTime);
            }
            const stamp : number = $options.buildTime.getTime();
            filter.buildTime = {$gte: stamp};
        }
        const size : number = await BuildPackage.Model().countDocuments(filter);
        const models : BuildPackage[] = await BuildPackage.Find(filter, {
            limit : $options.limit,
            offset: $options.offset
        });
        return {
            data  : models,
            limit : $options.limit,
            offset: $options.offset,
            size
        };
    }

    @Extern()
    public async getDownloadLink($options : IPackageFindOptions = null, $relative : boolean = false) : Promise<string> {
        const models : IModelListResult = await this.FindPackages($options);
        if (models.size === 0) {
            throw new Error("No model found for specified find options.");
        } else if (models.size > 1) {
            throw new Error("Multiple models found for specified find options.");
        }
        const model : BuildPackage = models.data[0];
        const platform : string = ObjectValidator.IsEmptyOrNull(model.Platform) ? "null" : model.Platform;
        const release : string = ObjectValidator.IsEmptyOrNull(model.Release) ? "null" : model.Release;
        const relativeDir : string = Loader.getInstance().getHttpManager().CreateLink(
            "/Update/" + model.Name + "/" + release + "/" + platform + "/" + model.Version);

        if ($relative) {
            return relativeDir;
        }
        let uri : string = Loader.getInstance().getHttpManager().getRequest().getUri();
        if (StringUtils.EndsWith(uri, "/")) {
            uri = StringUtils.Substring(uri, 0, uri.length - 1);
        }
        return uri + relativeDir;
    }

    public async ToDatabase() : Promise<boolean> {
        try {
            const register : SelfupdatePackage[] = this.getRegister();
            for await (const item of register) {
                if (ObjectValidator.IsEmptyOrNull(await BuildPackage.Find(item))) {
                    const model : BuildPackage = new BuildPackage();
                    model.Name = item.name;
                    model.Release = item.release;
                    model.Platform = item.platform;
                    model.Version = item.version;
                    model.BuildTime = item.buildTime;
                    model.FileName = item.fileName;
                    model.Type = item.type;
                    await model.Save();
                } else {
                    LogIt.Debug("Package {0}[{1}] conversion skipped - package already exist", item.name, item.platform);
                }
            }
            Loader.getInstance().getFileSystemHandler().Rename(this.getRegisterPath(), this.getRegisterPath() + ".back");
            return true;
        } catch (ex) {
            LogIt.Error(ex);
        }
        return false;
    }
}

export interface ISelfupdatePackage extends IFilePath {
    version : string;
    modified : number;
}

export interface IPackageFindOptions {
    id? : string;
    offset? : number;
    limit? : number;
    name? : string;
    release? : string;
    platform? : string;
    version? : string;
    buildTime? : Date;
}

// generated-code-start
/* eslint-disable */
export const ISelfupdatePackage = globalThis.RegisterInterface(["version", "modified"], <any>IFilePath);
export const IPackageFindOptions = globalThis.RegisterInterface(["id", "offset", "limit", "name", "release", "platform", "version", "buildTime"]);
/* eslint-enable */
// generated-code-end

/*! ******************************************************************************************************** *
 *
 * Copyright 2010-2013 Jakub Cieslar
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2019 NXP
 * Copyright 2019-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { BaseEnum } from "@io-oidis-commons/Io/Oidis/Commons/Primitives/BaseEnum.js";
import { BaseObject } from "@io-oidis-commons/Io/Oidis/Commons/Primitives/BaseObject.js";
import { Partial } from "@io-oidis-commons/Io/Oidis/Commons/Primitives/Decorators.js";
import { LogIt } from "@io-oidis-commons/Io/Oidis/Commons/Utils/LogIt.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { StringUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/StringUtils.js";
import { ErrorLocalizable } from "@io-oidis-localhost/Io/Oidis/Localhost/Exceptions/Type/ErrorLocalizable.js";
import { SentryTunnelResolver } from "@io-oidis-localhost/Io/Oidis/Localhost/HttpProcessor/Resolvers/SentryTunnelResolver.js";
import { BaseConnector } from "@io-oidis-localhost/Io/Oidis/Localhost/Primitives/BaseConnector.js";
import { AuthManagerConnector } from "@io-oidis-services/Io/Oidis/Services/Connectors/AuthManagerConnector.js";
import { EnvironmentState } from "@io-oidis-services/Io/Oidis/Services/Enums/EnvironmentState.js";
import { AppsAuthManager } from "../AuthProcessor/AppsAuthManager.js";
import { AuthManagerHelper } from "../AuthProcessor/AuthManagerHelper.js";
import { AuthMethodCategory, AuthMethodsManager } from "../AuthProcessor/AuthMethodsManager.js";
import { AuthTokensManager } from "../AuthProcessor/AuthTokensManager.js";
import { BasicAuthenticationAPI } from "../AuthProcessor/BasicAuthenticationAPI.js";
import { GroupsAuthenticationAPI } from "../AuthProcessor/GroupsAuthenticationAPI.js";
import { GroupsManager } from "../AuthProcessor/GroupsManager.js";
import { OrganizationsManager } from "../AuthProcessor/OrganizationsManager.js";
import { TwoFactorAuthenticationAPI } from "../AuthProcessor/TwoFactorAuthenticationAPI.js";
import { UsersManager } from "../AuthProcessor/UsersManager.js";
import { LoggerConnector } from "../Connectors/LoggerConnector.js";
import { User } from "../DAO/Models/User.js";
import { IAgentsRegisterUser, IAuthManagerWorker } from "../Interfaces/IProject.js";
import { Loader } from "../Loader.js";
import { AgentsRegister } from "../Primitives/AgentsRegister.js";
import { AuthCodeManager } from "./AuthCodeManager.js";
import { ConnectorHub } from "./ConnectorHub.js";
import { EnvironmentHelper } from "./EnvironmentHelper.js";
import { MailEngine } from "./MailEngine.js";
import { WebTokenManager } from "./WebTokenManager.js";

/// TODO: specific userId can be specified only under correct permissions
//        -> this should be solved by AuthManager extension for admins
/// TODO: enable to use AuthManager adapters for ability to solve difference in user, app, service, etc. handling

/// TODO: account activation is required for any other Auth API other than LogIn and user profile
//        -> validation should be part of IsAuthorizedFor

@Partial(UsersManager, GroupsManager, OrganizationsManager, AppsAuthManager, AuthTokensManager, AuthMethodsManager,
    BasicAuthenticationAPI, TwoFactorAuthenticationAPI, GroupsAuthenticationAPI, AuthManagerHelper)
// DO NOT REMOVE @ts-ignore as it is intentional due to need for register of protected methods from Partial classes below
export class AuthManager extends BaseConnector {

    public getUser = UsersManager.prototype.getUser;
    public getUserId = UsersManager.prototype.getUserId;
    public Register = UsersManager.prototype.Register;
    public ActivateUser = UsersManager.prototype.ActivateUser;
    public DeactivateUser = UsersManager.prototype.DeactivateUser;
    public ResetPassword = UsersManager.prototype.ResetPassword;
    public ResetPasswordConfirm = UsersManager.prototype.ResetPasswordConfirm;
    public ChangePassword = UsersManager.prototype.ChangePassword;
    public RemoveUser = UsersManager.prototype.RemoveUser;
    public DeleteUser = UsersManager.prototype.DeleteUser;
    public RestoreUser = UsersManager.prototype.RestoreUser;
    public FindUsers = UsersManager.prototype.FindUsers;
    public getUserProfile = UsersManager.prototype.getUserProfile;
    public setUserProfile = UsersManager.prototype.setUserProfile;

    public CreateGroup = GroupsManager.prototype.CreateGroup;
    public FindGroups = GroupsManager.prototype.FindGroups;
    public DeleteGroup = GroupsManager.prototype.DeleteGroup;
    public AddGroup = GroupsManager.prototype.AddGroup;
    public getGroups = GroupsManager.prototype.getGroups;
    public getActiveGroupsList = GroupsManager.prototype.getActiveGroupsList;
    public RemoveGroup = GroupsManager.prototype.RemoveGroup;
    public RestoreGroup = GroupsManager.prototype.RestoreGroup;
    public getGroupMethods = GroupsManager.prototype.getGroupMethods;
    public ResetDefaultGroups = GroupsManager.prototype.ResetDefaultGroups;
    public DeleteRedundantGroups = GroupsManager.prototype.DeleteRedundantGroups;
    public getSystemGroups = GroupsManager.prototype.getSystemGroups;
    public SyncSystemGroups = GroupsManager.prototype.SyncSystemGroups;
    public HasGroup = GroupsManager.prototype.HasGroup;

    public RegisterOrganization = OrganizationsManager.prototype.RegisterOrganization;
    public setOrganizationProfile = OrganizationsManager.prototype.setOrganizationProfile;
    public getOrganizationProfile = OrganizationsManager.prototype.getOrganizationProfile;
    public getOrganizationChart = OrganizationsManager.prototype.getOrganizationChart;
    public AddUserToOrganization = OrganizationsManager.prototype.AddUserToOrganization;
    public RemoveUserFromOrganization = OrganizationsManager.prototype.RemoveUserFromOrganization;
    public FindOrganizations = OrganizationsManager.prototype.FindOrganizations;
    public DeactivateOrganization = OrganizationsManager.prototype.DeactivateOrganization;
    public DeleteOrganization = OrganizationsManager.prototype.DeleteOrganization;
    public RemoveOrganization = OrganizationsManager.prototype.RemoveOrganization;
    public RestoreOrganization = OrganizationsManager.prototype.RestoreOrganization;

    public GenerateAuthToken = AuthTokensManager.prototype.GenerateAuthToken;
    public IsAuthTokenValid = AuthTokensManager.prototype.IsAuthTokenValid;
    public getAuthToken = AuthTokensManager.prototype.getAuthToken;
    public getAuthTokenInfo = AuthTokensManager.prototype.getAuthTokenInfo;
    public RevokeAuthToken = AuthTokensManager.prototype.RevokeAuthToken;
    public UpdateAuthToken = AuthTokensManager.prototype.UpdateAuthToken;

    public getAuthorizedMethods = AuthMethodsManager.prototype.getAuthorizedMethods;
    public getAuthorizedMethodsFor = AuthMethodsManager.prototype.getAuthorizedMethodsFor;
    public setAuthorizedMethods = AuthMethodsManager.prototype.setAuthorizedMethods;
    public ExportAuthMethods = AuthMethodsManager.prototype.ExportAuthMethods;
    public ImportAuthMethods = AuthMethodsManager.prototype.ImportAuthMethods;
    public ResetDefaultAuthMethods = AuthMethodsManager.prototype.ResetDefaultAuthMethods;

    public RegisterApp = AppsAuthManager.prototype.RegisterApp;
    public getAppMethods = AppsAuthManager.prototype.getAppMethods;
    public getAllAppsMethods = AppsAuthManager.prototype.getAllAppsMethods;

    public LogIn = BasicAuthenticationAPI.prototype.LogIn;
    public LogOut = BasicAuthenticationAPI.prototype.LogOut;
    public IsAuthenticated = BasicAuthenticationAPI.prototype.IsAuthenticated;
    public IsAuthorizedFor = BasicAuthenticationAPI.prototype.IsAuthorizedFor;
    public Authenticate = BasicAuthenticationAPI.prototype.Authenticate;
    public IsPasswordValid = BasicAuthenticationAPI.prototype.IsPasswordValid;

    public Generate2FAKey = TwoFactorAuthenticationAPI.prototype.Generate2FAKey;
    public Validate2FA = TwoFactorAuthenticationAPI.prototype.Validate2FA;
    public Has2FAActivated = TwoFactorAuthenticationAPI.prototype.Has2FAActivated;

    public AuthenticateAdmin = GroupsAuthenticationAPI.prototype.AuthenticateAdmin;
    public RegisterAdmin = GroupsAuthenticationAPI.prototype.RegisterAdmin;
    public SyncServiceAccounts = GroupsAuthenticationAPI.prototype.SyncServiceAccounts;
    public getServiceAccounts = GroupsAuthenticationAPI.prototype.getServiceAccounts;
    public SwitchToAccount = GroupsAuthenticationAPI.prototype.SwitchToAccount;

    public CheckActivation = AuthManagerHelper.prototype.CheckActivation;
    public CheckResetTokens = AuthManagerHelper.prototype.CheckResetTokens;
    public CheckEmailDuplicity = AuthManagerHelper.prototype.CheckEmailDuplicity;
    public getPermissionsReport = AuthManagerHelper.prototype.getPermissionsReport;
    public ResetCache = AuthManagerHelper.prototype.ResetCache;

    /**
     * Default accessible methods for everyone hardcoded in code
     */
    protected readonly defaultAccessibleMethods : string[];
    /**
     * Default accessible methods for groups (login required) hardcoded in code
     */
    protected readonly defaultGroupAccess : string[];
    protected readonly emailSender : MailEngine;
    protected readonly tokens : WebTokenManager;
    protected readonly config : IAuthManagerWorker;
    protected authCache : any;
    protected readonly systemPermissions : any;
    protected readonly systemAccounts : any;

    // @ts-expect-error: override of partial protected API
    protected processSystemGroups = GroupsManager.prototype.processSystemGroups; // @ts-expect-error
    protected getGroupInstance = GroupsManager.prototype.getGroupInstance; // @ts-expect-error
    protected getGroupByName = GroupsManager.prototype.getGroupByName;

    // @ts-expect-error: override of partial protected API
    protected getOrganizationInstance = OrganizationsManager.prototype.getOrganizationInstance; // @ts-expect-error
    protected processOrganizationData = OrganizationsManager.prototype.processOrganizationData; // @ts-expect-error
    protected normalizeOrganizationModel = OrganizationsManager.prototype.normalizeOrganizationModel;

    // @ts-expect-error: override of partial protected API
    protected checkAuthToken = AuthTokensManager.prototype.checkAuthToken; // @ts-expect-error
    protected generateAuthMethodsFrom = AuthMethodsManager.prototype.generateAuthMethodsFrom; // @ts-expect-error
    protected generateAuthMethods = AuthMethodsManager.prototype.generateAuthMethods; // @ts-expect-error
    protected authMethodsToPatterns = AuthMethodsManager.prototype.authMethodsToPatterns;

    // @ts-expect-error: override of partial protected API
    protected addSystemPermissions = AuthManagerHelper.prototype.addSystemPermissions; // @ts-expect-error
    protected normalizeDate = AuthManagerHelper.prototype.normalizeDate; // @ts-expect-error
    protected getClassFromInstance = AuthManagerHelper.prototype.getClassFromInstance; // @ts-expect-error
    protected getRandomSha = AuthManagerHelper.prototype.getRandomSha; // @ts-expect-error
    protected getLocalizationsFor = AuthManagerHelper.prototype.getLocalizationsFor; // @ts-expect-error
    protected getGravatar = AuthManagerHelper.prototype.getGravatar; // @ts-expect-error
    protected getExpireHeader = AuthManagerHelper.prototype.getExpireHeader; // @ts-expect-error
    protected createPermissionBackup = AuthManagerHelper.prototype.createPermissionBackup; // @ts-expect-error
    protected validateFeaturesFormat = AuthManagerHelper.prototype.validateFeaturesFormat;

    // @ts-expect-error: override of partial protected API
    protected getUserInstance = UsersManager.prototype.getUserInstance; // @ts-expect-error
    protected getUserBy = UsersManager.prototype.getUserBy; // @ts-expect-error
    protected getUserByName = UsersManager.prototype.getUserByName; // @ts-expect-error
    protected getUserByEmail = UsersManager.prototype.getUserByEmail; // @ts-expect-error
    protected getUsersByEmail = UsersManager.prototype.getUsersByEmail; // @ts-expect-error
    protected getUserDocument = UsersManager.prototype.getUserDocument; // @ts-expect-error
    protected processUserData = UsersManager.prototype.processUserData;

    // @ts-expect-error: override of partial protected API
    protected getAuthCodeManagerInstance = TwoFactorAuthenticationAPI.prototype.getAuthCodeManagerInstance;

    // @ts-expect-error: override of partial protected API
    protected addServiceAccount = GroupsAuthenticationAPI.prototype.addServiceAccount;

    // @ts-expect-error: override of partial private API
    private getSystemGroupsInterface = GroupsManager.prototype.getSystemGroupsInterface;

    // @ts-expect-error: override of partial private API
    private ssoValidateToken = BasicAuthenticationAPI.prototype.ssoValidateToken; // @ts-expect-error
    private ssoLogout = BasicAuthenticationAPI.prototype.ssoLogout;

    /**
     * @deprecated IsSupported method is no longer applicable for AuthManger.
     * AuthManager is must have for Hub functionality and can not be turned off
     * @returns {boolean} Returns true if AuthManager is supported otherwise false.
     */
    public static IsSupported() : boolean {
        return true;
    }

    public static getClient() : AuthManagerConnector {
        const instance : AuthManagerConnector =
            new AuthManagerConnector(true, Loader.getInstance().getHttpManager().CreateLink("/connector.config.jsonp"));
        AuthManager.getClient = () : AuthManagerConnector => {
            return instance;
        };
        return instance;
    }

    constructor() {
        super();
        this.config = Loader.getInstance().getAppConfiguration().authManagerWorker;
        this.emailSender = Loader.getInstance().getMailEngine();
        this.tokens = new WebTokenManager(this.config.multiSessionEnabled);
        this.defaultAccessibleMethods = [];
        this.defaultGroupAccess = [];
        this.systemPermissions = {};
        this.systemAccounts = {};
        this.authCache = {};
        this.validateFeaturesFormat();

        this.generateAuthMethodsFrom<this>(AuthMethodCategory.FOR_ALL_USERS,
            [this, AuthManager], (proto) => [
                proto.LogIn,
                proto.LogOut,
                proto.IsAuthorizedFor,
                proto.IsAuthenticated,
                proto.getAuthorizedMethods,
                proto.Register,
                proto.RegisterOrganization,
                proto.ResetPassword,
                proto.ResetPasswordConfirm,
                proto.ActivateUser,
                proto.getSystemGroupsInterface,
                proto.HasGroup,
                proto.ssoLogout,
                proto.ssoValidateToken,
                proto.AuthenticateAdmin
            ]);

        this.generateAuthMethodsFrom<EnvironmentHelper>(AuthMethodCategory.FOR_ALL_USERS,
            [Loader.getInstance().getEnvironmentHelper(), EnvironmentHelper], (proto) => [
                proto.getEnvType,
                proto.InitPageEnabled,
                proto.IsEnvironmentLoaded,
                proto.AddLoadingListener,
                proto.getReleaseNotesConfig,
                proto.getReleaseNotesFor,
                proto.getVersion,
                proto.getBuildTime,
                proto.getUpTime,
                proto.getFAQTopics,
                proto.AttachToPushNotifications,
                proto.getAppSettings
            ]);

        // todo(mkelnar) do not forget to review these methods below after backend LogIn integration!!!
        // TODO: validate if ConnectorHub and AgentsRegister can not be solved by namespaces aliases?
        this.generateAuthMethodsFrom<ConnectorHub>(AuthMethodCategory.FOR_ALL_USERS,
            [ConnectorHub, AgentsRegister], (proto) => [
                proto.RegisterAgent,
                proto.ForwardMessage,
                proto.ForwardRequest,
                proto.UpdateAgentStatus
            ]);

        this.generateAuthMethodsFrom<LoggerConnector>(AuthMethodCategory.FOR_ALL_USERS,
            LoggerConnector, (proto) => [proto.ProcessTrace]);
        this.generateAuthMethodsFrom<SentryTunnelResolver>(AuthMethodCategory.FOR_ALL_USERS,
            SentryTunnelResolver, (proto) => [proto.Proxy]);

        this.generateAuthMethodsFrom<this>(AuthMethodCategory.ONLY_FOR_AUTHORIZED_USERS,
            [this, AuthManager], (proto) => [
                proto.getUserProfile,
                proto.setUserProfile,
                proto.getOrganizationProfile,
                proto.GenerateAuthToken,
                proto.getAuthTokenInfo,
                proto.RevokeAuthToken,
                proto.UpdateAuthToken,
                proto.Generate2FAKey,
                proto.Validate2FA,
                proto.Has2FAActivated
            ]);
        this.generateAuthMethodsFrom<EnvironmentHelper>(AuthMethodCategory.ONLY_FOR_AUTHORIZED_USERS,
            EnvironmentHelper, (proto) => [proto.SendFeedback]);
        this.generateAuthMethodsFrom<AuthCodeManager>(AuthMethodCategory.ONLY_FOR_AUTHORIZED_USERS,
            AuthCodeManager, (proto) => [
                proto.RequestAuthCode,
                proto.Validate
            ]);

        this.addSystemPermissions("Admin", "*");
        this.addSystemPermissions("Agent",
            "*", /// TODO: this must be more strict!!
            ...this.generateAuthMethodsFrom<ConnectorHub>([ConnectorHub, AgentsRegister], (proto) => [
                proto.getAgentHandshakeToken
            ])
        );
        this.addSystemPermissions("OrganizationOwner", AuthFeatures.ORGANIZATION_OWNER,
            ...this.generateAuthMethodsFrom<this>(this, (proto) => [
                proto.setOrganizationProfile,
                proto.getOrganizationChart,
                proto.FindUsers,
                proto.AddUserToOrganization,
                proto.RemoveUserFromOrganization,
                proto.RemoveGroup,
                proto.AddGroup
            ])
        );
        this.addSystemPermissions("Member", AuthFeatures.ORGANIZATION_MEMBER);
        this.addSystemPermissions("SysAdmin", AuthFeatures.SYSADMIN);
        this.addSystemPermissions("AuthAdmin", AuthFeatures.TOKEN_MANAGER);

        const agentsRegister : IAgentsRegisterUser = Loader.getInstance().getAppConfiguration().agentsRegisterUser;
        this.addServiceAccount("AgentsRegister", {
            user : agentsRegister.user,
            pass : agentsRegister.pass,
            group: "Agent",
            token: {
                enabled     : true,
                name        : "agents-registration-token",
                noExpiration: true
            }
        });
        this.addServiceAccount("Contributor", {
            group: "SysAdmin",
            user : "contributor"
        });
        this.addServiceAccount("Viewer", {
            user: "viewer"
        });
    }

    public setOnTokenExpireHandler($handler : ($token : string) => void) : void {
        this.tokens.setOnTokenExpireHandler($handler);
    }

    protected getTokenManager() : WebTokenManager {
        return this.tokens;
    }

    protected getAuthFeatures() : string[] {
        return AuthFeatures.getProperties().map(($key : string) : string => AuthFeatures[$key]);
    }
}

export abstract class AuthManagerNotification extends BaseEnum {
    public static readonly MissingName : string = "MissingName";
    public static readonly MissingEmail : string = "MissingEmail";
    public static readonly UserExist : string = "UserExist";
    public static readonly EmailSendFailure : string = "EmailSendFailure";
    public static readonly MissingOrganizationName : string = "MissingOrgName";
    public static readonly OrganizationExist : string = "UserExist";
    public static readonly MissingCurrentUser : string = "MissingCurrentUser";
}

export abstract class AuthFeatures extends BaseEnum {
    public static readonly TOKEN_MANAGER : string = "#TokenManager";
    public static readonly SYSADMIN : string = "#SysAdmin";
    public static readonly ORGANIZATION_MEMBER : string = "#OrganizationMember";
    public static readonly ORGANIZATION_OWNER : string = "#OrganizationOwner";
}

export class AdminInitHelper extends BaseObject {
    private adminExists : boolean;
    private rootUser : User;
    private auth : AuthManager;

    constructor() {
        super();

        this.rootUser = null;
        this.adminExists = false;
        this.auth = Loader.getInstance().getAuthManager();
    }

    public IsRequired() : boolean {
        return !this.adminExists;
    }

    public async CreateToken() : Promise<boolean> {
        const rootName : string = "__RootUser__";
        const rootUsers : User[] = await User.Find({userName: rootName}, {partial: true});
        if (!ObjectValidator.IsEmptyOrNull(rootUsers)) {
            await this.auth.RemoveUser(rootUsers.map(($user : User) : string => {
                return $user.Id;
            }));
        }
        this.adminExists = !ObjectValidator.IsEmptyOrNull(await User.Find({
            deleted: {$ne: true},
            groups : (await this.auth.getSystemGroups()).Admin.Id
        }, {partial: true}));
        LogIt.Debug("Creating default root user required for activation of admin account...");
        const rootPass : string = this.getUID();
        if (await this.auth.Register({
            name          : rootName,
            password      : StringUtils.getSha1(rootPass),
            withActivation: false,
            withInvitation: false
        })) {
            if (!await this.auth.AddGroup((await this.auth.getSystemGroups()).Admin, rootName)) {
                LogIt.Warning("Failed to set default role - using default accessible methods.");
            }
            LogIt.Info("\n" +
                "--------------------------------------------------\n" +
                "Authorization token for backdoor url /#/AdminInit:\n" +
                rootPass + "\n" +
                "--------------------------------------------------\n");
            this.rootUser = await this.auth.getUser(rootName);
            return true;
        }
        return false;
    }

    public async Authenticate($authToken : string) : Promise<string> {
        if (!ObjectValidator.IsEmptyOrNull(this.rootUser)) {
            return this.auth.LogIn(this.rootUser.UserName, $authToken);
        }
        return null;
    }

    public async Register($name : string, $pass? : string) : Promise<void> {
        if (!ObjectValidator.IsEmptyOrNull($name) && !ObjectValidator.IsEmptyOrNull($pass)) {
            await this.auth.Register({
                name           : $name,
                password       : $pass,
                throwExceptions: true,
                withActivation : false,
                withInvitation : false
            });
        }
        const user : User = await this.auth.getUser($name);
        if (!ObjectValidator.IsEmptyOrNull(user)) {
            if (await this.auth.AddGroup((await this.auth.getSystemGroups()).Admin, user)) {
                await this.auth.ChangePassword(this.getUID(), this.rootUser.Id);
                this.adminExists = true;
                Loader.getInstance().getStatePool().State(EnvironmentState.ADMIN_ENABLED);
                setTimeout(() : void => {
                    Loader.getInstance().getStatePool().State(Loader.getInstance().getStatePool().EndState());
                    setTimeout(() : void => {
                        LogIt.Warning("Server has been stopped due to Admin init process.");
                        Loader.getInstance().Exit();
                    }, 2000);
                }, 10000);
            } else {
                throw new ErrorLocalizable("Failed to add Admin group to required user");
            }
        } else {
            throw new ErrorLocalizable("Specified user for admin enablement has not been found");
        }
    }
}

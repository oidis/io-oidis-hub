/*! ******************************************************************************************************** *
 *
 * Copyright 2023 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { ArrayList } from "@io-oidis-commons/Io/Oidis/Commons/Primitives/ArrayList.js";
import { BaseObject } from "@io-oidis-commons/Io/Oidis/Commons/Primitives/BaseObject.js";
import { LogIt } from "@io-oidis-commons/Io/Oidis/Commons/Utils/LogIt.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { IPushNotificationEvent } from "@io-oidis-hub-gui/Io/Oidis/Hub/Gui/Connectors/EnvironmentConnector.js";
import { PushNotificationType } from "@io-oidis-hub-gui/Io/Oidis/Hub/Gui/Enums/PushNotificationType.js";
import { IResponse } from "@io-oidis-localhost/Io/Oidis/Localhost/Interfaces/IResponse.js";

export class PushNotifications extends BaseObject {
    private readonly handlers : ArrayList<IResponse>;
    private lastHeaderEvent : IPushNotificationEvent;

    constructor() {
        super();
        this.handlers = new ArrayList<IResponse>();
        this.lastHeaderEvent = null;
    }

    public Push($value : IPushNotificationEvent) : void {
        if ($value.type === PushNotificationType.HEADER_CHANGE || $value.type === PushNotificationType.HEADER_RESTORE) {
            this.lastHeaderEvent = $value;
        }
        this.handlers.foreach(($handler : IResponse) : void => {
            try {
                $handler.OnChange($value);
            } catch (ex) {
                $handler.OnError(ex);
            }
        });
    }

    public AddHandler($value : IResponse) : void {
        const key : string = "res_" + $value.getId();
        this.handlers.Add($value, key);
        if (!ObjectValidator.IsEmptyOrNull(this.lastHeaderEvent)) {
            $value.OnChange(this.lastHeaderEvent);
        }
        LogIt.Debug("> registered push notifications listener: {0}", key);
        $value.AddAbortHandler(() : void => {
            this.handlers.RemoveAt(this.handlers.getKeys().indexOf(key));
            this.handlers.Reindex();
            LogIt.Debug("> removed push notifications listener: {0}", key);
        });
    }
}

/*! ******************************************************************************************************** *
 *
 * Copyright 2023-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { HttpStatusType } from "@io-oidis-commons/Io/Oidis/Commons/Enums/HttpStatusType.js";
import { IClassName } from "@io-oidis-commons/Io/Oidis/Commons/Interfaces/Interface.js";
import { JsonUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/JsonUtils.js";
import { LogIt } from "@io-oidis-commons/Io/Oidis/Commons/Utils/LogIt.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { StringUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/StringUtils.js";
import { IPrintAPI } from "@io-oidis-hub-gui/Io/Oidis/Hub/Gui/Bindings/PrintBinding.js";
import { PrintType } from "@io-oidis-hub-gui/Io/Oidis/Hub/Gui/Enums/PrintType.js";
import { IExportData, IExportStatus } from "@io-oidis-hub-gui/Io/Oidis/Hub/Gui/Interfaces/IExportStatus.js";
import { IPDFOptions } from "@io-oidis-hub-gui/Io/Oidis/Hub/Gui/Interfaces/IPDFOptions.js";
import { StreamResponse } from "@io-oidis-localhost/Io/Oidis/Localhost/HttpProcessor/ResponseApi/Handlers/StreamResponse.js";
import { IHttpResponse } from "@io-oidis-localhost/Io/Oidis/Localhost/Interfaces/IRoute.js";
import { BaseConnector } from "@io-oidis-localhost/Io/Oidis/Localhost/Primitives/BaseConnector.js";
import { Extern, Route } from "@io-oidis-localhost/Io/Oidis/Localhost/Primitives/Decorators.js";
import { ISchemaError, SchemaValidator } from "@io-oidis-localhost/Io/Oidis/Localhost/Utils/SchemaValidator.js";
import { User } from "../DAO/Models/User.js";
import { IAPIConfig, IServerConfigurationDomain } from "../Interfaces/IProject.js";
import { Loader } from "../Loader.js";
import { Convert } from "./Convert.js";
import { PuppeteerHandler } from "./PuppeteerHandler.js";

export class PrintManager extends BaseConnector {
    private static printRequests : any = {};
    private static exportsBuffer : any = {};
    private readonly config : IAPIConfig;

    public static getExportDataOnce($id : string) : IExportData {
        if (this.exportsBuffer.hasOwnProperty($id)) {
            const copy : IExportData = {
                asStream: this.exportsBuffer[$id].asStream,
                data    : this.exportsBuffer[$id].data.slice(),
                encoding: this.exportsBuffer[$id].encoding,
                format  : this.exportsBuffer[$id].format,
                name    : this.exportsBuffer[$id].name
            };
            delete this.exportsBuffer[$id];
            return copy;
        }
        return null;
    }

    constructor() {
        super();
        this.config = Loader.getInstance().getAppConfiguration().apiConfig;
    }

    @Extern()
    public getPrintOptions($id : string) : IPrintAPI {
        if (PrintManager.printRequests.hasOwnProperty($id)) {
            return PrintManager.printRequests[$id];
        }
        return null;
    }

    @Extern()
    public async GeneratePDF($input : string, $options? : IPDFOptions) : Promise<IExportStatus> {
        const exportId : string = StringUtils.getSha1(new Date().getTime().toString());
        $options = JsonUtils.Extend(<IPDFOptions>{
            asStream: true,
            name    : exportId + ".pdf"
        }, $options);
        const retVal : IExportStatus = <IExportStatus>{
            data     : {
                asStream: $options.asStream,
                data    : await (<any>this.getConvertInstance()).HtmlToPDF($input, $options),
                encoding: "utf-8",
                format  : "pdf",
                name    : $options.name
            },
            id       : exportId,
            isPartial: false
        };
        if ($options.asStream) {
            retVal.data.data = retVal.data.data.toString("base64");
        } else {
            PrintManager.exportsBuffer[exportId] = retVal.data;
        }
        return retVal;
    }

    @Route("POST /print", {responseHandler: new StreamResponse()})
    @Route("POST /v1/print", {responseHandler: new StreamResponse()})
    public async Print($data : IPrintAPI) : Promise<any> {
        /// TODO: message for missing permissions in AuthManager for Route end-point is not correct or misleading
        const validator : SchemaValidator = new SchemaValidator();
        validator.Schema(this.getPrintSchema());
        const errors : ISchemaError[] = validator.Validate($data);
        if (!ObjectValidator.IsEmptyOrNull(errors)) {
            validator.ErrorsToString(errors);
            return {body: "API schema error: " + validator.ErrorsToString(errors), status: HttpStatusType.ERROR};
        }

        $data = JsonUtils.Extend(this.getPrintDefaultValues(), $data);

        if (!ObjectValidator.IsEmptyOrNull($data.itemsIds)) {
            const requestsCount : number = Object.keys(PrintManager.printRequests).length;
            LogIt.Debug("Print request #" + requestsCount);
            if (requestsCount + 1 > this.config.maxPrintRequests) {
                LogIt.Warning("Too many consequent print requests. {0}", $data);
                return {
                    body  : "Too many consequent print requests. Please slow down.",
                    status: HttpStatusType.FORBIDDEN
                };
            }
            if ($data.itemsIds.length > this.config.maxIdsPerPrintRequests) {
                LogIt.Warning("Maximum number of items IDs per one request. {0}", $data);
                return {
                    body  : "Maximum number of items IDs per one request is " + this.config.maxIdsPerPrintRequests + ".",
                    status: HttpStatusType.FORBIDDEN
                };
            }
            const printId : string = StringUtils.getSha1(new Date().getTime().toString());
            PrintManager.printRequests[printId] = $data;

            const result : IHttpResponse = this.validatePrintData($data);
            if (!ObjectValidator.IsEmptyOrNull(result)) {
                return result;
            }
            try {
                LogIt.Debug("Preparing print for: {0}", $data);
                const domainConfig : IServerConfigurationDomain = Loader.getInstance().getAppConfiguration().domain;
                const serviceAccount : User = Loader.getInstance().getServiceAccount();
                const data : IExportData = {
                    data    : await PuppeteerHandler.getInstanceSingleton().Export({
                        url: "http://" + domainConfig.localhostName + ":" + domainConfig.ports.http +
                            "/#/print/" + serviceAccount.UserName + "/" + serviceAccount.AuthTokens[0].Token
                        // debug: true
                    }, <any>{
                        height           : $data.settings.height + "mm",
                        width            : $data.settings.width + "mm",
                        preferCSSPageSize: true,
                        inputs           : [{name: "printId", value: printId}]
                    }),
                    asStream: true,
                    encoding: "utf8",
                    format  : "pdf"
                };
                delete PrintManager.printRequests[printId];

                if (!StringUtils.ContainsIgnoreCase($data.fileName, ".pdf")) {
                    $data.fileName += "." + data.format;
                }
                const bundle : Buffer = <Buffer>data.data;
                if (ObjectValidator.IsEmptyOrNull(bundle)) {
                    delete PrintManager.printRequests[printId];
                    return {body: "Failed to generated PDF", status: HttpStatusType.ERROR};
                }
                const headers : any = {};
                if ($data.withCache) {
                    const lastModifiedTime : number = new Date().getTime();
                    const lastModifiedTimeGMT : string = Convert.TimeToGMTformat(lastModifiedTime);
                    headers.etag = StringUtils.getSha1($data.fileName + lastModifiedTimeGMT);
                    headers.expires = 0;
                    headers["last-modified"] = lastModifiedTimeGMT;
                }

                const fileSize : number = bundle.length;
                headers["cache-control"] = "no-store, no-cache, must-revalidate, post-check=0, pre-check=0";
                headers["accept-ranges"] = "bytes";
                headers["content-encoding"] = "identity";
                headers["content-transfer-encoding"] = "binary";
                headers["content-description"] = "File Transfer";
                headers["content-length"] = fileSize;
                headers["content-disposition"] = (data.asStream ? "inline" : "attachment") +
                    "; filename*=UTF-8''" + $data.fileName + "; filename=\"" + $data.fileName + "\"";
                if (!data.asStream) {
                    headers["x-download-options"] = "noopen";
                }

                headers["content-type"] = "application/" + data.format + "; charset=" + data.encoding;

                return {
                    body  : bundle,
                    headers,
                    status: HttpStatusType.SUCCESS
                };
            } catch (ex) {
                delete PrintManager.printRequests[printId];
                LogIt.Error("Failed to generated PDF", ex);
                return {body: "Failed to generated PDF", status: HttpStatusType.ERROR};
            }
        }
        return {body: "List of items IDs is mandatory", status: HttpStatusType.FORBIDDEN};
    }

    protected getPrintSchema() : any {
        return {
            id        : "api/print",
            type      : "object",
            properties: {
                // Array of itemsIds which should be printed into the generated PDF [MANDATORY]
                itemsIds: {
                    type : "array",
                    items: {
                        type: "string"
                    }
                },
                // Name of generated file send as part of response header content [OPTIONAL]
                fileName: {
                    type: "string"
                },
                // Set of settings for PDF content [OPTIONAL]
                //  - each option in settings is also optional and has its own default value
                settings: {
                    type      : "object",
                    properties: {
                        // Define print format as shortcut for specification of width, height and limit
                        //  - default value is 1 as numeric representation of A4 from enumeration of [Own, A4]
                        //  - if not defined width, height and limit will be overridden by A4 definition
                        format: {
                            type   : "integer",
                            minimum: 0,
                            maximum: 2
                        },
                        // Define starting position for first item from 1 to page limit
                        //  - default value is 1
                        //  - offset 2 means that position 1 will be left empty
                        //  - offset 3 means that position 1 and 2 will be left empty, etc.
                        //  - offset must be less that limit
                        offset: {
                            type   : "integer",
                            minimum: 1
                        },
                        // Define maximum number of items per page
                        //  - default value is 8
                        //  - supported are only even integers in range from 2 to 16
                        // TODO: this should be replaced by cols and rows and limit will be calculated or removed at all
                        limit: {
                            type   : "integer",
                            minimum: 1,
                            maximum: 16
                        },
                        // Define page width in [mm] units
                        //  - default value is 210 mm same as for A4 format
                        width: {
                            type   : "integer",
                            minimum: 10,
                            maximum: 1000
                        },
                        // Define page height in [mm] units
                        //  - default value is 297 mm same as for A4 format
                        height: {
                            type   : "integer",
                            minimum: 10,
                            maximum: 1000
                        },
                        // Define item padding in [mm] units
                        //  - default value is 10 mm
                        padding: {
                            type   : "integer",
                            minimum: 0,
                            maximum: 50
                        },
                        // Define if item should be printed with dashed outer line
                        //  - default value is false
                        grid: {
                            type: "boolean"
                        }
                    }
                },
                // Type of print document [RESERVED][OPTIONAL]
                printType: {
                    type: "string"
                },
                // Define if list of items should be cached and driven by etag headers [RESERVED][OPTIONAL]
                //   - default value is false meaning no-cache
                withCache: {
                    type: "boolean"
                }
            }
        };
    }

    protected getPrintDefaultValues() : IPrintAPI {
        const now : Date = new Date();
        return {
            itemsIds : [],
            version  : "",
            fileName : "Print_" + StringUtils.Remove(Convert.DateToCsFormat(now) + Convert.TimeToLocalFormat(now), ".", ":"),
            printType: "default",
            settings : {
                offset : 0,
                limit  : 8,
                width  : 210,
                height : 297,
                format : PrintType.A4,
                padding: 10,
                grid   : false
            },
            withCache: false
        };
    }

    protected validatePrintData($data : IPrintAPI) : IHttpResponse {
        switch ($data.settings.format) {
        case PrintType.A4:
            $data.settings.width = 210;
            $data.settings.height = 297;
            $data.settings.limit = 8;
            break;
        case PrintType.Own:
            // do nothing
            break;
        default:
            return {body: "Incompatible format type: " + $data.settings.format, status: HttpStatusType.ERROR};
        }
        /// TODO: maybe not needed or needs to be converted to row and cols limits
        if ($data.settings.limit !== 1 && $data.settings.limit % 2 !== 0) {
            return {body: "Limit must be 1 or even integer within range <2-16>", status: HttpStatusType.ERROR};
        }
        return null;
    }

    protected getConvertInstance() : IClassName {
        return Convert;
    }
}

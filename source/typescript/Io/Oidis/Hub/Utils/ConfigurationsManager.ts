/*! ******************************************************************************************************** *
 *
 * Copyright 2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2018 NXP
 * Copyright 2019-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { LogIt } from "@io-oidis-commons/Io/Oidis/Commons/Utils/LogIt.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { StringUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/StringUtils.js";
import { IRegistryItem } from "@io-oidis-hub-gui/Io/Oidis/Hub/Gui/Interfaces/IRegistryContent.js";
import { ConfigFile } from "@io-oidis-hub-gui/Io/Oidis/Hub/Gui/Models/ConfigFile.js";
import { FileSystemHandler } from "@io-oidis-localhost/Io/Oidis/Localhost/Connectors/FileSystemHandler.js";
import { HttpManager } from "@io-oidis-localhost/Io/Oidis/Localhost/HttpProcessor/HttpManager.js";
import { IFilePath } from "@io-oidis-localhost/Io/Oidis/Localhost/HttpProcessor/Resolvers/FileRequestResolver.js";
import { ResponseFactory } from "@io-oidis-localhost/Io/Oidis/Localhost/HttpProcessor/ResponseApi/ResponseFactory.js";
import { ExternResponseType } from "@io-oidis-localhost/Io/Oidis/Localhost/Interfaces/IExternOptions.js";
import { IResponse } from "@io-oidis-localhost/Io/Oidis/Localhost/Interfaces/IResponse.js";
import { Extern } from "@io-oidis-localhost/Io/Oidis/Localhost/Primitives/Decorators.js";
import { ConfigEntry } from "../DAO/Models/ConfigEntry.js";
import { FileEntry } from "../DAO/Models/FileEntry.js";
import { Loader } from "../Loader.js";
import { BaseRegister } from "../Primitives/BaseRegister.js";

export class ConfigurationsManager extends BaseRegister<ConfigFile> {

    @Extern(ExternResponseType.FULL_CONTROL)
    public Download($appName : string, $configName : string, $version : string = null, $callback? : IResponse) : void {
        this.getFileDescriptor($appName, $configName, $version).then(($descriptor : IFilePath) : void => {
            if (!ObjectValidator.IsEmptyOrNull($descriptor.blob.data)) {
                ResponseFactory.getResponse($callback).Send(true, $descriptor.blob.data.toString());
            } else {
                ResponseFactory.getResponse($callback).Send(false, "");
            }
        });
    }

    @Extern()
    public async Upload($appName : string, $configName : string, $version : string, $data : string) : Promise<boolean> {
        $configName = StringUtils.Remove($configName, ".jsonp");
        const newConfigFile : ConfigEntry = new ConfigEntry();
        newConfigFile.Name = $appName;
        newConfigFile.Version = $version;
        newConfigFile.ConfigName = $configName;
        newConfigFile.Timestamp = (new Date()).getTime();

        let configExists : boolean = false;
        const filePath : string = this.getConfigPath(newConfigFile, false);
        try {
            const models : ConfigEntry[] = await ConfigEntry.Find({
                configName: newConfigFile.ConfigName,
                name      : newConfigFile.Name,
                version   : newConfigFile.Version
            });
            if (!ObjectValidator.IsEmptyOrNull(models)) {
                for await (const model of models) {
                    model.Timestamp = newConfigFile.Timestamp;
                    model.Data.Path = filePath;
                    model.Data.ContentType = "jsonp";
                    await model.Data.Data(Buffer.from($data));
                    await model.Save();
                    configExists = true;
                }
            }
            if (!configExists) {
                newConfigFile.Data.Path = filePath;
                newConfigFile.Data.ContentType = "jsonp";
                await newConfigFile.Data.Data(Buffer.from($data));
                await newConfigFile.Save();
            }
            LogIt.Debug("Register config file {0} {1}[{2}]", $appName, $configName, $version);
            return true;
        } catch (ex) {
            LogIt.Error(ex);
        }
        return false;
    }

    @Extern()
    public async Delete($appName : string, $configName : string, $version : string) : Promise<void> {
        const models : ConfigEntry[] = await ConfigEntry.Find({
            configName: $configName,
            name      : $appName,
            version   : $version
        });
        if (!ObjectValidator.IsEmptyOrNull(models)) {
            for await (const config of models) {
                await config.Remove();
            }
        }
    }

    public async getFileDescriptor($appName : string, $configName : string, $version : string = null) : Promise<IFilePath> {
        $configName = StringUtils.Remove($configName, ".jsonp");
        LogIt.Debug("Download config file {0} {1}[{2}]", $appName, $configName, $version);
        let file : FileEntry = null;
        let lastVersion : string = "";
        const models : ConfigEntry[] = await ConfigEntry.Find({
            configName: $configName,
            name      : $appName
        });
        if (!ObjectValidator.IsEmptyOrNull(models)) {
            models.forEach(($configFile : ConfigEntry) : void => {
                if ((!ObjectValidator.IsEmptyOrNull($version) && $configFile.Version === $version) ||
                    (ObjectValidator.IsEmptyOrNull($version) && ObjectValidator.IsEmptyOrNull(lastVersion) ||
                        StringUtils.VersionIsLower(lastVersion, $configFile.Version))
                ) {
                    file = $configFile.Data;
                    if (!ObjectValidator.IsEmptyOrNull(file) && ObjectValidator.IsEmptyOrNull(file.Path)) {
                        file.Path = this.getConfigPath($configFile, true);
                    }
                    if (ObjectValidator.IsEmptyOrNull($version)) {
                        lastVersion = $configFile.Version;
                    }
                }
            });
        }

        let fileDescription : string = $configName;
        if (!ObjectValidator.IsEmptyOrNull(file)) {
            const filePath : string = file.Path;
            fileDescription = filePath;
            const data : Buffer = await file.Data();
            if (!ObjectValidator.IsEmptyOrNull(data)) {
                LogIt.Debug("Transferring file \"{0}\" with size: {1}", filePath, file.Size + "");
                return {
                    blob     : {
                        data,
                        mtime: file.LastModified.getTime(),
                        size : file.Size
                    },
                    extension: "jsonp",
                    name     : $configName,
                    path     : filePath,
                    url      : ""
                };
            }
        }
        LogIt.Debug("Configuration file for \"{0}\" does not exist.", fileDescription);
        return {
            extension: "jsonp",
            name     : $configName,
            path     : "",
            url      : ""
        };
    }

    public async getRegistryItems() : Promise<IRegistryItem[]> {
        const manager : HttpManager = Loader.getInstance().getHttpManager();
        const links : IRegistryItem[] = [];
        const models : ConfigEntry[] = await ConfigEntry.Find({});
        if (!ObjectValidator.IsEmptyOrNull(models)) {
            models.forEach(($configFile : ConfigEntry) : void => {
                links.push({
                    link    : manager.CreateLink(
                        "/Configuration/" + $configFile.Name + "/" + $configFile.ConfigName + "/" + $configFile.Version),
                    name    : $configFile.Name,
                    platform: $configFile.ConfigName,
                    version : $configFile.Version
                });
            });
        }
        return links;
    }

    public async ToDatabase() : Promise<boolean> {
        try {
            const fileSystem : FileSystemHandler = Loader.getInstance().getFileSystemHandler();
            const register : ConfigFile[] = this.getRegister();
            for await (const item of register) {
                const models : ConfigEntry[] = await ConfigEntry.Find(item);

                let model : ConfigEntry;
                if (ObjectValidator.IsEmptyOrNull(models)) {
                    model = new ConfigEntry();
                    model.Name = item.name;
                    model.ConfigName = item.configName;
                    model.Version = item.version;
                    model.Timestamp = item.timestamp;
                } else {
                    if (models.length > 1) {
                        LogIt.Debug("Found more then one model for {0}. Using first one.", item.configName);
                    }
                    model = models[0];
                }

                const filePath : string = this.getConfigPath(model);
                const relativePath : string = this.getConfigPath(model, false);
                if (model.Data.Path !== relativePath) {
                    model.Data.Path = relativePath;
                    model.Data.ContentType = "jsonp";
                    if (fileSystem.Exists(filePath)) {
                        await model.Data.Data(<Buffer>fileSystem.Read(filePath));
                    } else {
                        LogIt.Debug("Config file {0} not found. Storing just meta data.", filePath);
                        await model.Data.Save();
                    }
                    await model.Save();
                } else {
                    LogIt.Debug("Config {0} conversion skipped - config already exist", item.configName);
                }
            }
            fileSystem.Rename(this.getRegisterPath(), this.getRegisterPath() + ".back");
            return true;
        } catch (ex) {
            LogIt.Error(ex);
        }
        return false;
    }

    private getConfigPath($config : ConfigEntry, $isAbsolute : boolean = true) : string {
        return this.getStoragePath($isAbsolute) + "/" + StringUtils.getSha1($config.Name + $config.ConfigName + $config.Version);
    }
}

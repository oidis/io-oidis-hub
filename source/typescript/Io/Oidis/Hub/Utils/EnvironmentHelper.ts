/*! ******************************************************************************************************** *
 *
 * Copyright 2022-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { JsonpFileReader } from "@io-oidis-commons/Io/Oidis/Commons/IOApi/Handlers/JsonpFileReader.js";
import { ArrayList } from "@io-oidis-commons/Io/Oidis/Commons/Primitives/ArrayList.js";
import { BaseObject } from "@io-oidis-commons/Io/Oidis/Commons/Primitives/BaseObject.js";
import { JsonUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/JsonUtils.js";
import { LogIt } from "@io-oidis-commons/Io/Oidis/Commons/Utils/LogIt.js";
import { ObjectDecoder } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectDecoder.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import {
    EventLoopActionType,
    EventLoopRecordType,
    IPushNotificationEvent
} from "@io-oidis-hub-gui/Io/Oidis/Hub/Gui/Connectors/EnvironmentConnector.js";
import { IFAQDao, IFAQTopic } from "@io-oidis-hub-gui/Io/Oidis/Hub/Gui/DAO/FAQDao.js";
import { FeedbackType } from "@io-oidis-hub-gui/Io/Oidis/Hub/Gui/Enums/FeedbackType.js";
import { IReleaseNotesConfig } from "@io-oidis-hub-gui/Io/Oidis/Hub/Gui/Interfaces/IReleaseNotesConfig.js";
import { ISendmailConfiguration } from "@io-oidis-hub-gui/Io/Oidis/Hub/Gui/Interfaces/ISendmailConfiguration.js";
import { IWhatsNewDao, IWhatsNewNote } from "@io-oidis-hub-gui/Io/Oidis/Hub/Gui/Models/WhatsNewDao.js";
import { RuntimeHandler } from "@io-oidis-localhost/Io/Oidis/Localhost/Connectors/RuntimeHandler.js";
import { EnvironmentArgs } from "@io-oidis-localhost/Io/Oidis/Localhost/EnvironmentArgs.js";
import { IFilePath } from "@io-oidis-localhost/Io/Oidis/Localhost/HttpProcessor/Resolvers/FileRequestResolver.js";
import { ResponseFactory } from "@io-oidis-localhost/Io/Oidis/Localhost/HttpProcessor/ResponseApi/ResponseFactory.js";
import { ExternResponseType } from "@io-oidis-localhost/Io/Oidis/Localhost/Interfaces/IExternOptions.js";
import { IResponse } from "@io-oidis-localhost/Io/Oidis/Localhost/Interfaces/IResponse.js";
import { Extern } from "@io-oidis-localhost/Io/Oidis/Localhost/Primitives/Decorators.js";
import { IModelListFilter, IModelListResult } from "@io-oidis-services/Io/Oidis/Services/Connectors/AuthManagerConnector.js";
import { EnvironmentState } from "@io-oidis-services/Io/Oidis/Services/Enums/EnvironmentState.js";
import { EnvironmentType } from "@io-oidis-services/Io/Oidis/Services/Enums/EnvironmentType.js";
import { AppSettings } from "../DAO/Models/AppSettings.js";
import { EmailRecord } from "../DAO/Models/EmailRecord.js";
import { User } from "../DAO/Models/User.js";
import { WebhookRecord } from "../DAO/Models/WebhookRecord.js";
import { IProject } from "../Interfaces/IProject.js";
import { Loader } from "../Loader.js";
import { AuthManager } from "./AuthManager.js";
import { ConfigurationsManager } from "./ConfigurationsManager.js";
import { Convert } from "./Convert.js";
import { PuppeteerHandler } from "./PuppeteerHandler.js";

export class EnvironmentStatePool extends BaseObject {
    private state : EnvironmentState;
    private endState : EnvironmentState;
    private readonly stateHandlers : ArrayList<IResponse>;

    constructor() {
        super();
        this.state = EnvironmentState.IDLE;
        this.endState = EnvironmentState.LOADED;
        this.stateHandlers = new ArrayList<IResponse>();
    }

    public EndState($value? : EnvironmentState) : EnvironmentState {
        if (ObjectValidator.IsSet($value)) {
            this.endState = $value;
        }
        return this.endState;
    }

    public State($value? : EnvironmentState) : EnvironmentState {
        if (ObjectValidator.IsSet($value)) {
            this.state = $value;
            this.stateHandlers.foreach(($handler : IResponse) : void => {
                try {
                    if (this.state !== this.endState) {
                        $handler.OnChange(this.state);
                    } else {
                        $handler.OnComplete();
                    }
                } catch (ex) {
                    $handler.OnError(ex);
                }
            });
        }
        return this.state;
    }

    public AddHandler($value : IResponse) : void {
        const key : string = "res_" + $value.getId();
        this.stateHandlers.Add($value, key);
        if (this.state !== this.endState) {
            $value.OnChange(this.state);
        }
        LogIt.Debug("> registered status listener: {0}", key);
        $value.AddAbortHandler(() : void => {
            this.stateHandlers.RemoveAt(this.stateHandlers.getKeys().indexOf(key));
            this.stateHandlers.Reindex();
            LogIt.Debug("> removed status listener: {0}", key);
        });
    }
}

export class EnvironmentHelper extends RuntimeHandler {
    protected readonly environmentArgs : EnvironmentArgs;
    protected readonly projectArgs : IProject;
    private readonly configsRegister : ConfigurationsManager;
    private loadedConfigs : any;

    constructor() {
        super();
        this.configsRegister = new ConfigurationsManager();
        this.environmentArgs = Loader.getInstance().getEnvironmentArgs();
        this.projectArgs = Loader.getInstance().getAppConfiguration();
        this.loadedConfigs = {};
    }

    @Extern()
    public getEnvType() : EnvironmentType {
        return this.projectArgs.target.environmentType;
    }

    @Extern()
    public InitPageEnabled() : boolean {
        if (Loader.getInstance().getAdminHelper().IsRequired()) {
            return true;
        }
        return this.projectArgs.target.initPageEnabled;
    }

    @Extern()
    public IsEnvironmentLoaded() : boolean {
        return Loader.getInstance().getStatePool().State() === Loader.getInstance().getStatePool().EndState();
    }

    @Extern(ExternResponseType.FULL_CONTROL)
    public AddLoadingListener($callback : (($status : EnvironmentState) => void) | IResponse) : void {
        Loader.getInstance().getStatePool().AddHandler(ResponseFactory.getResponse($callback));
    }

    @Extern()
    public getSendMailConfig() : ISendmailConfiguration {
        const config : ISendmailConfiguration = this.projectArgs.sendmail;
        return <any>{
            admins       : config.admins,
            blackList    : config.blackList,
            defaultSender: config.defaultSender,
            domain       : config.domain,
            fetchTick    : config.fetchTick,
            location     : config.location,
            port         : config.port,
            secure       : config.secure,
            serviceType  : config.serviceType,
            user         : config.user
        };
    }

    @Extern()
    public async getReleaseNotesConfig() : Promise<IReleaseNotesConfig> {
        const data : IWhatsNewDao = await this.getWhatsNewData();
        const notes : IWhatsNewNote[] = data.notes;
        const versions : string[] = [];
        for (const version in notes) {
            if (notes.hasOwnProperty(version)) {
                versions.push(version);
            }
        }
        return {
            forceShow: data.forceShow,
            versions
        };
    }

    @Extern()
    public async getReleaseNotesFor($version : string) : Promise<IWhatsNewNote> {
        const notes : IWhatsNewNote[] = (await this.getWhatsNewData()).notes;
        if (notes.hasOwnProperty($version)) {
            return notes[$version];
        }
        return null;
    }

    @Extern()
    public async getFAQTopics() : Promise<IFAQTopic[]> {
        return (await this.getFAQData()).topics;
    }

    @Extern()
    public async getEventLoopRecords($type : string, $filter : IModelListFilter = null) : Promise<IModelListResult> {
        const output : IModelListResult = {
            data  : [],
            limit : -1,
            offset: 0,
            size  : 0
        };
        JsonUtils.Extend(output, $filter);
        if ($type === EventLoopRecordType.EMAIL_RECORD) {
            const models : EmailRecord[] = await EmailRecord.Find({active: false});
            models.forEach(($record : EmailRecord) : void => {
                output.data.push({
                    data: {email: $record.Email, type: $record.Type},
                    type: $type,
                    uuid: $record.Id
                });
            });
            output.size = output.data.length;
        } else if ($type === EventLoopRecordType.WEBHOOK_RECORD) {
            const models : WebhookRecord[] = await WebhookRecord.Find({active: false});
            models.forEach(($record : WebhookRecord) : void => {
                output.data.push({
                    data: {
                        code : $record.Code,
                        owner: $record.OwnerId,
                        type : $record.Type,
                        time : Convert.TimeToGMTformat($record.Created)
                    },
                    type: $type,
                    uuid: $record.Id
                });
            });
            output.size = output.data.length;
        }
        return output;
    }

    @Extern()
    public async setEventLoopRecords($uuid : string, $type : string, $action : string) : Promise<void> {
        if ($type === EventLoopRecordType.EMAIL_RECORD) {
            const model : EmailRecord = await EmailRecord.FindById($uuid);
            if (!ObjectValidator.IsEmptyOrNull(model)) {
                if ($action === EventLoopActionType.DELETE) {
                    await model.Remove();
                } else if ($action === EventLoopActionType.ACTIVATE) {
                    model.Active = true;
                    await model.Save();
                }
            }
        } else if ($type === EventLoopRecordType.WEBHOOK_RECORD) {
            const model : WebhookRecord = await WebhookRecord.FindById($uuid);
            if (!ObjectValidator.IsEmptyOrNull(model)) {
                if ($action === EventLoopActionType.DELETE) {
                    await model.Remove();
                } else if ($action === EventLoopActionType.ACTIVATE) {
                    model.Active = true;
                    await model.Save();
                }
            }
        }
    }

    @Extern()
    public async SendFeedback($email : string, $category : FeedbackType, $message : string, $uuid? : string,
                              $isPublic : boolean = false) : Promise<boolean> {
        return Loader.getInstance().getMailEngine().Feedback($email, $category, $message, $uuid, $isPublic);
    }

    @Extern()
    public PushNotification($event : IPushNotificationEvent) : void {
        Loader.getInstance().PushNotifications().Push($event);
    }

    @Extern(ExternResponseType.FULL_CONTROL)
    public AttachToPushNotifications($callback : (($event : IPushNotificationEvent) => void) | IResponse) : void {
        Loader.getInstance().PushNotifications().AddHandler(ResponseFactory.getResponse($callback));
    }

    @Extern()
    public ResetConfigsCache() : void {
        this.loadedConfigs = {};
    }

    @Extern()
    public async getAppSettings() : Promise<AppSettings> {
        const modelClass : any = this.getAppSettingsModel();
        const settings : AppSettings[] = await modelClass.Find({});
        let instance : AppSettings;
        if (ObjectValidator.IsEmptyOrNull(settings)) {
            instance = new modelClass();
            await instance.Save();
        } else {
            if (settings.length > 1) {
                LogIt.Warning("Multiple internal setting structures found in DB, using first one.");
            }
            instance = settings[0];
        }
        return instance;
    }

    @Extern()
    public async SaveAppSettings($settings : AppSettings) : Promise<void> {
        const settings : AppSettings = await this.getAppSettings();
        settings.SyncWith($settings);
        await settings.Save();
        await settings.SyncTo(Loader.getInstance().getAppConfiguration());
    }

    @Extern()
    public async SyncAppSettings($resetDB : boolean = false) : Promise<void> {
        const settings : AppSettings = await this.getAppSettings();
        const config : IProject = Loader.getInstance().getAppConfiguration();
        if ($resetDB ||
            settings.Version !== config.version ||
            settings.LastModified.getTime() < config.build.timestamp) {
            if (config.target.preferDBSettings && !$resetDB) {
                settings.Version = config.version;
                await settings.Save();
                await settings.SyncTo(config);
                LogIt.Debug("> preferred application settings from DB");
            } else {
                // TODO: create app settings backup before sync?
                await settings.SyncFrom(config);
                LogIt.Debug("> application settings pushed to DB");
            }
        } else {
            await settings.SyncTo(config);
            LogIt.Debug("> application settings loaded from DB");
        }
    }

    @Extern()
    public async InitPDFPrint() : Promise<void> {
        await PuppeteerHandler.getInstanceSingleton().Init();
    }

    @Extern()
    public async getProjectConfig($key : any) : Promise<any> {
        const auth : AuthManager = Loader.getInstance().getAuthManager();
        const user : User = await auth.getUser(this.getCurrentToken());
        if (await auth.HasGroup((await auth.getSystemGroups()).Admin, user)) {
            try {
                const {subtle} = require("crypto").webcrypto;

                $key = JSON.parse(ObjectDecoder.Base64($key));
                const algorithm : string = "AES-GCM";
                const key = await subtle.importKey("jwk", $key.jwk, {
                    length: 256,
                    name  : algorithm
                }, false, ["encrypt"]);

                return Buffer.from(await subtle.encrypt({
                        iv  : new TextEncoder().encode($key.iv),
                        name: algorithm
                    },
                    key,
                    new TextEncoder().encode(JSON.stringify(Loader.getInstance().getAppConfiguration())))).toString("base64");
            } catch (ex) {
                LogIt.Error("Failed to encrypt data.", ex);
            }
        }

        return null;
    }

    public async SyncConfigsData() : Promise<void> {
        const fs : any = require("fs");
        const configs : any = this.getConfigsSource();
        const sources : any[] = [];
        for (const name in configs) {
            if (configs.hasOwnProperty(name)) {
                sources.push({name, path: configs[name]});
            }
        }
        let updated : boolean = false;
        for await (const source of sources) {
            const file : IFilePath = await this.configsRegister.getFileDescriptor(this.environmentArgs.getProjectName(), source.name);
            if (ObjectValidator.IsEmptyOrNull(file.blob) ||
                !ObjectValidator.IsEmptyOrNull(file.blob.data) && file.blob.mtime < fs.statSync(source.path).mtime) {
                const data : string = Loader.getInstance().getFileSystemHandler().Read(source.path).toString("utf-8");
                if (ObjectValidator.IsEmptyOrNull(file.blob) ||
                    !ObjectValidator.IsEmptyOrNull(file.blob.data) && file.blob.data.toString("utf-8") !== data) {
                    LogIt.Info("> updating config file: " + source.name);
                    const versions : any = /^\s*(version:|"version":)\s*"(\d+\.\d+\.\d+(?:[-.][a-zA-Z]+)*)"/gm.exec(data);
                    let version : string = "1.0.0";
                    if (versions !== null && versions.length >= 2) {
                        version = versions[2];
                    }
                    await this.configsRegister.Upload(this.environmentArgs.getProjectName(),
                        source.name, version, data);
                    updated = true;
                }
            }
        }
        if (!updated) {
            LogIt.Info("> all configs are up to date");
        }
    }

    @Extern()
    public async CleanEvenLoopRecords() : Promise<void> {
        const date : Date = new Date();
        date.setMonth(date.getMonth() - 3);
        const hooks : WebhookRecord[] = await WebhookRecord.Find({active: false, created: {$lte: date}});
        if (!ObjectValidator.IsEmptyOrNull(hooks)) {
            LogIt.Warning("Removing inactive hooks older than " + Convert.TimeToGMTformat(date));
            for await (const model of hooks) {
                await model.Remove();
            }
        }
    }

    protected getConfigsSource() : any {
        return {
            FAQ         : "resource/data/Io/Oidis/Hub/Localization/FAQ.jsonp",
            ReleaseNotes: "resource/data/Io/Oidis/Hub/Localization/ReleaseNotes.jsonp"
        };
    }

    protected getAppSettingsModel() : any {
        return AppSettings;
    }

    @Extern()
    private setSendMailConfig($config : ISendmailConfiguration) : void {
        JsonUtils.Extend(this.projectArgs.sendmail, $config);
    }

    private async getWhatsNewData() : Promise<IWhatsNewDao> {
        return this.getDaoData<IWhatsNewDao>("ReleaseNotes");
    }

    private async getFAQData() : Promise<IFAQDao> {
        return this.getDaoData<IFAQDao>("FAQ");
    }

    private async getDaoData<T>($type : string) : Promise<T> {
        if (this.loadedConfigs.hasOwnProperty($type)) {
            return this.loadedConfigs[$type];
        }
        const file : IFilePath = await this.configsRegister.getFileDescriptor(this.environmentArgs.getProjectName(), $type);
        if (!ObjectValidator.IsEmptyOrNull(file.blob.data)) {
            return new Promise<T>(($resolve : any, $reject : any) : void => {
                JsonpFileReader.LoadString(file.blob.data.toString("utf-8"), ($data : T) : void => {
                        this.loadedConfigs[$type] = $data;
                        $resolve($data);
                    }, ($eventArgs : ErrorEvent | Error) : void => {
                        if (!ObjectValidator.IsEmptyOrNull((<Error>$eventArgs).stack)) {
                            $reject(new Error((<Error>$eventArgs).message +
                                " For blob: " + file.name + "." + file.extension + " (" + file.path + ")"));
                        } else {
                            $reject((<ErrorEvent>$eventArgs).error);
                        }
                    }
                );
            });
        }
        return null;
    }
}

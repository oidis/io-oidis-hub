/*! ******************************************************************************************************** *
 *
 * Copyright 2010-2013 Jakub Cieslar
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2018 NXP
 * Copyright 2019-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { HttpMethodType } from "@io-oidis-commons/Io/Oidis/Commons/Enums/HttpMethodType.js";
import { HttpStatusType } from "@io-oidis-commons/Io/Oidis/Commons/Enums/HttpStatusType.js";
import { BaseObject } from "@io-oidis-commons/Io/Oidis/Commons/Primitives/BaseObject.js";
import { JsonUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/JsonUtils.js";
import { LogIt } from "@io-oidis-commons/Io/Oidis/Commons/Utils/LogIt.js";
import { ObjectDecoder } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectDecoder.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { StringUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/StringUtils.js";
import { ISendmailConfiguration, ISendmailDkim } from "@io-oidis-hub-gui/Io/Oidis/Hub/Gui/Interfaces/ISendmailConfiguration.js";
import { ISendMailOptions } from "@io-oidis-hub-gui/Io/Oidis/Hub/Gui/Interfaces/ISendMailOptions.js";
import { FileSystemHandler, IDownloadResult } from "@io-oidis-localhost/Io/Oidis/Localhost/Connectors/FileSystemHandler.js";
import { StreamResponse } from "@io-oidis-localhost/Io/Oidis/Localhost/HttpProcessor/ResponseApi/Handlers/StreamResponse.js";
import { ResponseFactory } from "@io-oidis-localhost/Io/Oidis/Localhost/HttpProcessor/ResponseApi/ResponseFactory.js";
import { ExternResponseType } from "@io-oidis-localhost/Io/Oidis/Localhost/Interfaces/IExternOptions.js";
import { IResponse } from "@io-oidis-localhost/Io/Oidis/Localhost/Interfaces/IResponse.js";
import { IHttpResponse } from "@io-oidis-localhost/Io/Oidis/Localhost/Interfaces/IRoute.js";
import { BaseConnector } from "@io-oidis-localhost/Io/Oidis/Localhost/Primitives/BaseConnector.js";
import { Extern, Route } from "@io-oidis-localhost/Io/Oidis/Localhost/Primitives/Decorators.js";
import { Loader } from "../Loader.js";
import { ProgramArgs } from "../Structures/ProgramArgs.js";

export class SendMail extends BaseConnector {
    private readonly config : ISendmailConfiguration;

    constructor() {
        super();
        this.config = Loader.getInstance().getAppConfiguration().sendmail;
    }

    @Extern(ExternResponseType.FULL_CONTROL)
    public Send($to : string | string[], $subject : string, $body : string, $attachments : string[] = null,
                $plainText : string = "", $copyTo : string | string[] = "", $from : string = "",
                $callback? : (($status : boolean) => void) | IResponse) : void {
        const response : IResponse = ResponseFactory.getResponse($callback);
        this.SendAsync({
            to         : $to,
            subject    : $subject,
            body       : $body,
            attachments: $attachments,
            plainText  : $plainText,
            copyTo     : $copyTo,
            from       : $from
        }).then(($status : boolean) : void => {
            response.Send($status);
        }).catch(($error : Error) : void => {
            response.OnError($error);
        });
    }

    public async SendAsync($options : ISendMailOptions) : Promise<boolean> {
        $options = JsonUtils.Extend({
            attachments: null,
            copyTo     : "",
            from       : "",
            plainText  : ""
        }, JsonUtils.Clone($options));
        if (ObjectValidator.IsArray($options.to)) {
            $options.to = (<string[]>$options.to).join(", ");
        }
        if (ObjectValidator.IsArray($options.copyTo)) {
            $options.copyTo = (<string[]>$options.copyTo).join(", ");
        }
        if (ObjectValidator.IsEmptyOrNull($options.from)) {
            $options.from = this.config.defaultSender;
        }

        if (ObjectValidator.IsEmptyOrNull($options.to)) {
            LogIt.Warning("Recipient must be specified.");
            return false;
        } else {
            const path : any = require("path");
            const fileSystem : FileSystemHandler = Loader.getInstance().getFileSystemHandler();

            $options.body = ObjectDecoder.Base64($options.body);
            if ((StringUtils.EndsWith($options.body, ".html") || StringUtils.EndsWith($options.body, ".txt")) &&
                fileSystem.Exists($options.body)) {
                $options.body = <string>fileSystem.Read($options.body);
            }
            $options.plainText = ObjectDecoder.Base64($options.plainText);
            if (!ObjectValidator.IsEmptyOrNull($options.body) && $options.body === StringUtils.StripTags($options.body)) {
                $options.plainText = $options.body;
                $options.body = "";
            }

            const programArgs : ProgramArgs = Loader.getInstance().getProgramArgs();
            const targetBase : string = programArgs.TargetBase();
            const storageBase : string = programArgs.AppDataPath();
            const attachments : any[] = [];
            if (!ObjectValidator.IsEmptyOrNull($options.attachments)) {
                $options.attachments.forEach(($filePath : string) : void => {
                    let absolutePath : string = targetBase + $filePath;
                    let reValidate : boolean = false;
                    if (!fileSystem.Exists(absolutePath)) {
                        absolutePath = storageBase + $filePath;
                        reValidate = true;
                    }
                    if (reValidate && !fileSystem.Exists(absolutePath)) {
                        LogIt.Debug("Invalid attachment path \"{0}\".", absolutePath);
                    } else {
                        attachments.push({
                            filename: path.basename(absolutePath),
                            path    : absolutePath
                        });
                    }
                });
            }
            if (!ObjectValidator.IsEmptyOrNull($options.body)) {
                const fileSystem : FileSystemHandler = Loader.getInstance().getFileSystemHandler();
                let index : number = 1;
                const images : string[] = [];
                const filter = /src=['"](.*?)['"]|url\(['"](.*?)['"]\)/gmi;
                let sources : any = filter.exec($options.body);
                while (sources != null) {
                    let path : string;
                    if (!ObjectValidator.IsEmptyOrNull(sources[1])) {
                        path = sources[1];
                    } else if (!ObjectValidator.IsEmptyOrNull(sources[2])) {
                        path = sources[2];
                    }
                    if (!ObjectValidator.IsEmptyOrNull(path)) {
                        if (images.indexOf(path)) {
                            images.push(path);
                        }
                    }
                    sources = filter.exec($options.body);
                }
                if (!ObjectValidator.IsEmptyOrNull(images)) {
                    images.forEach(($src : string) : void => {
                        if (!ObjectValidator.IsEmptyOrNull($src)) {
                            let absolutePath : string = targetBase + "/" + $src;
                            let reValidate : boolean = false;
                            if (!fileSystem.Exists(absolutePath)) {
                                absolutePath = storageBase + "/" + $src;
                                reValidate = true;
                            }
                            if (reValidate && !fileSystem.Exists(absolutePath)) {
                                LogIt.Debug("Invalid image path \"{0}\".", absolutePath);
                            } else {
                                let cid : string;
                                if (index < 10) {
                                    cid = "00" + index;
                                } else if (index < 100) {
                                    cid = "0" + index;
                                } else {
                                    cid = "" + index;
                                }
                                cid = "image." + cid + ".src";
                                index++;
                                attachments.push({
                                    cid,
                                    filename: path.basename(absolutePath),
                                    path    : absolutePath
                                });
                                $options.body = StringUtils.Replace($options.body, "src=\"" + $src + "\"", "src=\"cid:" + cid + "\"");
                                $options.body = StringUtils.Replace($options.body, "src='" + $src + "'", "src='cid:" + cid + "'");
                                $options.body = StringUtils.Replace($options.body, "url(\"" + $src + "\")", "url(cid:" + cid + ")");
                                $options.body = StringUtils.Replace($options.body, "url('" + $src + "')", "url(cid:" + cid + ")");
                            }
                        }
                    });
                }
            }

            const logMessage : string = "Message with subject: \"{0}\" send to: \"{1}\" with status: {2}";

            try {
                const mailContent : any = {
                    headers       : {
                        "X-Mailer": Loader.getInstance().getEnvironmentArgs().getProjectName() + "/" +
                            Loader.getInstance().getEnvironmentArgs().getProjectVersion()
                    },
                    from          : $options.from,
                    to            : $options.to,
                    cc            : $options.copyTo,
                    subject       : $options.subject,
                    text          : $options.plainText,
                    html          : $options.body,
                    attachments,
                    attachDataUrls: true
                };
                if (!ObjectValidator.IsEmptyOrNull($options.sender)) {
                    let onBehalfEnabled : boolean = false;
                    this.config.onBehalfPatterns.forEach(($pattern : string) : void => {
                        if (StringUtils.PatternMatched($pattern, $options.from)) {
                            onBehalfEnabled = true;
                        }
                    });
                    if (onBehalfEnabled) {
                        mailContent.sender = $options.sender;
                        mailContent.headers["X-Sender"] = $options.sender;
                    } else {
                        mailContent.from = $options.sender;
                        mailContent.headers["X-Sender"] = $options.sender;
                        mailContent.replyTo = $options.from;
                    }
                } else {
                    mailContent.headers["X-Sender"] = $options.from;
                }
                let info : any;
                if (this.config.serviceType === "relay") {
                    let url : string = this.config.location;
                    if (!StringUtils.EndsWith(url, "/api/relay-mail")) {
                        url += "/api/relay-mail";
                    }
                    const res : IDownloadResult = await Loader.getInstance().getFileSystemHandler().DownloadAsync({
                        url,
                        method      : HttpMethodType.POST,
                        headers     : {
                            "Accept"       : "application/json",
                            "Content-Type" : "application/json",
                            "Authorization": "Bearer " + this.config.pass
                        },
                        body        : JSON.stringify(mailContent),
                        streamOutput: true,
                        verbose     : false
                    });
                    info = JSON.parse(res.bodyOrPath);
                } else {
                    info = await this.invokeNodeMailer(mailContent);
                }
                if (!ObjectValidator.IsEmptyOrNull(info.rejected)) {
                    LogIt.Warning("Some recipients has rejected mail: " + JSON.stringify(info.rejected));
                }
                LogIt.Debug(logMessage, $options.subject, $options.to, "SUCCESS");
                return true;
            } catch (ex) {
                LogIt.Debug(logMessage, $options.subject, $options.to, "FAILURE");
                if (!ObjectValidator.IsEmptyOrNull(ex.message)) {
                    LogIt.Warning(ex.message);
                }
            }
            return false;
        }
    }

    @Route("POST /relay-mail", {responseHandler: new StreamResponse()})
    private async invokeNodeMailer($mailContent : any) : Promise<IHttpResponse> {
        const sendmail : Nodemailer = new Nodemailer();
        sendmail.setConfig(this.config);
        const info : any = await sendmail.Send($mailContent);
        return {
            body  : info,
            status: HttpStatusType.SUCCESS
        };
    }
}

export class Nodemailer extends BaseObject {
    private transporter : any;

    public setConfig($value : ISendmailConfiguration) : void {
        const nodemailer : any = require("nodemailer");
        const fileSystem : FileSystemHandler = Loader.getInstance().getFileSystemHandler();
        const programArgs : ProgramArgs = Loader.getInstance().getProgramArgs();
        const targetBase : string = programArgs.TargetBase();
        const storageBase : string = programArgs.AppDataPath();

        const transportConfig : any = {};

        if (!ObjectValidator.IsEmptyOrNull($value.serviceType)) {
            if ($value.serviceType === "sendmail") {
                JsonUtils.Extend(transportConfig, {
                    newline : "unix",
                    path    : "/usr/sbin/sendmail",
                    secure  : false,
                    sendmail: true
                });
            } else {
                transportConfig.service = $value.serviceType;
            }
        }

        if (!ObjectValidator.IsEmptyOrNull($value.location)) {
            JsonUtils.Extend(transportConfig, {
                host: $value.location,
                port: $value.port
            });
        }

        if (!ObjectValidator.IsEmptyOrNull($value.secure)) {
            transportConfig.secure = $value.secure;
        }

        if (!ObjectValidator.IsEmptyOrNull($value.user)) {
            transportConfig.auth = {
                pass: $value.pass,
                user: $value.user
            };
        }

        if (!ObjectValidator.IsEmptyOrNull($value.dkim)) {
            const keys : ISendmailDkim[] = JsonUtils.Clone($value.dkim);
            keys.forEach(($key : ISendmailDkim) : void => {
                if (StringUtils.Contains($key.privateKey, "/")) {
                    let privateKey : string = fileSystem.NormalizePath($key.privateKey);
                    if (StringUtils.StartsWith(privateKey, "./")) {
                        privateKey = targetBase + "/" + StringUtils.Substring(privateKey, 2);
                    } else if (!StringUtils.StartsWith(privateKey, "/") && !StringUtils.Contains(privateKey, ":/")) {
                        privateKey = storageBase + "/" + privateKey;
                    }
                    if (fileSystem.Exists(privateKey)) {
                        $key.privateKey = fileSystem.Read(privateKey).toString();
                    }
                }
            });
            transportConfig.dkim = {cacheDir: false, keys};
        }

        this.transporter = nodemailer.createTransport(transportConfig);
    }

    public async Send($data : any) : Promise<any> {
        return this.transporter.sendMail($data);
    }
}

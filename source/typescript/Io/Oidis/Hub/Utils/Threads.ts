/*! ******************************************************************************************************** *
 *
 * Copyright 2021-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { BaseObject } from "@io-oidis-commons/Io/Oidis/Commons/Primitives/BaseObject.js";
import { JsonUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/JsonUtils.js";
import { LogIt } from "@io-oidis-commons/Io/Oidis/Commons/Utils/LogIt.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { StringUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/StringUtils.js";
import { nodejsRoot } from "@io-oidis-localhost/Io/Oidis/Localhost/Utils/EnvironmentHelper.js";
import { IProject } from "../Interfaces/IProject.js";
import { Loader } from "../Loader.js";

export class Threads extends BaseObject {
    private worker : any;

    public static IsMainThread() : boolean {
        const {isMainThread} = require("worker_threads");
        return isMainThread;
    }

    public static OnMessage($callback : ($data : any) => void) : void {
        const {parentPort} = require("worker_threads");
        parentPort.on("message", $callback);
    }

    public static Send($data : any) : void {
        const {parentPort} = require("worker_threads");
        parentPort.postMessage($data);
    }

    public static getData() : any {
        const {workerData} = require("worker_threads");
        return workerData;
    }

    public static async Sleep($time : number = 1000) : Promise<void> {
        return new Promise<void>(($resolve : any) : void => {
            setTimeout(() : void => {
                $resolve();
            }, $time);
        });
    }

    constructor() {
        super();
        this.worker = null;
    }

    public Create($options? : IThreadOptions) : void {
        $options = JsonUtils.Extend(<IThreadOptions>{
            binClone  : true,
            initData  : {},
            scriptPath: ""
        }, $options);

        let source : string;
        let scriptEval : boolean = true;
        if ($options.binClone) {
            if (!ObjectValidator.IsEmptyOrNull($options.scriptPath)) {
                LogIt.Warning("It is not possible to combine binClone mode with defined script. Continuing with binClone.");
            }
            source = Loader.getInstance().getProgramArgs().BinBase() + "/resource/javascript/loader.min.js";
            scriptEval = false;
        } else if (ObjectValidator.IsEmptyOrNull($options.scriptPath)) {
            let packageName : string = Loader.getInstance().getEnvironmentArgs().getProjectName() + "-" +
                Loader.getInstance().getEnvironmentArgs().getProjectVersion();
            packageName = StringUtils.Replace(packageName, ".", "-");
            const config : IProject = JsonUtils.Clone(Loader.getInstance().getAppConfiguration());
            JsonUtils.Extend(config.logger, {
                adapters: ["std"]
            });
            source = `
                var window = {};
                var document = {};
                var localStorage = {};
                var nodejsRoot = "${nodejsRoot}";
                ${Loader.getInstance().getFileSystemHandler().Read("resource/javascript/" + packageName + ".min.js").toString()}
                ${Loader.ClassName()}.Load(${JSON.stringify(config)});
                `;
        } else {
            source = $options.scriptPath;
        }

        const {Worker, SHARE_ENV} = require("worker_threads");
        this.worker = new Worker(source, {
            argv      : [],
            env       : SHARE_ENV,
            eval      : scriptEval,
            workerData: $options.initData
        });
        this.worker.on("error", ($error : any) : void => {
            LogIt.Error($error);
        });
        this.worker.on("exit", ($code) => {
            if ($code !== 0) {
                LogIt.Error("Worker stopped with exit code {0}", $code);
            } else {
                LogIt.Info("Worker ended.");
            }
        });
    }

    public Send($data : any) : void {
        if (!ObjectValidator.IsEmptyOrNull(this.worker)) {
            this.worker.postMessage($data);
        }
    }

    public OnMessage($callback : ($data : any) => void) : void {
        if (!ObjectValidator.IsEmptyOrNull(this.worker) && !ObjectValidator.IsEmptyOrNull($callback)) {
            this.worker.on("message", ($data : any) : void => {
                $callback($data);
            });
        }
    }
}

export interface IThreadOptions {
    scriptPath? : string;
    initData? : any;
    binClone? : boolean;
}

// generated-code-start
export const IThreadOptions = globalThis.RegisterInterface(["scriptPath", "initData", "binClone"]);
// generated-code-end

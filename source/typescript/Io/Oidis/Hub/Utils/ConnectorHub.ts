/*! ******************************************************************************************************** *
 *
 * Copyright 2017-2018 NXP
 * Copyright 2019-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { HttpStatusType } from "@io-oidis-commons/Io/Oidis/Commons/Enums/HttpStatusType.js";
import { LogIt } from "@io-oidis-commons/Io/Oidis/Commons/Utils/LogIt.js";
import { ObjectDecoder } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectDecoder.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { LiveContentResponse } from "@io-oidis-localhost/Io/Oidis/Localhost/HttpProcessor/ResponseApi/Handlers/LiveContentResponse.js";
import { ResponseFactory } from "@io-oidis-localhost/Io/Oidis/Localhost/HttpProcessor/ResponseApi/ResponseFactory.js";
import { ExternResponseType } from "@io-oidis-localhost/Io/Oidis/Localhost/Interfaces/IExternOptions.js";
import { IResponse } from "@io-oidis-localhost/Io/Oidis/Localhost/Interfaces/IResponse.js";
import { Extern } from "@io-oidis-localhost/Io/Oidis/Localhost/Primitives/Decorators.js";
import { IRequestForwardingProtocol } from "@io-oidis-services/Io/Oidis/Services/Connectors/AgentsRegisterConnector.js";
import { ILiveContentProtocol, IWebServiceProtocol } from "@io-oidis-services/Io/Oidis/Services/Interfaces/IWebServiceProtocol.js";
import { Loader } from "../Loader.js";
import { AgentsRegister, IAgent, IAgentTaskRegisterInfo } from "../Primitives/AgentsRegister.js";

export class ConnectorHub extends AgentsRegister {
    private pipes : IResponse[][] = <any>{};

    @Extern(ExternResponseType.FULL_CONTROL)
    public async ForwardRequest($agentId : string, $data : IWebServiceProtocol, $callback? : IResponse) : Promise<void> {
        await this.createForwardRequest($callback, $agentId, $data);
    }

    @Extern(ExternResponseType.FULL_CONTROL)
    public InitPipe($pipeId : string, $callback? : IResponse) : void {
        if (ObjectValidator.IsEmptyOrNull(this.pipes[$pipeId])) {
            this.pipes[$pipeId] = <any>{};
        }
        const response : IResponse = ResponseFactory.getResponse($callback);
        const clients : string[] = this.pipes[$pipeId];

        if (Object.keys(clients).length !== 2) {
            const clientId : string = ConnectorHub.UID();
            clients[clientId] = $callback;
            LogIt.Info("Client [" + clientId + "] has connected to pipe [" + $pipeId +
                "]. Adding to register ...");
            response.Send([true]);

            const clientIds : string[] = Object.keys(clients);
            if (clientIds.length === 2) {
                clientIds.forEach(($id : string) : void => {
                    ResponseFactory.getResponse(clients[$id]).OnStart($id);
                });
            }
            response.AddAbortHandler(() : void => {
                /* eslint-disable @typescript-eslint/no-array-delete */
                LogIt.Warning("Client [" + clientId + "] has been disconnected from pipe [" + $pipeId +
                    "]. Removing from register ...");
                delete clients[clientId];
                const clientIds : string[] = Object.keys(clients);
                clientIds.forEach(($id : string) : void => {
                    ResponseFactory.getResponse(clients[$id]).OnComplete();
                });
                if (clientIds.length === 0) {
                    delete this.pipes[$pipeId];
                }
                /* eslint-enable */
            });
        } else {
            response.Send([false, "Pipe [" + $pipeId + "] is already initialized."]);
        }
    }

    @Extern(ExternResponseType.FULL_CONTROL)
    public WriteToPipe($sourceClientId : string, $pipeId : string, $data : string,
                       $callback? : IResponse) : void {
        const clients : string[] = this.pipes[$pipeId];
        if (!ObjectValidator.IsEmptyOrNull(this.pipes[$pipeId])) {
            const clientIds : string[] = Object.keys(clients);
            if (clientIds.indexOf($sourceClientId) !== -1) {
                clientIds.forEach(($clientId : string) : void => {
                    if ($clientId !== $sourceClientId) {
                        ResponseFactory.getResponse(clients[$clientId]).OnChange($data);
                    }
                });
                ResponseFactory.getResponse($callback).Send([true]);
            } else {
                ResponseFactory.getResponse($callback).Send([false, "Client \"" + $sourceClientId + "\" doesn't exist."]);
            }
        } else {
            ResponseFactory.getResponse($callback).Send([false, "Pipe \"" + $pipeId + "\" doesn't exist."]);
        }
    }

    protected async createForwardRequest($response : IResponse, $agentId : string, $data : IWebServiceProtocol) : Promise<string> {
        const taskId : string = ConnectorHub.UID();
        const agent : IAgent = this.getAgent($agentId);
        const response : LiveContentResponse = new LiveContentResponse(ResponseFactory.getResponse($response));
        if (!ObjectValidator.IsEmptyOrNull(agent)) {
            AgentsRegister.tasks.push({id: taskId, response: $response});
            let authorized : boolean = true;
            if (ObjectValidator.IsObject($data) && ObjectValidator.IsString($data.data)) {
                try {
                    const decoded : ILiveContentProtocol = JSON.parse(ObjectDecoder.Base64(<string>$data.data));
                    authorized = await Loader.getInstance().getAuthManager().IsAuthorizedFor(decoded.name, $data.token);
                } catch (ex) {
                    LogIt.Warning("Failed to parse forwarded message: " + ex.stack);
                }
            }
            if (authorized) {
                agent.connection.OnChange(<IRequestForwardingProtocol>{
                    protocol: $data,
                    taskId
                });
                response.Send(true);
                response.AddAbortHandler(() : void => {
                    const index : number = AgentsRegister.tasks
                        .findIndex(($item : IAgentTaskRegisterInfo) : boolean => $item.id === taskId);
                    if (index > -1) {
                        AgentsRegister.tasks.splice(index, 1);
                    }
                });
            } else {
                $data.status = HttpStatusType.NOT_AUTHORIZED;
                response.SendObject($data);
            }
        } else {
            response.Send(false);
        }
        return taskId;
    }
}

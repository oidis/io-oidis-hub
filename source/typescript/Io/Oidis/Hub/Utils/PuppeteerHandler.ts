/*! ******************************************************************************************************** *
 *
 * Copyright 2023-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { LogSeverity } from "@io-oidis-commons/Io/Oidis/Commons/Enums/LogSeverity.js";
import { BaseObject } from "@io-oidis-commons/Io/Oidis/Commons/Primitives/BaseObject.js";
import { JsonUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/JsonUtils.js";
import { LogIt } from "@io-oidis-commons/Io/Oidis/Commons/Utils/LogIt.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { StringUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/StringUtils.js";
import { IPDFOptions } from "@io-oidis-hub-gui/Io/Oidis/Hub/Gui/Interfaces/IPDFOptions.js";
import { EnvironmentHelper } from "@io-oidis-localhost/Io/Oidis/Localhost/Utils/EnvironmentHelper.js";
import { IPuppeteerConfiguration } from "../Interfaces/IProject.js";
import { Loader } from "../Loader.js";
import { Threads } from "./Threads.js";

export class PuppeteerHandler extends BaseObject {
    private readonly puppeteer : any;
    private browser : any;
    private pages : IPuppeteerPageHandler[];
    private readonly config : IPuppeteerConfiguration;

    public static getInstanceSingleton() : PuppeteerHandler {
        const instance : PuppeteerHandler = new PuppeteerHandler();
        PuppeteerHandler.getInstanceSingleton = () : PuppeteerHandler => {
            return instance;
        };
        return instance;
    }

    constructor() {
        super();
        try {
            this.puppeteer = require("puppeteer-core");
        } catch (ex) {
            LogIt.Error(ex);
            throw new Error("PuppeteerHandler requires usage of pdfPrint.jsonp and oidis/oidis-chromium-base.");
        }
        this.config = JsonUtils.Clone(Loader.getInstance().getAppConfiguration().puppeteer);
    }

    public setConfig($value : IPuppeteerConfiguration) : void {
        JsonUtils.Extend(this.config, $value);
    }

    public async Export($input : IPuppeteerExportOptions, $options? : IPDFOptions) : Promise<Buffer> {
        $input = JsonUtils.Extend({
            cookies: [],
            format : "pdf",
            html   : "",
            url    : "",
            verbose: true
        }, $input);

        $options = JsonUtils.Extend({
            cookies            : [],
            css                : "",
            displayHeaderFooter: false,
            inputs             : [],
            landscape          : false,
            language           : "en",
            license            : `
<!--

Copyright ${StringUtils.YearFrom(2019)} Oidis

SPDX-License-Identifier: BSD-3-Clause
The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution

or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText

-->`,
            margin             : {
                bottom: 0,
                left  : 0,
                right : 0,
                top   : 0
            }
        }, $options);
        if (ObjectValidator.IsEmptyOrNull($options.width) && ObjectValidator.IsEmptyOrNull($options.height) &&
            ObjectValidator.IsEmptyOrNull($options.format)) {
            $options.format = "A4";
        }
        $input.cookies = $options.cookies;

        if (!ObjectValidator.IsEmptyOrNull($input.html)) {
            const style : HTMLStyleElement = document.createElement("style");
            style.type = "text/css";
            style.appendChild(document.createTextNode(`
*, ::after, ::before {box-sizing: border-box}
body {font-family: "Segoe UI", Roboto, "Helvetica Neue", Arial, "Noto Sans", "Liberation Sans", sans-serif;font-size: 1rem;font-weight: 400;line-height: 1.5;}
html,body,section {padding:0;margin: 0;height: 100%; width: 100%;}
.container-fluid {margin-left: auto;margin-right: auto;width: 100%;}
.row {display: flex;flex-wrap: wrap;}
.row > * {flex-shrink: 0;width: 100%;max-width: 100%;}
.col {flex: 1 0 0%;}
.col-2 {flex: 0 0 auto;width: 16.6666666667%;}
@media (min-width: 1200px) {.col-xl-4 {flex: 0 0 auto;width: 33.3333333333%}}
.flex-column {flex-direction: column;}
.d-flex {display: flex;}
.h-100 {height: 100%;}
.mt-auto {margin-top: auto;}
.justify-content-center {justify-content: center;}
.text-center {text-align: center;}
table {caption-side: bottom;border-collapse: collapse;}
th {text-align: inherit;text-align: -webkit-match-parent}
tbody,td,tfoot,th,thead,tr {border-color: inherit;border-style: solid;border-width: 0;}
table,thead,tbody,tr {page-break-after: avoid;page-break-before: avoid;page-break-inside: avoid;}
.table {width: 100%;margin-bottom: 1rem;vertical-align: top;border-color: #000000;}
.table > :not(caption) > * > * {padding: .5rem .5rem;border-bottom-width: 1px;}
.table > tbody {vertical-align: inherit;}
.table > thead {vertical-align: bottom;}
.table-sm > :not(caption) > * > * {padding: .25rem .25rem}
.table-bordered > :not(caption) > * {border-width: 1px 0;}
.table-bordered > :not(caption) > * > * {border-width: 0 1px;}
p{margin-top: 0;margin-bottom: 1rem}
.d-none {display: none!important;}
` + $options.css));
            $input.html = `
<!DOCTYPE html>
${$options.license}
<html lang="${$options.language}">
<body>
${style.outerHTML}
${$input.html}
</body>
</html>
`;
        }

        return this.Extract($input, async ($page : any) : Promise<any> => {
            if (!ObjectValidator.IsEmptyOrNull($input.url)) {
                await $page.waitForSelector("#printStart");
                if (!ObjectValidator.IsEmptyOrNull($options.inputs)) {
                    for await (const input of $options.inputs) {
                        await $page.$eval("#" + input.name, (el, value) => el.value = value, input.value);
                    }
                }
                const button : any = await $page.$("#printStart");
                await button.evaluate((b) => b.click());
                await $page.waitForSelector("#printComplete");
                if (!ObjectValidator.IsEmptyOrNull($options.css)) {
                    await $page.addStyleTag({content: $options.css});
                }
            }
            const data : any = $input.format === "pdf" ? await $page.pdf($options) : await $page.screenshot();
            return Buffer.from(<any>Object.values(data));
        });
    }

    public async Extract($input : IPuppeteerExtractOptions, $convertor : ($page : any) => Promise<any>) : Promise<any> {
        $input = JsonUtils.Extend({
            debug  : false,
            html   : "",
            url    : "",
            verbose: false
        }, $input);
        const handlerObject : IPuppeteerPageHandler = await this.getHandler($input.url, $input.verbose, $input.debug);
        const waitIndex : number[] = new Array(this.config.maxWaitTime);
        if (handlerObject.processing) {
            let skipped : boolean = false;
            for await (const index of waitIndex) {
                await Threads.Sleep(index * 100);
                if (!handlerObject.processing) {
                    skipped = true;
                    break;
                }
            }
            if (!skipped) {
                LogIt.Warning("Max wait time reached");
                return null;
            }
        }
        try {
            handlerObject.processing = true;
            if (!ObjectValidator.IsEmptyOrNull($input.url)) {
                if (!ObjectValidator.IsEmptyOrNull($input.cookies)) {
                    await handlerObject.handler.setCookie(...$input.cookies);
                }
                if (handlerObject.content !== $input.url) {
                    await handlerObject.handler.goto($input.url, {
                        waitUntil: ["load", "networkidle0"]
                    });
                    handlerObject.content = $input.url;
                }
            } else {
                await handlerObject.handler.setContent($input.html, {
                    waitUntil: "networkidle0"
                });
            }
            const output : any = await $convertor(handlerObject.handler);
            handlerObject.processing = false;
            return output;
        } catch (ex) {
            LogIt.Error(ex);
            if (!ObjectValidator.IsEmptyOrNull(this.browser)) {
                try {
                    await this.browser.close();
                } catch (ex) {
                    LogIt.Warning("Failed to close puppeteer.\n" + ex.stack);
                    try {
                        process.kill(this.browser.process().pid);
                    } catch (ex) {
                        LogIt.Warning("Failed to kill puppeteer.\n" + ex.stack);
                    }
                }
                this.browser = null;
            }
        }
        return null;
    }

    public async Init($onProgress? : ($downloadedBytes : number, $totalBytes : number) => void) : Promise<string> {
        const browsers : any = require("@puppeteer/browsers");
        let platform : string = "linux";
        if (EnvironmentHelper.IsWindows()) {
            platform = EnvironmentHelper.Is64bit() ? "win64" : "win32";
        } else if (EnvironmentHelper.IsMac()) {
            platform = EnvironmentHelper.IsArm() ? "mac_arm" : "mac";
        }
        let cacheDir : string = this.config.cacheDir;
        if (ObjectValidator.IsEmptyOrNull(cacheDir)) {
            cacheDir = Loader.getInstance().getProgramArgs().ProjectBase();
            if (StringUtils.Contains(cacheDir, "/build/target")) {
                cacheDir += "/../appData";
            } else {
                cacheDir = Loader.getInstance().getFileSystemHandler().getTempPath() + "/" +
                    Loader.getInstance().getEnvironmentArgs().getProjectName();
            }
            cacheDir += "/puppeteer";
        }
        LogIt.Info("> Puppeteer browser cacheDir: " + cacheDir);

        const result : any = await browsers.install({
            browser                 : "chrome",
            buildId                 : this.config.chromeBuildId,
            cacheDir,
            downloadProgressCallback: ($downloadedBytes : number, $totalBytes : number) : void => {
                if (!ObjectValidator.IsEmptyOrNull($onProgress)) {
                    $onProgress($downloadedBytes, $totalBytes);
                } else {
                    LogIt.Info($downloadedBytes + "/" + $totalBytes, LogSeverity.LOW);
                }
            },
            platform
        });
        return result.executablePath;
    }

    private async getHandler($forUrl : string = null, $verbose : boolean = true,
                             $debug : boolean = false) : Promise<IPuppeteerPageHandler> {
        if (ObjectValidator.IsEmptyOrNull(this.browser)) {
            const args : string[] = [
                "--no-sandbox",
                "--disable-setuid-sandbox",
                "--font-render-hinting=none"
            ];
            if (this.config.proxy.enabled) {
                args.push("--proxy-server=" + this.config.proxy.location);
            }
            if ($debug) {
                args.push("--start-maximized");
            }
            const options : any = {args, executablePath: await this.Init()};
            if ($debug) {
                JsonUtils.Extend(options, {
                    defaultViewport: null,
                    headless       : false
                });
            }
            this.browser = await this.puppeteer.launch(options);
            this.pages = [{handler: await this.browser.newPage(), content: "inlineCode", processing: false}];
            for (let index : number = 0; index < this.config.urlThreads; index++) {
                this.pages.push({handler: await this.browser.newPage(), content: null, processing: false});
            }
            if ($verbose) {
                this.pages.forEach(($handler : any) : void => {
                    $handler.handler
                        .on("console", (message) => LogIt.Debug("[PUPPETEER] " + message.text()))
                        .on("pageerror", ({message}) => LogIt.Error("[PUPPETEER] " + message));
                });
            }
        }
        if (this.pages.length === 1) {
            return this.pages[0];
        }
        let handler : IPuppeteerPageHandler = null;
        this.pages.forEach(($handler : any) : void => {
            if (ObjectValidator.IsEmptyOrNull($forUrl)) {
                if ($handler.content === "inlineCode") {
                    handler = $handler;
                }
            } else {
                if ($handler.content === $forUrl && !$handler.processing) {
                    handler = $handler;
                }
            }
        });
        if (ObjectValidator.IsEmptyOrNull(handler) && !ObjectValidator.IsEmptyOrNull($forUrl)) {
            this.pages.forEach(($handler : any) : void => {
                if (!$handler.processing) {
                    handler = $handler;
                }
            });
            if (ObjectValidator.IsEmptyOrNull(handler)) {
                handler = this.pages[1];
            }
        }
        return handler;
    }
}

export interface IPuppeteerExtractOptions {
    url? : string;
    html? : string;
    verbose? : boolean;
    cookies? : any[];
    debug? : boolean;
}

export interface IPuppeteerExportOptions extends IPuppeteerExtractOptions {
    format? : string;
}

export interface IPuppeteerPageHandler {
    handler : any;
    content : string;
    processing : boolean;
}

// generated-code-start
export const IPuppeteerExtractOptions = globalThis.RegisterInterface(["url", "html", "verbose", "cookies", "debug"]);
export const IPuppeteerExportOptions = globalThis.RegisterInterface(["format"], <any>IPuppeteerExtractOptions);
export const IPuppeteerPageHandler = globalThis.RegisterInterface(["handler", "content", "processing"]);
// generated-code-end

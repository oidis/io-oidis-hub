/*! ******************************************************************************************************** *
 *
 * Copyright 2022-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { IModel } from "@io-oidis-commons/Io/Oidis/Commons/Interfaces/IBaseObject.js";
import { LogIt } from "@io-oidis-commons/Io/Oidis/Commons/Utils/LogIt.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { StringUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/StringUtils.js";
import { IAuthCodeRequestResult, IAuthCodeValidateResult } from "@io-oidis-hub-gui/Io/Oidis/Hub/Gui/Connectors/AuthCodeConnector.js";
import { ISystemGroups } from "@io-oidis-hub-gui/Io/Oidis/Hub/Gui/Connectors/AuthManagerConnector.js";
import { AuthCode } from "@io-oidis-hub-gui/Io/Oidis/Hub/Gui/Models/AuthCode.js";
import { BaseConnector } from "@io-oidis-localhost/Io/Oidis/Localhost/Primitives/BaseConnector.js";
import { Extern } from "@io-oidis-localhost/Io/Oidis/Localhost/Primitives/Decorators.js";
import { User } from "../DAO/Models/User.js";
import { Loader } from "../Loader.js";
import { SystemLog } from "../LogProcessor/SystemLog.js";
import { AuthManager } from "./AuthManager.js";
import { MailEngine } from "./MailEngine.js";

export class AuthCodeManager extends BaseConnector {
    protected auth : AuthManager;
    protected localization : IAuthCodeManagerLocalization;
    private emailSender : MailEngine;

    constructor() {
        super();
        this.auth = Loader.getInstance().getAuthManager();
        this.emailSender = Loader.getInstance().getMailEngine();
        this.localization = {
            modelNotFound      : "Protected model has not been found: ",
            tooManyRequests    : "Too many request for new personal code. Try it later.",
            tooManyAttempts    : "Too many wrong attempts for code validation. Try it later.",
            newCodeSend        : "New personal code has been sent to account primary email.",
            missingCodeValue   : "Code value must be entered.",
            unauthorizedRequest: "Unauthorized request for protected model."
        };
    }

    @Extern()
    public async SendAuthCode($id : string | IModelWithAuthCode, $force : boolean = false, $to : string = null) : Promise<void> {
        const model : IModelWithAuthCode = ObjectValidator.IsString($id) ? (await this.getModelById(<string>$id)) : <IModelWithAuthCode>$id;
        if (ObjectValidator.IsEmptyOrNull(model)) {
            throw new Error(this.localization.modelNotFound + $id);
        }
        if (ObjectValidator.IsEmptyOrNull(model.AuthCode)) {
            model.AuthCode = new AuthCode();
            model.AuthCode.Size = 4;
        }
        const user : User = await this.auth.getUser(this.getCurrentToken());
        let sendAllowed : boolean = await this.sendAllowed(model, user) || $force;
        if (sendAllowed) {
            if (model.AuthCode.Send && !$force) {
                sendAllowed = false;
            }
        }
        if (sendAllowed) {
            const code : string = this.generateCode(model.AuthCode.Size);
            model.AuthCode.Code = StringUtils.getSha1(code);
            const email : string = await this.getOwnerEmail(model, user);
            if (!ObjectValidator.IsEmptyOrNull(email)) {
                if (model.AuthCode.Send) {
                    let sendTo : string = email;
                    if (!ObjectValidator.IsEmptyOrNull($to) && $to !== sendTo) {
                        sendTo = $to;
                    }
                    await this.emailSender.ResendNewCode(sendTo, model.Id, code, email);
                } else {
                    await this.emailSender.SendNewCode(email, model.Id, code);
                    model.AuthCode.Send = true;
                }
                await model.Save();
            } else {
                LogIt.Debug("Failed to send tracking code for " + model.Id + " due to missing recipient email.");
            }
        }
    }

    @Extern()
    public async RequestAuthCode($id : string, $email : string, $verifyEmail : string,
                                 $verifyPhone : string) : Promise<IAuthCodeRequestResult> {
        const model : IModelWithAuthCode = await this.getModelById($id);
        if (ObjectValidator.IsEmptyOrNull(model)) {
            return {
                message: this.localization.modelNotFound + $id,
                size   : 0,
                status : false
            };
        }
        const user : User = await this.auth.getUser(this.getCurrentToken());
        let validationPassed : boolean = true;
        if (!ObjectValidator.IsEmptyOrNull($verifyEmail)) {
            const email : string = await this.getOwnerEmail(model, user);
            if (ObjectValidator.IsEmptyOrNull(email) ||
                StringUtils.ToLowerCase(email) !== StringUtils.ToLowerCase($verifyEmail)) {
                validationPassed = false;
            }
        } else if (!ObjectValidator.IsEmptyOrNull($verifyPhone)) {
            const phone : string = await this.getOwnerPhone(model, user);
            if (ObjectValidator.IsEmptyOrNull(phone)) {
                validationPassed = false;
            } else if (phone.substring(phone.length - 9) !== $verifyPhone.substring($verifyPhone.length - 9)) {
                validationPassed = false;
            }
        } else {
            validationPassed = false;
        }

        if (validationPassed) {
            model.AuthCode.RequestAttempts = 0;
        } else if (model.AuthCode.RequestAttempts < 4) {
            model.AuthCode.RequestAttempts++;
        } else {
            if (new Date().getTime() - model.AuthCode.LastRequest.getTime() >= 15000) {
                model.AuthCode.RequestAttempts = 0;
            } else {
                return {
                    message: this.localization.tooManyRequests,
                    size   : model.AuthCode.Size,
                    status : false
                };
            }
        }
        model.AuthCode.LastRequest = new Date();
        if (validationPassed) {
            await this.SendAuthCode(model, true, $email);
            await SystemLog.Trace({
                message  : "Requested new code for: " + $id,
                token    : this.getCurrentToken(),
                traceBack: this.getClassNameWithoutNamespace() + ".RequestAuthCode"
            });
        } else {
            await model.Save();
        }
        return {
            size  : model.AuthCode.Size,
            status: validationPassed
        };
    }

    @Extern()
    public async Validate($id : string, $code : string) : Promise<IAuthCodeValidateResult> {
        const model : IModelWithAuthCode = await this.getModelById($id);
        if (ObjectValidator.IsEmptyOrNull(model)) {
            return {
                codeEnabled: false,
                error      : this.localization.modelNotFound + $id
            };
        }
        const user : User = await this.auth.getUser(this.getCurrentToken());

        if (!await this.validateModelOwner(model, user)) {
            return {codeEnabled: true, codePassed: false, error: this.localization.unauthorizedRequest};
        }

        const force : boolean = Loader.getInstance().getAppConfiguration().authManagerWorker.twoFactorAuthentication.force;
        /// TODO: validation should pass also in case of that user does not have 2FA and OneTimeCode is not enabled for whole AuthManger
        /// TODO: code send should be on request or generated as part of 2FA enablement?
        //        In case of that user does not have 2FA it should be generated if 2FA is configured as mandatory for AuthManager
        if ((force || ObjectValidator.IsEmptyOrNull($code)) && (
            ObjectValidator.IsEmptyOrNull(user.TwoFASecret) &&
            (ObjectValidator.IsEmptyOrNull(model.AuthCode) || ObjectValidator.IsEmptyOrNull(model.AuthCode?.Code)))) {
            const groups : ISystemGroups = await this.auth.getSystemGroups();
            if ((
                (!force && ObjectValidator.IsEmptyOrNull(user.TwoFASecret)) ||
                (force && (ObjectValidator.IsEmptyOrNull(model.AuthCode) || ObjectValidator.IsEmptyOrNull(model.AuthCode?.Code)))
            ) && !await this.auth.HasGroup(groups.Admin, user)) {
                await this.SendAuthCode(model.Id, true);
            }
            // return {codeEnabled: true, codePassed: false, error: this.localization.newCodeSend};
        }
        if (ObjectValidator.IsEmptyOrNull($code)) {
            const output : IAuthCodeValidateResult = {
                codeEnabled  : true,
                codePassed   : false,
                emailTemplate: "",
                error        : "",
                phoneTemplate: ""
            };
            const email : string = await this.getOwnerEmail(model, user);
            const phone : string = await this.getOwnerPhone(model, user);
            if (!ObjectValidator.IsEmptyOrNull(email)) {
                output.emailTemplate = email.substring(0, 3) + "***@***" + email.substring(email.length - 2);
            }
            if (!ObjectValidator.IsEmptyOrNull(phone)) {
                output.phoneTemplate = "*** *** " + phone.substring(phone.length - 3);
            }
            return output;
        }

        let valid : boolean = false;
        if (!ObjectValidator.IsEmptyOrNull($code)) {
            if (model.AuthCode.Code === $code) {
                model.AuthCode.ValidationAttempts = 0;
                /// TODO: this should be conditional if Code should be used as onetime
                model.AuthCode.Code = "";
                valid = true;
            } else if (model.AuthCode.ValidationAttempts < 3) {
                model.AuthCode.ValidationAttempts++;
            }
            model.AuthCode.LastValidation = new Date();
            await model.Save();
        }
        if (!valid && model.AuthCode.ValidationAttempts >= 3) {
            if (new Date().getTime() - model.AuthCode.LastValidation.getTime() >= 15000) {
                model.AuthCode.ValidationAttempts = 0;
                model.AuthCode.LastValidation = new Date();
                await model.Save();
            } else {
                return {
                    codeEnabled: true,
                    codePassed : false,
                    error      : this.localization.tooManyAttempts
                };
            }
        }
        return {codeEnabled: true, codePassed: valid};
    }

    protected async validateModelOwner($model : IModelWithAuthCode, $owner : User) : Promise<boolean> {
        return !ObjectValidator.IsEmptyOrNull($model) && !ObjectValidator.IsEmptyOrNull($owner) && $model.Id === $owner.Id;
    }

    protected async getModelById($id : string) : Promise<IModelWithAuthCode> {
        return this.auth.getUser($id);
    }

    protected async sendAllowed($model : IModelWithAuthCode, $owner : User) : Promise<boolean> {
        return true;
    }

    protected async getOwnerEmail($model : IModelWithAuthCode, $owner : User) : Promise<string> {
        return $owner.Email;
    }

    protected async getOwnerPhone($model : IModelWithAuthCode, $owner : User) : Promise<string> {
        return $owner.Phone;
    }

    private generateCode($size : number = 4) : string {
        let code : string = "";
        const min : number = 1;
        const max : number = 9;
        for (let index : number = 0; index < $size; index++) {
            code += Math.floor(Math.random() * (max - min + 1)) + min;
        }
        return code;
    }
}

export interface IModelWithAuthCode extends IModel {
    Id : string;
    AuthCode : AuthCode;
}

export interface IAuthCodeManagerLocalization {
    modelNotFound : string;
    tooManyRequests : string;
    tooManyAttempts : string;
    newCodeSend : string;
    missingCodeValue : string;
    unauthorizedRequest : string;
}

// generated-code-start
/* eslint-disable */
export const IModelWithAuthCode = globalThis.RegisterInterface(["Id", "AuthCode"], <any>IModel);
export const IAuthCodeManagerLocalization = globalThis.RegisterInterface(["modelNotFound", "tooManyRequests", "tooManyAttempts", "newCodeSend", "missingCodeValue", "unauthorizedRequest"]);
/* eslint-enable */
// generated-code-end

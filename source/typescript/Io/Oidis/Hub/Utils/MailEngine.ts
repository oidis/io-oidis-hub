/*! ******************************************************************************************************** *
 *
 * Copyright 2021 PASAJA Authors
 * Copyright 2021-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { LanguageType } from "@io-oidis-commons/Io/Oidis/Commons/Enums/LanguageType.js";
import { LogLevel } from "@io-oidis-commons/Io/Oidis/Commons/Enums/LogLevel.js";
import { ILoggerTrace } from "@io-oidis-commons/Io/Oidis/Commons/LogProcessor/Logger.js";
import { BaseEnum } from "@io-oidis-commons/Io/Oidis/Commons/Primitives/BaseEnum.js";
import { BaseObject } from "@io-oidis-commons/Io/Oidis/Commons/Primitives/BaseObject.js";
import { JsonUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/JsonUtils.js";
import { LogIt } from "@io-oidis-commons/Io/Oidis/Commons/Utils/LogIt.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { Property } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Property.js";
import { StringUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/StringUtils.js";
import { EventsManager } from "@io-oidis-gui/Io/Oidis/Gui/Events/EventsManager.js";
import { FeedbackType } from "@io-oidis-hub-gui/Io/Oidis/Hub/Gui/Enums/FeedbackType.js";
import { ISendmailConfiguration } from "@io-oidis-hub-gui/Io/Oidis/Hub/Gui/Interfaces/ISendmailConfiguration.js";
import { ISendMailOptions } from "@io-oidis-hub-gui/Io/Oidis/Hub/Gui/Interfaces/ISendMailOptions.js";
import { StringReplaceType } from "@io-oidis-localhost/Io/Oidis/Localhost/Enums/StringReplaceType.js";
import { StringReplace } from "@io-oidis-localhost/Io/Oidis/Localhost/Utils/StringReplace.js";
import { MailEngineDAO } from "../DAO/MailEngineDAO.js";
import { EmailRecord } from "../DAO/Models/EmailRecord.js";
import { IMailContent, IMailEngineConfiguration } from "../Interfaces/DAO/IMailEngineConfiguration.js";
import { Loader } from "../Loader.js";
import { SystemLog } from "../LogProcessor/SystemLog.js";
import { SendMail } from "./SendMail.js";

export class MailEngine extends BaseObject {
    protected readonly config : ISendmailConfiguration;
    protected readonly sender : SendMail;
    protected readonly domain : string;
    private readonly dao : MailEngineDAO;
    private language : LanguageType;
    private readonly loopState : IMailEngineLoopInfo;
    private loopThread : number;

    constructor() {
        super();
        this.config = Loader.getInstance().getAppConfiguration().sendmail;
        this.domain = this.config.domain;
        if (ObjectValidator.IsEmptyOrNull(this.domain)) {
            this.domain = Loader.getInstance().getHttpManager().getRequest().getHostUrl();
        }
        this.dao = new MailEngineDAO();
        this.language = LanguageType.EN;
        this.sender = new SendMail();
        this.loopState = {
            fetchTick: Loader.getInstance().getAppConfiguration().sendmail.fetchTick,
            status   : IMailEngineLoopStatus.Stopped
        };
        this.loopThread = null;
    }

    public Language($value? : LanguageType) : LanguageType {
        return this.language = Property.EnumType(this.language, $value, LanguageType, LanguageType.EN);
    }

    public async Register($email : string, $token : string) : Promise<boolean> {
        return this.sendMail($email, this.getUrl("/accountActivation", $token), EmailType.REGISTER);
    }

    public async ResetPassword($email : string, $token : string) : Promise<boolean> {
        return this.sendMail($email, this.getUrl("/resetPassword", $token), EmailType.RESET_PASSWORD);
    }

    public async ResetPasswordConfirm($email : string) : Promise<boolean> {
        return this.sendMail($email, "mailto:<? @var accountsAddress ?>", EmailType.RESET_PASSWORD_CONFIRM);
    }

    public async Invitation($email : string, $group : string, $owner : string, $token : string) : Promise<boolean> {
        return this.sendMail($email, this.getUrl("/invite", $token), EmailType.INVITATION, {group: $group, owner: $owner});
    }

    public async Activation($email : string, $token : string, $daysLeft : number) : Promise<boolean> {
        return this.sendMail($email, this.getUrl("/accountActivation", $token), EmailType.ACTIVATION, {days: $daysLeft});
    }

    public async Deactivation($email : string, $token : string) : Promise<boolean> {
        return this.sendMail($email, this.getUrl("/accountActivation", $token), EmailType.DEACTIVATION);
    }

    public async Removed($email : string) : Promise<boolean> {
        return this.sendMail($email, this.getUrl("/singUp"), EmailType.REMOVED);
    }

    public async Restored($email : string) : Promise<boolean> {
        return this.sendMail($email, this.getUrl("/singIn"), EmailType.RESTORED);
    }

    public async ReportFailure($traces : ILoggerTrace[]) : Promise<boolean> {
        try {
            const logger : any = LogIt.getLogger();
            let message : string = "";
            $traces.forEach(($trace : ILoggerTrace) : void => {
                message += logger.anonymize(logger.serializeMessage($trace.message, true, "<br>")) + "<br>";
            });
            return this.sendMail("", this.getUrl("/logs"), EmailType.REPORT_FAILURE, {
                message
            });
        } catch (ex) {
            console.error(ex); // eslint-disable-line no-console
        }
    }

    public async Feedback($email : string, $category : FeedbackType, $message : string, $uuid? : string,
                          $isPublic : boolean = false) : Promise<boolean> {
        let category : string;
        switch ($category) {
        case FeedbackType.Registry:
            category = "Registry";
            break;
        case FeedbackType.Agents:
            category = "Agents";
            break;
        case FeedbackType.Logs:
            category = "Logs and Reports";
            break;
        case FeedbackType.Email:
            category = "Emails";
            break;
        case FeedbackType.Permissions:
            category = "Permissions";
            break;
        default:
            category = "General request";
            break;
        }
        if (ObjectValidator.IsEmptyOrNull($uuid)) {
            $uuid = "Unknown";
        }
        return this.sendMail($email, "", $isPublic ? EmailType.PUBLIC_FEEDBACK : EmailType.FEEDBACK, {
            category,
            clientMail: $email,
            message   : $message,
            uuid      : $uuid
        });
    }

    public async SendNewCode($email : string, $id : string, $code : string) : Promise<boolean> {
        return this.sendMail($email, this.getUrl("/login"), EmailType.NEW_AUTH_CODE, {
            code  : $code,
            dataId: $id
        });
    }

    public async ResendNewCode($email : string, $id : string, $code : string, $ccEmail? : string) : Promise<boolean> {
        const env : any = {
            code  : $code,
            dataId: $id
        };
        if (!ObjectValidator.IsEmptyOrNull($ccEmail) && $email !== $ccEmail) {
            env.copyTo = $ccEmail;
        }
        return this.sendMail($email, this.getUrl("/login"), EmailType.AUTH_CODE_RESEND, env);
    }

    public StartLoop($fetchTick? : number) : void {
        let fetchInterval : number = Loader.getInstance().getAppConfiguration().sendmail.fetchTick;
        if (!ObjectValidator.IsEmptyOrNull($fetchTick)) {
            fetchInterval = $fetchTick;
            LogIt.Info("Using new fetchTick passed as argument to StartLoop method: " + fetchInterval);
        }
        if (fetchInterval > 0 && !this.config.enabled) {
            fetchInterval = 0;
            LogIt.Warning("MailEngine sender loop skipped due to sendmail.enabled configuration");
        }
        this.loopState.fetchTick = fetchInterval;
        if (fetchInterval > 0) {
            LogIt.Info("Starting MailEngine sender loop. Fetch tick: " + this.loopState.fetchTick + "ms");
            let currentInterval : number = 1000; // just first one is shorter
            const process : any = () : void => {
                if (!ObjectValidator.IsEmptyOrNull(this.loopThread)) {
                    clearTimeout(this.loopThread);
                }
                this.loopThread = EventsManager.getInstanceSingleton().FireAsynchronousMethod(async () : Promise<void> => {
                    currentInterval = fetchInterval;
                    this.loopState.status = IMailEngineLoopStatus.Running;
                    try {
                        await this.processEmailRecords();
                    } catch (ex) {
                        LogIt.Error("processEmailRecords error", ex);
                        await SystemLog.Trace({
                            level    : LogLevel.ERROR,
                            message  : "processEmailRecords error: " + ex.trace,
                            traceBack: this.getClassNameWithoutNamespace() + ".processEmailRecords"
                        });
                    }
                    this.loopState.status = IMailEngineLoopStatus.Idle;
                    process();
                }, currentInterval);
            };
            process();
        } else {
            LogIt.Warning("Running without MailEngine synchronization due to sendmail.fetchTick <= 0");
        }
    }

    public StopLoop() : void {
        if (!ObjectValidator.IsEmptyOrNull(this.loopThread)) {
            clearTimeout(this.loopThread);
            this.loopThread = null;
            this.loopState.status = IMailEngineLoopStatus.Stopped;
            LogIt.Info("MailEngine sender loop has been stopped.");
        }
    }

    protected async getTemplate($type : EmailType, $options? : any) : Promise<any> {
        await (new Promise<void>(($resolve : any, $reject : any) : void => {
            this.dao.setErrorHandler(($eventArgs : ErrorEvent) : void => {
                $reject(new Error($eventArgs.message));
            });
            this.dao.Load(this.language, () : void => {
                $resolve();
            });
        }));
        let clientEmail : string = "";
        if ($type === EmailType.FEEDBACK || $type === EmailType.PUBLIC_FEEDBACK) {
            if (!ObjectValidator.IsEmptyOrNull($options) && !ObjectValidator.IsEmptyOrNull($options.env)) {
                clientEmail = $options.env.email;
            }
        }
        const url : string = $options.env.url;
        $options = JsonUtils.Extend({
            embedImages: true,
            env        : {
                accountsAddress: this.dao.getStaticConfiguration().accountsAddress,
                domain         : this.domain,
                link           : `<a href="${url}" target="_blank">${url}</a>`
            }
        }, $options);
        const content : IMailContent = this.dao.getContentFor($type);
        const replacement : StringReplace = new StringReplace(this.getContentEnvironment($options, content));
        delete $options.embedImages;
        delete $options.env;

        let templateSource : string = content.template;
        if (ObjectValidator.IsEmptyOrNull(templateSource)) {
            templateSource = this.dao.getStaticConfiguration().template;
        }
        const template : string = replacement.Content(Loader.getInstance().getFileSystemHandler()
            .Read(templateSource).toString("utf8"), StringReplaceType.VARIABLES);
        const generated : any = require("mjml")(replacement.Content(template, StringReplaceType.VARIABLES), JsonUtils.Extend({
            beautify       : false,
            minify         : false,
            preprocessors  : [],
            validationLevel: "strict"
        }, $options));

        const output : any = {
            body     : generated.html,
            plainText: replacement.Content(this.dao.getContentFor($type).plainBody, StringReplaceType.VARIABLES),
            subject  : replacement.Content(this.dao.getContentFor($type).subject, StringReplaceType.VARIABLES)
        };
        if (!ObjectValidator.IsEmptyOrNull(clientEmail)) {
            output.sender = this.config.defaultSender;
            output.from = clientEmail;
        }
        return output;
    }

    protected getContentEnvironment($options : any, $content : IMailContent) : any {
        const footer : string = !ObjectValidator.IsEmptyOrNull(this.dao.getStaticConfiguration().footer) ?
            this.dao.getStaticConfiguration().footer : Loader.getInstance().getAppConfiguration().target.copyright;
        return JsonUtils.Extend({
            build   : {
                year: new Date(Loader.getInstance().getEnvironmentArgs().getBuildTime()).getFullYear() + ""
            },
            title   : ObjectValidator.IsEmptyOrNull($content.title) ? $content.subject : $content.title,
            logo    : ($options.embedImages ? "" : this.domain) + this.dao.getStaticConfiguration().logo,
            favicon : this.dao.getStaticConfiguration().favicon,
            body    : {
                header : $content.header,
                content: $content.htmlBody
            },
            navigate: {
                text: $content.navigateText,
                link: $options.env.url
            },
            footer
        }, $options.env);
    }

    protected getTemplateConfiguration() : IMailEngineConfiguration {
        return <IMailEngineConfiguration>this.dao.getStaticConfiguration();
    }

    protected async sendMail($email : string, $url : string, $type : EmailType, $environment? : any) : Promise<boolean> {
        if (!this.config.enabled) {
            LogIt.Warning("Email send skipped due to sendmail.enabled configuration");
            return true;
        } else if (!this.allowedEmailTypes().includes($type)) {
            LogIt.Debug("Send of " + $type + " skipped");
            return true;
        } else {
            let result : boolean = true;
            if (!this.directEmailSend().includes($type) && this.loopState.status !== IMailEngineLoopStatus.Stopped) {
                const record : EmailRecord = new EmailRecord();
                record.Email = $email;
                record.Type = $type + "";
                record.Url = $url;
                if (!ObjectValidator.IsEmptyOrNull($environment)) {
                    record.Environment = JSON.stringify($environment);
                }
                await record.Save();
            } else {
                result = await this.directSend($email, $url, $type, $environment);
            }
            return result;
        }
    }

    protected getUrl($value : string, $token? : string) : string {
        if (!ObjectValidator.IsEmptyOrNull($value) && !StringUtils.StartsWith($value, "mailto")) {
            return this.domain + "/#" + $value + (!ObjectValidator.IsEmptyOrNull($token) ? "/" + $token : "");
        }
        return $value;
    }

    protected setTemplatePath($value : string) : void {
        this.dao.setConfigurationPath($value);
    }

    protected async getMailList($emailRecipient : string, $type : EmailType, $environment? : any) : Promise<string[][]> {
        if ([
            EmailType.REPORT_FAILURE,
            EmailType.FEEDBACK,
            EmailType.PUBLIC_FEEDBACK
        ].includes(<string>$type)) {
            return [this.reducesMailList(this.config.admins, $emailRecipient)];
        }
        return [[$emailRecipient]];
    }

    protected allowedEmailTypes() : EmailType[] {
        const values : EmailType[] = [];
        EmailType.getProperties().forEach(($property : string) : void => {
            values.push(EmailType[$property]);
        });
        return values;
    }

    protected directEmailSend() : EmailType[] {
        return [EmailType.REPORT_FAILURE];
    }

    protected async process($options : ISendMailOptions) : Promise<boolean> {
        return this.sender.SendAsync($options);
    }

    protected reducesMailList($list : string[], $ignore : string) : string[] {
        if (!ObjectValidator.IsEmptyOrNull($list)) {
            const output : string[] = [];
            $list.forEach(($item : string) : void => {
                if (!StringUtils.ContainsIgnoreCase($item, $ignore)) {
                    output.push($item);
                }
            });
            return output;
        }
        return $list;
    }

    private async processEmailRecords() : Promise<number> {
        const records : EmailRecord[] = await EmailRecord.Find({active: {$ne: false}});
        let processed : number = 0;
        if (!ObjectValidator.IsEmptyOrNull(records)) {
            for await (const record of records) {
                try {
                    LogIt.Debug("Processing MailEngine record: " + record.Id);
                    let environment : any = null;
                    if (!ObjectValidator.IsEmptyOrNull(record.Environment)) {
                        environment = JSON.parse(record.Environment);
                    }
                    if (!await this.directSend(record.Email, record.Url, record.Type, environment)) {
                        LogIt.Error("Failed to process MailEngine record: {0}[{1}] -> {2}", record.Type, record.Id, record.Email);
                        record.Active = false;
                        await record.Save();
                    } else {
                        await record.Remove();
                    }
                    processed++;
                } catch (ex) {
                    record.Active = false;
                    await record.Save();
                    LogIt.Error(ex);
                }
            }
        }
        return processed;
    }

    private async directSend($email : string, $url : string, $type : EmailType, $environment? : any) : Promise<boolean> {
        let result : boolean = true;
        const mailTo : string[][] = await this.getMailList($email, $type, $environment);
        for await (const to of mailTo) {
            const recipients : string[] = [];
            to.forEach(($email : string) : void => {
                if (!this.config.blackList.includes(StringUtils.ToLowerCase($email))) {
                    recipients.push($email);
                }
            });
            if (!ObjectValidator.IsEmptyOrNull(to)) {
                const emailOptions : any = {to: recipients};
                if (!ObjectValidator.IsEmptyOrNull($environment) && !ObjectValidator.IsEmptyOrNull($environment.copyTo)) {
                    emailOptions.copyTo = $environment.copyTo;
                }
                if (!await this.process(JsonUtils.Extend(await this.getTemplate($type, {
                    env: JsonUtils.Extend({
                        email: $email,
                        url  : $url
                    }, $environment)
                }), emailOptions))) {
                    result = false;
                }
            }
        }
        return result;
    }

    private getLoopInfo() : IMailEngineLoopInfo {
        return this.loopState;
    }
}

export class EmailType extends BaseEnum {
    public static REGISTER : string = "register";
    public static RESET_PASSWORD : string = "resetPassword";
    public static RESET_PASSWORD_CONFIRM : string = "resetPasswordConfirm";
    public static INVITATION : string = "invitation";
    public static ACTIVATION : string = "activation";
    public static DEACTIVATION : string = "deactivation";
    public static REMOVED : string = "removed";
    public static RESTORED : string = "restored";
    public static REPORT_FAILURE : string = "reportFailure";
    public static FEEDBACK : string = "feedback";
    public static PUBLIC_FEEDBACK : string = "publicFeedback";
    public static NEW_AUTH_CODE : string = "newAuthCode";
    public static AUTH_CODE_RESEND : string = "authCodeResend";
}

export interface IMailEngineLoopInfo {
    fetchTick : number;
    status : IMailEngineLoopStatus;
}

export enum IMailEngineLoopStatus {
    Idle,
    Running,
    Stopped
}

// generated-code-start
export const IMailEngineLoopInfo = globalThis.RegisterInterface(["fetchTick", "status"]);
// generated-code-end

/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2018 NXP
 * Copyright 2019-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { Convert } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Convert.js";
import { LogIt } from "@io-oidis-commons/Io/Oidis/Commons/Utils/LogIt.js";
import { ObjectDecoder } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectDecoder.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { StringUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/StringUtils.js";
import { FileSystemHandler } from "@io-oidis-localhost/Io/Oidis/Localhost/Connectors/FileSystemHandler.js";
import { ResponseFactory } from "@io-oidis-localhost/Io/Oidis/Localhost/HttpProcessor/ResponseApi/ResponseFactory.js";
import { ExternResponseType } from "@io-oidis-localhost/Io/Oidis/Localhost/Interfaces/IExternOptions.js";
import { IResponse } from "@io-oidis-localhost/Io/Oidis/Localhost/Interfaces/IResponse.js";
import { BaseConnector } from "@io-oidis-localhost/Io/Oidis/Localhost/Primitives/BaseConnector.js";
import { Extern } from "@io-oidis-localhost/Io/Oidis/Localhost/Primitives/Decorators.js";
import { IReportProtocol } from "@io-oidis-services/Io/Oidis/Services/Connectors/ReportServiceConnector.js";
import { Loader } from "../Loader.js";
import { SendMail } from "./SendMail.js";

export class ReportAPI extends BaseConnector {
    private readonly storageBase : string;
    private readonly crashLogsPath : string;
    private readonly templatePath : string;
    private readonly mailListPath : string;

    constructor() {
        super();
        const targetBase : string = Loader.getInstance().getProgramArgs().TargetBase();
        this.storageBase = Loader.getInstance().getProgramArgs().AppDataPath();
        this.crashLogsPath = "/resource/data/Io/Oidis/Hub/Utils/ReportAPI/Logs";
        const fileSystem : FileSystemHandler = Loader.getInstance().getFileSystemHandler();
        this.templatePath = "/resource/data/Io/Oidis/Hub/Templates/CrashReport.html";
        if (fileSystem.Exists(this.storageBase + this.templatePath)) {
            this.templatePath = this.storageBase + this.templatePath;
        } else {
            this.templatePath = targetBase + this.templatePath;
        }
        this.mailListPath = "/resource/data/Io/Oidis/Hub/Configuration/CrashReportMailList.json";
        if (fileSystem.Exists(this.storageBase + this.mailListPath)) {
            this.mailListPath = this.storageBase + this.mailListPath;
        } else {
            this.mailListPath = targetBase + this.mailListPath;
        }
    }

    @Extern()
    public LogIt($message : string) : boolean {
        try {
            LogIt.Info(ObjectDecoder.Base64($message));
            return true;
        } catch (ex) {
            LogIt.Warning(ex.stack);
            return false;
        }
    }

    @Extern(ExternResponseType.FULL_CONTROL)
    public CreateReport($data : IReportProtocol, $sendEmail : boolean, $callback : (($status : boolean) => void) | IResponse) : void {
        try {
            const fileSystem : FileSystemHandler = Loader.getInstance().getFileSystemHandler();
            const appName : string = ObjectDecoder.Base64($data.appName);
            const appVersion : string = ObjectDecoder.Base64($data.appVersion);

            const logDirectory : string = this.crashLogsPath + "/" + $data.appId;
            let logFilePath : string = "Without log file";
            let printScreenPath : string = "Without print screen";
            if (!ObjectValidator.IsEmptyOrNull($data.log)) {
                logFilePath = logDirectory + "/" + $data.timeStamp + ".log";
                const logFileContent : string = StringUtils.Format(
                    "================================================={2}" +
                    "                   STACK TRACE                   {2}" +
                    "================================================={2}{2}" +
                    "{0}{2}{2}" +
                    "================================================={2}" +
                    "                     APP LOG                     {2}" +
                    "================================================={2}{2}" +
                    "{1}{2}" +
                    "-------------------- LOG END --------------------{2}",
                    ObjectValidator.IsEmptyOrNull($data.trace) ? "Unavailable" :
                        StringUtils.Replace(StringUtils.Replace(ObjectDecoder.Base64($data.trace), "\r\n", "\n"),
                            "\n", StringUtils.NewLine(false)),
                    ObjectDecoder.Base64($data.log),
                    StringUtils.NewLine(false));
                fileSystem.Write(this.storageBase + logFilePath, logFileContent);
            }
            if (!ObjectValidator.IsEmptyOrNull($data.printScreen)) {
                printScreenPath = logDirectory + "/" + $data.timeStamp + ".png";
                fileSystem.Write(this.storageBase + printScreenPath, Buffer.from($data.printScreen, "base64"));
            }
            const crashReport : string = <string>fileSystem.Read(this.templatePath);
            const crashTime : string = Convert.TimeToGMTformat(Convert.ToFixed($data.timeStamp, 0));

            if (!ObjectValidator.IsEmptyOrNull(crashReport)) {
                const reportServiceLink : string =
                    Loader.getInstance().getHttpManager().getRequest().getHostUrl() + "#/report/" + $data.appId;
                const body : string = StringUtils.Format(crashReport,
                    appName + " " + appVersion,
                    $data.appId,
                    reportServiceLink,
                    crashTime
                );
                const recipients : any = this.getRecipients(appName, appVersion);

                LogIt.Debug("Application crash has been detected. At: {0}, from: {1}", crashTime, appName);
                if ($sendEmail) {
                    new SendMail().Send(
                        recipients.to,
                        "Oidis application crash report, time: " + crashTime,
                        body,
                        [logFilePath, printScreenPath],
                        "This report is accessible online at: " + reportServiceLink,
                        recipients.cc,
                        "",
                        $callback);
                } else {
                    ResponseFactory.getResponse($callback).Send(true);
                }
            } else {
                LogIt.Warning("CrashReport template has not been found or is empty.");
                ResponseFactory.getResponse($callback).Send(false);
            }
        } catch (ex) {
            LogIt.Warning(ex.stack);
            ResponseFactory.getResponse($callback).Send(false);
        }
    }

    public getRecipients($appName : string, $appVersion : string) : any {
        const fileSystem : FileSystemHandler = Loader.getInstance().getFileSystemHandler();
        const to : string[] = [];
        const cc : string[] = [];

        if (!fileSystem.Exists(this.mailListPath)) {
            LogIt.Warning("Path to configuration file \"" + this.mailListPath + "\" does not exist.");
        } else {
            const configuration : any = JSON.parse(<string>fileSystem.Read(this.mailListPath));
            if (ObjectValidator.IsEmptyOrNull(configuration)) {
                LogIt.Warning("Invalid configuration file \"" + this.mailListPath + "\"");
            }

            const appHash : string = StringUtils.getSha1($appName + $appVersion);

            let listForAppName : string;
            for (listForAppName in configuration) {
                if (configuration.hasOwnProperty(listForAppName)) {
                    const versionOrMailList : any = configuration[listForAppName];
                    if (StringUtils.PatternMatched(listForAppName, $appName) || listForAppName === appHash) {
                        if (ObjectValidator.IsArray(versionOrMailList)) {
                            versionOrMailList.forEach(($mail : string) : void => {
                                if (to.indexOf($mail) === -1) {
                                    to.push($mail);
                                }
                            });
                        } else if (ObjectValidator.IsObject(versionOrMailList)) {
                            let listForVersion : string;
                            for (listForVersion in versionOrMailList) {
                                if (versionOrMailList.hasOwnProperty(listForVersion)) {
                                    const mailListOrGroup : any = versionOrMailList[listForVersion];
                                    if (StringUtils.PatternMatched(listForVersion, $appVersion)) {
                                        if (ObjectValidator.IsArray(mailListOrGroup)) {
                                            mailListOrGroup.forEach(($mail : string) : void => {
                                                if (to.indexOf($mail) === -1) {
                                                    to.push($mail);
                                                }
                                            });
                                        } else if (ObjectValidator.IsObject(mailListOrGroup)) {
                                            if (ObjectValidator.IsSet(mailListOrGroup.to)) {
                                                mailListOrGroup.to.forEach(($mail : string) : void => {
                                                    if (to.indexOf($mail) === -1) {
                                                        to.push($mail);
                                                    }
                                                });
                                            }
                                            if (ObjectValidator.IsSet(mailListOrGroup.cc)) {
                                                mailListOrGroup.cc.forEach(($mail : string) : void => {
                                                    if (cc.indexOf($mail) === -1) {
                                                        cc.push($mail);
                                                    }
                                                });
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        return {
            cc,
            to
        };
    }

    public getReportingApps() : string[] {
        const cwd : string = this.storageBase + this.crashLogsPath;
        return Loader.getInstance().getFileSystemHandler().Expand(cwd + "/*").map(($value : string) : string => {
            return StringUtils.Remove($value, cwd + "/");
        });
    }

    public getReports($appId : string) : string[] {
        return Loader.getInstance().getFileSystemHandler().Expand(this.storageBase + this.crashLogsPath + "/" + $appId + "/**/*");
    }

    public getContent($reportId : string) : string {
        let content : any = Loader.getInstance().getFileSystemHandler().Read($reportId);
        if (StringUtils.EndsWith($reportId, ".png")) {
            content = "data:image/png;base64," + content.toString("base64");
        } else {
            content = content.toString();
        }
        return content;
    }
}

export interface IReportsPromise {
    Then($callback : ($list : string[]) => void) : void;
}

export interface IReportContentPromise {
    Then($callback : ($value : string) => void) : void;
}

// generated-code-start
export const IReportsPromise = globalThis.RegisterInterface(["Then"]);
export const IReportContentPromise = globalThis.RegisterInterface(["Then"]);
// generated-code-end

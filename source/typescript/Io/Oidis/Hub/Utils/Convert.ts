/*! ******************************************************************************************************** *
 *
 * Copyright 2022-2023 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { IPDFOptions } from "@io-oidis-hub-gui/Io/Oidis/Hub/Gui/Interfaces/IPDFOptions.js";
import { Convert as Parent } from "@io-oidis-hub-gui/Io/Oidis/Hub/Gui/Utils/Convert.js";
import { PuppeteerHandler } from "./PuppeteerHandler.js";

export class Convert extends Parent {

    public static async HtmlToPDF($input : string, $options? : IPDFOptions) : Promise<Buffer> {
        return PuppeteerHandler.getInstanceSingleton().Export({html: $input}, $options);
    }
}

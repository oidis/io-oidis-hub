/*! ******************************************************************************************************** *
 *
 * Copyright 2017-2018 NXP
 * Copyright 2019-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { LogIt } from "@io-oidis-commons/Io/Oidis/Commons/Utils/LogIt.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { StringUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/StringUtils.js";
import { ResponseFactory } from "@io-oidis-localhost/Io/Oidis/Localhost/HttpProcessor/ResponseApi/ResponseFactory.js";
import { ExternResponseType } from "@io-oidis-localhost/Io/Oidis/Localhost/Interfaces/IExternOptions.js";
import { IResponse } from "@io-oidis-localhost/Io/Oidis/Localhost/Interfaces/IResponse.js";
import { Extern } from "@io-oidis-localhost/Io/Oidis/Localhost/Primitives/Decorators.js";
import { AgentsRegister, IAgent } from "../Primitives/AgentsRegister.js";

export class BuilderHub extends AgentsRegister {

    @Extern(ExternResponseType.FULL_CONTROL)
    public async RunTask($task : IBuilderTask, $callback? : IResponse) : Promise<void> {
        let found : boolean = false;
        let message : string = "No agent is available.";
        for (const agent of AgentsRegister.agents) {
            if (!found) {
                if (agent.owner === this) {
                    if ((ObjectValidator.IsEmptyOrNull($task.agent.name) ||
                            !ObjectValidator.IsEmptyOrNull($task.agent.name) && $task.agent.name === agent.name) &&
                        ($task.agent.version === "latest" || $task.agent.version === agent.version)) {
                        LogIt.Debug("Searching builders for platforms: {0}", $task.platforms);
                        $task.platforms.forEach(($platform : string) : boolean => {
                            let agentPlatform : string = "linux";
                            if (StringUtils.Contains($platform, "-win-", "-win32-")) {
                                agentPlatform = "win";
                            } else if (StringUtils.Contains($platform, "-mac-")) {
                                agentPlatform = "mac";
                            }

                            if (StringUtils.Contains(agent.platform, agentPlatform)) {
                                LogIt.Info("Forward task \"" + JSON.stringify($task.args) + "\" " +
                                    "to builder [" + agent.id + "] with platform: " + agent.platform);
                                const taskId : string = BuilderHub.UID();
                                AgentsRegister.tasks.push({id: taskId, response: $callback});

                                ResponseFactory.getResponse($callback).Send(<IBuilderTaskProtocol>{
                                    builderId: agent.id,
                                    taskId
                                });
                                ResponseFactory.getResponse($callback).AddAbortHandler(() : void => {
                                    const index : number = AgentsRegister.tasks.findIndex(($item) => $item.id === taskId);
                                    if (index > -1) {
                                        AgentsRegister.tasks.splice(index, 1);
                                    }
                                });
                                agent.connection.OnChange(<IBuilderTaskProtocol>{
                                    data: $task.args,
                                    taskId,
                                    type: "taskRequest"
                                });
                                found = true;
                            } else {
                                message = "No agent with suitable platform has been found.";
                            }
                            return !found;
                        });
                    } else {
                        message = "No agent with matching specified name \"" + $task.agent.name + "\" has been found.";
                    }
                } else {
                    message = "No compatible agent found.";
                }
            }
        }
        if (!found) {
            ResponseFactory.getResponse($callback).OnComplete(<IBuilderTaskErrorProtocol>{
                message,
                status: false
            });
        }
    }
}

export interface IBuilderTask {
    args : string[];
    platforms : string[];
    agent : IAgent;
}

export interface IBuilderTaskProtocol {
    builderId : string;
    taskId : string;
    data? : any;
    type? : string;
}

export interface IBuilderTaskErrorProtocol {
    status : boolean;
    message : string;
}

// generated-code-start
export const IBuilderTask = globalThis.RegisterInterface(["args", "platforms", "agent"]);
export const IBuilderTaskProtocol = globalThis.RegisterInterface(["builderId", "taskId", "data", "type"]);
export const IBuilderTaskErrorProtocol = globalThis.RegisterInterface(["status", "message"]);
// generated-code-end

/*! ******************************************************************************************************** *
 *
 * Copyright 2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2018 NXP
 * Copyright 2019-2023 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { LogSeverity } from "@io-oidis-commons/Io/Oidis/Commons/Enums/LogSeverity.js";
import { BaseObject } from "@io-oidis-commons/Io/Oidis/Commons/Primitives/BaseObject.js";
import { JsonUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/JsonUtils.js";
import { LogIt } from "@io-oidis-commons/Io/Oidis/Commons/Utils/LogIt.js";
import { ObjectDecoder } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectDecoder.js";
import { ObjectEncoder } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectEncoder.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { Property } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Property.js";
import { StringUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/StringUtils.js";
import { TokenPayload } from "../DAO/Models/TokenPayload.js";
import { ITokenHeader } from "../Interfaces/ITokenHeader.js";

export class WebTokenManager extends BaseObject {
    private authExpirationTime : number = 15 * 60; // sec
    private hashingAlgorithm : string = "sha512";
    private readonly isMultiSession : boolean;
    private onExpireHandlers : any[];

    constructor($isMultiSession : boolean = false) {
        super();
        this.isMultiSession = $isMultiSession;
        this.onExpireHandlers = [];
    }

    public ExpirationTime($valueInSeconds? : number) : number {
        return this.authExpirationTime = Property.PositiveInteger(this.authExpirationTime, $valueInSeconds);
    }

    public setOnTokenExpireHandler($handler : ($token : string) => void) : void {
        if (ObjectValidator.IsEmptyOrNull($handler)) {
            this.onExpireHandlers.push($handler);
        }
    }

    public async Create($id : string, $noExpire : boolean = false, $expireTime? : number) : Promise<string> {
        await this.clearExpiredTokens();
        if ($noExpire) {
            LogIt.Debug("Constructing token with no expiration for: {0}", $id);
        }
        let payload : TokenPayload = await this.getUserPayload($id);
        if (ObjectValidator.IsEmptyOrNull(payload)) {
            payload = new TokenPayload();
            payload.Owner = $id;
            if (!$noExpire) {
                payload.Expire = Property.Time(payload.Expire,
                    "+" + (ObjectValidator.IsEmptyOrNull($expireTime) ? this.authExpirationTime : $expireTime) + " sec");
            }
            await payload.Save();
        } else {
            if ($noExpire) {
                if (!ObjectValidator.IsEmptyOrNull(payload.Expire)) {
                    await payload.Remove("Expire");
                }
            } else {
                if (ObjectValidator.IsEmptyOrNull(payload.Expire)) {
                    payload.Expire = Property.Time(payload.Expire,
                        "+" + (ObjectValidator.IsEmptyOrNull($expireTime) ? this.authExpirationTime : $expireTime) + " sec");
                    await payload.Save();
                }
            }
        }
        return this.calculateToken(payload);
    }

    public async Clear($tokenOrId : string) : Promise<boolean> {
        let id : string = $tokenOrId;
        if (StringUtils.Contains($tokenOrId, ".")) {
            id = this.getPayloadId($tokenOrId);
        }
        await this.clearUserPayloads(id);
        await this.clearExpiredTokens();
        return ObjectValidator.IsEmptyOrNull(await this.getUserPayload(id));
    }

    public async Validate($token : string) : Promise<boolean> {
        try {
            const tokenParts : string[] = StringUtils.Split($token, ".");
            if (tokenParts.length === 3) {
                const header : ITokenHeader = JSON.parse(ObjectDecoder.Base64(tokenParts[0]));
                const payload : TokenPayload = await this.getUserPayload(ObjectDecoder.Base64(tokenParts[1]));

                if (ObjectValidator.IsObject(header) && !ObjectValidator.IsEmptyOrNull(payload)) {
                    if ($token === this.calculateToken(payload, header) && (
                        ObjectValidator.IsEmptyOrNull(payload.Expire) || (payload.Expire - new Date().getTime()) > 0
                    )) {
                        if (!ObjectValidator.IsEmptyOrNull(payload.Expire)) {
                            payload.Expire = Property.Time(payload.Expire, "+" + this.authExpirationTime + " sec");
                            await payload.Save();
                        }
                        LogIt.Info("token is valid", LogSeverity.LOW);
                        return true;
                    } else {
                        LogIt.Debug("token is invalid");
                        if (!this.isMultiSession) {
                            await this.Clear($token);
                        }
                    }
                } else {
                    let payloadOutput : any = null;
                    if (!ObjectValidator.IsEmptyOrNull(payload)) {
                        payloadOutput = payload.ToJSON();
                    }
                    LogIt.Debug("token header or payload is invalid {0} {1}", header, payloadOutput);
                }
            } else {
                LogIt.Debug("token format is invalid");
            }
        } catch (ex) {
            LogIt.Warning("Failed to validated token: {0}\n{1}", ex.message, ex.stack);
        }
        return false;
    }

    public getPayloadId($token : string) : string {
        const tokenParts : string[] = StringUtils.Split($token, ".");
        if (tokenParts.length === 3) {
            return ObjectDecoder.Base64(tokenParts[1]);
        }
        return null;
    }

    private async getUserPayload($id : string) : Promise<TokenPayload> {
        const payloads : TokenPayload[] = await TokenPayload.Find({owner: $id});
        if (!ObjectValidator.IsEmptyOrNull(payloads)) {
            return payloads[0];
        }
        return null;
    }

    private async clearUserPayloads($id : string) : Promise<void> {
        const payloads : TokenPayload[] = await TokenPayload.Find({owner: $id});
        for await (const payload of payloads) {
            await payload.Remove();
        }
    }

    private async clearExpiredTokens() : Promise<void> {
        const payloads : TokenPayload[] = await TokenPayload.Find({expire: {$lt: new Date().getTime()}});
        for await (const payload of payloads) {
            LogIt.Debug("Removing expired token: {0}", payload.Id);
            const token : string = this.calculateToken(payload);
            this.onExpireHandlers.forEach(($handler : any) : void => {
                $handler(token);
            });
            await payload.Remove();
        }
    }

    private calculateToken(payload : TokenPayload, $header? : ITokenHeader) : string {
        $header = JsonUtils.Extend({
            alg : this.hashingAlgorithm,
            type: "jwt"
        }, $header);
        const crypto : any = require("crypto");
        const headerWithPayload : string = ObjectEncoder.Base64(JSON.stringify($header)) + "." + ObjectEncoder.Base64(payload.Owner);
        const hmac : any = crypto.createHmac($header.alg, payload.Id);
        hmac.update(headerWithPayload);
        return headerWithPayload + "." + ObjectEncoder.Base64(hmac.digest("binary"));
    }
}

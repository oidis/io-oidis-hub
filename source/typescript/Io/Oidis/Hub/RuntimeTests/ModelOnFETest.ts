/*! ******************************************************************************************************** *
 *
 * Copyright 2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

/* dev:start */
import { RuntimeTestRunner } from "@io-oidis-gui/Io/Oidis/Gui/HttpProcessor/Resolvers/RuntimeTestRunner.js";
import { EmailValidator } from "@io-oidis-hub-gui/Io/Oidis/Hub/Gui/DAO/Validators/EmailValidator.js";
import { PhoneValidator } from "@io-oidis-hub-gui/Io/Oidis/Hub/Gui/DAO/Validators/PhoneValidator.js";
import { RegexValidator } from "@io-oidis-hub-gui/Io/Oidis/Hub/Gui/DAO/Validators/RegexValidator.js";
import { BaseModel } from "../DAO/Models/BaseModel.js";
import { Entity, Property } from "../Primitives/Decorators.js";

@Entity()
export class MockModel extends BaseModel<MockModel> {
    @Property({validator: new RegexValidator(".*")})
    public declare Name : string;

    @Property({validator: new EmailValidator()})
    public declare Email : string;

    @Property({validator: new PhoneValidator()})
    public declare Phone : string;

    @Property({validator: new RegexValidator("^[a-zA-Z0-9_\\-\\.]+$")})
    public declare List : string[];

    constructor() {
        super();

        this.List = [];
    }

    protected getSchema() : any {
        return super.getSchema()
            .add({
                email: {_id: false, required: true, type: String},
                list : [
                    {_id: false, type: String}
                ],
                name : {_id: false, required: true, type: String},
                phone: {_id: false, required: true, type: String}
            });
    }
}

export class ModelOnFETest extends RuntimeTestRunner {

    public async testModelAPI() : Promise<void> {
        const model : MockModel = new MockModel();
        await this.assertApiIssue(model.Save());
        await this.assertApiIssue(model.Refresh());
        await this.assertApiIssue(model.Remove());
        await this.assertApiIssue(model.Delete());
        await this.assertApiIssue(model.ToJSON());
        await this.assertApiIssue(MockModel.Find({}));
        await this.assertApiIssue(MockModel.FindById(""));
        this.assertOk(model.Validate());
    }

    public testModelValidation() : void {
        const model : MockModel = new MockModel();

        model.Email = "valid.mail@mail.co";
        this.assertOk(model.Validate("email"));
        model.Email = "invalid.mail";
        this.assertOk(!model.Validate("email"));

        model.Phone = "123456789";
        this.assertOk(model.Validate("phone"));
        model.Phone = "+420123456789";
        this.assertOk(model.Validate("phone"));
        model.Phone = "256";
        this.assertOk(!model.Validate("phone"));
    }

    private async assertApiIssue($promise : Promise<any>) {
        try {
            await $promise;
        } catch ($e) {
            this.assertEquals($e.message, "Method or feature is allowed only at backend. This is code design issue.");
        }
    }
}
/* dev:end */

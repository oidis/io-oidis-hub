/*! ******************************************************************************************************** *
 *
 * Copyright 2019-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

/* dev:start */
import { ErrorEventArgs } from "@io-oidis-commons/Io/Oidis/Commons/Events/Args/ErrorEventArgs.js";
import { Echo } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Echo.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { PersistenceType } from "@io-oidis-gui/Io/Oidis/Gui/Enums/PersistenceType.js";
import { RuntimeTestRunner } from "@io-oidis-gui/Io/Oidis/Gui/HttpProcessor/Resolvers/RuntimeTestRunner.js";
import { PersistenceFactory } from "@io-oidis-gui/Io/Oidis/Gui/PersistenceFactory.js";
import { BaseConnector } from "@io-oidis-localhost/Io/Oidis/Localhost/Primitives/BaseConnector.js";
import { Extern } from "@io-oidis-localhost/Io/Oidis/Localhost/Primitives/Decorators.js";
import { AuthManagerConnector } from "@io-oidis-services/Io/Oidis/Services/Connectors/AuthManagerConnector.js";
import { AuthHttpResolver } from "@io-oidis-services/Io/Oidis/Services/ErrorPages/AuthHttpResolver.js";

export class UserManagerTestHandler extends BaseConnector {

    @Extern()
    public SecuredMethod() : boolean {
        return true;
    }
}

export class AuthorizationTest extends RuntimeTestRunner {
    private client : AuthManagerConnector;

    public async testInitUser() : Promise<void> {
        Echo.Printf("To pass this test root:root has to be configured in private.conf defaultUser section");
        PersistenceFactory.getPersistence(PersistenceType.CLIENT_IP, AuthHttpResolver.ClassName())
            .Variable("token", await this.client.LogIn("root", "root"));

        await this.client.RemoveUser("admin");
        const status : boolean = await this.client.Register({
            name          : "admin",
            password      : "admin",
            withActivation: false
        });
        this.assertOk(status);
        if (status) {
            const roles : string[] = await this.client.getGroups("root");
            for await (const role of roles) {
                await this.client.AddGroup(role, "admin");
            }
            PersistenceFactory.getPersistence(PersistenceType.CLIENT_IP, AuthHttpResolver.ClassName())
                .Variable("token", await this.client.LogIn("admin", "admin"));
        }
    }

    public testLogIn() : void {
        const userNameId : string = this.getClassNameWithoutNamespace() + "_userName";
        const userPassId : string = this.getClassNameWithoutNamespace() + "_userPass";

        Echo.Print("" +
            "User:  <input type='text' id='" + userNameId + "'/>" +
            "Pass:  <input type='password' id='" + userPassId + "'/>");
        this.addButton("LogIn", async () : Promise<void> => {
            const userName : HTMLInputElement = <HTMLInputElement>document.getElementById(userNameId);
            const userPass : HTMLInputElement = <HTMLInputElement>document.getElementById(userPassId);

            try {
                const token : string = await this.client.LogIn(userName.value, userPass.value);
                if (!ObjectValidator.IsEmptyOrNull(token)) {
                    PersistenceFactory.getPersistence(PersistenceType.CLIENT_IP, AuthHttpResolver.ClassName())
                        .Variable("token", token);
                    Echo.Printf("Log is succeed.");
                } else {
                    Echo.Printf("Failed to log in.");
                }
            } catch (ex) {
                Echo.Printf(ex.message);
            }
        });
    }

    public testUserManagement() : void {
        const userNameId : string = this.getClassNameWithoutNamespace() + "_regUserName";
        const userPassId : string = this.getClassNameWithoutNamespace() + "_regUserPass";

        Echo.Print("" +
            "User:  <input type='text' id='" + userNameId + "'/>" +
            "Pass:  <input type='password' id='" + userPassId + "'/>");
        this.addButton("Register", async () : Promise<void> => {
            const userName : HTMLInputElement = <HTMLInputElement>document.getElementById(userNameId);
            const userPass : HTMLInputElement = <HTMLInputElement>document.getElementById(userPassId);
            await this.client.setAuthorizedMethods("Admin", [
                "Io.Oidis.Hub.Utils.AuthManager.Register"
            ], true);

            if (await this.client.Register({
                name          : userName.value,
                password      : userPass.value,
                withActivation: false
            })) {
                Echo.Printf("Registration has succeed.");
                Echo.Printf(await this.client.getAuthorizedMethods(await this.client.LogIn(userName.value, userPass.value)));
            } else {
                Echo.Printf("Failed to register user.");
            }
        });

        this.addButton("Set All Auth Permissions", async () : Promise<void> => {
            const userName : HTMLInputElement = <HTMLInputElement>document.getElementById(userNameId);
            await this.client.setAuthorizedMethods("Admin", [
                "Io.Oidis.Hub.Utils.AuthManager.getGroups"
            ], true);

            const currentMethods : string[] = await this.client.getGroups(userName.value);
            Echo.Printf(currentMethods);
            if (await this.client.setAuthorizedMethods("Admin", ["Io.Oidis.Hub.Utils.AuthManager.*"])) {
                Echo.Printf("All permissions have been set.");
            } else {
                Echo.Printf("Failed to set permissions.");
            }
        });

        this.addButton("Remove user", async () : Promise<void> => {
            await this.client.setAuthorizedMethods("Admin", [
                "Io.Oidis.Hub.Utils.AuthManager.RemoveUser"
            ], true);
            const userName : HTMLInputElement = <HTMLInputElement>document.getElementById(userNameId);
            if (await this.client.RemoveUser(userName.value)) {
                Echo.Printf("User has been removed");
            } else {
                Echo.Printf("Failed to remove user.");
            }
        });
    }

    public async testIsAuthenticated() : Promise<void> {
        const token : string = PersistenceFactory.getPersistence(PersistenceType.CLIENT_IP, AuthHttpResolver.ClassName())
            .Variable("token");
        const status : boolean = await this.client.IsAuthenticated(token);
        this.assertOk(status);
        if (status) {
            Echo.Printf("> Auth token: {0}; {1}", token, await this.client.GenerateAuthToken());
        }
    }

    public async testSetAuthorizedMethods() : Promise<void> {
        this.assertOk(await this.client.setAuthorizedMethods("Admin", [
            "Io.Oidis.Hub.Utils.AuthManager.AddGroup",
            "Io.Oidis.Hub.Utils.AuthManager.CreateGroup"
        ], true));
        this.assertOk(await this.client.CreateGroup("Test", [], true));
        this.assertOk(await this.client.setAuthorizedMethods("Test", [
            "Io.Oidis.Hub.Utils.AuthManager.AddGroup",
            "test.method.a", "test.method.b", UserManagerTestHandler.ClassName() + ".SecuredMethod"
        ]));
        this.assertOk(await this.client.AddGroup("Test", "admin"));
        const token : string = await this.client.LogIn("admin", "admin");
        PersistenceFactory.getPersistence(PersistenceType.CLIENT_IP, AuthHttpResolver.ClassName()).Variable("token", token);

        const authMethods : string[] = await this.client.getAuthorizedMethods(token);
        if (authMethods.indexOf("*") === -1) {
            this.assertDeepEqual(authMethods, [
                "Io.Oidis.Hub.Utils.AuthManager.LogIn",
                "Io.Oidis.Hub.Utils.AuthManager.LogOut",
                "Io.Oidis.Hub.Utils.AuthManager.IsAuthorizedFor",
                "Io.Oidis.Hub.Utils.AuthManager.IsAuthenticated",
                "Io.Oidis.Hub.Utils.AuthManager.getAuthorizedMethods",
                "Io.Oidis.Hub.Utils.AuthManager.*",
                "Io.Oidis.Hub.Utils.AuthManager.AddGroup",
                "Io.Oidis.Hub.Utils.AuthManager.CreateGroup",
                "test.method.a",
                "test.method.b",
                "Io.Oidis.Hub.RuntimeTests.UserManagerTestHandler.SecuredMethod"
            ]);
        } else {
            this.assertDeepEqual(authMethods, [
                "Io.Oidis.Hub.Utils.AuthManager.LogIn",
                "Io.Oidis.Hub.Utils.AuthManager.LogOut",
                "Io.Oidis.Hub.Utils.AuthManager.IsAuthorizedFor",
                "Io.Oidis.Hub.Utils.AuthManager.IsAuthenticated",
                "Io.Oidis.Hub.Utils.AuthManager.getAuthorizedMethods",
                "*"
            ]);
        }
    }

    public async testRegisterMethodsVersion() : Promise<void> {
        this.assertOk(await this.client.setAuthorizedMethods("Admin", [
            "Io.Oidis.Hub.Utils.AuthManager.RegisterApp"
        ], true));
        this.assertOk(await this.client.RegisterApp({
            appName    : this.getEnvironmentArgs().getProjectName(),
            releaseName: "Win",
            platform   : "app-win-nodejs",
            version    : "0.0.0",
            buildTime  : 1561958909002,
            authMethods: [
                {
                    className    : "ReportPageConnector",
                    methods      : [
                        "getReports",
                        "getReport"
                    ],
                    namespaceName: "Io.Oidis.Hub.Controllers.Pages"
                },
                {
                    className    : "AgentsRegister",
                    methods      : [
                        "RegisterAgent",
                        "ForwardMessage",
                        "getAgentsList"
                    ],
                    namespaceName: "Io.Oidis.Hub.Primitives"
                }
            ]
        }));
        this.assertOk(await this.client.RegisterApp({
            appName    : this.getEnvironmentArgs().getProjectName(),
            releaseName: "Win",
            platform   : "app-win-nodejs",
            version    : "0.0.1",
            buildTime  : "1561958909003",
            authMethods: [
                {
                    className    : "ReportPageConnector",
                    methods      : [
                        "getBBB",
                        "getBBB2"
                    ],
                    namespaceName: "Io.Oidis.Hub.Controllers.Pages"
                }
            ]
        }));

        this.assertOk(await this.client.RegisterApp({
            appName    : this.getEnvironmentArgs().getProjectName(),
            releaseName: "Win",
            platform   : "app-win-nodejs",
            version    : "0.0.0",
            buildTime  : "Mon Jul 01 2019 07:33:33 GMT+0200",
            authMethods: [
                {
                    className    : "ReportPageConnector",
                    methods      : [
                        "getCCC",
                        "getCCC2"
                    ],
                    namespaceName: "Io.Oidis.Hub.Controllers.Pages"
                }
            ]
        }));
    }

    public async testGetAuthMethods_1() : Promise<void> {
        this.assertOk(await this.client.setAuthorizedMethods("Admin", [
            "Io.Oidis.Hub.Utils.AuthManager.getAppMethods"
        ], true));
        this.assertDeepEqual(await this.client.getAppMethods({
            appName    : this.getEnvironmentArgs().getProjectName(),
            releaseName: "Win",
            platform   : "app-win-nodejs",
            version    : "0.0.0",
            buildTime  : 1561958909000
        }), [
            {
                className    : "ReportPageConnector",
                methods      : [
                    "getCCC",
                    "getCCC2"
                ],
                namespaceName: "Io.Oidis.Hub.Controllers.Pages"
            }
        ]);
    }

    public async testGetAuthMethods_2() : Promise<void> {
        this.assertDeepEqual(await this.client.getAppMethods({
            appName    : this.getEnvironmentArgs().getProjectName(),
            releaseName: "Win",
            platform   : "app-win-nodejs",
            version    : "0.0.1",
            buildTime  : "1561958909001"
        }), [
            {
                className    : "ReportPageConnector",
                methods      : [
                    "getBBB",
                    "getBBB2"
                ],
                namespaceName: "Io.Oidis.Hub.Controllers.Pages"
            }
        ]);
    }

    public async testGetAuthMethods_3() : Promise<void> {
        this.assertDeepEqual(await this.client.getAppMethods({
            appName    : this.getEnvironmentArgs().getProjectName(),
            releaseName: "Win",
            platform   : "app-win-nodejs",
            version    : "0.0.0",
            buildTime  : "Mon Jul 01 2019 07:33:33 GMT+0200"
        }), [
            {
                className    : "ReportPageConnector",
                methods      : [
                    "getCCC",
                    "getCCC2"
                ],
                namespaceName: "Io.Oidis.Hub.Controllers.Pages"
            }
        ]);
    }

    protected before() : void {
        this.client = new AuthManagerConnector(true, this.getRequest().getHostUrl() + "connector.config.jsonp");
        this.client.getEvents().OnError(($eventArgs : ErrorEventArgs) : void => {
            Echo.Printf($eventArgs.Exception().ToString());
        });
    }

    // protected async tearDown() : Promise<void> {
    //     /// TODO: remove test user and test methods
    // }
}

/* dev:end */

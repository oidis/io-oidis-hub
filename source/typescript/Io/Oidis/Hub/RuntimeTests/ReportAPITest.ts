/*! ******************************************************************************************************** *
 *
 * Copyright 2017-2018 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

/* dev:start */
import { ErrorEventArgs } from "@io-oidis-commons/Io/Oidis/Commons/Events/Args/ErrorEventArgs.js";
import { IRuntimeTestPromise } from "@io-oidis-commons/Io/Oidis/Commons/HttpProcessor/Resolvers/RuntimeTestRunner.js";
import { Echo } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Echo.js";
import { StringUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/StringUtils.js";
import { RuntimeTestRunner } from "@io-oidis-gui/Io/Oidis/Gui/HttpProcessor/Resolvers/RuntimeTestRunner.js";
import { IReportProtocol } from "@io-oidis-services/Io/Oidis/Services/Connectors/ReportServiceConnector.js";
import { WebServiceClientType } from "@io-oidis-services/Io/Oidis/Services/Enums/WebServiceClientType.js";
import { IWebServiceClient } from "@io-oidis-services/Io/Oidis/Services/Interfaces/IWebServiceClient.js";
import { LiveContentWrapper } from "@io-oidis-services/Io/Oidis/Services/WebServiceApi/LiveContentWrapper.js";
import { WebServiceClientFactory } from "@io-oidis-services/Io/Oidis/Services/WebServiceApi/WebServiceClientFactory.js";
import { SlackConnector } from "../Connectors/SlackConnector.js";
import { ReportAPI } from "../Utils/ReportAPI.js";

export class ReportAPITest extends RuntimeTestRunner {

    private client : IWebServiceClient;

    constructor() {
        super();

        this.client = WebServiceClientFactory.getClient(WebServiceClientType.WEB_SOCKETS,
            this.getRequest().getHostUrl() + "connector.config.jsonp");
        this.client.getEvents().OnError(($eventArgs : ErrorEventArgs) : void => {
            Echo.Printf($eventArgs.Exception().ToString());
        });
    }

    public testReportMessage() : IRuntimeTestPromise {
        return ($done : () => void) : void => {
            LiveContentWrapper.InvokeMethod(this.client, ReportAPI.ClassName(), "LogIt",
                "Test message"
            ).Then(($status : boolean) : void => {
                this.assertEquals($status, true);
                $done();
            });
        };
    }

    public testReportCrash() : IRuntimeTestPromise {
        const url : string = this.getRequest().getHostUrl() + "#/report/" + StringUtils.getSha1(
            this.getEnvironmentArgs().getProjectName() + this.getEnvironmentArgs().getProjectVersion());
        Echo.Println("<a href=\"" + url + "\" target=\"_blank\">" + url + "</a>");

        return ($done : () => void) : void => {
            LiveContentWrapper.InvokeMethod(this.client, ReportAPI.ClassName(), "CreateReport",
                <IReportProtocol>{
                    appId      : StringUtils.getSha1(this.getEnvironmentArgs().getProjectName() +
                        this.getEnvironmentArgs().getProjectVersion()),
                    appName    : this.getEnvironmentArgs().getProjectName(),
                    appVersion : this.getEnvironmentArgs().getProjectVersion(),
                    log        : "some log",
                    printScreen: "",
                    timeStamp  : new Date().getTime(),
                    trace      : "some stack trace"
                },
                false
            ).Then(($status : boolean) : void => {
                this.assertEquals($status, true);
                $done();
            });
        };
    }

    public testSlackNotify() : IRuntimeTestPromise {
        return ($done : () => void) : void => {
            LiveContentWrapper.InvokeMethod(this.client, SlackConnector.ClassName(), "NotifyAs",
                "test", "Reporter", "test message from:\n<https://localhost.oidis.io/#/web|localhost.oidis.io>"
            ).Then(($status : boolean, $message? : string) : void => {
                this.assertEquals($status, true);
                if (!$status) {
                    Echo.Printf($message);
                }
                $done();
            });
        };
    }
}
/* dev:end */

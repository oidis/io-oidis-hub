/*! ******************************************************************************************************** *
 *
 * Copyright 2018 NXP
 * Copyright 2019-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

/* dev:start */
import { ErrorEventArgs } from "@io-oidis-commons/Io/Oidis/Commons/Events/Args/ErrorEventArgs.js";
import { IRuntimeTestPromise } from "@io-oidis-commons/Io/Oidis/Commons/HttpProcessor/Resolvers/RuntimeTestRunner.js";
import { Echo } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Echo.js";
import { ObjectEncoder } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectEncoder.js";
import { RuntimeTestRunner } from "@io-oidis-gui/Io/Oidis/Gui/HttpProcessor/Resolvers/RuntimeTestRunner.js";
import { WebServiceClientType } from "@io-oidis-services/Io/Oidis/Services/Enums/WebServiceClientType.js";
import { IWebServiceClient } from "@io-oidis-services/Io/Oidis/Services/Interfaces/IWebServiceClient.js";
import { LiveContentWrapper } from "@io-oidis-services/Io/Oidis/Services/WebServiceApi/LiveContentWrapper.js";
import { WebServiceClientFactory } from "@io-oidis-services/Io/Oidis/Services/WebServiceApi/WebServiceClientFactory.js";
import { Com } from "../../../../Com/Wui/Framework/FallbacksMapping.js";
import { FileTransferHandler } from "../Connectors/FileTransferHandler.js";

export class FileUploadTest extends RuntimeTestRunner {

    private client : IWebServiceClient;

    constructor() {
        super();

        this.client = WebServiceClientFactory.getClient(WebServiceClientType.WEB_SOCKETS,
            this.getRequest().getHostUrl() + "connector.config.jsonp");
        this.client.getEvents().OnError(($eventArgs : ErrorEventArgs) : void => {
            Echo.Printf($eventArgs.Exception().ToString());
        });
        // this.setMethodFilter("testUploadFallback");
    }

    public testUpload() : IRuntimeTestPromise {
        const url : string = this.getRequest().getHostUrl() + "Download/testFile.txt";
        Echo.Println("<a href=\"" + url + "\" target=\"_blank\">" + url + "</a>");
        return ($done : () => void) : void => {
            LiveContentWrapper.InvokeMethod(this.client, FileTransferHandler.ClassName(), "UploadFile",
                {
                    data : ObjectEncoder.Base64("test data"),
                    index: 0,
                    path : "testFile.txt"
                })
                .Then(($status : boolean) : void => {
                    this.assertEquals($status, true);
                    $done();
                });
        };
    }

    public testUploadFallback() : IRuntimeTestPromise {
        const url : string = this.getRequest().getHostUrl() + "Download/testFile.txt";
        Echo.Println("<a href=\"" + url + "\" target=\"_blank\">" + url + "</a>");
        return ($done : () => void) : void => {
            LiveContentWrapper.InvokeMethod(this.client, Com.Wui.Framework.Hub.HttpProcessor.Resolvers.FileUploadHandler.ClassName(),
                "Upload",
                {
                    data : ObjectEncoder.Base64("test data"),
                    id   : "testFile.txt",
                    index: 0,
                    name : "testFile.txt"
                })
                .Then(($status : boolean) : void => {
                    this.assertEquals($status, true);
                    $done();
                });
        };
    }

    public testProxy() : void {
        const url : string = this.getRequest().getHostUrl() + "Download/" +
            ObjectEncoder.Base64("http://www.angusj.com/resourcehacker/resource_hacker.zip");
        Echo.Println("<a href=\"" + url + "\" target=\"_blank\">" + url + "</a>");
    }
}

/* dev:end */

/*! ******************************************************************************************************** *
 *
 * Copyright 2017-2018 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

/* dev:start */
import { ErrorEventArgs } from "@io-oidis-commons/Io/Oidis/Commons/Events/Args/ErrorEventArgs.js";
import { IRuntimeTestPromise } from "@io-oidis-commons/Io/Oidis/Commons/HttpProcessor/Resolvers/RuntimeTestRunner.js";
import { Echo } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Echo.js";
import { LogIt } from "@io-oidis-commons/Io/Oidis/Commons/Utils/LogIt.js";
import { RuntimeTestRunner } from "@io-oidis-gui/Io/Oidis/Gui/HttpProcessor/Resolvers/RuntimeTestRunner.js";
import { ResponseFactory } from "@io-oidis-localhost/Io/Oidis/Localhost/HttpProcessor/ResponseApi/ResponseFactory.js";
import { ExternResponseType } from "@io-oidis-localhost/Io/Oidis/Localhost/Interfaces/IExternOptions.js";
import { IResponse } from "@io-oidis-localhost/Io/Oidis/Localhost/Interfaces/IResponse.js";
import { BaseConnector } from "@io-oidis-localhost/Io/Oidis/Localhost/Primitives/BaseConnector.js";
import { Extern } from "@io-oidis-localhost/Io/Oidis/Localhost/Primitives/Decorators.js";
import { WebServiceClientType } from "@io-oidis-services/Io/Oidis/Services/Enums/WebServiceClientType.js";
import { IWebServiceClient } from "@io-oidis-services/Io/Oidis/Services/Interfaces/IWebServiceClient.js";
import { LiveContentWrapper } from "@io-oidis-services/Io/Oidis/Services/WebServiceApi/LiveContentWrapper.js";
import { WebServiceClientFactory } from "@io-oidis-services/Io/Oidis/Services/WebServiceApi/WebServiceClientFactory.js";
import { Com } from "../../../../Com/Wui/Framework/FallbacksMapping.js";
import { Loader } from "../Loader.js";
import { ConfigurationsManager } from "../Utils/ConfigurationsManager.js";

export class ConfigurationManagerTestHandler extends BaseConnector {

    @Extern(ExternResponseType.FULL_CONTROL)
    public static DownloadOverHttp($url : string, $callback : IResponse) : void {
        LogIt.Debug("download: {0}", $url);
        Loader.getInstance().getFileSystemHandler().Download(<any>{
            streamOutput: true,
            url         : $url
        }, ($headers : string, $bodyOrPath : string) : void => {
            ResponseFactory.getResponse($callback).Send($headers, $bodyOrPath);
        });
    }
}

export class ConfigurationManagerTest extends RuntimeTestRunner {

    private client : IWebServiceClient;

    constructor() {
        super();

        this.client = WebServiceClientFactory.getClient(WebServiceClientType.WEB_SOCKETS,
            this.getRequest().getHostUrl() + "connector.config.jsonp");
        this.client.getEvents().OnError(($eventArgs : ErrorEventArgs) : void => {
            Echo.Printf($eventArgs.Exception().ToString());
        });
    }

    public testUpload() : IRuntimeTestPromise {
        const execute : any = ($className : string, $callback : () => void) : void => {
            Echo.Printf("Upload under namespace: {0}", $className);
            LiveContentWrapper.InvokeMethod(this.client, $className, "Upload",
                "testApp", "testConfig.jsonp", "2018.1.0", JSON.stringify({
                    test: "testData"
                }))
                .Then(($status : boolean) : void => {
                    this.assertEquals($status, true);
                    $callback();
                });
        };
        return ($done : () => void) : void => {
            execute(ConfigurationsManager.ClassName(), () : void => {
                execute(Com.Wui.Framework.Hub.Utils.ConfigurationsManager.ClassName(), $done);
            });
        };
    }

    public testDownloadWS() : IRuntimeTestPromise {
        const execute : any = ($className : string, $callback : () => void) : void => {
            Echo.Printf("Download under namespace: {0}", $className);
            LiveContentWrapper.InvokeMethod(this.client, $className, "Download",
                "testApp", "testConfig.jsonp", "2018.1.0")
                .Then(($status : boolean, $config : any) : void => {
                    this.assertEquals($status, true);
                    Echo.Printf($config);
                    $callback();
                });
        };
        return ($done : () => void) : void => {
            execute(ConfigurationsManager.ClassName(), () : void => {
                execute(Com.Wui.Framework.Hub.Utils.ConfigurationsManager.ClassName(), $done);
            });
        };
    }

    public testDownloadHttp() : IRuntimeTestPromise {
        const url : string = this.getRequest().getHostUrl() + "Configuration/testApp/testConfig/2018.1.0";
        Echo.Println("<a href=\"" + url + "\" target=\"_blank\">" + url + "</a>");
        return ($done : () => void) : void => {
            LiveContentWrapper.InvokeMethod(this.client, ConfigurationManagerTestHandler.ClassName(), "DownloadOverHttp",
                url + "?refresh=true")
                .Then(($headers : string, $body : string) : void => {
                    Echo.Printf("{0} {1}", $headers, $body);
                    $done();
                });
        };
    }
}

/* dev:end */

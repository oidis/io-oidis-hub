/*! ******************************************************************************************************** *
 *
 * Copyright 2017-2018 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

/* dev:start */
import { HttpMethodType } from "@io-oidis-commons/Io/Oidis/Commons/Enums/HttpMethodType.js";
import { EnvironmentArgs } from "@io-oidis-commons/Io/Oidis/Commons/EnvironmentArgs.js";
import { ErrorEventArgs } from "@io-oidis-commons/Io/Oidis/Commons/Events/Args/ErrorEventArgs.js";
import { IRuntimeTestPromise } from "@io-oidis-commons/Io/Oidis/Commons/HttpProcessor/Resolvers/RuntimeTestRunner.js";
import { Convert } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Convert.js";
import { Echo } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Echo.js";
import { LogIt } from "@io-oidis-commons/Io/Oidis/Commons/Utils/LogIt.js";
import { ObjectEncoder } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectEncoder.js";
import { StringUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/StringUtils.js";
import { RuntimeTestRunner } from "@io-oidis-gui/Io/Oidis/Gui/HttpProcessor/Resolvers/RuntimeTestRunner.js";
import { FileSystemHandler } from "@io-oidis-localhost/Io/Oidis/Localhost/Connectors/FileSystemHandler.js";
import { ResponseFactory } from "@io-oidis-localhost/Io/Oidis/Localhost/HttpProcessor/ResponseApi/ResponseFactory.js";
import { ExternResponseType } from "@io-oidis-localhost/Io/Oidis/Localhost/Interfaces/IExternOptions.js";
import { IResponse } from "@io-oidis-localhost/Io/Oidis/Localhost/Interfaces/IResponse.js";
import { BaseConnector } from "@io-oidis-localhost/Io/Oidis/Localhost/Primitives/BaseConnector.js";
import { Extern } from "@io-oidis-localhost/Io/Oidis/Localhost/Primitives/Decorators.js";
import { EnvironmentHelper } from "@io-oidis-localhost/Io/Oidis/Localhost/Utils/EnvironmentHelper.js";
import { IFileSystemDownloadOptions } from "@io-oidis-services/Io/Oidis/Services/Connectors/FileSystemHandlerConnector.js";
import { WebServiceClientType } from "@io-oidis-services/Io/Oidis/Services/Enums/WebServiceClientType.js";
import { IWebServiceClient } from "@io-oidis-services/Io/Oidis/Services/Interfaces/IWebServiceClient.js";
import { LiveContentWrapper } from "@io-oidis-services/Io/Oidis/Services/WebServiceApi/LiveContentWrapper.js";
import { WebServiceClientFactory } from "@io-oidis-services/Io/Oidis/Services/WebServiceApi/WebServiceClientFactory.js";
import { Transform } from "stream";
import { Com } from "../../../../Com/Wui/Framework/FallbacksMapping.js";
import { Loader } from "../Loader.js";
import { SelfupdatesManager } from "../Utils/SelfupdatesManager.js";

export class SelfupdateManagerTestHandler extends BaseConnector {

    @Extern(ExternResponseType.FULL_CONTROL)
    public static DownloadOverHttp($url : string, $callback : IResponse) : void {
        LogIt.Debug("download: {0}", $url);
        Loader.getInstance().getFileSystemHandler().Download(<any>{
            streamOutput: true,
            url         : $url
        }, ($headers : string, $bodyOrPath : string) : void => {
            ResponseFactory.getResponse($callback).Send($headers, $bodyOrPath);
        });
    }

    @Extern(ExternResponseType.FULL_CONTROL)
    public static DownloadPOSTHttp($url : string, $data : string, $callback : IResponse) : void {
        LogIt.Debug("download POST: {0}", $url);
        Loader.getInstance().getFileSystemHandler().Download(<IFileSystemDownloadOptions>{
            body        : "jsonData={\"data\":\"" + $data + "\",\"id\":1558926930000,\"origin\":\"http://localhost\",\"token\":\"\"," +
                "\"type\":\"LiveContentWrapper.InvokeMethod\"}",
            headers     : {
                "charset"     : "UTF-8",
                "content-type": "application/x-www-form-urlencoded; charset=UTF-8",
                "user-agent"  : "Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 " +
                    "(KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36"
            },
            method      : HttpMethodType.POST,
            streamOutput: true,
            url         : $url
        }, ($headers : string, $bodyOrPath : string) : void => {
            ResponseFactory.getResponse($callback).Send($headers, $bodyOrPath);
        });
    }

    @Extern(ExternResponseType.FULL_CONTROL)
    public static UploadTestPackage($projectName : string, $releaseName : string, $platform : string, $projectVersion : string,
                                    $buildTime : number, $type : string, $callback : IResponse) : void {
        const fileSystem : FileSystemHandler = Loader.getInstance().getFileSystemHandler();
        const filePath : string = fileSystem.NormalizePath(Loader.getInstance().getProgramArgs().ProjectBase() + "/../" +
            $projectName + "-" +
            StringUtils.Replace(Loader.getInstance().getEnvironmentArgs().getProjectVersion(), ".", "-") +
            (EnvironmentHelper.IsWindows() ? ".zip" : ".tar.gz"));

        if (fileSystem.Exists(filePath)) {
            const maxChunkSize : number = 1024 * 1024 * 1.5;
            const path : any = require("path");
            const fs : any = require("fs");
            const {Transform} = require("stream");
            let chunkIndex : number = 0;
            const contentSize : number = fs.statSync(filePath).size;

            const chunk : any = {
                data : "",
                end  : maxChunkSize,
                id   : path.basename(filePath),
                index: chunkIndex,
                path : ObjectEncoder.Base64(path.basename(filePath)),
                size : contentSize,
                start: 0
            };

            try {
                LogIt.Info("Uploading file \"" + filePath + "\"");

                chunk.id = (new Date()).getTime() + "";

                let buffer : Buffer = null;
                let sendChunk : any;
                const pipeTransform : typeof Transform = new Transform({
                    transform($data : any, $encoding : string, $callback : any) : void {
                        if (buffer === null) {
                            buffer = Buffer.from($data);
                        } else if (buffer.byteLength < maxChunkSize) {
                            buffer = Buffer.concat([buffer, Buffer.from($data)]);
                        }
                        if (buffer.byteLength >= maxChunkSize) {
                            sendChunk($callback);
                        } else {
                            $callback();
                        }
                    }
                });
                sendChunk = ($callback : any) : void => {
                    if (buffer !== null && buffer.byteLength !== 0) {
                        chunk.index = chunkIndex;
                        chunk.data = buffer.toString("base64");
                        chunk.end = chunk.start + maxChunkSize;
                        if (chunk.end > chunk.size) {
                            chunk.end = chunk.size;
                        }

                        LogIt.Info("Sending chunk #" + chunkIndex +
                            " (" + Convert.IntegerToSize(chunk.end) + "/" + Convert.IntegerToSize(chunk.size) + ")");
                        Com.Wui.Framework.Hub.Utils.SelfupdatesManager.Upload(chunk, <any>(($status : boolean) : void => {
                            if ($status) {
                                chunk.start = chunk.end;
                                chunkIndex++;
                                buffer = null;
                                $callback();
                            } else {
                                ResponseFactory.getResponse($callback).OnError("Chunk upload has failed");
                            }
                        }));
                    } else {
                        $callback();
                    }
                };

                const stream : any = fs.createReadStream(filePath, <any>{
                    bufferSize: maxChunkSize
                });
                stream
                    .on("end", () : void => {
                        sendChunk(() : void => {
                            LogIt.Info("File has been uploaded successfully.");
                            Com.Wui.Framework.Hub.Utils.SelfupdatesManager.Register(
                                $projectName,
                                $releaseName,
                                $platform,
                                $projectVersion,
                                $buildTime,
                                chunk.id,
                                $type,
                                $callback);
                        });
                    })
                    .on("error", ($error : Error) : void => {
                        ResponseFactory.getResponse($callback).OnError($error);
                        LogIt.Warning($error.message);
                    })
                    .pipe(pipeTransform);
            } catch (ex) {
                ResponseFactory.getResponse($callback).OnError(ex);
                LogIt.Warning(ex.message);
            }
        } else {
            const message : string = "File for upload does not exists: " + filePath;
            ResponseFactory.getResponse($callback).OnError(message);
            LogIt.Warning(message);
        }
    }
}

export class SelfupdateManagerTest extends RuntimeTestRunner {

    private client : IWebServiceClient;

    constructor() {
        super();

        this.client = WebServiceClientFactory.getClient(WebServiceClientType.WEB_SOCKETS,
            this.getRequest().getHostUrl() + "connector.config.jsonp");
        this.client.getEvents().OnError(($eventArgs : ErrorEventArgs) : void => {
            Echo.Printf($eventArgs.Exception().ToString());
        });

        // this.setMethodFilter(
        //     // "testUploadVersion",
        //     // "testUploadZip",
        //     // "testUploadTar",
        //     // "testUpdateExists",
        //     // "testUpdateNotExists",
        //     // "testDownloadHttp",
        //     // "testDownloadPOST"
        // );
    }

    public testUploadVersion() : IRuntimeTestPromise {
        this.timeoutLimit(-1);
        return ($done : () => void) : void => {
            LiveContentWrapper.InvokeMethod(this.client, SelfupdateManagerTestHandler.ClassName(), "UploadTestPackage",
                this.getEnvironmentArgs().getProjectName(),
                "Win",
                "app-win-nodejs",
                "2018.1.0-alpha",
                1539853031794,
                "zip")
                .Then(($status : boolean) : void => {
                    this.assertEquals($status, true);
                    $done();
                });
        };
    }

    public testUploadZip() : IRuntimeTestPromise {
        this.timeoutLimit(-1);
        return ($done : () => void) : void => {
            LiveContentWrapper.InvokeMethod(this.client, SelfupdateManagerTestHandler.ClassName(), "UploadTestPackage",
                this.getEnvironmentArgs().getProjectName(),
                "Win",
                "app-win-nodejs",
                this.getEnvironmentArgs().getProjectVersion(),
                new Date(this.getEnvironmentArgs().getBuildTime()).getTime(),
                "zip")
                .Then(($status : boolean) : void => {
                    this.assertEquals($status, true);
                    $done();
                });
        };
    }

    public testUploadNoRelease() : IRuntimeTestPromise {
        this.timeoutLimit(-1);
        return ($done : () => void) : void => {
            LiveContentWrapper.InvokeMethod(this.client, SelfupdateManagerTestHandler.ClassName(), "UploadTestPackage",
                this.getEnvironmentArgs().getProjectName(),
                "null",
                "app-mac-nodejs",
                this.getEnvironmentArgs().getProjectVersion(),
                new Date(this.getEnvironmentArgs().getBuildTime()).getTime(),
                "tar.gz")
                .Then(($status : boolean) : void => {
                    this.assertEquals($status, true);
                    $done();
                });
        };
    }

    public testUploadTar() : IRuntimeTestPromise {
        this.timeoutLimit(-1);
        return ($done : () => void) : void => {
            LiveContentWrapper.InvokeMethod(this.client, SelfupdateManagerTestHandler.ClassName(), "UploadTestPackage",
                this.getEnvironmentArgs().getProjectName(),
                "Linux",
                "app-linux-nodejs",
                this.getEnvironmentArgs().getProjectVersion(),
                new Date(this.getEnvironmentArgs().getBuildTime()).getTime(),
                "tar.bz2")
                .Then(($status : boolean) : void => {
                    this.assertEquals($status, true);
                    $done();
                });
        };
    }

    public testUpdateExists() : IRuntimeTestPromise {
        const execute : any = ($className : string, $callback : () => void) : void => {
            Echo.Printf("Validate under namespace: {0}", $className);
            LiveContentWrapper.InvokeMethod(this.client, $className, "UpdateExists",
                this.getEnvironmentArgs().getProjectName(),
                "Win",
                "app-win-nodejs",
                "2018.1.0-alpha",
                1539853031793,
                false)
                .Then(($status : boolean) : void => {
                    this.assertEquals($status, true);
                    $callback();
                });
        };
        return ($done : () => void) : void => {
            execute(Com.Wui.Framework.Hub.Utils.SelfupdatesManager.ClassName(), () : void => {
                execute(SelfupdatesManager.ClassName(), $done);
            });
        };
    }

    public testUpdateNotExists() : IRuntimeTestPromise {
        return ($done : () => void) : void => {
            LiveContentWrapper.InvokeMethod(this.client, Com.Wui.Framework.Hub.Utils.SelfupdatesManager.ClassName(), "UpdateExists",
                this.getEnvironmentArgs().getProjectName(),
                "Win",
                "app-win-nodejs",
                this.getEnvironmentArgs().getProjectVersion(),
                new Date().getTime(),
                false)
                .Then(($status : boolean) : void => {
                    this.assertEquals($status, false);
                    $done();
                });
        };
    }

    public testDownloadHttp() : IRuntimeTestPromise {
        const environment : EnvironmentArgs = this.getEnvironmentArgs();
        const baseUrl : string = this.getRequest().getHostUrl() + "Update/" + environment.getProjectName();
        let url : string = baseUrl + "/Win/app-win-nodejs";
        Echo.Println("<a href=\"" + url + "\" target=\"_blank\">" + url + "</a>");
        url += "/2018.1.0-alpha";
        Echo.Println("<a href=\"" + url + "\" target=\"_blank\">" + url + "</a>");
        url = baseUrl + "/Linux/app-linux-nodejs";
        Echo.Println("<a href=\"" + url + "\" target=\"_blank\">" + url + "</a>");
        url = baseUrl + "/null/app-mac-nodejs";
        Echo.Println("<a href=\"" + url + "\" target=\"_blank\">" + url + "</a>");

        return ($done : () => void) : void => {
            LiveContentWrapper.InvokeMethod(this.client, SelfupdateManagerTestHandler.ClassName(), "DownloadOverHttp",
                url)
                .Then(($headers : string, $body : string) : void => {
                    Echo.Printf("{0} {1}", $headers, $body);
                    $done();
                });
            $done();
        };
    }

    public testDownloadPOST() : IRuntimeTestPromise {
        const environment : EnvironmentArgs = this.getEnvironmentArgs();
        const baseUrl : string = this.getRequest().getHostUrl() + "xorigin";

        return ($done : () => void) : void => {
            LiveContentWrapper.InvokeMethod(this.client, SelfupdateManagerTestHandler.ClassName(), "UploadTestPackage",
                environment.getProjectName(),
                "Win",
                "app-win-nodejs",
                environment.getProjectVersion(),
                new Date(environment.getBuildTime()).getTime(),
                "zip")
                .Then(($status : boolean) : void => {
                    this.assertEquals($status, true);
                    LiveContentWrapper.InvokeMethod(this.client, SelfupdateManagerTestHandler.ClassName(), "DownloadPOSTHttp",
                        baseUrl, ObjectEncoder.Base64(JSON.stringify({
                            args: ObjectEncoder.Base64(JSON.stringify([environment.getProjectName(), "Win", "app-win-nodejs"])),
                            name: "Com.Wui.Framework.Rest.Services.Utils.SelfupdatesManager.Download"
                        })))
                        .Then(($headers : string[], $body : string) : void => {
                            this.assertEquals($headers.hasOwnProperty("last-modified"), true);
                            this.assertEquals($headers.hasOwnProperty("content-type"), true);
                            this.assertEquals($headers["content-type"], "application/octet-stream");
                            Echo.Printf("{0} {1}", $headers, $body.length);
                            LiveContentWrapper.InvokeMethod(this.client, SelfupdateManagerTestHandler.ClassName(), "DownloadPOSTHttp",
                                baseUrl, ObjectEncoder.Base64(JSON.stringify({
                                    args: ObjectEncoder.Base64(JSON.stringify([environment.getProjectName(), "Win", "app-win-nodejs"])),
                                    name: "Io.Oidis.Hub.Utils.SelfupdatesManager.DownloadRaw"
                                })))
                                .Then(($headers : string[], $body : string) : void => {
                                    this.assertEquals($headers.hasOwnProperty("last-modified"), true);
                                    this.assertEquals($headers.hasOwnProperty("content-type"), true);
                                    this.assertEquals($headers["content-type"], "application/octet-stream");
                                    Echo.Printf("{0} {1}", $headers, $body.length);
                                    $done();
                                });
                        });
                });
        };
    }
}

/* dev:end */

/*! ******************************************************************************************************** *
 *
 * Copyright 2021-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

/* dev:start */
import { ErrorEventArgs } from "@io-oidis-commons/Io/Oidis/Commons/Events/Args/ErrorEventArgs.js";
import { Echo } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Echo.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { PersistenceType } from "@io-oidis-gui/Io/Oidis/Gui/Enums/PersistenceType.js";
import { RuntimeTestRunner } from "@io-oidis-gui/Io/Oidis/Gui/HttpProcessor/Resolvers/RuntimeTestRunner.js";
import { PersistenceFactory } from "@io-oidis-gui/Io/Oidis/Gui/PersistenceFactory.js";
import { Extern } from "@io-oidis-localhost/Io/Oidis/Localhost/Primitives/Decorators.js";
import { AgentsRegisterConnectorAsync, IAgentInfo } from "@io-oidis-services/Io/Oidis/Services/Connectors/AgentsRegisterConnector.js";
import { AuthManagerConnector } from "@io-oidis-services/Io/Oidis/Services/Connectors/AuthManagerConnector.js";
import { FileSystemHandlerConnectorAsync } from "@io-oidis-services/Io/Oidis/Services/Connectors/FileSystemHandlerConnector.js";
import { WebServiceClientType } from "@io-oidis-services/Io/Oidis/Services/Enums/WebServiceClientType.js";
import { AuthHttpResolver } from "@io-oidis-services/Io/Oidis/Services/ErrorPages/AuthHttpResolver.js";
import { BaseConnector } from "@io-oidis-services/Io/Oidis/Services/Primitives/BaseConnector.js";
import { ConnectorHub } from "../Utils/ConnectorHub.js";

export class MockConnectorHub extends ConnectorHub {
    @Extern()
    public async TryBackendForwardedMethod($agentId : string, $url : string) : Promise<string> {
        // TODO(mkelnar) there should be option to forward request directly to this.getAgent() of AgentsRegister class
        //  main question is how to properly handle LCP directly, perhaps a special client derived from BaseWebServiceClient
        //  instead of WebSocketsClient with proper override will solve it - something like LocalLoopbackClient etc...
        //  so it will work exactly with the same api but without another WS connection to the same app - Hub
        // TODO(mkelnar) Second point is regarding the services BaseConnector construction which is fully initialized in constructor
        //  so in the case of agent forwarding it connects once in constructor and second connection is done in setAgent() method
        //  this behaviour should be refactored too!
        const fs : FileSystemHandlerConnectorAsync = new FileSystemHandlerConnectorAsync(false, $url,
            WebServiceClientType.FORWARD_CLIENT);
        fs.setAgent($agentId, $url);
        return fs.getAppDataPath();
    }
}

export class MockConnectorHubConnector extends BaseConnector {

    public async TryBackendForwardedMethod($agentId : string, $url : string) : Promise<string> {
        return this.asyncInvoke("TryBackendForwardedMethod", $agentId, $url);
    }

    protected getClientType() : WebServiceClientType {
        return WebServiceClientType.HUB_CONNECTOR;
    }

    protected getServerNamespaces() : any {
        const namespaces : any = {};
        namespaces[WebServiceClientType.HUB_CONNECTOR] = MockConnectorHub.ClassName();
        return namespaces;
    }
}

export class MockAgentsRegisterConnectorAsync extends AgentsRegisterConnectorAsync {
    // TBD
}

export class AgentsRegisterTest extends RuntimeTestRunner {
    private auth : AuthManagerConnector;
    private client : MockAgentsRegisterConnectorAsync;
    private remoteFs : FileSystemHandlerConnectorAsync;
    private mockHubCon : MockConnectorHubConnector;

    constructor() {
        super();

        // this.setMethodFilter("testGenerateTestData");
    }

    public __IgnoretestGenerateTestData() : void {
        // This is just for ability to generate data for FE R&D
        let index : number = 0;
        this.addButton("RegisterAgent", async () : Promise<void> => {
            index++;
            if (await this.client.RegisterAgent({
                domain  : "localhost",
                name    : "test-connector-" + index,
                platform: "platform" + index,
                poolName: "pool" + index,
                tag     : "tag" + index,
                version : "2022.0." + index
            })) {
                Echo.Printf("Registration success");
            } else {
                Echo.Printf("Registration failed");
            }
        });
    }

    public async testInitUser() : Promise<void> {
        Echo.Printf("To pass this test root:root has to be configured in private.conf defaultUser section");
        PersistenceFactory.getPersistence(PersistenceType.CLIENT_IP, AuthHttpResolver.ClassName())
            .Variable("token", await this.auth.LogIn("root", "root"));

        // if (ObjectValidator.IsEmptyOrNull(await this.auth.FindUsers("test-agent"))) {
        //     await this.auth.Register({name: "test-agent", password: "test-agent"});
        //     await this.auth.CreateGroup("test-agent-group", ["*"], false);
        //     await this.auth.AddGroup("test-agent-group", "test-agent");
        // }
        // await this.auth.LogIn("test-agent", "test-agent");
    }

    public async testRegister() : Promise<void> {
        Echo.Printf("" +
            "<button id='genToken' type='button'>Generate Token</button>" +
            "<input id='generatedToken' type='text' readonly width='350' onClick='this.setSelectionRange(0, this.value.length)'/>" +
            "<br>" +
            "<textarea id=\"agents_info\" readonly cols=\"100\" rows=\"7\"/>");
        const agentsInfo : HTMLTextAreaElement = <HTMLTextAreaElement>document.getElementById("agents_info");
        const generatedToken : HTMLInputElement = <HTMLInputElement>document.getElementById("generatedToken");
        const genToken : HTMLButtonElement = <HTMLButtonElement>document.getElementById("genToken");

        genToken.onclick = async () : Promise<void> => {
            generatedToken.value = (await this.auth.GenerateAuthToken("", "test-agent")).value;
        };

        const loadInfo : any = () : void => {
            setTimeout(async () : Promise<void> => {
                const list : IAgentInfo[] = await this.client.getAgentsList();
                agentsInfo.value = "";
                for await (const info of list) {
                    this.remoteFs.setAgent(info.id, this.getRequest().getHostUrl() + "connector.config.jsonp");
                    this.remoteFs.setForwardingMethod(ConnectorHub.ClassName() + ".ForwardRequest");
                    const path1 : string = await this.remoteFs.getAppDataPath();
                    const path2 : string = await this.mockHubCon.TryBackendForwardedMethod(info.id,
                        this.getRequest().getHostUrl() + "connector.config.jsonp");
                    const isOk : boolean = !ObjectValidator.IsEmptyOrNull(path1);
                    const isOk2 : boolean = path1 === path2;

                    agentsInfo.value = info.domain + "-" + info.platform + " [" + info.id + "] - " + isOk + "/" +
                        isOk2 + "\n" + agentsInfo.value;
                }
                loadInfo();
            }, 1000);
        };
        loadInfo();
    }

    protected before() : void {
        this.auth = new AuthManagerConnector(true, this.getRequest().getHostUrl() + "connector.config.jsonp");
        this.client = new MockAgentsRegisterConnectorAsync(true, this.getRequest().getHostUrl() + "connector.config.jsonp");
        this.remoteFs = new FileSystemHandlerConnectorAsync(true, this.getRequest().getHostUrl() + "connector.config.jsonp",
            WebServiceClientType.FORWARD_CLIENT);
        this.mockHubCon = new MockConnectorHubConnector(true, this.getRequest().getHostUrl() + "connector.config.jsonp");
        // this has to be there because it fail otherwise, another reason to not call
        // this.init inside connector constructor!!!
        this.auth.getEvents().OnError(($eventArgs : ErrorEventArgs) : void => {
            Echo.Printf($eventArgs.Exception().ToString());
        });
        this.client.getEvents().OnError(($eventArgs : ErrorEventArgs) : void => {
            Echo.Printf($eventArgs.Exception().ToString());
        });
        this.remoteFs.getEvents().OnError(($eventArgs : ErrorEventArgs) : void => {
            Echo.Printf($eventArgs.Exception().ToString());
        });
        this.mockHubCon.getEvents().OnError(($eventArgs : ErrorEventArgs) : void => {
            Echo.Printf($eventArgs.Exception().ToString());
        });
    }

    // protected async tearDown() : Promise<void> {
    //     /// TODO: remove test user and test methods
    // }
}

/* dev:end */

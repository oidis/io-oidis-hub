/*! ******************************************************************************************************** *
 *
 * Copyright 2024-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { BaseObject } from "@io-oidis-commons/Io/Oidis/Commons/Primitives/BaseObject.js";
import { Partial } from "@io-oidis-commons/Io/Oidis/Commons/Primitives/Decorators.js";
import { JsonUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/JsonUtils.js";
import { LogIt } from "@io-oidis-commons/Io/Oidis/Commons/Utils/LogIt.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { Reflection } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Reflection.js";
import { StringUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/StringUtils.js";
import { IGroupsDump, IPermissionsReport } from "@io-oidis-hub-gui/Io/Oidis/Hub/Gui/Connectors/AuthManagerConnector.js";
import { Extern } from "@io-oidis-localhost/Io/Oidis/Localhost/Primitives/Decorators.js";
import { AppSettings } from "../DAO/Models/AppSettings.js";
import { AuthorizedMethods } from "../DAO/Models/AuthEntry.js";
import { Group } from "../DAO/Models/Group.js";
import { User } from "../DAO/Models/User.js";
import { Loader } from "../Loader.js";
import { AuthManager } from "../Utils/AuthManager.js";
import { Convert } from "../Utils/Convert.js";

@Partial()
export abstract class AuthMethodsManager extends BaseObject {

    @Extern()
    public async getAuthorizedMethods(this : AuthManager, $token : string | User = null) : Promise<string[]> {
        if (ObjectValidator.IsEmptyOrNull($token)) {
            $token = this.getCurrentToken();
        }
        const authMethods : string[] = [];
        const appendMethods : any = ($list : string[]) : void => {
            if (!ObjectValidator.IsEmptyOrNull($list)) {
                $list.forEach(($method : string) : void => {
                    if (!ObjectValidator.IsEmptyOrNull($method) && !authMethods.includes($method)) {
                        authMethods.push($method);
                    }
                });
            }
        };
        const appSettings : AppSettings = await Loader.getInstance().getEnvironmentHelper().getAppSettings();
        appendMethods(this.defaultAccessibleMethods);
        appendMethods(appSettings.DefaultAccessibleMethods);

        const collectMethods : any = ($user : User) : void => {
            appendMethods(this.defaultGroupAccess);
            appendMethods(appSettings.DefaultGroupAccess);

            [$user.DefaultGroup].concat($user.Groups).forEach(($group : Group) : void => {
                if (!ObjectValidator.IsEmptyOrNull($group) && !$group.Deleted) {
                    appendMethods($group.AuthorizedMethods);
                }
            });
        };
        if (!ObjectValidator.IsEmptyOrNull($token)) {
            let user : User = <User>$token;
            if (ObjectValidator.IsString(user)) {
                if (await this.tokens.Validate(<string>$token)) {
                    user = await this.getUser(<string>$token);
                } else {
                    if (this.authCache.hasOwnProperty(<string>$token)) {
                        delete this.authCache[<string>$token];
                    }
                    user = null;
                }
            }
            if (!ObjectValidator.IsEmptyOrNull(user)) {
                collectMethods(user);
            }
        }
        return authMethods;
    }

    @Extern()
    public async getAuthorizedMethodsFor(this : AuthManager, $userId : string) : Promise<string[]> {
        return this.getAuthorizedMethods(await this.getUser($userId));
    }

    @Extern()
    public async setAuthorizedMethods(this : AuthManager, $group : string | Group, $authMethods : string[], $append : boolean = false,
                                      $force : boolean = false) : Promise<boolean> {
        if (ObjectValidator.IsString($group)) {
            const groupNameOrId : string = <string>$group;
            $group = await this.getGroupByName(groupNameOrId);
            if (ObjectValidator.IsEmptyOrNull($group)) {
                $group = await this.getClassFromInstance(this.getGroupInstance()).FindById(groupNameOrId);
            }
        }
        if (!ObjectValidator.IsEmptyOrNull($group) && (!(<Group>$group).Deleted || $force)) {
            if ($append) {
                const newMethods : string[] = (<Group>$group).AuthorizedMethods;
                $authMethods.forEach(($method : string) : void => {
                    if (!newMethods.includes($method)) {
                        newMethods.push($method);
                    }
                });
                $authMethods = newMethods;
            }
            (<Group>$group).AuthorizedMethods = $authMethods;
            await (<Group>$group).Save();
            /// TODO: this can be done better if in cache will be possible to detect changes?
            this.authCache = {};
            return true;
        }
        return false;
    }

    @Extern()
    public async ExportAuthMethods(this : AuthManager) : Promise<IGroupsDump[]> {
        const dump : IGroupsDump[] = [];
        (await Loader.getInstance().getGQLContext().Execute(`{groupAll(limit:-1){id,authorizedMethods}}`)).data.groupAll
            .forEach(($group : any) : void => {
                dump.push({
                    Id               : $group.id,
                    AuthorizedMethods: $group.authorizedMethods
                });
            });
        return dump;
    }

    @Extern()
    public async ImportAuthMethods(this : AuthManager, $data : IGroupsDump[]) : Promise<void> {
        for await (const record of $data) {
            await this.setAuthorizedMethods(record.Id, record.AuthorizedMethods, true);
        }
    }

    @Extern()
    public async ResetDefaultAuthMethods(this : AuthManager) : Promise<void> {
        const report : IPermissionsReport = await this.getPermissionsReport();
        const groups : any[] = (await this.createPermissionBackup()).filter(($record : any) : boolean => {
            return report.groupsWithDuplicity.includes($record.Id);
        });
        for await (const group of groups) {
            await this.setAuthorizedMethods(group.Id, group.AuthorizedMethods.filter(($method : string) : boolean => {
                return !report.allDefaultMethods.includes($method);
            }), false, true);
        }
    }

    /// TODO: try to limit invoke only to non-string values and for string enable to use only specific API
    protected generateAuthMethodsFrom<T>($for : AuthMethodCategory, $class : any | any[],
                                         $methods : ($proto : T) => any[]) : string[];
    protected generateAuthMethodsFrom<T>($class : any | any[], $methods : ($proto : T) => any[]) : string[];
    protected generateAuthMethodsFrom(this : AuthManager, ...$args : any[]) : string[] {
        const apiMap : any = {
            category: null,
            class   : null,
            methods : []
        };
        if ($args.length === 3) {
            apiMap.category = $args[0];
            apiMap.class = $args[1];
            apiMap.methods = $args[2];
        } else {
            apiMap.class = $args[0];
            apiMap.methods = $args[1];
        }

        let defaultClass : any = apiMap.class;
        if (ObjectValidator.IsArray(defaultClass)) {
            defaultClass = apiMap.class[0];
        }
        let classPrototype : any = defaultClass;
        if (ObjectValidator.IsString(defaultClass)) {
            classPrototype = Reflection.getInstance().getClass(defaultClass).prototype;
        } else if (!ObjectValidator.IsEmptyOrNull(defaultClass.prototype)) {
            classPrototype = defaultClass.prototype;
        }
        return this.generateAuthMethods({
            category: apiMap.category,
            class   : apiMap.class,
            methods : apiMap.methods(classPrototype)
        });
    }

    protected generateAuthMethods(this : AuthManager, $options : IAuthMethodsDefinition) : string[] {
        $options = JsonUtils.Extend({
            category: null,
            class   : null,
            methods : []
        }, $options);
        if (ObjectValidator.IsEmptyOrNull($options.methods)) {
            return [];
        }
        if (ObjectValidator.IsEmptyOrNull($options.class)) {
            throw new Error(`Failed to generate authorize methods for undefined class.`);
        }
        let defaultClass : any = $options.class;
        if (ObjectValidator.IsArray(defaultClass)) {
            defaultClass = $options.class.shift();
        }
        const output : string[] = [];
        let classPrototype : any = defaultClass;
        if (ObjectValidator.IsString(defaultClass)) {
            classPrototype = Reflection.getInstance().getClass(defaultClass).prototype;
        } else if (!ObjectValidator.IsEmptyOrNull(defaultClass.prototype)) {
            classPrototype = defaultClass.prototype;
        }

        const owner : string = classPrototype.getClassName();
        const namespaces : string[] = [].concat($options.class).map(($namespace : any) : string => {
            if (ObjectValidator.IsString($namespace)) {
                return $namespace;
            }
            if (ObjectValidator.IsSet($namespace.getClassName)) {
                return $namespace.getClassName();
            }
            return $namespace.ClassName();
        });
        if (!namespaces.includes(owner)) {
            namespaces.push(owner);
        }

        $options.methods.forEach(($pattern : any) : void => {
            if (!ObjectValidator.IsString($pattern)) {
                $pattern = $pattern.name;
                if (ObjectValidator.IsEmptyOrNull($pattern)) {
                    throw new Error(`Name of authorize method was not detected from: ${Convert.ObjectToString($pattern)}
    Hint: this issue can happen by referencing of static methods from parent class`);
                }
            }
            $pattern = StringUtils.Remove($pattern, owner + ".");
            if (classPrototype.hasOwnProperty($pattern) ||
                ObjectValidator.IsSet(classPrototype[$pattern])) {

                namespaces.forEach(($namespace : string) : void => {
                    const method : string = $namespace + "." + $pattern;
                    if (!output.includes(method)) {
                        output.push(method);
                    }
                });
            } else {
                LogIt.Error(`Failed to generate authorize method: "${$pattern}" was not found in "${owner}"`);
            }
        });
        if (!ObjectValidator.IsEmptyOrNull($options.category)) {
            switch ($options.category) {
            case AuthMethodCategory.FOR_ALL_USERS:
                output.forEach(($method : string) : void => {
                    if (this.defaultGroupAccess.includes($method)) {
                        LogIt.Error(`Registration of authorize method skipped: Method "${$method}" is already part of methods for Authorized users. This registration is either incorrect or permissions for Authorized users should be reviewed.`);
                    } else if (!this.defaultAccessibleMethods.includes($method)) {
                        this.defaultAccessibleMethods.push($method);
                    }
                });
                break;
            case AuthMethodCategory.ONLY_FOR_AUTHORIZED_USERS:
                this.defaultGroupAccess.push(...output);
                output.forEach(($method : string) : void => {
                    if (this.defaultAccessibleMethods.includes($method)) {
                        LogIt.Warning(`Registration of authorize method skipped: Method "${$method}" is already part of methods for ALL users. This registration is either incorrect or permissions for ALL users should be restricted.`);
                    } else if (!this.defaultGroupAccess.includes($method)) {
                        this.defaultGroupAccess.push($method);
                    }
                });
                break;
            case AuthMethodCategory.UNDEFINED:
                // do nothing
                break;
            default:
                throw new Error("Failed to register authorize methods to unrecognized category: " + $options.category);
            }
        }

        return output;
    }

    protected authMethodsToPatterns(this : AuthManager, $entries : AuthorizedMethods[], $patternsCache : string[] = []) : string[] {
        $entries.forEach(($entry : AuthorizedMethods) : void => {
            let pattern : string = $entry.Namespace + ".*";
            if (!$patternsCache.includes(pattern)) {
                $patternsCache.push(pattern);
            }
            pattern = $entry.Namespace + "." + $entry.Class + ".*";
            if (!$patternsCache.includes(pattern)) {
                $patternsCache.push(pattern);
            }
            $entry.Methods.forEach(($method : string) : void => {
                pattern = $entry.Namespace + "." + $entry.Class + "." + $method;
                if (!$patternsCache.includes(pattern)) {
                    $patternsCache.push(pattern);
                }
            });
        });
        return $patternsCache;
    }
}

export enum AuthMethodCategory {
    UNDEFINED,
    FOR_ALL_USERS,
    ONLY_FOR_AUTHORIZED_USERS
}

export interface IAuthMethodsDefinition {
    category? : AuthMethodCategory;
    class : any | any[];
    methods : any[];
}

// generated-code-start
export const IAuthMethodsDefinition = globalThis.RegisterInterface(["category", "class", "methods"]);
// generated-code-end

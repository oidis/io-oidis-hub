/*! ******************************************************************************************************** *
 *
 * Copyright 2024 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { BaseObject } from "@io-oidis-commons/Io/Oidis/Commons/Primitives/BaseObject.js";
import { Partial } from "@io-oidis-commons/Io/Oidis/Commons/Primitives/Decorators.js";
import { LogIt } from "@io-oidis-commons/Io/Oidis/Commons/Utils/LogIt.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { StringUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/StringUtils.js";
import { Extern } from "@io-oidis-localhost/Io/Oidis/Localhost/Primitives/Decorators.js";
import { IAppProfileOptions, IAppRegisterOptions } from "@io-oidis-services/Io/Oidis/Services/Connectors/AuthManagerConnector.js";
import { IAuthorizedMethods } from "@io-oidis-services/Io/Oidis/Services/Structures/IAuthorizedMethods.js";
import { AuthEntry, AuthorizedMethods } from "../DAO/Models/AuthEntry.js";
import { SystemLog } from "../LogProcessor/SystemLog.js";
import { AuthManager } from "../Utils/AuthManager.js";

@Partial()
export abstract class AppsAuthManager extends BaseObject {

    @Extern()
    public async RegisterApp(this : AuthManager, $options : IAppRegisterOptions) : Promise<boolean> {
        $options.releaseName = ObjectValidator.IsEmptyOrNull($options.releaseName) ||
        $options.releaseName === "null" ? "" : $options.releaseName;
        $options.platform = ObjectValidator.IsEmptyOrNull($options.platform) || $options.platform === "null" ? "" : $options.platform;
        const newPackage : AuthEntry = new AuthEntry();
        newPackage.Name = $options.appName;
        newPackage.Release = $options.releaseName;
        newPackage.Platform = $options.platform;
        newPackage.Version = $options.version;
        newPackage.BuildTime = new Date().getTime();
        // newPackage.BuildTime = this.normalizeDate($options.buildTime);
        // if (!ObjectValidator.IsInteger(newPackage.BuildTime)) {
        //     return false;
        // }
        newPackage.AuthorizedMethods = [];
        for await (const method of $options.authMethods) {
            const sha : string = StringUtils.getSha1(JSON.stringify(method));
            let model : AuthorizedMethods;
            const existing : AuthorizedMethods[] = await AuthorizedMethods.Find({sha});
            if (!ObjectValidator.IsEmptyOrNull(existing)) {
                model = existing[0];
            } else {
                model = new AuthorizedMethods();
                model.Class = method.className;
                model.Namespace = method.namespaceName;
                model.Methods = method.methods;
                model.Sha = sha;
                await model.Save();
            }
            newPackage.AuthorizedMethods.push(model);
        }

        const register : AuthEntry[] = await AuthEntry.Find({
            name    : newPackage.Name,
            platform: newPackage.Platform,
            release : newPackage.Release,
            version : newPackage.Version
        });
        try {
            let packageExists : boolean = false;
            for await (const entry of register) {
                entry.BuildTime = newPackage.BuildTime;
                entry.AuthorizedMethods = newPackage.AuthorizedMethods;
                await entry.Save();
                packageExists = true;
            }
            if (!packageExists) {
                LogIt.Debug("Register authorized methods for package {0}(\"{1}\",{2}) {3} {4}",
                    $options.appName, $options.releaseName, $options.platform, $options.version, $options.buildTime);
                await newPackage.Save();
                await SystemLog.Trace({
                    message  : "New package registered for: " + $options.appName,
                    traceBack: this.getClassNameWithoutNamespace() + ".RegisterApp"
                });
            }
            return true;
        } catch (ex) {
            LogIt.Warning(ex.message);
        }
        return false;
    }

    @Extern()
    public async getAppMethods(this : AuthManager, $options : IAppProfileOptions) : Promise<IAuthorizedMethods[]> {
        /// TODO: enable to consume build/compiled/externApi.json if on localhost?
        let authMethods : IAuthorizedMethods[] = [];
        $options.releaseName = ObjectValidator.IsEmptyOrNull($options.releaseName) ||
        $options.releaseName === "null" ? "" : $options.releaseName;
        $options.platform = ObjectValidator.IsEmptyOrNull($options.platform) || $options.platform === "null" ? "" : $options.platform;

        const filter : any = {
            name    : $options.appName,
            platform: $options.platform,
            version : $options.version
        };
        if (!ObjectValidator.IsEmptyOrNull($options.releaseName)) {
            filter.release = $options.releaseName;
        }
        const register : AuthEntry[] = await AuthEntry.Find(filter, {sort: {buildTime: -1}});
        if (register.length > 0) {
            let entry : AuthEntry = register[0];
            const buildTime : number = this.normalizeDate($options.buildTime);
            if (ObjectValidator.IsInteger(buildTime)) {
                // TODO(mkelnar) buildTime for deployed package is equal to deploy time but buildTime in registered method is
                //  stamp from buid (replaced by method registration time too) but it sill not match. we need to connect auth entries with
                //  package ID or let this filter as +-several seconds around (up) and take closest newest one.
                //  Why do we store all buildTimes while we have only latest deployed package?
                //  Done by simple window search so break after first window match or ends with default latest
                for (let index = 0; index < register.length; index++) {
                    const bigger : number = register[index].BuildTime;
                    if (index === (register.length - 1)) {
                        if (buildTime <= bigger) {
                            entry = register[index];
                            break;
                        }
                    } else {
                        const lower : number = register[index + 1].BuildTime;
                        if (buildTime <= bigger && buildTime >= lower) {
                            entry = register[index + 1];
                            break;
                        }
                    }
                }
            }
            // todo filter methods by user ID
            entry.AuthorizedMethods.forEach(($method : AuthorizedMethods) : void => {
                authMethods = authMethods.concat({
                    className    : $method.Class,
                    methods      : $method.Methods,
                    namespaceName: $method.Namespace
                });
            });
        }

        return authMethods;
    }

    @Extern()
    public async getAllAppsMethods(this : AuthManager) : Promise<string[]> {
        const authMethods : string[] = [];
        const register : AuthEntry[] = await AuthEntry.Find({});
        register.forEach(($entry : AuthEntry) : void => {
            this.authMethodsToPatterns($entry.AuthorizedMethods, authMethods);
        });

        return authMethods;
    }
}

/*! ******************************************************************************************************** *
 *
 * Copyright 2024-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { LogLevel } from "@io-oidis-commons/Io/Oidis/Commons/Enums/LogLevel.js";
import { BaseObject } from "@io-oidis-commons/Io/Oidis/Commons/Primitives/BaseObject.js";
import { Partial } from "@io-oidis-commons/Io/Oidis/Commons/Primitives/Decorators.js";
import { JsonUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/JsonUtils.js";
import { LogIt } from "@io-oidis-commons/Io/Oidis/Commons/Utils/LogIt.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { IPermissionsReport, ISystemGroups } from "@io-oidis-hub-gui/Io/Oidis/Hub/Gui/Connectors/AuthManagerConnector.js";
import { Group as FEGroup } from "@io-oidis-hub-gui/Io/Oidis/Hub/Gui/Models/Group.js";
import { Extern } from "@io-oidis-localhost/Io/Oidis/Localhost/Primitives/Decorators.js";
import { IAuthFindGroupsOptions, IModelListResult } from "@io-oidis-services/Io/Oidis/Services/Connectors/AuthManagerConnector.js";
import { Group } from "../DAO/Models/Group.js";
import { User } from "../DAO/Models/User.js";
import { IExecutionResult } from "../Interfaces/DAO/GraphQL/IExecutionResult.js";
import { Loader } from "../Loader.js";
import { SystemLog } from "../LogProcessor/SystemLog.js";
import { AuthManager } from "../Utils/AuthManager.js";

@Partial()
export abstract class GroupsManager extends BaseObject {
    private static systemGroups : ISystemGroups = null;

    // TODO(mkelnar) why is this restricted to be unique by ID and also by Name? what if multiple users has same request to have
    //  their own "Default", "General", etc group name
    @Extern()
    public async CreateGroup(this : AuthManager, $name : string, $methods : string[] = [], $force : boolean = false) : Promise<string> {
        let group : Group = await this.getGroupByName($name);
        let canCreate : boolean = true;
        if (ObjectValidator.IsEmptyOrNull(group)) {
            group = this.getGroupInstance();
        } else if (group.Deleted || $force) {
            group.Deleted = false;
        } else {
            canCreate = false;
        }
        if (canCreate) {
            group.Name = $name;
            group.AuthorizedMethods = $methods;
            await group.Save();

            await SystemLog.Trace({
                message  : "New group created: " + group.Name,
                traceBack: this.getClassNameWithoutNamespace() + ".CreateGroup"
            });
            this.authCache = {};
            return group.Id;
        } else {
            LogIt.Warning("Failed to create group \"{0}\": group already exists", $name);
        }
        return null;
    }

    @Extern()
    public async FindGroups(this : AuthManager, $idOrNameFilter : string | any,
                            $options : IAuthFindGroupsOptions = null) : Promise<IModelListResult<Group>> {
        $options = JsonUtils.Extend({
            limit               : -1,
            offset              : 0,
            withHidden          : false,
            withDeleted         : false,
            withEmptyPermissions: true,
            onlyWithAsterix     : false,
            onlySystem          : false
        }, $options);
        const groupModel : any = this.getClassFromInstance(this.getGroupInstance());
        let groups : Group[] = [];
        let group : Group = null;
        if (ObjectValidator.IsString($idOrNameFilter)) {
            group = await groupModel.FindById($idOrNameFilter, {
                partial: true
            });
        }
        let size : number;
        let isFilterQuery : boolean = false;
        if (!ObjectValidator.IsEmptyOrNull(group)) {
            groups.push(group);
            size = 1;
        } else {
            isFilterQuery = true;
            const filter : any = ObjectValidator.IsString($idOrNameFilter) ? {name: new RegExp($idOrNameFilter)} : $idOrNameFilter;
            if (!$options.withDeleted) {
                filter.deleted = {$ne: true};
            }
            if (!$options.withHidden) {
                filter.hidden = {$ne: true};
            }
            if (!ObjectValidator.IsSet(filter.authorizedMethods)) {
                if (!$options.withEmptyPermissions) {
                    filter.authorizedMethods = {$ne: []};
                }
                if ($options.onlyWithAsterix) {
                    filter.authorizedMethods = new RegExp("\\*");
                }
            }
            if ($options.onlySystem) {
                const ids : string[] = [];
                for (const name in GroupsManager.systemGroups) {
                    if (GroupsManager.systemGroups.hasOwnProperty(name)) {
                        ids.push(GroupsManager.systemGroups[name].Id);
                    }
                }
                filter._id = ids;
            }
            size = await groupModel.Model().countDocuments(filter);
            groups = await groupModel.Find(filter, {
                limit  : $options.limit,
                offset : $options.offset,
                partial: true
            });
        }

        const output : IModelListResult = {
            data  : [],
            limit : $options.limit,
            offset: $options.offset,
            size
        };
        if (!ObjectValidator.IsEmptyOrNull(groups)) {
            for await (const group of groups) {
                if (($options.withDeleted || !group.Deleted) && ($options.withHidden || !group.Hidden) &&
                    ($options.withEmptyPermissions || !ObjectValidator.IsEmptyOrNull(group.AuthorizedMethods))) {
                    output.data.push(await group.ToJSON());
                }
            }
            if (!isFilterQuery) {
                output.size = output.data.length;
            }
        }
        return output;
    }

    @Extern()
    public async DeleteGroup(this : AuthManager, $idOrName : string, $force : boolean = false) : Promise<boolean> {
        const groupModel : any = this.getClassFromInstance(this.getGroupInstance());
        let groups : Group[] = [];
        const group : Group = await groupModel.FindById($idOrName);
        if (!ObjectValidator.IsEmptyOrNull(group)) {
            groups.push(group);
        } else {
            groups = await groupModel.Find({name: $idOrName});
        }
        if (!ObjectValidator.IsEmptyOrNull(groups)) {
            let deleted : boolean = false;
            for await (const group of groups) {
                if (!group.Hidden || $force) {
                    await group.Delete();
                    deleted = true;
                    await SystemLog.Trace({
                        level    : LogLevel.WARNING,
                        message  : "Group deleted: " + group.Name,
                        traceBack: this.getClassNameWithoutNamespace() + ".DeleteGroup"
                    });
                }
            }
            if (deleted) {
                this.authCache = {};
            }
            return true;
        }
        return false;
    }

    @Extern()
    public async AddGroup(this : AuthManager, $groupName : string | Group | FEGroup, $userId : string | User = null) : Promise<boolean> {
        let user : User;
        if (ObjectValidator.IsEmptyOrNull($userId)) {
            $userId = this.getCurrentToken();
        }
        if (ObjectValidator.IsString($userId)) {
            user = await this.getUser(<string>$userId);
        } else {
            user = <User>$userId;
        }

        if (!ObjectValidator.IsEmptyOrNull(user)) {
            let group : Group;
            if (ObjectValidator.IsString($groupName)) {
                group = await this.getGroupByName(<string>$groupName);
            } else {
                group = <Group>$groupName;
            }
            if (!ObjectValidator.IsEmptyOrNull(group)) {
                let exist : boolean = false;
                user.Groups.forEach(($userGroup : Group) : void => {
                    if ($userGroup.Id === group.Id) {
                        exist = true;
                    }
                });
                if (!exist) {
                    user.Groups.push(group);
                    await user.Save();
                    this.authCache = {};
                    await SystemLog.Trace({
                        message  : "New group " + group.Name + " added to: " + user.UserName,
                        token    : this.getCurrentToken(),
                        traceBack: this.getClassNameWithoutNamespace() + ".AddGroup"
                    });
                }
                return true;
            }
        }
        return false;
    }

    @Extern()
    public async getGroups(this : AuthManager, $userId : string = null) : Promise<string[]> {
        const roles : string[] = [];
        if (ObjectValidator.IsEmptyOrNull($userId)) {
            $userId = this.getCurrentToken();
        }
        const user : User = await this.getUser($userId);
        if (!ObjectValidator.IsEmptyOrNull(user)) {
            user.Groups.forEach(($group : Group) : void => {
                if (!$group.Deleted && !$group.Hidden) {
                    roles.push($group.Name);
                }
            });
        }
        return roles;
    }

    @Extern()
    public async getActiveGroupsList(this : AuthManager) : Promise<string[]> {
        const output : string[] = [];
        const result : IExecutionResult = await Loader.getInstance().getGQLContext().Execute(
            `{groupAll(limit:-1,filter:{AND:[{OR:[{deleted:false},{deleted:null}]},{OR:[{hidden:false},{hidden:null}]}]}){name}}`
        );
        result.data.groupAll.forEach(($group : any) : void => {
            output.push($group.name);
        });
        return output;
    }

    @Extern()
    public async RemoveGroup(this : AuthManager, $groupName : string, $userId : string = null) : Promise<boolean> {
        let currentUser : string = $userId;
        if (ObjectValidator.IsEmptyOrNull(currentUser)) {
            currentUser = this.getCurrentToken();
        }
        const user : User = await this.getUser(currentUser);
        if (!ObjectValidator.IsEmptyOrNull(user)) {
            let groupFound : boolean = false;
            const newGroups : Group[] = [];
            user.Groups.forEach(($group : Group) : void => {
                if ($group.Name !== $groupName) {
                    newGroups.push($group);
                } else {
                    groupFound = true;
                }
            });
            if (groupFound) {
                user.Groups = newGroups;
                await user.Save();
                await SystemLog.Trace({
                    level    : LogLevel.WARNING,
                    message  : "Removed group: " + $groupName + " from: " + user.UserName,
                    traceBack: this.getClassNameWithoutNamespace() + ".RemoveGroup"
                });
            } else if (ObjectValidator.IsEmptyOrNull($userId)) {
                const group : Group = await this.getClassFromInstance(this.getGroupInstance()).FindById($groupName);
                if (!ObjectValidator.IsEmptyOrNull(group)) {
                    await group.Remove();
                    LogIt.Warning("Removed group: " + $groupName);
                    await SystemLog.Trace({
                        level    : LogLevel.WARNING,
                        message  : "Removed group: " + $groupName,
                        traceBack: this.getClassNameWithoutNamespace() + ".RemoveGroup"
                    });
                }
            }
            this.authCache = {};
            return true;
        }
        return false;
    }

    @Extern()
    public async RestoreGroup(this : AuthManager, $id : string) : Promise<boolean> {
        const group : Group = await this.getClassFromInstance(this.getGroupInstance()).FindById($id);
        if (!ObjectValidator.IsEmptyOrNull(group)) {
            try {
                group.Deleted = false;
                await group.Save();
                await SystemLog.Trace({
                    message  : "Group restored: " + group.Name,
                    traceBack: this.getClassNameWithoutNamespace() + ".RestoreGroup"
                });
            } catch (ex) {
                LogIt.Warning(ex.message);
                return false;
            }
        } else {
            LogIt.Error("Failed to find group with ID: " + $id);
            return false;
        }
        return true;
    }

    @Extern()
    public async getGroupMethods(this : AuthManager, $group : string) : Promise<string[]> {
        let authMethods : string[] = [];
        const group : Group = await this.getGroupByName($group);
        if (!ObjectValidator.IsEmptyOrNull(group) && !group.Deleted && !group.Hidden) {
            authMethods = group.AuthorizedMethods;

        }
        return authMethods;
    }

    @Extern()
    public async ResetDefaultGroups(this : AuthManager) : Promise<void> {
        await this.createPermissionBackup();
        const report : IPermissionsReport = await this.getPermissionsReport();
        for await (const id of report.groupsCleanUpCandidates) {
            await this.setAuthorizedMethods(id, [], false, true);
        }
    }

    @Extern()
    public async DeleteRedundantGroups(this : AuthManager) : Promise<void> {
        const report : IPermissionsReport = await this.getPermissionsReport();
        for await (const id of report.redundantGroups) {
            await this.DeleteGroup(id, true);
        }
    }

    public async getSystemGroups(this : AuthManager) : Promise<ISystemGroups> {
        if (ObjectValidator.IsEmptyOrNull(GroupsManager.systemGroups)) {
            await this.SyncSystemGroups();
        }
        return GroupsManager.systemGroups;
    }

    public async SyncSystemGroups(this : AuthManager) : Promise<void> {
        return this.processSystemGroups();
    }

    @Extern()
    public async HasGroup(this : AuthManager, $group : Group | FEGroup | string, $user : User | string) : Promise<boolean> {
        if (ObjectValidator.IsEmptyOrNull($group) || ObjectValidator.IsEmptyOrNull($user)) {
            return false;
        }
        if (!ObjectValidator.IsString($group)) {
            $group = (<Group>$group).Id;
        }
        if (ObjectValidator.IsString($user)) {
            const userId : string = this.tokens.getPayloadId(<string>$user);
            if (!ObjectValidator.IsEmptyOrNull(userId)) {
                $user = userId;
            }
        } else {
            $user = (<User>$user).Id;
        }
        if (ObjectValidator.IsEmptyOrNull($group) || ObjectValidator.IsEmptyOrNull($user)) {
            return false;
        }
        let exist : boolean = false;
        try {
            exist = !ObjectValidator.IsEmptyOrNull(await this.getUserDocument().find({_id: $user, groups: $group}).exec());
        } catch (ex) {
            LogIt.Warning(ex.message);
        }
        return exist;
    }

    protected getGroupInstance(this : AuthManager) : Group {
        return new Group();
    }

    protected async getGroupByName(this : AuthManager, $name : string) : Promise<Group> {
        const groups : Group[] = await this.getClassFromInstance(this.getGroupInstance()).Find({name: $name});
        if (!ObjectValidator.IsEmptyOrNull(groups) && groups.length === 1) {
            return groups[0];
        }
        return null;
    }

    protected async processSystemGroups(this : AuthManager, $appendPerms : boolean = true) : Promise<void> {
        GroupsManager.systemGroups = <any>{};
        const names : string[] = [];
        for (const name in this.systemPermissions) {
            if (this.systemPermissions.hasOwnProperty(name)) {
                names.push(name);
            }
        }
        for await (const name of names) {
            const groups : Group[] = await this.getClassFromInstance(this.getGroupInstance()).Find({name});
            if (ObjectValidator.IsEmptyOrNull(groups)) {
                GroupsManager.systemGroups[name] = this.getGroupInstance();
            } else if (groups.length === 1) {
                GroupsManager.systemGroups[name] = groups[0];
            } else {
                throw new Error("Failed to sync default group \"" + name + "\" " +
                    "because multiple groups with same name has been found.");
            }
            GroupsManager.systemGroups[name].Readonly = true;
            GroupsManager.systemGroups[name].Name = name;
            await GroupsManager.systemGroups[name].Save();
            await this.setAuthorizedMethods(GroupsManager.systemGroups[name], this.systemPermissions[name], $appendPerms);
        }
        LogIt.Debug("Successfully synchronized system groups");
    }

    @Extern()
    private async forceSyncSystemGroups(this : AuthManager) : Promise<void> {
        LogIt.Info("Performed manual synchronization of system groups to DB.");
        await this.processSystemGroups(false);
        await this.ResetCache();
    }

    @Extern()
    private async getSystemGroupsInterface(this : AuthManager) : Promise<ISystemGroups> {
        if (ObjectValidator.IsEmptyOrNull(GroupsManager.systemGroups)) {
            await this.SyncSystemGroups();
        }
        const output : any = {};
        const names : string[] = [];
        for (const name in GroupsManager.systemGroups) {
            if (GroupsManager.systemGroups.hasOwnProperty(name)) {
                names.push(name);
            }
        }
        for await (const name of names) {
            output[name] = await GroupsManager.systemGroups[name].ToJSON();
        }
        return output;
    }
}

/*! ******************************************************************************************************** *
 *
 * Copyright 2024-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { LogLevel } from "@io-oidis-commons/Io/Oidis/Commons/Enums/LogLevel.js";
import { BaseObject } from "@io-oidis-commons/Io/Oidis/Commons/Primitives/BaseObject.js";
import { Partial } from "@io-oidis-commons/Io/Oidis/Commons/Primitives/Decorators.js";
import { JsonUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/JsonUtils.js";
import { LogIt } from "@io-oidis-commons/Io/Oidis/Commons/Utils/LogIt.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { StringUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/StringUtils.js";
import { IPermissionsReport } from "@io-oidis-hub-gui/Io/Oidis/Hub/Gui/Connectors/AuthManagerConnector.js";
import { ErrorLocalizable } from "@io-oidis-localhost/Io/Oidis/Localhost/Exceptions/Type/ErrorLocalizable.js";
import { Extern } from "@io-oidis-localhost/Io/Oidis/Localhost/Primitives/Decorators.js";
import {
    IAuthFindUsersOptions,
    IModelListResult,
    IUserProfileOptions,
    IUserRegisterOptions
} from "@io-oidis-services/Io/Oidis/Services/Connectors/AuthManagerConnector.js";
import { FileEntry } from "../DAO/Models/FileEntry.js";
import { User } from "../DAO/Models/User.js";
import { IModelFindOptions } from "../Interfaces/DAO/IModel.js";
import { Loader } from "../Loader.js";
import { SystemLog } from "../LogProcessor/SystemLog.js";
import { AuthManager, AuthManagerNotification } from "../Utils/AuthManager.js";

@Partial()
export abstract class UsersManager extends BaseObject {

    public async getUser<TUser>(this : AuthManager, $tokenOrIdOrName : string, $options? : IModelFindOptions) : Promise<TUser> {
        let user : TUser = null;
        const userId : string = this.tokens.getPayloadId($tokenOrIdOrName);
        if (!ObjectValidator.IsEmptyOrNull(userId)) {
            user = await this.getClassFromInstance(this.getUserInstance()).FindById(userId, $options);
        } else if (!ObjectValidator.IsEmptyOrNull($tokenOrIdOrName)) {
            user = await this.getClassFromInstance(this.getUserInstance()).FindById($tokenOrIdOrName, $options);
            if (ObjectValidator.IsEmptyOrNull(user)) {
                user = <any>await this.getUserByName($tokenOrIdOrName, $options);
            }
        }
        return user;
    }

    @Extern()
    public async getUserId(this : AuthManager, $tokenOrName : string) : Promise<string> {
        const user : User = await this.getUser($tokenOrName, {partial: true});
        if (!ObjectValidator.IsEmptyOrNull(user)) {
            return user.Id;
        }
        return "";
    }

    @Extern()
    public async Register(this : AuthManager, $options : IUserRegisterOptions) : Promise<boolean> {
        if (ObjectValidator.IsEmptyOrNull($options.email) && ObjectValidator.IsEmptyOrNull($options.name)) {
            const message : string = "Failed to register user: email or name must be defined";
            if ($options.throwExceptions) {
                throw new ErrorLocalizable(message, this.getLocalizationsFor(AuthManagerNotification.MissingName));
            }
            LogIt.Error(message);
            return false;
        }
        if (ObjectValidator.IsEmptyOrNull($options.email) && ($options.withActivation || $options.withInvitation)) {
            const message : string = "Failed to register user: missing email or disable activation and invitation options";
            if ($options.throwExceptions) {
                throw new ErrorLocalizable(message, this.getLocalizationsFor(AuthManagerNotification.MissingEmail));
            }
            LogIt.Error(message);
            return false;
        }
        let userExist : boolean = false;
        if (!ObjectValidator.IsEmptyOrNull($options.name)) {
            userExist = !ObjectValidator.IsEmptyOrNull(await this.getUserByName($options.name));
        }
        if (!ObjectValidator.IsEmptyOrNull($options.email)) {
            userExist = !ObjectValidator.IsEmptyOrNull(await this.getUserByEmail($options.email));
        }
        if (!userExist) {
            const newUser : User = this.getUserInstance();

            // TODO(mkelnar) it could be more efficient when default group will be stored in .Groups and referenced in .DefaultGroup
            //  because .Groups.concat(.DefaultGroup) for every lookup will not be needed. This default group should be then marked as
            //  private or something else to disable possible deletion/transfer/sharing...
            newUser.DefaultGroup = this.getGroupInstance();
            newUser.DefaultGroup.Name = "Default";
            newUser.DefaultGroup.Hidden = true;
            await newUser.DefaultGroup.Save();

            newUser.Password = StringUtils.getSha1($options.password);
            $options = JsonUtils.Extend({
                withActivation: true,
                withInvitation: false
            }, $options);
            await this.setUserProfile($options, newUser);

            LogIt.Debug("Create new user: " + $options.name);

            if ($options.withActivation || $options.withInvitation) {
                const token : string = this.getRandomSha();
                if ($options.withActivation) {
                    newUser.SSOTokens.Activation = token;
                } else {
                    newUser.SSOTokens.Invite = token;
                }
                await newUser.Save();
                if ($options.withActivation && await this.emailSender.Register($options.email, token) ||
                    /// TODO: group and owner must be defined somehow ...
                    $options.withInvitation && await this.emailSender.Invitation($options.email, "", "", token)) {
                    await SystemLog.Trace({
                        message  : "New user registered with activation: " + newUser.UserName,
                        traceBack: this.getClassNameWithoutNamespace() + ".Register"
                    });
                    return true;
                } else {
                    const message : string = "Failed to send activation email to: " + $options.email;
                    if ($options.throwExceptions) {
                        throw new ErrorLocalizable(message,
                            this.getLocalizationsFor(AuthManagerNotification.EmailSendFailure, $options.email));
                    }
                    LogIt.Error(message);
                }
            } else {
                await newUser.Save();
                await SystemLog.Trace({
                    message  : "New user registered: " + newUser.UserName,
                    traceBack: this.getClassNameWithoutNamespace() + ".Register"
                });
                return true;
            }
        } else {
            if ($options.throwExceptions) {
                throw new ErrorLocalizable("Failed to register user: user email or name already exists.",
                    this.getLocalizationsFor(AuthManagerNotification.UserExist));
            }
            LogIt.Warning("User already exists.");
        }
        return false;
    }

    @Extern()
    public async ActivateUser(this : AuthManager, $token : string) : Promise<boolean> {
        const user : User = await this.getUserBy({"ssoTokens.activation": $token});
        if (!ObjectValidator.IsEmptyOrNull(user)) {
            user.SSOTokens.Activation = "";
            user.SSOTokens.Invite = "";
            let restored : boolean = false;
            if (user.Deleted) {
                user.Deleted = false;
                user.DefaultGroup.Deleted = false;
                await user.DefaultGroup.Save();
                for await (const token of user.AuthTokens) {
                    token.Deleted = false;
                    if (this.authCache.hasOwnProperty(token.Token)) {
                        delete this.authCache[token.Token];
                    }
                    await token.Save();
                }
                restored = true;
            }
            await user.Save();
            if (restored) {
                await this.emailSender.Restored(user.Email);
            }
            LogIt.Debug("User has been activated");
            await SystemLog.Trace({
                message  : "User activated: " + user.UserName,
                traceBack: this.getClassNameWithoutNamespace() + ".ActivateUser"
            });
            return true;
        } else {
            LogIt.Error("Failed to find activation token: " + $token);
            return false;
        }
    }

    @Extern()
    public async DeactivateUser(this : AuthManager, $id : string, $value : boolean = true) : Promise<boolean> {
        const user : User = await this.getUser($id);
        if (!ObjectValidator.IsEmptyOrNull(user)) {
            user.Activated = !$value;
            await user.Save();
            LogIt.Debug("User activation has been changed to: {0}", !$value);
            await SystemLog.Trace({
                level    : LogLevel.WARNING,
                message  : "User deactivated: " + user.UserName,
                traceBack: this.getClassNameWithoutNamespace() + ".DeactivateUser"
            });
            return true;
        } else {
            LogIt.Error("Failed to find user with ID: " + $id);
            return false;
        }
    }

    @Extern()
    public async ResetPassword(this : AuthManager, $email : string) : Promise<boolean> {
        const user : User = await this.getUserByEmail($email);
        if (!ObjectValidator.IsEmptyOrNull(user)) {
            const token : string = this.getRandomSha();
            user.SSOTokens.Reset = token;
            user.AuditLog.LastResetAction = new Date();
            if (await this.emailSender.ResetPassword($email, token)) {
                await user.Save();
                return true;
            } else {
                LogIt.Error("Failed to send confirm email to: " + $email);
                return false;
            }
        } else {
            LogIt.Warning("None or multiple users has been found.");
            return false;
        }
    }

    @Extern()
    public async ResetPasswordConfirm(this : AuthManager, $token : string, $newPassword : string) : Promise<boolean> {
        const user : User = await this.getUserBy({"ssoTokens.reset": $token});
        if (!ObjectValidator.IsEmptyOrNull(user)) {
            user.Password = StringUtils.getSha1($newPassword);
            await user.Save();
            LogIt.Debug("User password has been updated");
            if (!await this.emailSender.ResetPasswordConfirm(user.Email)) {
                LogIt.Error("Failed to send confirm email to: " + user.Email);
            }
            await SystemLog.Trace({
                message  : "User password change confirmed: " + user.UserName,
                traceBack: this.getClassNameWithoutNamespace() + ".ResetPasswordConfirm"
            });
            return true;
        } else {
            LogIt.Error("Failed to find reset token: " + $token);
            return false;
        }
    }

    @Extern()
    public async ChangePassword(this : AuthManager, $password : string, $userId : string = null) : Promise<boolean> {
        if (ObjectValidator.IsEmptyOrNull($userId)) {
            $userId = this.getCurrentToken();
        }
        const user : User = await this.getUser($userId);
        if (!ObjectValidator.IsEmptyOrNull(user)) {
            user.Password = StringUtils.getSha1($password);
            await user.Save();
            await SystemLog.Trace({
                message  : "User password changed: " + user.UserName,
                traceBack: this.getClassNameWithoutNamespace() + ".ChangePassword"
            });
            return true;
        }
        return false;
    }

    @Extern()
    public async RemoveUser(this : AuthManager, $userIds : string | string[]) : Promise<boolean> {
        let userIds : string[];
        if (ObjectValidator.IsNativeArray($userIds)) {
            userIds = <string[]>$userIds;
        } else {
            userIds = [<string>$userIds];
        }
        try {
            for await (const userId of userIds) {
                const user : User = await this.getUser(userId);
                if (!ObjectValidator.IsEmptyOrNull(user)) {
                    await user.DefaultGroup.Remove();
                    await user.SSOTokens.Remove();
                    for await (const token of user.AuthTokens) {
                        if (this.authCache.hasOwnProperty(token.Token)) {
                            delete this.authCache[token.Token];
                        }
                        await token.Remove();
                    }
                    // TODO(mkelnar) add safe delete/transfer for unused groups before user remove?
                    //  > can be moved to background task (authManager thread)
                    // user.Groups
                    await user.Remove();
                    await SystemLog.Trace({
                        level    : LogLevel.WARNING,
                        message  : "User removed: " + user.UserName,
                        traceBack: this.getClassNameWithoutNamespace() + ".RemoveUser"
                    });
                }
            }
        } catch (ex) {
            LogIt.Warning(ex.message);
            return false;
        }
        return true;
    }

    @Extern()
    public async DeleteUser(this : AuthManager, $id : string) : Promise<boolean> {
        const user : User = await this.getClassFromInstance(this.getUserInstance()).FindById($id);
        if (!ObjectValidator.IsEmptyOrNull(user)) {
            try {
                user.Activated = false;
                await user.Delete();
                await SystemLog.Trace({
                    level    : LogLevel.WARNING,
                    message  : "User deleted: " + user.UserName,
                    traceBack: this.getClassNameWithoutNamespace() + ".DeleteUser"
                });
            } catch (ex) {
                LogIt.Warning(ex.message);
                return false;
            }
        } else {
            LogIt.Error("Failed to find user with ID: " + $id);
            return false;
        }
        return true;
    }

    @Extern()
    public async RestoreUser(this : AuthManager, $id : string) : Promise<boolean> {
        const user : User = await this.getClassFromInstance(this.getUserInstance()).FindById($id);
        if (!ObjectValidator.IsEmptyOrNull(user)) {
            try {
                user.Deleted = false;
                await user.Save();
                await SystemLog.Trace({
                    message  : "User restored: " + user.UserName,
                    traceBack: this.getClassNameWithoutNamespace() + ".RestoreUser"
                });
            } catch (ex) {
                LogIt.Warning(ex.message);
                return false;
            }
        } else {
            LogIt.Error("Failed to find user with ID: " + $id);
            return false;
        }
        return true;
    }

    @Extern()
    public async FindUsers(this : AuthManager, $idOrUserNameFilter : string | any,
                           $options : IAuthFindUsersOptions = null) : Promise<IModelListResult<User>> {
        $options = JsonUtils.Extend({
            limit                 : -1,
            offset                : 0,
            onlyActive            : true,
            withDeleted           : false,
            onlyWithOwnPermissions: false
        }, $options);
        const model : any = this.getClassFromInstance(this.getUserInstance());
        if (ObjectValidator.IsString($idOrUserNameFilter)) {
            const user : User = await model.FindById($idOrUserNameFilter);
            if (!ObjectValidator.IsEmptyOrNull(user)) {
                return {
                    data  : [await user.ToJSON()],
                    limit : $options.limit,
                    offset: $options.offset,
                    size  : 1
                };
            }
        }
        let filter : any = $idOrUserNameFilter;
        if (ObjectValidator.IsString($idOrUserNameFilter)) {
            filter = {userName: new RegExp($idOrUserNameFilter)};
        }

        if ($options.withDeleted) {
            if ($options.onlyActive) {
                filter.$or = [{deleted: true}, {activated: {$ne: false}}];
            }
        } else {
            filter.deleted = {$ne: true};
            if ($options.onlyActive) {
                filter.activated = {$ne: false};
            }
        }
        if ($options.onlyWithOwnPermissions) {
            const report : IPermissionsReport = await this.getPermissionsReport();
            const ids : string[] = [];
            report.usersForCheck.forEach(($record : any) : void => {
                ids.push($record.id);
            });
            filter._id = ids;
        }

        const size : number = await model.Model().countDocuments(filter);
        const users : User[] = await model.Find(filter, {
            limit : $options.limit,
            offset: $options.offset
        });
        const output : IModelListResult = {
            data  : [],
            limit : $options.limit,
            offset: $options.offset,
            size
        };
        for await (const user of users) {
            output.data.push(await user.ToJSON());
        }
        return output;
    }

    @Extern()
    public async getUserProfile(this : AuthManager, $withData : boolean = false, $userIdOrToken : string = null) : Promise<any> {
        if (ObjectValidator.IsEmptyOrNull($userIdOrToken)) {
            $userIdOrToken = this.getCurrentToken();
        }
        const user : User = await this.getUser($userIdOrToken, {partial: !$withData});
        if (!ObjectValidator.IsEmptyOrNull(user)) {
            if ($withData) {
                await user.Refresh({expanded: true});
            }
            const userProfile : any = await user.ToJSON();
            let tryGravatar : boolean = false;
            if ($withData && !ObjectValidator.IsEmptyOrNull(user.Image)) {
                const data : Buffer = await user.Image.Data();
                if (!ObjectValidator.IsEmptyOrNull(data)) {
                    userProfile.Image = "data:image/png;base64, " + data.toString("base64");
                } else {
                    tryGravatar = true;
                    delete userProfile.Image;
                }
            } else {
                tryGravatar = true;
                delete userProfile.Image;
            }
            if (tryGravatar) {
                try {
                    userProfile.Image = "data:image/png;base64, " + (await this.getGravatar(userProfile.Email)).toString("base64");
                } catch {
                    delete userProfile.Image;
                }
            }
            if (ObjectValidator.IsEmptyOrNull(userProfile.Activated)) {
                userProfile.Activated = true;
            }
            return userProfile;
        }
        return null;
    }

    @Extern()
    public async setUserProfile(this : AuthManager, $options : IUserProfileOptions, $userId : string | User) : Promise<boolean> {
        let user : User = <User>$userId;
        if (ObjectValidator.IsEmptyOrNull($userId)) {
            user = await this.getUser(this.getCurrentToken());
        } else if (ObjectValidator.IsString($userId)) {
            user = await this.getUser(<string>$userId);
        }

        if (!ObjectValidator.IsEmptyOrNull(user)) {
            await this.processUserData(user, $options);
            await user.Save();
            return true;
        }
        return false;
    }

    protected getUserInstance() : User {
        return new User();
    }

    protected async getUserBy(this : AuthManager, $filter : any, $options? : IModelFindOptions) : Promise<User> {
        const users : User[] = await this.getClassFromInstance(this.getUserInstance()).Find($filter, $options);
        if (!ObjectValidator.IsEmptyOrNull(users) && users.length >= 1) {
            if (users.length > 1) {
                LogIt.Warning("Multiple users found for specified criteria {0}", JSON.stringify($filter));
            }
            return users[0];
        }
        return null;
    }

    protected async getUserByName(this : AuthManager, $userName : string, $options? : IModelFindOptions) : Promise<User> {
        return this.getUserBy({userName: $userName}, $options);
    }

    protected async getUserByEmail(this : AuthManager, $email : string, $options? : IModelFindOptions) : Promise<User> {
        const users : User[] = await this.getUsersByEmail($email, $options);
        if (!ObjectValidator.IsEmptyOrNull(users) && users.length >= 1) {
            if (users.length > 1) {
                LogIt.Error("Multiple users found for specified criteria {0}", $email);
            }
            return this.getClassFromInstance(this.getUserInstance()).FindById(users[0].Id);
        }
        return null;
    }

    protected async getUsersByEmail(this : AuthManager, $email : string, $options? : IModelFindOptions) : Promise<User[]> {
        let users : User[];
        if (!this.config.strictEmailCheck) {
            /* eslint-disable no-useless-escape */
            $email = $email.replace(/[!#$%&‘*+–=^_{}~,.\-]*/g, "");
            // TODO(mkelnar) current solution will match google.com and goo--gle.com as the same domain!
            //  when strict match for domain will be needed then use: /(.*)@((?:[a-z0-9]+(?:[\-.]+[a-z0-9]*)*\.[a-z]{2,6}))$/g
            //  to parse mail with $addFields: {parsedEmail: {$regexFindAll: {input: "$email", regex: <mentioned-regex-query>}}} and apply
            //  sanitizer only for $parsedEmail.captures[0] (captures[1] will be domain), not sure how to handle mangled entries like
            //  email: "@", or email: "addressDomain.cz" with missing @ etc...
            users = await this.getClassFromInstance(this.getUserInstance()).Aggregate([
                {$addFields: {sanitized: {$regexFindAll: {input: "$email", regex: /[^!#$%&‘*+–=^_{}~,.\-]/g}}}},
                {$addFields: {sanitized: {$reduce: {input: "$sanitized.match", initialValue: "", in: {$concat: ["$$value", "$$this"]}}}}},
                {$match: {sanitized: {$regex: $email, $options: "i"}}}
            ], JsonUtils.Extend($options, <IModelFindOptions>{partial: true}));
            /* eslint-enable */
        } else {
            users = await this.getClassFromInstance(this.getUserInstance()).Find({email: $email}, $options);
        }
        return !ObjectValidator.IsEmptyOrNull(users) ? users : [];
    }

    protected getUserDocument(this : AuthManager) : any {
        return Loader.getInstance().getDBDriver().getContextFor(this.getUserInstance()).getDocument();
    }

    protected async processUserData(this : AuthManager, $user : User, $options : IUserProfileOptions) : Promise<void> {
        if (ObjectValidator.IsSet($options.name) && $options.name !== $user.UserName) {
            if (ObjectValidator.IsEmptyOrNull($options.name)) {
                if (ObjectValidator.IsSet($options.email)) {
                    $options.name = $options.email;
                } else {
                    $options.name = $user.Email;
                }
            }
            $options.name = StringUtils.Remove($options.name, ".");
            $options.name = StringUtils.Split($options.name, "@")[0];
            if (!ObjectValidator.IsEmptyOrNull($options.name)) {
                let userName : string = $options.name;
                let initIndex : number = 0;
                while (await this.getUserByName($options.name + (initIndex === 0 ? "" : initIndex))) {
                    initIndex++;
                    userName = $options.name + initIndex;
                }
                if (initIndex > 0) {
                    $options.name = userName;
                }
            }
            $user.UserName = $options.name;
        }
        if (ObjectValidator.IsSet($options.firstname)) {
            $user.Firstname = $options.firstname;
        }
        if (ObjectValidator.IsSet($options.lastname)) {
            $user.Lastname = $options.lastname;
        }
        if (ObjectValidator.IsSet($options.email) && ObjectValidator.IsEmptyOrNull(await this.getUserByEmail($options.email))) {
            $user.Email = $options.email;
        }
        if (ObjectValidator.IsSet($options.phone)) {
            $user.Phone = $options.phone;
        }
        if (ObjectValidator.IsSet($options.altEmails)) {
            $user.AltEmails = $options.altEmails;
        }
        if (ObjectValidator.IsSet($options.image)) {
            if (ObjectValidator.IsEmptyOrNull($user.Image)) {
                $user.Image = new FileEntry();
                $user.Image.Owner = this.getUserInstance().getClassNameWithoutNamespace();
            }
            if (!ObjectValidator.IsEmptyOrNull($options.image)) {
                await $user.Image.Data(Buffer.from($options.image, "base64"));
            } else {
                await $user.Image.Remove();
            }
        }
        if (ObjectValidator.IsSet($options.activated)) {
            $user.Activated = $options.activated;
        }
        if (ObjectValidator.IsSet($options.twoFA)) {
            if (!ObjectValidator.IsEmptyOrNull($options.twoFA)) {
                $user.TwoFASecret = $options.twoFA;
            } else {
                await $user.Remove("TwoFASecret");
            }
        }
        if (ObjectValidator.IsSet($options.whatsNewSeen)) {
            $user.WhatsNewSeen = $options.whatsNewSeen;
        }
    }
}

/*! ******************************************************************************************************** *
 *
 * Copyright 2024-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { BaseObject } from "@io-oidis-commons/Io/Oidis/Commons/Primitives/BaseObject.js";
import { Partial } from "@io-oidis-commons/Io/Oidis/Commons/Primitives/Decorators.js";
import { Echo } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Echo.js";
import { JsonUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/JsonUtils.js";
import { LogIt } from "@io-oidis-commons/Io/Oidis/Commons/Utils/LogIt.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { StringUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/StringUtils.js";
import { IServiceAccounts } from "@io-oidis-hub-gui/Io/Oidis/Hub/Gui/Connectors/AuthManagerConnector.js";
import { Extern } from "@io-oidis-localhost/Io/Oidis/Localhost/Primitives/Decorators.js";
import { AuthToken } from "../DAO/Models/AuthToken.js";
import { Group } from "../DAO/Models/Group.js";
import { User } from "../DAO/Models/User.js";
import { Loader } from "../Loader.js";
import { AuthManager } from "../Utils/AuthManager.js";

@Partial()
export abstract class GroupsAuthenticationAPI extends BaseObject {
    private static serviceAccounts : IServiceAccounts = null;

    @Extern()
    public async AuthenticateAdmin(this : AuthManager, $token : string) : Promise<string> {
        return Loader.getInstance().getAdminHelper().Authenticate($token);
    }

    @Extern()
    public async RegisterAdmin(this : AuthManager, $name : string, $pass? : string) : Promise<void> {
        return Loader.getInstance().getAdminHelper().Register($name, $pass);
    }

    public async SyncServiceAccounts(this : AuthManager) : Promise<void> {
        GroupsAuthenticationAPI.serviceAccounts = <any>{};
        const names : string[] = [];
        for (const name in this.systemAccounts) {
            if (this.systemAccounts.hasOwnProperty(name)) {
                names.push(name);
            }
        }
        for await (const name of names) {
            const config : IServiceAccountConfig = this.systemAccounts[name];
            if (ObjectValidator.IsEmptyOrNull(config.user)) {
                throw new Error("Can't sync service account without name definition.");
            }
            if (StringUtils.Contains(config.user, " ")) {
                config.user = StringUtils.Replace(config.user, " ", "-");
                LogIt.Warning("Service accounts should not contain spaces in names. " +
                    "Converting spaces automatically to dashes: " + config.user);
            }

            if (ObjectValidator.IsEmptyOrNull(config.token.name)) {
                config.token.name = config.user + "-token";
            }

            if (ObjectValidator.IsEmptyOrNull(config.pass)) {
                config.pass = config.user + "-" + this.getUID();
                if (config.token.enabled) {
                    LogIt.Info("Account \"" + config.user + "\" is miss password explicitly defined, " +
                        "so using automatic one and you can access this services account by generated token only.");
                } else {
                    LogIt.Info("Account \"" + config.user + "\" is miss password explicitly defined and token init is disabled, " +
                        "so you can access this services account by SwitchToAccount API only.");
                }
            }

            let serviceUser : User = await this.getUser(config.user);
            if (ObjectValidator.IsEmptyOrNull(serviceUser)) {
                if (!await this.Register({
                    name          : config.user,
                    password      : StringUtils.getSha1(config.pass),
                    withActivation: false,
                    withInvitation: false
                })) {
                    throw new Error("Failed to create service account user");
                }
                serviceUser = await this.getUser(config.user);
                if (!ObjectValidator.IsEmptyOrNull(config.group)) {
                    await this.AddGroup(config.group, serviceUser);
                }
            }

            if (config.token.enabled) {
                let newToken : boolean = false;
                if (serviceUser.AuthTokens.findIndex(($token : AuthToken) : boolean => $token.Name === config.token.name) === -1) {
                    if (!ObjectValidator.IsEmptyOrNull(await this.GenerateAuthToken(config.token.name, serviceUser.Id,
                        config.token.noExpiration))) {
                        newToken = true;
                    } else {
                        LogIt.Warning("Failed to create service account token for: " + config.user);
                    }
                }

                serviceUser = await this.getUser(config.user);
                const authToken : AuthToken = serviceUser.AuthTokens
                    .find(($token : AuthToken) : boolean => $token.Name === config.token.name);
                if (!ObjectValidator.IsEmptyOrNull(authToken)) {
                    if (newToken) {
                        await this.UpdateAuthToken(authToken, {expireTime: 365 * 24 * 60 * 60 * 1000}, serviceUser.Id);
                    }
                    if (await this.IsAuthTokenValid(authToken.Token, serviceUser.Id)) {
                        Echo.Printf(`\n${config.user} token: ` + authToken.Token + "\n");
                    } else {
                        LogIt.Warning(`\n${config.user} token expired or invalid, please update expire date or revoke in dashboard.\n`);
                    }
                } else {
                    LogIt.Info(config.user + " token is missing, create it to enable authentication for predefined services account.");
                }
            }
            GroupsAuthenticationAPI.serviceAccounts[name] = serviceUser;
        }
        LogIt.Debug("Successfully synchronized system accounts");
    }

    public async getServiceAccounts(this : AuthManager) : Promise<IServiceAccounts> {
        if (ObjectValidator.IsEmptyOrNull(GroupsAuthenticationAPI.serviceAccounts)) {
            await this.SyncServiceAccounts();
        }
        return GroupsAuthenticationAPI.serviceAccounts;
    }

    @Extern()
    public async SwitchToAccount(this : AuthManager, $account : User) : Promise<string> {
        const requester : User = await this.getUser(this.getCurrentToken());
        if (ObjectValidator.IsEmptyOrNull(requester)) {
            throw new Error("Failed to identify current user");
        }
        if (await this.HasGroup((await this.getSystemGroups()).Admin, requester)) {
            const accounts : IServiceAccounts = await this.getServiceAccounts();
            let account : User = null;
            for (const name in accounts) {
                if (accounts.hasOwnProperty(name) && accounts[name].Id === $account.Id) {
                    account = accounts[name];
                }
            }
            if (!ObjectValidator.IsEmptyOrNull(account)) {
                await this.LogOut(this.getCurrentToken());
                await this.tokens.Clear($account.Id);
                return this.tokens.Create($account.Id);
            } else {
                LogIt.Error("Account \"" + $account.UserName + "\" was not identified as system account.");
            }
        } else {
            throw new Error("SwitchToAccount can be invoked only by admins");
        }
        return null;
    }

    protected addServiceAccount(this : AuthManager, $name : string, $config : IServiceAccountConfig) : void {
        this.systemAccounts[$name] = JsonUtils.Extend({
            group: null,
            pass : null,
            token: {
                enabled     : false,
                name        : null,
                noExpiration: true
            },
            user : null
        }, $config);
    }

    @Extern()
    private async getServiceAccountsInterface(this : AuthManager) : Promise<IServiceAccounts> {
        if (ObjectValidator.IsEmptyOrNull(GroupsAuthenticationAPI.serviceAccounts)) {
            await this.SyncServiceAccounts();
        }
        const output : any = {};
        const names : string[] = [];
        for (const name in GroupsAuthenticationAPI.serviceAccounts) {
            if (GroupsAuthenticationAPI.serviceAccounts.hasOwnProperty(name)) {
                names.push(name);
            }
        }
        for await (const name of names) {
            /// TODO: this should send only bare minimum required by backdoorLogin,
            //        but if required with deeper info it should be changed to ToJSON() in same way as in getSystemGroupsInterface
            const account : User = GroupsAuthenticationAPI.serviceAccounts[name];
            output[name] = {
                Groups  : account.Groups.map(($group : Group) => {
                    return {Id: $group.Id, Name: $group.Name};
                }),
                Id      : account.Id,
                UserName: account.UserName
            };
        }
        return output;
    }
}

export interface IServiceAccountConfig {
    user : string;
    pass? : string;
    group? : Group | string;
    token? : IServiceAccountTokenConfig;
}

export interface IServiceAccountTokenConfig {
    enabled? : boolean;
    name? : string;
    noExpiration? : boolean;
}

// generated-code-start
export const IServiceAccountConfig = globalThis.RegisterInterface(["user", "pass", "group", "token"]);
export const IServiceAccountTokenConfig = globalThis.RegisterInterface(["enabled", "name", "noExpiration"]);
// generated-code-end

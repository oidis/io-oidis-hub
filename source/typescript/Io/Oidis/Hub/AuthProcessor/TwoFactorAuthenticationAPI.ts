/*! ******************************************************************************************************** *
 *
 * Copyright 2024 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { BaseObject } from "@io-oidis-commons/Io/Oidis/Commons/Primitives/BaseObject.js";
import { Partial } from "@io-oidis-commons/Io/Oidis/Commons/Primitives/Decorators.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { I2FAKey } from "@io-oidis-hub-gui/Io/Oidis/Hub/Gui/Connectors/AuthManagerConnector.js";
import { ErrorLocalizable } from "@io-oidis-localhost/Io/Oidis/Localhost/Exceptions/Type/ErrorLocalizable.js";
import { Extern } from "@io-oidis-localhost/Io/Oidis/Localhost/Primitives/Decorators.js";
import { Totp } from "@nodejs/time2fa/dist/index.mjs";
import { User } from "../DAO/Models/User.js";
import { AuthCodeManager } from "../Utils/AuthCodeManager.js";
import { AuthManager, AuthManagerNotification } from "../Utils/AuthManager.js";

@Partial()
export abstract class TwoFactorAuthenticationAPI extends BaseObject {

    @Extern()
    public async Generate2FAKey(this : AuthManager) : Promise<I2FAKey> {
        const user : User = await this.getUser(this.getCurrentToken());
        if (ObjectValidator.IsEmptyOrNull(user)) {
            throw new ErrorLocalizable("Failed to identify current user.",
                this.getLocalizationsFor(AuthManagerNotification.MissingCurrentUser));
        }
        const key : any = Totp.generateKey({
            issuer: this.config.twoFactorAuthentication.identityName,
            user  : !ObjectValidator.IsEmptyOrNull(user.Email) ? user.Email : user.UserName
        }, {
            algo      : "sha1",
            digits    : 6,
            period    : 30,
            secretSize: 10
        });
        return {
            secret: key.secret,
            url   : key.url
        };
    }

    @Extern()
    public async Validate2FA(this : AuthManager, $code : string, $secret? : string) : Promise<boolean> {
        if (ObjectValidator.IsEmptyOrNull($secret)) {
            const currentUser : User = await this.getUser(this.getCurrentToken());
            if (ObjectValidator.IsEmptyOrNull(currentUser)) {
                throw new ErrorLocalizable("Failed to identify current user.",
                    this.getLocalizationsFor(AuthManagerNotification.MissingCurrentUser));
            }
            if (ObjectValidator.IsEmptyOrNull(currentUser.TwoFASecret)) {
                return false;
            }
            $secret = currentUser.TwoFASecret;
        }
        if (ObjectValidator.IsEmptyOrNull($code) || ObjectValidator.IsEmptyOrNull($secret)) {
            return false;
        }
        return Totp.validate({passcode: $code, secret: $secret});
    }

    @Extern()
    public async Has2FAActivated(this : AuthManager) : Promise<boolean> {
        if (this.config.twoFactorAuthentication.force) {
            return true;
        }
        const currentUser : User = await this.getUser(this.getCurrentToken());
        if (ObjectValidator.IsEmptyOrNull(currentUser)) {
            throw new ErrorLocalizable("Failed to identify current user.",
                this.getLocalizationsFor(AuthManagerNotification.MissingCurrentUser));
        }
        return !ObjectValidator.IsEmptyOrNull(currentUser.TwoFASecret);
    }

    protected getAuthCodeManagerInstance() : AuthCodeManager {
        return new AuthCodeManager();
    }
}

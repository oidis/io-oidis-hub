/*! ******************************************************************************************************** *
 *
 * Copyright 2024-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { BaseObject } from "@io-oidis-commons/Io/Oidis/Commons/Primitives/BaseObject.js";
import { Partial } from "@io-oidis-commons/Io/Oidis/Commons/Primitives/Decorators.js";
import { LogIt } from "@io-oidis-commons/Io/Oidis/Commons/Utils/LogIt.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { StringUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/StringUtils.js";
import { ISystemGroups } from "@io-oidis-hub-gui/Io/Oidis/Hub/Gui/Connectors/AuthManagerConnector.js";
import { Extern, Route } from "@io-oidis-localhost/Io/Oidis/Localhost/Primitives/Decorators.js";
import { ISchemaError, SchemaValidator } from "@io-oidis-localhost/Io/Oidis/Localhost/Utils/SchemaValidator.js";
import { IAuthToken, IAuthTokenOptions } from "@io-oidis-services/Io/Oidis/Services/Connectors/AuthManagerConnector.js";
import { AuthToken } from "../DAO/Models/AuthToken.js";
import { User } from "../DAO/Models/User.js";
import { AuthFeatures, AuthManager } from "../Utils/AuthManager.js";

@Partial()
export abstract class AuthTokensManager extends BaseObject {

    @Extern()
    public async GenerateAuthToken(this : AuthManager, $name : string = "", $userId : string = null,
                                   $noExpire : boolean = false) : Promise<IAuthToken> {
        const authToken : IAuthToken = {
            expire: null,
            id    : "",
            name  : "",
            value : ""
        };
        const requester : User = await this.getUser(this.getCurrentToken());
        if (ObjectValidator.IsEmptyOrNull($userId)) {
            $userId = this.getCurrentToken();
        }
        const user : User = await this.getUser($userId);
        if (!ObjectValidator.IsEmptyOrNull(user) && (
            !this.isExternCall ||
            (requester.Id === user.Id || await this.IsAuthorizedFor(AuthFeatures.TOKEN_MANAGER, requester)))) {
            if (this.isExternCall && requester.Id !== user.Id) {
                const groups : ISystemGroups = await this.getSystemGroups();
                if ((await this.HasGroup(groups.Admin, user) || await this.HasGroup(groups.SysAdmin, user)) &&
                    !await this.HasGroup(groups.Admin, requester)) {
                    LogIt.Warning("Admins and SysAdmins tokens can be managed only by admins");
                    return authToken;
                }
            }
            const date : number = new Date().getTime();
            authToken.value = this.getRandomSha(date);
            const model : AuthToken = new AuthToken();
            model.Token = authToken.value;
            model.Date = date;
            if (!$noExpire) {
                model.Expire = 10 * 60 * 60 * 1000;
                authToken.expire = date + model.Expire;
            }
            await model.Save();
            user.AuthTokens.push(model);
            await user.Save();
            await this.UpdateAuthToken(model, {
                name: $name
            }, user);
            authToken.id = model.Id;
            authToken.name = model.Name;
        }
        return authToken;
    }

    @Extern()
    public async IsAuthTokenValid(this : AuthManager, $authToken : string, $userId : string = null) : Promise<boolean> {
        if (ObjectValidator.IsEmptyOrNull($userId)) {
            $userId = this.getCurrentToken();
        }
        const user : User = await this.getUser($userId);
        return this.checkAuthToken(user, $authToken);
    }

    @Extern()
    public async getAuthToken(this : AuthManager, $name : string, $userId : string = null) : Promise<string> {
        const requester : User = await this.getUser(this.getCurrentToken());
        if (ObjectValidator.IsEmptyOrNull($userId)) {
            $userId = this.getCurrentToken();
        }
        const user : User = await this.getUser($userId);
        let value : string = "";
        if (!ObjectValidator.IsEmptyOrNull(user) && (
            !this.isExternCall ||
            (requester.Id === user.Id || await this.IsAuthorizedFor(AuthFeatures.TOKEN_MANAGER, requester)))) {
            if (this.isExternCall && requester.Id !== user.Id) {
                const groups : ISystemGroups = await this.getSystemGroups();
                if ((await this.HasGroup(groups.Admin, user) || await this.HasGroup(groups.SysAdmin, user)) &&
                    !await this.HasGroup(groups.Admin, requester)) {
                    LogIt.Warning("Admins and SysAdmins tokens can be read only by admins");
                    return "";
                }
            }
            user.AuthTokens.forEach(($data : AuthToken) : void => {
                if ($data.Name === $name && !$data.Deleted) {
                    value = $data.Token;
                }
            });
            return value;
        }
        return value;
    }

    @Route("GET /tokens?tokenNameOrId")
    public async getAuthTokenInfo(this : AuthManager, $tokenNameOrId : string) : Promise<IAuthTokenInfo> {
        const tokens : AuthToken[] = (<User>await this.getUser(this.getCurrentToken())).AuthTokens
            .filter(($token : AuthToken) : boolean =>
                $token.Token === $tokenNameOrId || $token.Name === $tokenNameOrId || $token.Id === $tokenNameOrId);
        if (!ObjectValidator.IsEmptyOrNull(tokens)) {
            if (tokens.length > 1) {
                LogIt.Warning("Found more than one token for requested criteria: using first one");
            }
            return {
                date  : tokens[0].Date,
                expire: ObjectValidator.IsEmptyOrNull(tokens[0].Expire) ? null : tokens[0].Expire,
                id    : tokens[0].Id,
                name  : tokens[0].Name,
                value : tokens[0].Token
            };
        }
        return {
            date  : null,
            expire: null,
            id    : "",
            name  : "",
            value : ""
        };
    }

    @Route("DELETE /tokens?tokenNameOrId")
    @Extern()
    public async RevokeAuthToken(this : AuthManager, $authTokenNameOrId : string, $userId : string = null) : Promise<boolean> {
        const requester : User = await this.getUser(this.getCurrentToken());
        if (ObjectValidator.IsEmptyOrNull($userId)) {
            $userId = this.getCurrentToken();
        }
        const user : User = await this.getUser($userId);
        if (!ObjectValidator.IsEmptyOrNull(user) && (
            !this.isExternCall ||
            (requester.Id === user.Id || await this.IsAuthorizedFor(AuthFeatures.TOKEN_MANAGER, requester)))) {
            if (this.isExternCall && requester.Id !== user.Id) {
                const groups : ISystemGroups = await this.getSystemGroups();
                if ((await this.HasGroup(groups.Admin, user) || await this.HasGroup(groups.SysAdmin, user)) &&
                    !await this.HasGroup(groups.Admin, requester)) {
                    throw new Error("Admins and SysAdmins tokens can be revoked only by admins");
                }
            }
            let token : AuthToken = null;
            user.AuthTokens = user.AuthTokens.filter(($value : AuthToken) : boolean => {
                if ($value.Id === $authTokenNameOrId || $value.Name === $authTokenNameOrId) {
                    token = $value;
                    return false;
                }
                return true;
            });
            if (!ObjectValidator.IsEmptyOrNull(token)) {
                await token.Remove();
                await user.Save();
                if (this.authCache.hasOwnProperty(token.Id)) {
                    delete this.authCache[token.Id];
                }
            }
            return true;
        }
        return false;
    }

    @Route("PUT /tokens?tokenNameOrId")
    @Extern()
    public async UpdateAuthToken(this : AuthManager, $authToken : AuthToken | string, $options : IAuthTokenOptions,
                                 $userId : User | string = null) : Promise<boolean> {
        const requester : User = await this.getUser(this.getCurrentToken());
        let user : User;
        if (ObjectValidator.IsEmptyOrNull($userId)) {
            user = await this.getUser(this.getCurrentToken());
        } else {
            user = <User>$userId;
            if (ObjectValidator.IsString(user)) {
                user = await this.getUser(<string>$userId);
            }
        }
        if (!ObjectValidator.IsEmptyOrNull(user)) {
            if (this.isExternCall && requester.Id !== user.Id) {
                if (await this.IsAuthorizedFor(AuthFeatures.TOKEN_MANAGER, requester)) {
                    const groups : ISystemGroups = await this.getSystemGroups();
                    if ((await this.HasGroup(groups.Admin, user) || await this.HasGroup(groups.SysAdmin, user)) &&
                        !await this.HasGroup(groups.Admin, requester)) {
                        throw new Error("Admins and SysAdmins tokens can be revoked only by admins");
                    }
                } else {
                    throw new Error("Tokens can be updated only by owners or admins");
                }
            }
            let token : AuthToken = <AuthToken>$authToken;
            if (ObjectValidator.IsString($authToken)) {
                token = await AuthToken.FindById(<string>$authToken);
            }
            if (!ObjectValidator.IsEmptyOrNull(token)) {
                let contains : boolean = false;
                user.AuthTokens.forEach(($data : AuthToken) : void => {
                    if ($data.Id === token.Id) {
                        contains = true;
                    }
                });
                if (contains) {
                    const validator : SchemaValidator = new SchemaValidator();
                    validator.Schema({
                        properties: {
                            expireTime: {
                                anyOf: [
                                    {
                                        type   : "integer",
                                        minimum: 0
                                    },
                                    {
                                        type: "null"
                                    }
                                ]
                            },
                            name      : {
                                type: "string"
                            }
                        }
                    });
                    const report : ISchemaError[] = validator.Validate($options);
                    if (!ObjectValidator.IsEmptyOrNull(report)) {
                        throw new Error(validator.ErrorsToString(report));
                    }
                    if (ObjectValidator.IsSet($options.expireTime)) {
                        if (!ObjectValidator.IsEmptyOrNull($options.expireTime)) {
                            token.Expire = $options.expireTime;
                        } else {
                            await token.Remove("Expire");
                        }
                    }
                    if (ObjectValidator.IsSet($options.name)) {
                        if (ObjectValidator.IsEmptyOrNull($options.name)) {
                            $options.name = token.Id;
                        }
                        let index : number = 0;
                        const nameValidator : any = () : boolean => {
                            let exits : boolean = false;
                            user.AuthTokens.forEach(($data : AuthToken) : void => {
                                if ($data.Name === $options.name + (index === 0 ? "" : index)) {
                                    exits = true;
                                }
                            });
                            return exits;
                        };
                        while (nameValidator()) {
                            index++;
                        }
                        token.Name = $options.name + (index === 0 ? "" : index);
                    }
                    await token.Save();
                    if (this.authCache.hasOwnProperty(token.Token)) {
                        delete this.authCache[token.Token];
                    }
                    return true;
                } else {
                    LogIt.Warning("Token is not owned by specified user.");
                }
            } else {
                LogIt.Warning("Token was not found.");
            }
        } else {
            LogIt.Warning("Failed to identify token owner.");
        }
        return false;
    }

    protected checkAuthToken(this : AuthManager, $user : User, $authToken : string) : boolean {
        let valid : boolean = false;
        if (!ObjectValidator.IsEmptyOrNull($user)) {
            $user.AuthTokens.forEach(($data : AuthToken) : void => {
                if (($data.Token === $authToken || StringUtils.getSha1($data.Token) === $authToken) && !$data.Deleted) {
                    const time : number = new Date().getTime();
                    if (ObjectValidator.IsEmptyOrNull($data.Expire) || ((($data.Date + $data.Expire) >= time) && ($data.Date < time))) {
                        valid = true;
                    } else {
                        if (this.authCache.hasOwnProperty($authToken)) {
                            delete this.authCache[$authToken];
                        }
                        LogIt.Debug("Authorization token expired name/id: " + $data.Name + "/" + $data.Id);
                    }
                }
            });
        }
        return valid;
    }
}

export interface IAuthTokenInfo extends IAuthToken {
    date : number;
}

// generated-code-start
export const IAuthTokenInfo = globalThis.RegisterInterface(["date"], <any>IAuthToken);
// generated-code-end

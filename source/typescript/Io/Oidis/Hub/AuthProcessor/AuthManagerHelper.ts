/*! ******************************************************************************************************** *
 *
 * Copyright 2024-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { LogLevel } from "@io-oidis-commons/Io/Oidis/Commons/Enums/LogLevel.js";
import { LogSeverity } from "@io-oidis-commons/Io/Oidis/Commons/Enums/LogSeverity.js";
import { BaseObject } from "@io-oidis-commons/Io/Oidis/Commons/Primitives/BaseObject.js";
import { Partial } from "@io-oidis-commons/Io/Oidis/Commons/Primitives/Decorators.js";
import { JsonUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/JsonUtils.js";
import { LogIt } from "@io-oidis-commons/Io/Oidis/Commons/Utils/LogIt.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { Reflection } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Reflection.js";
import { StringUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/StringUtils.js";
import { IDefaultGroupOwner, IPermissionsReport } from "@io-oidis-hub-gui/Io/Oidis/Hub/Gui/Connectors/AuthManagerConnector.js";
import { Extern } from "@io-oidis-localhost/Io/Oidis/Localhost/Primitives/Decorators.js";
import { AuthHttpResolver } from "@io-oidis-services/Io/Oidis/Services/ErrorPages/AuthHttpResolver.js";
import { ILocalization } from "@io-oidis-services/Io/Oidis/Services/Interfaces/DAO/IBaseConfiguration.js";
import { FileTransferHandler } from "../Connectors/FileTransferHandler.js";
import { AppSettings } from "../DAO/Models/AppSettings.js";
import { AuthorizedMethods } from "../DAO/Models/AuthEntry.js";
import { BaseModel } from "../DAO/Models/BaseModel.js";
import { Group } from "../DAO/Models/Group.js";
import { User } from "../DAO/Models/User.js";
import { Loader } from "../Loader.js";
import { SystemLog } from "../LogProcessor/SystemLog.js";
import { AuthManager, AuthManagerNotification } from "../Utils/AuthManager.js";

@Partial()
export abstract class AuthManagerHelper extends BaseObject {

    public async CheckActivation(this : AuthManager) : Promise<void> {
        const now : number = (new Date()).getTime();
        const users : User[] = await this.getClassFromInstance(this.getUserInstance()).Find({
            $and: [
                {"ssoTokens.activation": {$ne: null}},
                {"ssoTokens.activation": {$ne: ""}},
                {email: {$ne: null}}
            ]
        });
        for await (const user of users) {
            let lastNotice : number = user.Created.getTime();
            if (!ObjectValidator.IsEmptyOrNull(user.AuditLog.LastActivationNotice)) {
                lastNotice = user.AuditLog.LastActivationNotice.getTime();
            }

            let noticeInterval : number = 10; // days
            let noticeCounter : number = 0;
            let daysLeft : number = 29;
            if (!ObjectValidator.IsEmptyOrNull(user.AuditLog.ActivationNoticeCounter)) {
                noticeCounter = user.AuditLog.ActivationNoticeCounter;
                switch (user.AuditLog.ActivationNoticeCounter) {
                case 1:
                    noticeInterval = 10;
                    daysLeft = 10;
                    break;
                case 2:
                    noticeInterval = 9;
                    daysLeft = 9;
                    break;
                case 3:
                    noticeInterval = 2;
                    daysLeft = 1;
                    break;
                case 4:
                    noticeInterval = 1;
                    break;
                case 5:
                    noticeInterval = 90;
                    break;
                default:
                    noticeInterval = -1;
                    break;
                }
            }

            noticeInterval *= 24 * 60 * 60 * 1000;

            if (noticeCounter <= 5 && (now - lastNotice) > noticeInterval) {
                user.AuditLog.LastActivationNotice = new Date();
                user.AuditLog.ActivationNoticeCounter = noticeCounter + 1;
                try {
                    await user.Save();

                    if (noticeCounter < 4) {
                        await this.emailSender.Activation(user.Email, user.SSOTokens.Activation, daysLeft);
                        LogIt.Debug("{0} user noticed about activation, days left: {1}", user.UserName, daysLeft);
                    } else if (noticeCounter === 4) {
                        try {
                            await user.Delete();
                            await user.DefaultGroup.Delete();
                            for await (const token of user.AuthTokens) {
                                await token.Delete();
                            }
                            await this.emailSender.Deactivation(user.Email, user.SSOTokens.Activation);
                            LogIt.Debug("{0} user deactivated", user.UserName);
                        } catch (ex) {
                            LogIt.Warning("Failed to archive user account.", ex.stack);
                        }
                    } else {
                        try {
                            await user.DefaultGroup.Remove();
                            for await (const token of user.AuthTokens) {
                                await token.Remove();
                            }
                            await user.Remove();
                            await this.emailSender.Removed(user.Email);
                            LogIt.Debug("{0} user removed", user.UserName);
                        } catch (ex) {
                            LogIt.Warning("Failed to delete user account.", ex.stack);
                        }
                    }
                } catch (ex) {
                    LogIt.Warning("Failed to modify user account.", ex.stack);
                }
            }
        }
    }

    public async CheckResetTokens(this : AuthManager) : Promise<void> {
        const now : number = (new Date()).getTime();
        const users : User[] = await this.getClassFromInstance(this.getUserInstance()).Find({
            $and: [
                {"ssoTokens.reset": {$ne: null}},
                {"ssoTokens.reset": {$ne: ""}},
                {email: {$ne: null}}
            ]
        });
        for await (const user of users) {
            let noticeInterval : number = 14; // days
            noticeInterval *= 24 * 60 * 60 * 1000;

            if ((now - user.AuditLog.LastResetAction.getTime()) > noticeInterval) {
                try {
                    user.SSOTokens.Reset = "";
                    await user.Save();
                    LogIt.Debug("Removed reset password token for user {0}", user.UserName);
                } catch (ex) {
                    LogIt.Warning("Failed to modify user account.", ex.stack);
                }
            }
        }
    }

    @Extern()
    public async CheckEmailDuplicity(this : AuthManager) : Promise<any> {
        const users : User[] = await this.getClassFromInstance(this.getUserInstance()).Find({email: {$ne: null}}, {partial: true});
        const checked : string[] = [];
        const duplicity : any = {};
        for await (const user of users) {
            if (!checked.includes(user.Email)) {
                checked.push(user.Email);
                const found : User[] = await this.getUsersByEmail(user.Email);
                if (found.length > 1) {
                    if (!duplicity.hasOwnProperty(user.Email)) {
                        duplicity[user.Email] = [];
                        found.forEach(($user : User) : void => {
                            duplicity[user.Email].push({id: $user.Id, name: $user.UserName});
                        });
                    }
                }
            }
        }
        return duplicity;
    }

    @Extern()
    public async getPermissionsReport(this : AuthManager) : Promise<IPermissionsReport> {
        const groups : any[] = [];
        const groupsData : any = {};
        (await Loader.getInstance().getGQLContext().Execute(
            `{groupAll(limit:-1){id,name,hidden,authorizedMethods,deleted}}`
        )).data.groupAll.forEach(($group : any) : void => {
            groupsData[$group.id] = {
                Id               : $group.id,
                Name             : $group.name,
                Hidden           : $group.hidden === true,
                Deleted          : $group.deleted === true,
                AuthorizedMethods: ObjectValidator.IsEmptyOrNull($group.authorizedMethods) ? [] : $group.authorizedMethods
            };
            groups.push(groupsData[$group.id]);
        });
        const users : any[] = [];
        const usersGroups : string[] = [];
        (await Loader.getInstance().getGQLContext().Execute(
            `{userAll(limit:-1){id,groups,defaultGroup,userName,email,activated,deleted}}`
        )).data.userAll.forEach(($user : any) : void => {
            users.push({
                Id          : $user.id,
                UserName    : $user.userName,
                Email       : $user.email,
                Activated   : $user.activated === true,
                Deleted     : $user.deleted === true,
                DefaultGroup: groupsData[$user.defaultGroup],
                Groups      : ObjectValidator.IsEmptyOrNull($user.groups) ? [] : $user.groups.map(($id : string) : any => {
                    return groupsData[$id];
                })
            });
            usersGroups.push($user.defaultGroup, ...$user.groups);
        });

        const allDefaultMethods : string[] = [];
        const appSettings : AppSettings = await Loader.getInstance().getEnvironmentHelper().getAppSettings();

        const authGroups : string[][] = [
            this.defaultAccessibleMethods,
            this.defaultGroupAccess,
            appSettings.DefaultAccessibleMethods,
            appSettings.DefaultGroupAccess
        ];
        authGroups.forEach(($methods : string[]) : void => {
            if (!ObjectValidator.IsEmptyOrNull($methods)) {
                $methods.forEach(($method : string) : void => {
                    if (!ObjectValidator.IsEmptyOrNull($method) && !allDefaultMethods.includes($method)) {
                        allDefaultMethods.push($method);
                    }
                });
            }
        });

        const sysPermsRelicts : any = {};
        const sysGroupsIds : string[] = [];
        for (const name in this.systemPermissions) {
            if (this.systemPermissions.hasOwnProperty(name)) {
                const sysPerms : string[] = this.systemPermissions[name];
                const sysGroups : Group[] = groups.filter(($group : any) : boolean => {
                    return $group.Name === name;
                });
                if (!ObjectValidator.IsEmptyOrNull(sysGroups)) {
                    sysGroups.forEach(($group : Group) : void => {
                        if (!sysGroupsIds.includes($group.Id)) {
                            sysGroupsIds.push($group.Id);
                        }
                        $group.AuthorizedMethods.forEach(($method : string) : void => {
                            if (!sysPerms.includes($method)) {
                                if (!sysPermsRelicts.hasOwnProperty(name)) {
                                    sysPermsRelicts[name] = [];
                                }
                                sysPermsRelicts[name].push($method);
                            }
                        });
                    });
                }
            }
        }

        let appMethods : string[] = [];
        if (ObjectValidator.IsEmptyOrNull(this.authCache.appMethods)) {
            try {
                let externApiData : any[] = [];
                const metaFilePath : string = "./resource/data/Io/Oidis/Hub/Configuration/ExternApiMeta.json";
                if (Loader.getInstance().getFileSystemHandler().Exists(metaFilePath)) {
                    externApiData = JSON.parse(Loader.getInstance().getFileSystemHandler()
                        .Read(metaFilePath).toString("utf8")).map(($record : any) : any => {
                        return {
                            Namespace: $record.namespaceName,
                            Class    : $record.className,
                            Methods  : $record.methods
                        };
                    });
                    externApiData = externApiData.sort(($a : AuthorizedMethods, $b : AuthorizedMethods) : number => {
                        const nameA : string = $a.Namespace.toUpperCase();
                        const nameB : string = $b.Namespace.toUpperCase();
                        return (nameA < nameB) ? -1 : (nameA > nameB) ? 1 : 0;
                    });
                }
                appMethods = this.authMethodsToPatterns(externApiData).filter(($method : string) : boolean => {
                    return !allDefaultMethods.includes($method);
                });
                this.authCache.appMethods = appMethods;
            } catch (ex) {
                LogIt.Warning("Failed to parse ExternApiMeta.json\n" + ex.stack);
            }
        } else {
            appMethods = this.authCache.appMethods;
        }

        const report : IPermissionsReport = <any>{
            allDefaultMethods,
            appMethods,
            features      : this.getAuthFeatures(),
            sysMethods    : this.systemPermissions,
            sysPermsRelicts,
            sysGroups     : sysGroupsIds,
            usersForCheck : [],
            groupsForCheck: []
        };
        const defaultGroups : IDefaultGroupOwner[] = [];
        const cleanUpGroups : string[] = [];
        const uniqueMethods : string[] = [];
        const redundantGroups : string[] = [];
        const groupsWithDuplicity : string[] = [];

        for await (const user of users) {
            const newMethods : string[] = [];
            const userAllMethods : string[] = JsonUtils.Clone(allDefaultMethods);
            if (!ObjectValidator.IsEmptyOrNull(user.Groups)) {
                user.Groups.forEach(($group : Group) : void => {
                    if (this.systemPermissions.hasOwnProperty($group.Name)) {
                        this.systemPermissions[$group.Name].forEach(($method : string) : void => {
                            if (!userAllMethods.includes($method)) {
                                userAllMethods.push($method);
                            }
                        });
                    }
                });
            }
            if (!ObjectValidator.IsEmptyOrNull(user.DefaultGroup)) {
                defaultGroups.push({UserName: user.UserName, UserId: user.Id, Id: user.DefaultGroup.Id});
                if (!ObjectValidator.IsEmptyOrNull(user.DefaultGroup.AuthorizedMethods)) {
                    let containsDefault : boolean = false;
                    user.DefaultGroup.AuthorizedMethods.forEach(($method : string) : void => {
                        if (!userAllMethods.includes($method)) {
                            newMethods.push($method);
                            if (!uniqueMethods.includes($method)) {
                                uniqueMethods.push($method);
                            }
                        } else {
                            containsDefault = true;
                        }
                    });
                    if (containsDefault) {
                        if (!groupsWithDuplicity.includes(user.DefaultGroup.Id)) {
                            groupsWithDuplicity.push(user.DefaultGroup.Id);
                        }
                        if (ObjectValidator.IsEmptyOrNull(newMethods) && !cleanUpGroups.includes(user.DefaultGroup.Id)) {
                            cleanUpGroups.push(user.DefaultGroup.Id);
                        }
                    }
                }
            }
            if (!ObjectValidator.IsEmptyOrNull(newMethods)) {
                report.usersForCheck.push({
                    id               : user.Id,
                    name             : user.UserName,
                    email            : user.Email,
                    active           : user.Activated,
                    deleted          : user.Deleted,
                    customAuthMethods: newMethods
                });
            }
        }
        report.groupsCleanUpCandidates = cleanUpGroups;

        for await (const group of groups) {
            if (!sysGroupsIds.includes(group.Id) &&
                !ObjectValidator.IsEmptyOrNull(defaultGroups.find(($group : IDefaultGroupOwner) : boolean => {
                    return $group.Id === group.Id;
                }))) {
                const newMethods : string[] = [];
                let hasDuplicity : boolean = false;
                if (!ObjectValidator.IsEmptyOrNull(group.AuthorizedMethods)) {
                    group.AuthorizedMethods.forEach(($method : string) : void => {
                        if (!allDefaultMethods.includes($method)) {
                            newMethods.push($method);
                        } else {
                            hasDuplicity = true;
                        }
                    });
                }
                if (hasDuplicity) {
                    if (!groupsWithDuplicity.includes(group.Id)) {
                        groupsWithDuplicity.push(group.Id);
                    }
                    let deleted : boolean = false;
                    const assignedTo : string[] = [];
                    if (group.Deleted !== true) {
                        users.forEach(($user : User) : void => {
                            let found : boolean = false;
                            if ($user.DefaultGroup.Id === group.Id) {
                                found = true;
                            } else {
                                $user.Groups.forEach(($group : Group) : void => {
                                    if ($group.Id === group.Id) {
                                        found = true;
                                    }
                                });
                            }
                            if (found && !assignedTo.includes($user.Id)) {
                                assignedTo.push($user.Id);
                            }
                        });
                    } else {
                        deleted = true;
                    }
                    const isRedundant : boolean = ObjectValidator.IsEmptyOrNull(assignedTo) || ObjectValidator.IsEmptyOrNull(newMethods);
                    report.groupsForCheck.push({
                        id               : group.Id,
                        name             : group.Name,
                        assignedTo,
                        isRedundant,
                        hidden           : group.Hidden,
                        deleted,
                        customAuthMethods: newMethods
                    });
                    newMethods.forEach(($method : string) : void => {
                        if (!uniqueMethods.includes($method)) {
                            uniqueMethods.push($method);
                        }
                    });
                    if (isRedundant && !redundantGroups.includes(group.Id)) {
                        redundantGroups.push(group.Id);
                    }
                }
            }
            if (group.Deleted && !redundantGroups.includes(group.Id) && !usersGroups.includes(group.Id)) {
                redundantGroups.push(group.Id);
            }
        }
        report.uniqueMethods = uniqueMethods;
        report.defaultGroups = defaultGroups;
        report.redundantGroups = redundantGroups;
        report.groupsWithDuplicity = groupsWithDuplicity;

        return report;
    }

    @Extern()
    public async ResetCache(this : AuthManager) : Promise<void> {
        this.authCache = {};
        LogIt.Info("Performed manual reset of permissions cache.");

        await SystemLog.Trace({
            level    : LogLevel.WARNING,
            message  : "Performed manual reset of permissions cache.",
            traceBack: this.getClassNameWithoutNamespace() + ".ResetCache"
        });
    }

    protected addSystemPermissions(this : AuthManager, $name : string, ...$permissions : string[]) : void {
        if (!this.systemPermissions.hasOwnProperty($name)) {
            this.systemPermissions[$name] = [];
        }
        if (!ObjectValidator.IsEmptyOrNull($permissions)) {
            $permissions.forEach(($permission : string) : void => {
                if (!this.systemPermissions[$name].includes($permission)) {
                    this.systemPermissions[$name].push($permission);
                }
            });
        }
    }

    protected normalizeDate(this : AuthManager, $date : string | number) : number {
        let date : number = new Date($date).getTime();
        if (!ObjectValidator.IsInteger(date) && ObjectValidator.IsString($date)) {
            date = new Date(parseInt(<string>$date, 10)).getTime();
            if (ObjectValidator.IsEmptyOrNull(date)) {
                LogIt.Error("Failed to get build time from input data " + $date + ".");
                date = NaN;
            }
        }
        return date;
    }

    protected getClassFromInstance(this : AuthManager, $instance : BaseModel<any>) : any {
        return Reflection.getInstance().getClass($instance.getClassName());
    }

    protected getRandomSha(this : AuthManager, $from? : number) : string {
        if (ObjectValidator.IsEmptyOrNull($from)) {
            $from = new Date().getTime();
        }
        /// TODO: use different algorithm for token generate (use crypto lib) -> enable to generate 16 chars
        let token = $from.toString();
        const randomInt = ($min : number, $max : number) : number => {
            return Math.floor(Math.random() * ($max - $min + 1)) + $min;
        };
        for (let i = 0; i < 8; i++) {
            token += randomInt(10, 64).toString();
        }
        return StringUtils.getSha1(token);
    }

    protected getLocalizationsFor(this : AuthManager, $type : AuthManagerNotification, ...$args : any[]) : ILocalization {
        return {};
    }

    protected async getGravatar(this : AuthManager, $email : string) : Promise<Buffer> {
        if (ObjectValidator.IsEmptyOrNull($email)) {
            return null;
        }
        let data : Buffer = null;
        try {
            const crypto : any = require("crypto");
            const hash : string = crypto.createHash("md5").update($email.trim().toLowerCase()).digest("hex");
            data = <any>(await Loader.getInstance().getFileSystemHandler().DownloadAsync({
                streamOutput       : true,
                streamReturnsBuffer: true,
                url                : "https://www.gravatar.com/avatar/" + hash + "?d=404",
                verbose            : false
            })).bodyOrPath;
        } catch (ex) {
            LogIt.Info("Gravatar icon fetch error: " + ex.stack, LogSeverity.LOW);
        }
        if (ObjectValidator.IsEmptyOrNull(data)) {
            LogIt.Info("Gravatar icon not exists for: " + $email, LogSeverity.LOW);
        }
        return data;
    }

    protected getExpireHeader(this : AuthManager) : any {
        return {
            "Set-Cookie": AuthHttpResolver.getTokenName() + "=; Expires=Thu, 01 Jan 1970 00:00:01 GMT;"
        };
    }

    protected async createPermissionBackup(this : AuthManager) : Promise<any> {
        const cwd : string = FileTransferHandler.getInstance<FileTransferHandler>().getDefaultPath();
        const dump : any = await this.ExportAuthMethods();
        Loader.getInstance().getFileSystemHandler().Write(cwd + "/groupsPermsDump_" + (new Date().getTime()) + ".json",
            JSON.stringify(dump));
        return dump;
    }

    protected validateFeaturesFormat(this : AuthManager) : void {
        const errors : string[] = [];
        this.getAuthFeatures().forEach(($value : string) : void => {
            if (!StringUtils.Contains($value, "#")) {
                errors.push($value);
            }
        });
        if (!ObjectValidator.IsEmptyOrNull(errors)) {
            throw new Error(`Auth Features must contain symbol "#" for ability to distinguishes it form methods and patterns. Incorrectly defined features found: ${errors.join(",")}`);
        }
    }
}

/*! ******************************************************************************************************** *
 *
 * Copyright 2024 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { HttpStatusType } from "@io-oidis-commons/Io/Oidis/Commons/Enums/HttpStatusType.js";
import { BaseObject } from "@io-oidis-commons/Io/Oidis/Commons/Primitives/BaseObject.js";
import { Partial } from "@io-oidis-commons/Io/Oidis/Commons/Primitives/Decorators.js";
import { Convert } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Convert.js";
import { LogIt } from "@io-oidis-commons/Io/Oidis/Commons/Utils/LogIt.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { Property } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Property.js";
import { StringUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/StringUtils.js";
import { StreamResponse } from "@io-oidis-localhost/Io/Oidis/Localhost/HttpProcessor/ResponseApi/Handlers/StreamResponse.js";
import { IHttpResponse } from "@io-oidis-localhost/Io/Oidis/Localhost/Interfaces/IRoute.js";
import { Extern, Route } from "@io-oidis-localhost/Io/Oidis/Localhost/Primitives/Decorators.js";
import { AuthHttpResolver } from "@io-oidis-services/Io/Oidis/Services/ErrorPages/AuthHttpResolver.js";
import { User } from "../DAO/Models/User.js";
import { Loader } from "../Loader.js";
import { AuthCodeManager } from "../Utils/AuthCodeManager.js";
import { AuthManager } from "../Utils/AuthManager.js";

@Partial()
export abstract class BasicAuthenticationAPI extends BaseObject {

    @Route("GET /login?user&pass")
    @Extern()
    public async LogIn(this : AuthManager, $userNameOrEmail : string, $password : string, $noExpire : boolean = false) : Promise<string> {
        let user : User = await this.getUserByName($userNameOrEmail);
        if (ObjectValidator.IsEmptyOrNull(user)) {
            user = await this.getUserByEmail($userNameOrEmail);
        }
        if (!ObjectValidator.IsEmptyOrNull(user)) {
            if (user.Deleted) {
                throw new Error("Account has been removed.");
            }
            let attempts : number = 0;
            if (this.config.singInCheck.enabled && !ObjectValidator.IsEmptyOrNull(user.AuditLog.LogInAttempts)) {
                attempts = user.AuditLog.LogInAttempts;

                if (attempts >= this.config.singInCheck.numberOfTries &&
                    (new Date().getTime() - user.AuditLog.LastLogIn.getTime()) >
                    this.config.singInCheck.interval * 1000) {
                    attempts = 0;
                }
            }

            if (attempts < this.config.singInCheck.numberOfTries) {
                user.AuditLog.LastLogIn = new Date();
                /// TODO: user is able to login by pass or token but it should not be possible to use token for LogIn
                //        rather there should be more complex Authenticate method using credentials metadata and allow also override
                //        for SSO, service accounts etc.
                if (user.Password === StringUtils.getSha1($password) || this.checkAuthToken(user, $password)) {
                    user.AuditLog.LogInAttempts = 0;
                    if (!ObjectValidator.IsEmptyOrNull(user.SSOTokens.Reset)) {
                        user.SSOTokens.Reset = "";
                    }
                    if (this.config.twoFactorAuthentication.force && !ObjectValidator.IsEmptyOrNull(user.AuthCode)) {
                        user.AuthCode.Send = false;
                    }
                    await user.Save();
                    if (!this.config.multiSessionEnabled) {
                        await this.tokens.Clear(user.Id);
                    }
                    const token : string = await this.tokens.Create(user.Id, $noExpire);
                    if (!ObjectValidator.IsEmptyOrNull(token)) {
                        this.authCache[token] = {methods: [], validated: true};
                    }
                    if (this.config.twoFactorAuthentication.force && ObjectValidator.IsEmptyOrNull(user.TwoFASecret)) {
                        const code : AuthCodeManager = this.getAuthCodeManagerInstance();
                        (<any>code).getCurrentToken = () : any => {
                            return user.Id;
                        };
                        await code.SendAuthCode(user.Id, true);
                    }
                    return token;
                } else {
                    user.AuditLog.LogInAttempts = attempts + 1;
                    await user.Save();
                    throw new Error("User pass not valid");
                }
            } else {
                throw new Error("Too much unsuccessful attempts for login");
            }
        } else {
            LogIt.Warning("Failed to identify user by: " + $userNameOrEmail);
            throw new Error("None or multiple users has been found.");
        }
    }

    @Extern()
    public LogOut(this : AuthManager, $token : string = null) : Promise<boolean> {
        if (ObjectValidator.IsEmptyOrNull($token)) {
            $token = this.getCurrentToken();
        }
        if (!ObjectValidator.IsEmptyOrNull($token) && this.authCache.hasOwnProperty($token)) {
            delete this.authCache[$token];
        }
        return this.tokens.Clear($token);
    }

    @Extern()
    public async IsAuthenticated(this : AuthManager, $token : string = null) : Promise<boolean> {
        if (ObjectValidator.IsEmptyOrNull($token)) {
            $token = this.getCurrentToken();
        }
        if (!ObjectValidator.IsEmptyOrNull($token)) {
            let authenticated : boolean = false;
            if (await this.tokens.Validate($token)) {
                const userId : string = this.tokens.getPayloadId($token);
                try {
                    authenticated = !ObjectValidator.IsEmptyOrNull(await this.getUserDocument().find({_id: userId}).exec());
                } catch (ex) {
                    LogIt.Warning(ex.message);
                }
            }
            if (!authenticated && this.authCache.hasOwnProperty($token)) {
                delete this.authCache[$token];
            }
            return authenticated;
        }
        return false;
    }

    /// TODO: enable to reuse for getProfile and use user role for filtering of user data
    @Extern()
    public async IsAuthorizedFor(this : AuthManager, $method : string, $token : string | User = null) : Promise<boolean> {
        if (StringUtils.Contains($method, ".")) {
            const parts : string[] = StringUtils.Split($method, ".");
            const method : string = parts[parts.length - 1];
            parts.pop();
            $method = Loader.getInstance().getConnectorsRegister().ResolveSymbol(parts.join(".")) + "." + method;
        }
        if (ObjectValidator.IsEmptyOrNull($token)) {
            $token = this.getCurrentToken();
        }

        let status : boolean = false;
        let notValid : boolean = false;
        if (!ObjectValidator.IsEmptyOrNull($method)) {
            let authMethods : string[] = [];
            let fromCache : boolean = false;
            let key : string = null;
            if (!ObjectValidator.IsEmptyOrNull($token)) {
                key = <string>$token;
                if (!ObjectValidator.IsString(key)) {
                    key = (<User>$token).Id;
                } else if (this.authCache.hasOwnProperty(key)) {
                    if (!this.authCache[key].validated) {
                        this.authCache[key].validated = await this.tokens.Validate(<string>$token);
                    }
                    if (this.authCache[key].hasOwnProperty($method) && this.authCache[key].validated) {
                        status = this.authCache[key][$method];
                        if (!status) {
                            LogIt.Warning("Cached token {0} not authorized for feature: {1}", $token, $method);
                        }
                        return status;
                    }
                    if (!ObjectValidator.IsEmptyOrNull(this.authCache[key].methods)) {
                        authMethods = this.authCache[key].methods;
                        fromCache = true;
                    }
                }
            }
            if (!fromCache && !notValid) {
                let methodsFor : string | User = $token;
                if (!ObjectValidator.IsEmptyOrNull($token) && ObjectValidator.IsString($token)) {
                    if (await this.tokens.Validate(<string>$token)) {
                        methodsFor = await this.getUser(<string>$token);
                    } else {
                        notValid = true;
                    }
                }
                authMethods = await this.getAuthorizedMethods(methodsFor);
                if (!ObjectValidator.IsEmptyOrNull(key)) {
                    this.authCache[key] = {methods: authMethods, validated: true};
                }
            }

            authMethods.forEach(($item : string) : void => {
                if ($item === "any") {
                    $item = "*";
                }
                if (StringUtils.PatternMatched($item, $method)) {
                    status = true;
                }
            });
            if (!ObjectValidator.IsEmptyOrNull(key)) {
                this.authCache[key][$method] = status;
            }
        }
        if (!status && ObjectValidator.IsString($token) && !notValid) {
            LogIt.Warning("Token {0} not authorized for feature: {1}", $token, $method);
        }
        return status;
    }

    @Extern()
    public async Authenticate(this : AuthManager, $userId : string, $authToken : string) : Promise<string> {
        const user : User = await this.getUser($userId);
        if (!ObjectValidator.IsEmptyOrNull(user)) {
            $userId = user.Id;
        } else {
            return null;
        }
        if (await this.IsAuthTokenValid($authToken, $userId)) {
            return this.tokens.Create($userId, false, 5);
        }
        return null;
    }

    @Extern()
    public async IsPasswordValid(this : AuthManager, $token : string, $password : string) : Promise<boolean> {
        if (!ObjectValidator.IsEmptyOrNull($token) && await this.tokens.Validate($token)) {
            let valid : boolean = false;
            if (!ObjectValidator.IsEmptyOrNull($token)) {
                const user : User = await this.getUser($token);
                if (!ObjectValidator.IsEmptyOrNull(user)) {
                    valid = user.Password === StringUtils.getSha1($password);
                }
            }
            return valid;
        }
        return false;
    }

    @Route("GET /sso/validate", {responseHandler: new StreamResponse()})
    private async ssoValidateToken(this : AuthManager) : Promise<IHttpResponse> {
        if (await this.IsAuthenticated(this.getCurrentToken())) {
            const time : number = Property.Time(new Date().getTime(), "+10s");
            return {
                body   : "Token is valid.",
                headers: {
                    "Set-Cookie": AuthHttpResolver.getTokenName() + "=" + this.getCurrentToken() + "; " +
                        "Expires=" + Convert.TimeToGMTformat(time) + "; " +
                        (Loader.getInstance().getAppConfiguration().authManagerWorker.strictSecureSSOcookies ? "secure; " : "") +
                        "SameSite=Strict; HttpOnly; " +
                        "Path=/;"
                },
                status : HttpStatusType.SUCCESS
            };
        } else if (ObjectValidator.IsEmptyOrNull(this.getCurrentToken())) {
            return {
                body   : "Token must be specified.",
                headers: this.getExpireHeader(),
                status : HttpStatusType.ERROR
            };
        }
        return {
            body   : "Token is not valid.",
            headers: this.getExpireHeader(),
            status : HttpStatusType.NOT_AUTHORIZED
        };
    }

    @Route("GET /sso/logout", {responseHandler: new StreamResponse()})
    private async ssoLogout(this : AuthManager) : Promise<IHttpResponse> {
        return {
            body   : "Token logged out.",
            headers: this.getExpireHeader(),
            status : HttpStatusType.SUCCESS
        };
    }
}

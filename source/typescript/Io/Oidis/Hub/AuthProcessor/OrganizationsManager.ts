/*! ******************************************************************************************************** *
 *
 * Copyright 2024-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { LogLevel } from "@io-oidis-commons/Io/Oidis/Commons/Enums/LogLevel.js";
import { BaseObject } from "@io-oidis-commons/Io/Oidis/Commons/Primitives/BaseObject.js";
import { Partial } from "@io-oidis-commons/Io/Oidis/Commons/Primitives/Decorators.js";
import { JsonUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/JsonUtils.js";
import { LogIt } from "@io-oidis-commons/Io/Oidis/Commons/Utils/LogIt.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { ErrorLocalizable } from "@io-oidis-localhost/Io/Oidis/Localhost/Exceptions/Type/ErrorLocalizable.js";
import { Extern } from "@io-oidis-localhost/Io/Oidis/Localhost/Primitives/Decorators.js";
import {
    IAuthFindOptions,
    IModelListFilter,
    IModelListResult, IOrganizationProfileOptions,
    IUserRegisterOptions
} from "@io-oidis-services/Io/Oidis/Services/Connectors/AuthManagerConnector.js";
import { FileEntry } from "../DAO/Models/FileEntry.js";
import { Group } from "../DAO/Models/Group.js";
import { Organization } from "../DAO/Models/Organization.js";
import { User } from "../DAO/Models/User.js";
import { SystemLog } from "../LogProcessor/SystemLog.js";
import { AuthManager, AuthManagerNotification } from "../Utils/AuthManager.js";

@Partial()
export abstract class OrganizationsManager extends BaseObject {

    @Extern()
    public async RegisterOrganization(this : AuthManager, $options : IOrganizationProfileOptions,
                                      $owner? : IUserRegisterOptions) : Promise<string> {
        if (ObjectValidator.IsEmptyOrNull($options.name)) {
            throw new ErrorLocalizable("Failed to register organization: missing name",
                this.getLocalizationsFor(AuthManagerNotification.MissingOrganizationName));
        }
        if (!ObjectValidator.IsEmptyOrNull(await Organization.Find({name: $options.name}))) {
            throw new ErrorLocalizable("Failed to register organization: organization with same name already exists",
                this.getLocalizationsFor(AuthManagerNotification.OrganizationExist));
        }
        const organization : Organization = this.getOrganizationInstance();
        if (!ObjectValidator.IsEmptyOrNull($owner)) {
            await this.Register($owner);
            const owner : User = <User>await this.getUserByEmail($owner.email);
            await this.AddGroup((await this.getSystemGroups()).OrganizationOwner, owner);
            organization.Groups.push(owner.DefaultGroup);
        }

        await this.processOrganizationData(organization, $options);
        await organization.Save();
        await SystemLog.Trace({
            message  : "New organization registered: " + organization.Name,
            traceBack: this.getClassNameWithoutNamespace() + ".RegisterOrganization"
        });

        return organization.Id;
    }

    @Extern()
    public async setOrganizationProfile(this : AuthManager, $options : IOrganizationProfileOptions,
                                        $organizationId : string | Organization) : Promise<boolean> {
        let organization : Organization = <Organization>$organizationId;
        if (ObjectValidator.IsString($organizationId)) {
            organization = await this.getClassFromInstance(this.getOrganizationInstance()).FindById(<string>$organizationId);
        }

        if (!ObjectValidator.IsEmptyOrNull(organization)) {
            await this.processOrganizationData(organization, $options);
            await organization.Save();
            return true;
        }
        return false;
    }

    @Extern()
    public async getOrganizationProfile(this : AuthManager, $idOrOwnerId : string, $withData : boolean = false) : Promise<any> {
        if (ObjectValidator.IsEmptyOrNull($idOrOwnerId)) {
            return null;
        }
        let data : Organization[] = [];
        const organization : Organization = await this.getClassFromInstance(this.getOrganizationInstance())
            .FindById($idOrOwnerId, {partial: !$withData});
        if (!ObjectValidator.IsEmptyOrNull(organization)) {
            data.push(organization);
        }
        if (ObjectValidator.IsEmptyOrNull(data)) {
            data = await this.getClassFromInstance(this.getOrganizationInstance())
                .Find({groups: $idOrOwnerId}, {partial: !$withData});
        }
        if (!ObjectValidator.IsEmptyOrNull(data)) {
            if (data.length === 1) {
                const organization : Organization = data[0];
                if ($withData) {
                    await organization.Refresh({expanded: true});
                }
                const profile : any = await organization.ToJSON();
                this.normalizeOrganizationModel(profile);
                if ($withData && !ObjectValidator.IsEmptyOrNull(organization.Image)) {
                    const data : Buffer = await organization.Image.Data();
                    if (!ObjectValidator.IsEmptyOrNull(data)) {
                        profile.Image = "data:image/png;base64, " + data.toString("base64");
                    } else {
                        delete profile.Image;
                    }
                } else {
                    delete profile.Image;
                }
                return profile;
            } else {
                LogIt.Warning("Found more than one organization for owner: {0}", $idOrOwnerId);
            }
        }
        return null;
    }

    @Extern()
    public async getOrganizationChart(this : AuthManager, $organizationId : string,
                                      $filter? : IModelListFilter) : Promise<IModelListResult<User>> {
        const organization : Organization = await Organization.FindById($organizationId);
        const data : IModelListResult = {
            data  : [],
            limit : -1,
            offset: 0,
            size  : 0
        };
        JsonUtils.Extend(data, $filter);
        if (!ObjectValidator.IsEmptyOrNull(organization)) {
            let index : number = 0;
            for await (const group of organization.Groups) {
                if (index >= data.offset && (data.limit === -1 || data.data.length < data.limit)) {
                    const filter : any = {defaultGroup: group.Id};
                    if (!organization.Deleted) {
                        filter.deleted = {$ne: true};
                    }
                    const users : User[] = await User.Find(filter);
                    if (!ObjectValidator.IsEmptyOrNull(users)) {
                        for await (const user of users) {
                            if (ObjectValidator.IsEmptyOrNull(user.Activated)) {
                                user.Activated = true;
                            }
                            data.data.push(await user.ToJSON());
                        }
                    }
                }
                index++;
            }
            data.size = index;
        } else {
            LogIt.Debug("Org not found for get chart: " + $organizationId);
        }
        return data;
    }

    @Extern()
    public async AddUserToOrganization(this : AuthManager, $userIdOrName : string, $organizationId : string) : Promise<boolean> {
        const organization : Organization = await this.getClassFromInstance(this.getOrganizationInstance()).FindById($organizationId);
        if (!ObjectValidator.IsEmptyOrNull(organization)) {
            const user : User = await this.getUser($userIdOrName);
            if (ObjectValidator.IsEmptyOrNull(user)) {
                await User.Find({userName: $userIdOrName, email: $userIdOrName});
            }
            if (!ObjectValidator.IsEmptyOrNull(user)) {
                let exist : boolean = false;
                organization.Groups.forEach(($group : Group) : void => {
                    if ($group.Id === user.DefaultGroup.Id) {
                        exist = true;
                    }
                });
                if (!exist) {
                    organization.Groups.push(user.DefaultGroup);
                    await organization.Save();
                    await SystemLog.Trace({
                        message  : "User " + user.UserName + " added to organization: " + organization.Name,
                        traceBack: this.getClassNameWithoutNamespace() + ".AddUserToOrganization"
                    });
                }
            }
            return true;
        }
        return false;
    }

    @Extern()
    public async RemoveUserFromOrganization(this : AuthManager, $userId : string, $organizationId : string) : Promise<boolean> {
        const organization : Organization = await this.getClassFromInstance(this.getOrganizationInstance()).FindById($organizationId);
        const user : User = await this.getUser($userId);
        if (!ObjectValidator.IsEmptyOrNull(organization) && !ObjectValidator.IsEmptyOrNull(user)) {
            const newGroups : Group[] = [];
            let updated : boolean = false;
            organization.Groups.forEach(($group : Group) : void => {
                if ($group.Id !== user.DefaultGroup.Id) {
                    newGroups.push($group);
                } else {
                    updated = true;
                }
            });
            if (updated) {
                organization.Groups = newGroups;
                await organization.Save();
                await SystemLog.Trace({
                    level    : LogLevel.WARNING,
                    message  : "User " + user.UserName + " removed from organization: " + organization.Name,
                    traceBack: this.getClassNameWithoutNamespace() + ".RemoveUserFromOrganization"
                });
            }
            return true;
        }
        return false;
    }

    @Extern()
    public async FindOrganizations(this : AuthManager, $filter : string | any,
                                   $options : IAuthFindOptions = null) : Promise<IModelListResult<Organization>> {
        $options = JsonUtils.Extend({
            limit      : -1,
            offset     : 0,
            onlyActive : true,
            withDeleted: false
        }, $options);
        let filter : any = $filter;
        if (ObjectValidator.IsString($filter)) {
            filter = {name: new RegExp($filter)};
        }
        if ($options.withDeleted) {
            if ($options.onlyActive) {
                filter.$or = [{deleted: true}, {activated: {$ne: false}}];
            }
        } else {
            filter.deleted = {$ne: true};
            if ($options.onlyActive) {
                filter.activated = {$ne: false};
            }
        }
        const size : number = await this.getClassFromInstance(this.getOrganizationInstance()).Model().countDocuments(filter);
        const organizations : Organization[] = await this.getClassFromInstance(this.getOrganizationInstance()).Find(filter, {
            limit : $options.limit,
            offset: $options.offset
        });
        const output : IModelListResult = {
            data  : [],
            limit : $options.limit,
            offset: $options.offset,
            size
        };
        for await (const organization of organizations) {
            this.normalizeOrganizationModel(organization);
            output.data.push(await organization.ToJSON());
        }
        return output;
    }

    @Extern()
    public async DeactivateOrganization(this : AuthManager, $id : string, $value : boolean = true) : Promise<boolean> {
        const organization : Organization = await this.getClassFromInstance(this.getOrganizationInstance()).FindById($id);
        if (!ObjectValidator.IsEmptyOrNull(organization)) {
            organization.Activated = !$value;
            await organization.Save();
            LogIt.Debug("Organization activation has been changed to: {0}", !$value);
            await SystemLog.Trace({
                level    : !organization.Activated ? LogLevel.WARNING : LogLevel.INFO,
                message  : "Organization " + (!organization.Activated ? "deactivated" : "activated") + ": " + organization.Name,
                traceBack: this.getClassNameWithoutNamespace() + ".DeactivateOrganization"
            });
            return true;
        } else {
            LogIt.Error("Failed to find organization with ID: " + $id);
            return false;
        }
    }

    @Extern()
    public async DeleteOrganization(this : AuthManager, $id : string) : Promise<boolean> {
        const organization : Organization = await this.getClassFromInstance(this.getOrganizationInstance()).FindById($id);
        if (!ObjectValidator.IsEmptyOrNull(organization)) {
            try {
                organization.Activated = false;
                await organization.Delete();
                await SystemLog.Trace({
                    level    : LogLevel.WARNING,
                    message  : "Organization deleted: " + organization.Name,
                    traceBack: this.getClassNameWithoutNamespace() + ".DeleteOrganization"
                });
            } catch (ex) {
                LogIt.Warning(ex.message);
                return false;
            }
        } else {
            LogIt.Error("Failed to find organization with ID: " + $id);
            return false;
        }
        return true;
    }

    @Extern()
    public async RemoveOrganization(this : AuthManager, $id : string) : Promise<boolean> {
        const organization : Organization = await this.getClassFromInstance(this.getOrganizationInstance()).FindById($id);
        if (!ObjectValidator.IsEmptyOrNull(organization)) {
            try {
                await organization.Remove();
                await SystemLog.Trace({
                    level    : LogLevel.WARNING,
                    message  : "Organization removed: " + organization.Name,
                    traceBack: this.getClassNameWithoutNamespace() + ".RemoveOrganization"
                });
            } catch (ex) {
                LogIt.Warning(ex.message);
                return false;
            }
        } else {
            LogIt.Error("Failed to find organization with ID: " + $id);
            return false;
        }
        return true;
    }

    @Extern()
    public async RestoreOrganization(this : AuthManager, $id : string) : Promise<boolean> {
        const organization : Organization = await this.getClassFromInstance(this.getOrganizationInstance()).FindById($id);
        if (!ObjectValidator.IsEmptyOrNull(organization)) {
            try {
                organization.Deleted = false;
                await organization.Save();
                await SystemLog.Trace({
                    message  : "Organization restored: " + organization.Name,
                    traceBack: this.getClassNameWithoutNamespace() + ".RestoreOrganization"
                });
            } catch (ex) {
                LogIt.Warning(ex.message);
                return false;
            }
        } else {
            LogIt.Error("Failed to find organization with ID: " + $id);
            return false;
        }
        return true;
    }

    protected getOrganizationInstance(this : AuthManager) : Organization {
        return new Organization();
    }

    protected async processOrganizationData(this : AuthManager, $organization : Organization,
                                            $options : IOrganizationProfileOptions) : Promise<void> {
        if (ObjectValidator.IsSet($options.name) && !ObjectValidator.IsEmptyOrNull($options.name)) {
            $organization.Name = $options.name;
        }
        if (ObjectValidator.IsSet($options.image)) {
            if (ObjectValidator.IsEmptyOrNull($organization.Image)) {
                $organization.Image = new FileEntry();
                $organization.Image.Owner = this.getOrganizationInstance().getClassNameWithoutNamespace();
            }
            if (!ObjectValidator.IsEmptyOrNull($options.image)) {
                await $organization.Image.Data(Buffer.from($options.image, "base64"));
            } else {
                await $organization.Image.Remove();
            }
        }
        if (ObjectValidator.IsSet($options.activated)) {
            $organization.Activated = $options.activated;
        }
    }

    protected normalizeOrganizationModel(this : AuthManager, $model : Organization) : void {
        if (ObjectValidator.IsEmptyOrNull($model.Activated)) {
            $model.Activated = true;
        }
    }

}

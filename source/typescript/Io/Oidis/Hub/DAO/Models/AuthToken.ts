/*! ******************************************************************************************************** *
 *
 * Copyright 2019-2024 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { Entity, Property } from "../../Primitives/Decorators.js";
import { BaseModel } from "./BaseModel.js";

@Entity("authToken")
export class AuthToken extends BaseModel<AuthToken> {
    @Property({type: String, required: false})
    public declare Name : string;

    @Property({type: String})
    public declare Token : string;

    @Property({type: Number})
    public declare Date : number;

    @Property({type: Number, required: false})
    public declare Expire : number;
}

/*! ******************************************************************************************************** *
 *
 * Copyright 2023 NXP
 * Copyright 2024 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { BaseModel } from "@io-oidis-commons/Io/Oidis/Commons/Primitives/BaseModel.js";
import { Entity, Property } from "@io-oidis-commons/Io/Oidis/Commons/Primitives/Decorators.js";
import { ISendmailConfiguration } from "@io-oidis-hub-gui/Io/Oidis/Hub/Gui/Interfaces/ISendmailConfiguration.js";
import { Loader } from "../../Loader.js";

@Entity("sendmail")
export class Sendmail extends BaseModel<Sendmail> {
    @Property({type: Boolean, default: false})
    public declare Enabled : boolean;

    @Property({type: [String], default: []})
    public declare AdminsList : string[];

    @Property({type: [String], default: []})
    public declare BlackList : string[];

    public async SyncFrom($config : ISendmailConfiguration) : Promise<void> {
        this.Enabled = $config.enabled;
        this.AdminsList = $config.admins;
        this.BlackList = $config.blackList;
    }

    public async SyncTo($config : ISendmailConfiguration) : Promise<void> {
        const enableStateChanged : boolean = $config.enabled !== this.Enabled;
        $config.enabled = this.Enabled;
        $config.admins = this.AdminsList;
        $config.blackList = this.BlackList;
        if (enableStateChanged) {
            if ($config.enabled) {
                Loader.getInstance().getMailEngine().StartLoop();
            } else {
                Loader.getInstance().getMailEngine().StopLoop();
            }
        }
    }
}

/*! ******************************************************************************************************** *
 *
 * Copyright 2022-2024 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { Entity, Property } from "../../Primitives/Decorators.js";
import { BaseModel } from "./BaseModel.js";

@Entity("logEntry")
export class LogEntry extends BaseModel<LogEntry> {

    @Property({type: String, required: false})
    public declare User : string;

    @Property({type: String, required: false})
    public declare TraceBack : string;

    @Property({type: String, required: false})
    public declare OnBehalf : string;

    @Property({type: String, required: false})
    public declare Message : string;

    @Property({type: [String], required: false})
    public declare Tags : string[];
}

/*! ******************************************************************************************************** *
 *
 * Copyright 2019-2024 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { ArrayConverter } from "@io-oidis-commons/Io/Oidis/Commons/DAO/Converters/ArrayConverter.js";
import { InclusionConverter } from "@io-oidis-commons/Io/Oidis/Commons/DAO/Converters/InclusionConverter.js";
import { BaseModel } from "@io-oidis-commons/Io/Oidis/Commons/Primitives/BaseModel.js";
import { Entity, Property } from "@io-oidis-commons/Io/Oidis/Commons/Primitives/Decorators.js";
import { EmailValidator } from "@io-oidis-hub-gui/Io/Oidis/Hub/Gui/DAO/Validators/EmailValidator.js";
import { PhoneValidator } from "@io-oidis-hub-gui/Io/Oidis/Hub/Gui/DAO/Validators/PhoneValidator.js";
import { AuthCode } from "@io-oidis-hub-gui/Io/Oidis/Hub/Gui/Models/AuthCode.js";
import { AuthTokenToRefConverter } from "../Converters/AuthTokenToRefConverter.js";
import { FileEntryToRefConverter } from "../Converters/FileEntryToRefConverter.js";
import { GroupListToRefConverter } from "../Converters/GroupListToRefConverter.js";
import { GroupToRefConverter } from "../Converters/GroupToRefConverter.js";
import { AuthToken } from "./AuthToken.js";
import { FileEntry } from "./FileEntry.js";
import { Group } from "./Group.js";
import { SSOTokens } from "./SSOTokens.js";
import { UserAuditLog } from "./UserAuditLog.js";

@Entity("user")
export class User extends BaseModel<User> {
    @Property({type: String})
    public declare UserName : string;

    @Property({type: String, required: false})
    public declare Firstname : string;

    @Property({type: String, required: false})
    public declare Lastname : string;

    @Property({type: String, required: false, validator: new EmailValidator()})
    public declare Email : string;

    @Property({type: [String], default: [], converter: new ArrayConverter()})
    public declare AltEmails : string[];

    @Property({type: String, required: false, validator: new PhoneValidator()})
    public declare Phone : string;

    @Property({type: String})
    public declare Password : string;

    @Property({type: [Group], default: [], converter: new GroupListToRefConverter()})
    public declare Groups : Group[];

    @Property({type: Group, converter: new GroupToRefConverter()})
    public declare DefaultGroup : Group;

    @Property({type: [AuthToken], default: [], converter: new AuthTokenToRefConverter()})
    public declare AuthTokens : AuthToken[];

    @Property({symbol: "imageData", required: false, type: FileEntry, converter: new FileEntryToRefConverter()})
    public declare Image : FileEntry;

    @Property({type: SSOTokens, converter: new InclusionConverter(SSOTokens), symbol: "ssoTokens"})
    public declare SSOTokens : SSOTokens;

    @Property({type: UserAuditLog, converter: new InclusionConverter(UserAuditLog)})
    public declare AuditLog : UserAuditLog;

    @Property({type: Boolean, required: false, default: true})
    public declare Activated : boolean;

    @Property({type: String, required: false})
    public declare TwoFASecret : string;

    @Property({type: AuthCode, converter: new InclusionConverter(AuthCode), required: false})
    public declare AuthCode : AuthCode;

    @Property({type: String, required: false})
    public declare WhatsNewSeen : string;

    @Property({symbol: "image", type: Buffer, required: false, noResolve: true})
    private declare imageBuffer : Buffer;
}

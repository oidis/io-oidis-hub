/*! ******************************************************************************************************** *
 *
 * Copyright 2021-2024 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { Entity, Property } from "../../Primitives/Decorators.js";
import { BaseModel } from "./BaseModel.js";

@Entity("ssoTokens")
export class SSOTokens extends BaseModel<SSOTokens> {
    @Property({type: String, required: false})
    public declare Invite : string;

    @Property({type: String, required: false})
    public declare Activation : string;

    @Property({type: String, required: false})
    public declare Reset : string;
}

/*! ******************************************************************************************************** *
 *
 * Copyright 2021-2024 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { IContext } from "../../Interfaces/DAO/IConverter.js";
import { Entity, Property } from "../../Primitives/Decorators.js";
import { BaseConverter } from "../Converters/BaseConverter.js";
import { BaseModel } from "./BaseModel.js";

@Entity("authorizedMethods")
export class AuthorizedMethods extends BaseModel<AuthorizedMethods> {
    @Property({type: String})
    public declare Class : string;

    @Property({type: String})
    public declare Namespace : string;

    @Property({type: [String]})
    public declare Methods : string[];

    @Property({type: String})
    public declare Sha : string;
}

export class AuthorizedMethodsToRefConverter extends BaseConverter {
    public async from($value : any, $context : IContext) : Promise<any> {
        const items : AuthorizedMethods[] = [];
        for await (const item of $value) {
            const instance : any = await AuthorizedMethods.FindById(item);
            items.push(instance);
        }
        return items;
    }

    public async to($value : any, $context : IContext) : Promise<any> {
        const ids : string[] = [];
        for await (const model of $value) {
            $context.parent.getDocument()[$context.property.symbol].push(model.Id);
            ids.push(model.Id);
            await model.Save();
        }
        return ids;
    }
}

@Entity("authEntry")
export class AuthEntry extends BaseModel<AuthEntry> {
    @Property({type: String})
    public declare Name : string;

    @Property({type: String, required: false})
    public declare Release : string;

    @Property({type: String})
    public declare Platform : string;

    @Property({type: String})
    public declare Version : string;

    @Property({type: Number})
    public declare BuildTime : number;

    @Property({type: [AuthorizedMethods], default: [], converter: new AuthorizedMethodsToRefConverter()})
    public declare AuthorizedMethods : AuthorizedMethods[];
}

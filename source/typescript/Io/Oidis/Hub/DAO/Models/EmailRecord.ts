/*! ******************************************************************************************************** *
 *
 * Copyright 2023-2024 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { Entity, Property } from "../../Primitives/Decorators.js";
import { BaseModel } from "./BaseModel.js";

@Entity("emailRecord")
export class EmailRecord extends BaseModel<EmailRecord> {

    @Property({type: String})
    public declare Type : string;

    @Property({type: String})
    public declare Email : string;

    @Property({type: String, required: false})
    public declare Url : string;

    @Property({type: String, required: false})
    public declare Environment : string;

    @Property({type: Boolean, required: false, default: true})
    public declare Active : boolean;
}

/*! ******************************************************************************************************** *
 *
 * Copyright 2023-2024 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { BaseModel } from "@io-oidis-commons/Io/Oidis/Commons/Primitives/BaseModel.js";
import { Entity, Property } from "@io-oidis-commons/Io/Oidis/Commons/Primitives/Decorators.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { StringUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/StringUtils.js";
import { IDbConfiguration } from "../../Interfaces/IProject.js";
import { Loader } from "../../Loader.js";
import { DataBlob } from "./DataBlob.js";

export enum FileEntryType {
    DATABASE,
    FILE_SYSTEM
}

@Entity("files")
export class FileEntry extends BaseModel<FileEntry> {
    @Property({type: String, default: "Standalone"})
    public declare Owner : string;

    @Property({type: String, required: false})
    public declare Path : string;

    @Property({type: String, required: false})
    public declare ContentType : string;

    @Property({type: String, required: false})
    public declare Version : string;

    @Property({type: Number, required: false})
    public declare ChunkId : number;

    @Property({type: Number, required: false})
    public declare Size : number;

    @Property({type: Number, default: FileEntryType.DATABASE})
    public declare StorageType : FileEntryType;

    @Property({type: String, required: false})
    private declare blobId : string;

    public async ConvertToFS($name? : string) : Promise<void> {
        if (!ObjectValidator.IsEmptyOrNull(this.blobId)) {
            this.StorageType = FileEntryType.DATABASE;
            const data : Buffer = await this.Data();
            if (ObjectValidator.IsEmptyOrNull(this.Path)) {
                this.Path = $name;
                if (ObjectValidator.IsEmptyOrNull(this.Path)) {
                    this.Path = this.getUID();
                }
            }
            Loader.getInstance().getFileSystemHandler().Write(this.getAbsolutePath(), data);
            const blob : DataBlob = await DataBlob.FindById(this.blobId);
            await blob.Remove();
            await this.Remove("blobId");
        }
        this.StorageType = FileEntryType.FILE_SYSTEM;
        await this.Save();
    }

    public async ConvertToDB() : Promise<void> {
        if (!ObjectValidator.IsEmptyOrNull(this.Path)) {
            const path : string = this.getAbsolutePath();
            if (Loader.getInstance().getFileSystemHandler().Exists(path)) {
                const blob : Buffer = await this.Data();
                this.StorageType = FileEntryType.DATABASE;
                await this.Data(blob);
                Loader.getInstance().getFileSystemHandler().Delete(path);
            }
        }
        this.StorageType = FileEntryType.DATABASE;
        await this.Save();
    }

    public getAbsolutePath() : string {
        if (!ObjectValidator.IsEmptyOrNull(this.Path)) {
            const rootPath : string = this.getStorageRootFolder();
            let path : string = StringUtils.Remove(this.Path, rootPath);
            if (!StringUtils.StartsWith(path, "/")) {
                path = "/" + path;
            }
            return rootPath + path;
        }
        return null;
    }

    public async Data($value? : Buffer) : Promise<Buffer> {
        let blob : DataBlob;
        if (!ObjectValidator.IsEmptyOrNull(this.blobId)) {
            blob = await DataBlob.FindById(this.blobId);
        }
        if (!ObjectValidator.IsEmptyOrNull($value)) {
            if (this.StorageType === FileEntryType.DATABASE) {
                if (ObjectValidator.IsEmptyOrNull(blob)) {
                    blob = new DataBlob();
                }
                blob.Data = $value;
                await blob.Save();
                this.blobId = blob.Id;
            } else {
                if (ObjectValidator.IsEmptyOrNull(this.Path)) {
                    this.Path = this.getUID();
                }
                Loader.getInstance().getFileSystemHandler().Write(this.getAbsolutePath(), $value);
                if (!ObjectValidator.IsEmptyOrNull(blob)) {
                    await blob.Remove();
                    await this.Remove("blobId");
                }
            }
            this.Size = $value.length;
            await this.Save();
        }
        if (!ObjectValidator.IsEmptyOrNull(blob)) {
            return blob.Data;
        }
        if (!ObjectValidator.IsEmptyOrNull(this.Path)) {
            const path : string = this.getAbsolutePath();
            if (Loader.getInstance().getFileSystemHandler().Exists(path)) {
                const fs : any = require("fs");
                const stats : any = fs.statSync(path);
                this.LastModified = new Date(stats.mtime.getTime());
                this.Size = stats.size;

                return <Buffer>Loader.getInstance().getFileSystemHandler().Read(path);
            }
        }
        return null;
    }

    public async Remove($property? : string) : Promise<void> {
        if (ObjectValidator.IsEmptyOrNull($property) && !ObjectValidator.IsEmptyOrNull(this.blobId)) {
            const blob : DataBlob = await DataBlob.FindById(this.blobId);
            await blob.Remove();
        }
        return super.Remove($property);
    }

    protected getStorageRootFolder() : string {
        let path : string = (<IDbConfiguration>Loader.getInstance().getAppConfiguration().dbConfigs).fileStorageLocation;
        if (ObjectValidator.IsEmptyOrNull(path)) {
            path = Loader.getInstance().getProgramArgs().AppDataPath();
            if (StringUtils.Contains(path, "/build/target")) {
                path += "/../appData";
            }
            path += "/FileEntryBlobs";
        }
        this.getStorageRootFolder = () : string => {
            return path;
        };
        return path;
    }
}

/*! ******************************************************************************************************** *
 *
 * Copyright 2023 NXP
 * Copyright 2024 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { InclusionConverter } from "@io-oidis-commons/Io/Oidis/Commons/DAO/Converters/InclusionConverter.js";
import { BaseModel } from "@io-oidis-commons/Io/Oidis/Commons/Primitives/BaseModel.js";
import { Entity, Property } from "@io-oidis-commons/Io/Oidis/Commons/Primitives/Decorators.js";
import { IProject } from "../../Interfaces/IProject.js";
import { Sendmail } from "./Sendmail.js";

@Entity("appSettings")
export class AppSettings extends BaseModel<AppSettings> {
    @Property({type: String, required: false})
    public declare Version : string;

    @Property({type: Sendmail, converter: new InclusionConverter(Sendmail)})
    public declare Sendmail : Sendmail;

    @Property({type: [String], default: []})
    public declare DefaultAccessibleMethods : string[];

    @Property({type: [String], default: []})
    public declare DefaultGroupAccess : string[];

    public async SyncFrom($config : IProject) : Promise<void> {
        this.Version = $config.version;
        await this.Sendmail.SyncFrom($config.sendmail);
        await this.Save();
    }

    public async SyncTo($config : IProject) : Promise<void> {
        await this.Sendmail.SyncTo($config.sendmail);
    }
}

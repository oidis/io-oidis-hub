/*! ******************************************************************************************************** *
 *
 * Copyright 2022-2024 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { Entity, Property } from "../../Primitives/Decorators.js";
import { BaseModel } from "./BaseModel.js";

@Entity("buildPackages")
export class BuildPackage extends BaseModel<BuildPackage> {
    @Property({type: String})
    public declare Name : string;

    @Property({type: String, required: false})
    public declare Release : string;

    @Property({type: String, required: false})
    public declare Platform : string;

    @Property({type: String})
    public declare Version : string;

    @Property({type: Number})
    public declare BuildTime : number;

    @Property({type: String})
    public declare FileName : string;

    @Property({type: String, required: false})
    public declare Type : string;
}

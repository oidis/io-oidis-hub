/*! ******************************************************************************************************** *
 *
 * Copyright 2022-2024 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { BaseModel } from "@io-oidis-commons/Io/Oidis/Commons/Primitives/BaseModel.js";
import { Entity, Property } from "@io-oidis-commons/Io/Oidis/Commons/Primitives/Decorators.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { FileEntryToRefConverter } from "../Converters/FileEntryToRefConverter.js";
import { FileEntry } from "./FileEntry.js";

@Entity("configs")
export class ConfigEntry extends BaseModel<ConfigEntry> {
    @Property({type: String})
    public declare Name : string;

    @Property({type: String})
    public declare ConfigName : string;

    @Property({type: String})
    public declare Version : string;

    @Property({type: Number})
    public declare Timestamp : number;

    @Property({type: FileEntry, converter: new FileEntryToRefConverter()})
    public declare Data : FileEntry;

    constructor() {
        super();

        this.Data.Owner = this.getClassNameWithoutNamespace();
    }

    public async Remove($property? : string) : Promise<void> {
        if (ObjectValidator.IsEmptyOrNull($property)) {
            await this.Data.Remove();
        }
        return super.Remove($property);
    }
}

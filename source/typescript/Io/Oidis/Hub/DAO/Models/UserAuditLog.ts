/*! ******************************************************************************************************** *
 *
 * Copyright 2021-2024 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { Entity, Property } from "../../Primitives/Decorators.js";
import { BaseModel } from "./BaseModel.js";

@Entity("userAuditLog")
export class UserAuditLog extends BaseModel<UserAuditLog> {
    @Property({type: Date, required: false})
    public declare LastActivationNotice : Date;

    @Property({type: Number, required: false})
    public declare ActivationNoticeCounter : number;

    @Property({type: Date, required: false})
    public declare LastResetAction : Date;

    @Property({type: Date, required: false})
    public declare LastLogIn : Date;

    @Property({type: Number, required: false})
    public declare LogInAttempts : number;
}

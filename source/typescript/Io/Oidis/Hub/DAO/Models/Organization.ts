/*! ******************************************************************************************************** *
 *
 * Copyright 2022-2024 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { BaseModel } from "@io-oidis-commons/Io/Oidis/Commons/Primitives/BaseModel.js";
import { Entity, Property } from "@io-oidis-commons/Io/Oidis/Commons/Primitives/Decorators.js";
import { FileEntryToRefConverter } from "../Converters/FileEntryToRefConverter.js";
import { GroupListToRefConverter } from "../Converters/GroupListToRefConverter.js";
import { FileEntry } from "./FileEntry.js";
import { Group } from "./Group.js";

@Entity("organization")
export class Organization extends BaseModel<Organization> {

    @Property({type: String, required: false})
    public declare Name : string;

    @Property({type: [Group], default: [], converter: new GroupListToRefConverter()})
    public declare Groups : Group[];

    @Property({symbol: "imageData", required: false, type: FileEntry, converter: new FileEntryToRefConverter()})
    public declare Image : FileEntry;

    @Property({type: Boolean, required: false, default: true})
    public declare Activated : boolean;

    @Property({symbol: "image", type: Buffer, required: false, noResolve: true})
    private declare imageBuffer : Buffer;
}

/*! ******************************************************************************************************** *
 *
 * Copyright 2020-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { BaseModel as Parent, IModelSaveOptions as ParentSaveOptions } from "@io-oidis-commons/Io/Oidis/Commons/Primitives/BaseModel.js";

/**
 * @deprecated Use BaseModel from Commons namespace
 */
export class BaseModel<T> extends Parent<T> {
    // Fallback mapping
}

/* eslint-disable @typescript-eslint/no-empty-object-type */
/**
 * @deprecated Use IModelSaveOptions from Commons namespace
 */
export interface IModelSaveOptions extends ParentSaveOptions {
    // Fallback mapping
}

/* eslint-enable */

// generated-code-start
export const IModelSaveOptions = globalThis.RegisterInterface([], <any>ParentSaveOptions);
// generated-code-end

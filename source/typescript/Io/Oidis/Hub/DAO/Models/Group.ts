/*! ******************************************************************************************************** *
 *
 * Copyright 2021-2024 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { Entity, Property } from "../../Primitives/Decorators.js";
import { ArrayConverter } from "../Converters/ArrayConverter.js";
import { BaseModel } from "./BaseModel.js";

@Entity("group")
export class Group extends BaseModel<Group> {
    @Property({type: String})
    public declare Name : string;

    @Property({type: [String], default: [], converter: new ArrayConverter()})
    public declare AuthorizedMethods : string[];

    @Property({type: Boolean, required: false, default: false})
    public declare Hidden : boolean;
}

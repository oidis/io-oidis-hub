/*! ******************************************************************************************************** *
 *
 * Copyright 2022-2024 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { BaseModel } from "@io-oidis-commons/Io/Oidis/Commons/Primitives/BaseModel.js";
import { Entity, Property } from "@io-oidis-commons/Io/Oidis/Commons/Primitives/Decorators.js";

@Entity("webhookRecord")
export class WebhookRecord extends BaseModel<WebhookRecord> {
    @Property({type: String, required: true})
    public declare Adapter : string;

    @Property({type: String, required: true})
    public declare AdapterClassName : string;

    @Property({type: String, required: true})
    public declare Type : string;

    @Property({type: String, required: false})
    public declare OwnerId : string;

    @Property({type: String, required: true})
    public declare Code : string;

    @Property({type: Boolean, required: true, default: true})
    public declare Active : boolean;
}

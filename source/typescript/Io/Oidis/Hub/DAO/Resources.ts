/*! ******************************************************************************************************** *
 *
 * Copyright 2018 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { StringUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/StringUtils.js";
import { FileSystemHandler } from "@io-oidis-localhost/Io/Oidis/Localhost/Connectors/FileSystemHandler.js";
import { Resources as Parent } from "@io-oidis-services/Io/Oidis/Services/DAO/Resources.js";
import { Loader } from "../Loader.js";

export class Resources extends Parent {
    protected static filterPath($value : string) : string {
        const values : string[] = StringUtils.Split($value, "||");
        const instance : Loader = Loader.getInstance();
        const fileSystem : FileSystemHandler = instance.getFileSystemHandler();
        const projectBase : string = instance.getProgramArgs().ProjectBase();
        const binBase : string = instance.getProgramArgs().BinBase();

        const nextValue : any = ($index : number = 0) : void => {
            if ($index < values.length) {
                let value : string = values[$index].trim();
                if (!StringUtils.Contains(value, "http://", "https://", "file://", ":/") &&
                    !StringUtils.StartsWith(value, "/")) {
                    if (StringUtils.Contains(value, "?")) {
                        value = StringUtils.Substring(value, 0, StringUtils.IndexOf(value, "?"));
                    }
                    const projectPath : string = projectBase + "/" + value;
                    if (!fileSystem.Exists(projectPath)) {
                        $value = binBase + "/" + value;
                        if (!fileSystem.Exists($value) &&
                            StringUtils.Contains($value, "/dependencies/") &&
                            !fileSystem.Exists(projectBase + "/dependencies")) {
                            nextValue($index + 1);
                        }
                    } else {
                        $value = projectPath;
                    }
                } else {
                    $value = value;
                }
            }
        };
        nextValue();
        return $value;
    }
}

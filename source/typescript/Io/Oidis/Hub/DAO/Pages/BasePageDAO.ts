/*! ******************************************************************************************************** *
 *
 * Copyright 2018 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { BaseDAO } from "@io-oidis-services/Io/Oidis/Services/DAO/BaseDAO.js";
import { BasePageDAO as Parent } from "@io-oidis-services/Io/Oidis/Services/DAO/BasePageDAO.js";

export class BasePageDAO extends Parent {
    public static defaultConfigurationPath : string =
        "resource/data/Io/Oidis/Hub/Localization/BasePageLocalization.jsonp";

    protected static getDaoInterfaceClassName($interfaceName : string) : any {
        switch ($interfaceName) {
        case "IBasePageConfiguration":
            return BasePageDAO;
        default:
            break;
        }
        return BaseDAO;
    }
}

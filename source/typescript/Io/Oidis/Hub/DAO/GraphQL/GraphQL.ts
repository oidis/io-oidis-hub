/*! ******************************************************************************************************** *
 *
 * Copyright 2019 PASAJA Authors
 * Copyright 2019-2024 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { IModel } from "@io-oidis-commons/Io/Oidis/Commons/Interfaces/IBaseObject.js";
import { BaseModel } from "@io-oidis-commons/Io/Oidis/Commons/Primitives/BaseModel.js";
import { BaseObject } from "@io-oidis-commons/Io/Oidis/Commons/Primitives/BaseObject.js";
import { LogIt } from "@io-oidis-commons/Io/Oidis/Commons/Utils/LogIt.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { Property } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Property.js";
import { Reflection } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Reflection.js";
import { IAuthPromise } from "@io-oidis-localhost/Io/Oidis/Localhost/Interfaces/IResponse.js";
import { IExecutionResult } from "../../Interfaces/DAO/GraphQL/IExecutionResult.js";
import { Loader } from "../../Loader.js";
import { GQLMethod, IGQLDescriptor } from "../../Primitives/Decorators.js";

export class GraphQL extends BaseObject {
    private readonly graphQL : any;
    private gqlSchema : any;
    private readonly typeSchemas : any;
    private graphQLComposer : any;
    private currentToken : string;

    constructor() {
        super();
        this.gqlSchema = null;
        this.graphQL = require("graphql").graphql;
        this.typeSchemas = {};
        this.currentToken = null;
    }

    public CurrentToken($value? : string) : string {
        return this.currentToken = Property.NullString(this.currentToken, $value);
    }

    public RegisterModels($models? : BaseModel[]) : void {
        if (!ObjectValidator.IsSet($models)) {
            $models = Loader.getInstance().getDBDriver().getAllModels();
        }

        const innerQueryRegistrar : any[] = [];
        for (const model of $models) {
            this.getSchemaType(model);
            innerQueryRegistrar.push(this.generateInnerSchema(model));
        }

        if (!ObjectValidator.IsEmptyOrNull(globalThis.oidisAPIGraphQLModels)) {
            for (const model of globalThis.oidisAPIGraphQLModels) {
                const obj = new (model().prototype)();
                this.getSchemaType(obj);
            }
        }

        this.generateSchema();

        // register inner resolvers after the rest
        for (const iqrof of innerQueryRegistrar) {
            iqrof();
        }
        this.gqlSchema = this.getComposer().buildSchema();
    }

    public Execute($query : string, $variableValues? : { [variable : string] : any }) : Promise<IExecutionResult> {
        return this.graphQL({
            contextValue  : {
                invoker: async ($entity : any, $args : any[], $method : any) : Promise<any> => {
                    const promise : IAuthPromise = $method.apply($entity, $args);
                    if (await Loader.getInstance().getAuthManager().IsAuthorizedFor(promise.auth, this.currentToken)) {
                        return promise.resolve.apply($entity, [this.currentToken]);
                    } else {
                        throw new Error("Not authorized for " + promise.auth);
                    }
                },
                token  : this.currentToken
            },
            schema        : this.gqlSchema,
            source        : $query,
            variableValues: $variableValues
        });
    }

    public async ExecuteAll($query : string | string[], $variableValues? : { [variable : string] : any }) : Promise<IExecutionResult[]> {
        if (ObjectValidator.IsArray($query)) {
            LogIt.Debug("Executing multiple requests: " + $query.length);
            const requests : any[] = [];
            for await (const $singleQuery of <string[]>$query) {
                requests.push(await this.Execute($singleQuery, $variableValues));
            }
            return requests;
        } else {
            return [await this.Execute(<string>$query, $variableValues)];
        }
    }

    public getSchema() : any {
        return this.gqlSchema;
    }

    public getRootElement() : any {
        return this.graphQLComposer.getOrCreateOTC("RootGQL");
    }

    public getComposer() : any {
        if (ObjectValidator.IsEmptyOrNull(this.graphQLComposer)) {
            this.graphQLComposer = require("graphql-compose").schemaComposer;
        }
        return this.graphQLComposer;
    }

    // generates gql schema with missing required flags and this schema should be used for filters or data transfer
    protected getSchemaType($model : BaseModel | any) : any {
        if (!($model.getClassName() in this.typeSchemas)) {
            const typeDescriptor : any = {
                description: "Schema descriptor for type " + $model.getClassName(),
                fields     : {},
                name       : this.normalizeModelName($model.getClassNameWithoutNamespace())
            };
            if (!ObjectValidator.IsEmptyOrNull($model.Properties)) {
                for (const property of $model.Properties) {
                    const isArray : boolean = ObjectValidator.IsArray(property.schemaOptions.type);
                    const typeCtor : any = isArray ? property.schemaOptions.type[0] : property.schemaOptions.type;
                    if (ObjectValidator.IsEmptyOrNull(typeCtor)) {
                        LogIt.Warning("Skipping property '" + $model.getClassName() + "." + property.name +
                            "' definition due to missing explicit definition of type");
                        continue;
                    }
                    if (typeCtor.hasOwnProperty("ClassName") && Reflection.getInstance().Implements(new (typeCtor)(), IModel)) {
                        // currently we have same interpretation for ref and inclusion, difference will be handled in resolvers
                        const ctype = this.getSchemaType(new (typeCtor)()).composer;
                        typeDescriptor.fields[property.name] = {
                            type: () => {
                                return isArray ? [ctype] : ctype;
                            }
                        };
                    } else if (typeCtor.hasOwnProperty("__model")) {
                        // currently the same as baseModel, but be careful
                        const ctype = this.getSchemaType(new (typeCtor)()).composer;
                        typeDescriptor.fields[property.name] = {
                            type: () => {
                                return isArray ? [ctype] : ctype;
                            }
                        };
                    } else {
                        if (typeCtor === String) {
                            typeDescriptor.fields[property.name] = "String";
                        } else if (typeCtor === Boolean) {
                            typeDescriptor.fields[property.name] = "Boolean";
                        } else if (typeCtor === Number) {
                            // TODO(mkelnar) numbers are mapped to Float, add support for Integer/float somehow in model definition?
                            typeDescriptor.fields[property.name] = "Float";
                        } else if (typeCtor === Date) {
                            typeDescriptor.fields[property.name] = "Date";
                        } else if (typeCtor === Buffer) {
                            // typeDescriptor.fields[property.name] = "Buffer";
                            // TODO(mkelnar) should we support buffers?
                        } else {
                            throw new Error("Not supported type by GQL schema type resolver: " + typeCtor.toString());
                        }
                        if (isArray && (property.name in typeDescriptor.fields)) {
                            typeDescriptor.fields[property.name] = [typeDescriptor.fields[property.name]];
                        }
                    }
                }
            }

            const composer = this.getComposer().createObjectTC(typeDescriptor);
            this.typeSchemas[$model.getClassName()] = {
                composer,
                descriptor: typeDescriptor
            };
        }
        return this.typeSchemas[$model.getClassName()];
    }

    // generates gql schema with complete set of params (full access to db?)
    protected generateSchema() : any {
        const resolveType : any = ($type, $isInput) : any => {
            let composer;
            const isArray = ObjectValidator.IsArray($type);
            let type = $type;
            if (isArray) {
                type = $type[0];
            }
            if (type.hasOwnProperty("ClassName") && Reflection.getInstance().Implements(new (<any>type)(), IModel)) {
                composer = this.getSchemaType(new (<any>type)()).composer;
            } else if (type.hasOwnProperty("__model")) {
                composer = this.getSchemaType(new (<any>type)()).composer;
            } else if (type === String) {
                composer = "String";
            } else if (type === Number) {
                composer = "Float";
            } else if (type === Boolean) {
                composer = "Boolean";
            } else {
                throw new Error("Not supported type by GQL type resolver: " + type.toString());
            }
            // TODO(mkelnar) rest of data types...
            if ($isInput && (ObjectValidator.IsObject(composer) && ("getInputType" in composer))) {
                composer = composer.getInputType();
            }
            if (isArray) {
                composer = [composer];
            }
            return composer;
        };

        if (!ObjectValidator.IsEmptyOrNull(globalThis.oidisAPIGraphQL)) {
            for (const gql of globalThis.oidisAPIGraphQL) {
                const data : IGQLDescriptor = gql();
                const args : any = {};
                if (!ObjectValidator.IsEmptyOrNull(data.options.args)) {
                    for (const arg of data.options.args) {
                        args[arg.name] = resolveType(arg.type, true);
                    }
                }
                this.getRootElement().addResolver({
                    args,
                    description: data.options?.description,
                    name       : data.options.symbol,
                    resolve    : async ({source, args, context}) => {
                        const argArray : any[] = [];
                        if (!ObjectValidator.IsEmptyOrNull(data.options.args)) {
                            for (const item of data.options.args) {
                                argArray.push(args[item.name]);
                            }
                        }
                        return context.invoker(this, argArray, data.method);
                    },
                    type       : resolveType(data.options.returnType)
                });
                const resReg : any = {};
                resReg[data.options.symbol] = this.getRootElement().getResolver(data.options.symbol);
                if (data.type === GQLMethod.Mutation) {
                    this.getComposer().Mutation.addFields(resReg);
                } else {
                    this.getComposer().Query.addFields(resReg);
                }
            }
        }

        this.gqlSchema = this.getComposer().buildSchema();
        return this.gqlSchema;
    }

    private normalizeModelName($className : string) : string {
        return $className.replace(/[.]/g, "");
    }

    private generateInnerSchema($model : BaseModel) : any {
        // TODO(mkelnar) remove this feature once resolvers behaviour will be fully discovered
        try {
            this.getComposer().getOTC(this.normalizeModelName($model.getClassName()));
            return () => {
                // dummy
            };
        } catch (ex) {
            const mongooseQLComposer : any = require("graphql-compose-mongoose").composeWithMongoose;
            const schemaComposer = mongooseQLComposer($model.getContext().getDocument(), {
                name          : this.normalizeModelName($model.getClassName()),
                schemaComposer: this.getComposer()
            });
            schemaComposer.removeField("_id");
            const symbol : string = $model.getSymbolicName();
            return () => {
                this.getComposer().Query.addFields({
                    [symbol + "All"]: schemaComposer.getResolver("findMany")
                });
            };
        }
    }
}

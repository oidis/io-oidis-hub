/*! ******************************************************************************************************** *
 *
 * Copyright 2021-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { BaseDAO } from "@io-oidis-services/Io/Oidis/Services/DAO/BaseDAO.js";
import { IMailContent, IMailEngineConfiguration } from "../Interfaces/DAO/IMailEngineConfiguration.js";
import { EmailType } from "../Utils/MailEngine.js";
import { Resources } from "./Resources.js";

export class MailEngineDAO extends BaseDAO {
    public static defaultConfigurationPath : string = "resource/data/Io/Oidis/Hub/Configuration/MailEngineConfiguration.jsonp";

    public getStaticConfiguration() : IMailEngineConfiguration {
        return <IMailEngineConfiguration>super.getStaticConfiguration();
    }

    public getContentFor($type : EmailType) : IMailContent {
        return this.getStaticConfiguration().contents[<any>$type];
    }

    protected getResourcesHandler() : any {
        return Resources;
    }
}

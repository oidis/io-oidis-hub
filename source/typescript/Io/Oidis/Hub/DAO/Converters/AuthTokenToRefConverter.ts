/*! ******************************************************************************************************** *
 *
 * Copyright 2020-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { IContext } from "../../Interfaces/DAO/IConverter.js";
import { AuthToken } from "../Models/AuthToken.js";
import { BaseListToRefConverter } from "./BaseListToRefConverter.js";

export class AuthTokenToRefConverter extends BaseListToRefConverter {
    public async from($value : any, $context : IContext) : Promise<any> {
        const items : AuthToken[] = [];
        for await (const item of $value) {
            let instance : any;
            if ($context.dataTranslation) {
                instance = await AuthToken.From(item);
            } else {
                instance = await AuthToken.FindById(item);
            }
            items.push(instance);
        }
        return items;
    }
}

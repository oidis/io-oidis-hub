/*! ******************************************************************************************************** *
 *
 * Copyright 2023 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { IContext } from "../../Interfaces/DAO/IConverter.js";
import { FileEntry } from "../Models/FileEntry.js";
import { BaseToRefConverter } from "./BaseToRefConverter.js";

export class FileEntryToRefConverter extends BaseToRefConverter {
    public async from($value : any, $context : IContext) : Promise<any> {
        if ($context.dataTranslation) {
            return FileEntry.From($value);
        } else {
            return FileEntry.FindById($value);
        }
    }
}

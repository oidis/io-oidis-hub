/*! ******************************************************************************************************** *
 *
 * Copyright 2022-2024 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { InclusionConverter as Parent } from "@io-oidis-commons/Io/Oidis/Commons/DAO/Converters/InclusionConverter.js";

/**
 * @deprecated Use InclusionConverter from Commons namespace
 */
export class InclusionConverter extends Parent {
    // Fallback mapping
}

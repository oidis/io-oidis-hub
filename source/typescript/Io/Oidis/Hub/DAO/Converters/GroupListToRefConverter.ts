/*! ******************************************************************************************************** *
 *
 * Copyright 2021-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { IContext } from "../../Interfaces/DAO/IConverter.js";
import { Group } from "../Models/Group.js";
import { BaseListToRefConverter } from "./BaseListToRefConverter.js";

export class GroupListToRefConverter extends BaseListToRefConverter {
    public async from($value : any, $context : IContext) : Promise<any> {
        const items : Group[] = [];
        for await (const item of $value) {
            let instance : any;
            if ($context.dataTranslation) {
                instance = await Group.From(item);
            } else {
                instance = await Group.FindById(item);
            }
            items.push(instance);
        }
        return items;
    }
}

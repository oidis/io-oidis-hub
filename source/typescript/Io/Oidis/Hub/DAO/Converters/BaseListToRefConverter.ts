/*! ******************************************************************************************************** *
 *
 * Copyright 2021-2024 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { BaseListToRefConverter as Parent } from "@io-oidis-commons/Io/Oidis/Commons/DAO/Converters/BaseListToRefConverter.js";

/**
 * @deprecated Use BaseListToRefConverter from Commons namespace
 */
export abstract class BaseListToRefConverter extends Parent {
    // Fallback mapping
}

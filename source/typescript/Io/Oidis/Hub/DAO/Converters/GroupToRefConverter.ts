/*! ******************************************************************************************************** *
 *
 * Copyright 2021-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { IContext } from "../../Interfaces/DAO/IConverter.js";
import { Group } from "../Models/Group.js";
import { BaseToRefConverter } from "./BaseToRefConverter.js";

export class GroupToRefConverter extends BaseToRefConverter {
    public async from($value : any, $context : IContext) : Promise<any> {
        if ($context.dataTranslation) {
            return Group.From($value);
        } else {
            return Group.FindById($value);
        }
    }
}

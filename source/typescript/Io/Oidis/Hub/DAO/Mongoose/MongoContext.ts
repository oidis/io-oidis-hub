/*! ******************************************************************************************************** *
 *
 * Copyright 2019 PASAJA Authors
 * Copyright 2019-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { BaseDbContext, BaseDbDriver } from "@io-oidis-commons/Io/Oidis/Commons/DAO/BaseDbContext.js";
import { IModel } from "@io-oidis-commons/Io/Oidis/Commons/Interfaces/IBaseObject.js";
import { IModelAggregateOptions } from "@io-oidis-commons/Io/Oidis/Commons/Primitives/BaseModel.js";
import { JsonUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/JsonUtils.js";
import { LogIt } from "@io-oidis-commons/Io/Oidis/Commons/Utils/LogIt.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { Reflection } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Reflection.js";
import { StringUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/StringUtils.js";
import { IModelFindOptions } from "../../Interfaces/DAO/IModel.js";
import { IDbConfiguration } from "../../Interfaces/IProject.js";

export class MongoDriver extends BaseDbDriver {
    private driver : any;
    private isOpen : boolean;

    constructor() {
        super();

        this.driver = require("mongoose");
        this.isOpen = false;
    }

    public async Open($config : IDbConfiguration, $autoReconnect : boolean = false) : Promise<boolean> {
        const options = <any>{
            // this is default authSource used by Oidis based setup of MongoDB, DB authentication without proper specification
            // of authSource is not working in most cases, so override if needed by property in dbConfigs or attributes
            // ** find rest of this presets doc here https://mongoosejs.com/docs/connections.html#multiple_connections
            authSource              : "admin",
            connectTimeoutMS        : 5000,
            serverSelectionTimeoutMS: 5000
            // useCreateIndex          : true,
            // useFindAndModify        : true,
            // useNewUrlParser         : true,
            // useUnifiedTopology      : true,
            // validateOptions         : true
        };
        if (ObjectValidator.IsSet($config.authSource)) {
            options.authSource = $config.authSource;
        }
        if (StringUtils.ContainsIgnoreCase($config.attributes, "authSource=")) {
            delete options.authSource;
        }
        if (ObjectValidator.IsSet($config.user) && ObjectValidator.IsSet($config.pass)) {
            options.user = $config.user;
            options.pass = $config.pass;
        }

        const reflection : Reflection = Reflection.getInstance();
        const baseClasses : string[] = [
            "Io.Oidis.Commons.Primitives.BaseModel", "Io.Oidis.Hub.Gui.Models.BaseModel", "Io.Oidis.Hub.DAO.Models.BaseModel"
        ];
        for (const modelClass of reflection.getAllClasses()) {
            const modelPrototype : any = reflection.getClass(modelClass);
            if (!ObjectValidator.IsEmptyOrNull(modelPrototype)) {
                if (reflection.ClassHasInterface(modelPrototype, IModel) && !baseClasses.includes(modelClass)) {
                    const instance : any = new modelPrototype();
                    if (ObjectValidator.IsSet(instance.getContext) && reflection.IsMemberOf(instance.getContext(), MongoContext)) {
                        if (!this.models.includes(instance)) {
                            this.models.push(instance);
                        }
                        instance.getDocument();  // ensure mongoose model is generated!
                    }
                }
            }
        }

        const connectionString : string = this.getConnectionString($config);
        if (!this.IsOpen()) {
            const connect = async () : Promise<boolean> => {
                this.driver.set("strictQuery", false);
                try {
                    await this.driver.connect(connectionString, options);
                    this.isOpen = true;
                    LogIt.Info("Successfully connected to mongo at {0}", connectionString);
                    this.driver.connection.on("error", ($error : any) : void => {
                        LogIt.Warning("Generic database connection error: " + $error.message);
                        if ($autoReconnect) {
                            this.driver.connection.close(true, async () : Promise<void> => {
                                await connect();
                            });
                        } else {
                            throw $error;
                        }
                    });
                    this.driver.connection.on("reconnected", () : void => {
                        LogIt.Info("MongoDB reconnected.");
                    });
                    this.driver.connection.on("reconnectFailed", () : void => {
                        LogIt.Info("MongoDB reconnect reaches maximal tries limit.");
                    });
                    this.driver.connection.on("disconnected", ($error : any) : void => {
                        if ($autoReconnect) {
                            // reconnecting handled by mongoose driver itself after initial connection established
                            LogIt.Warning("MongoDB disconnected, reconnecting...");
                        } else {
                            LogIt.Warning("MongoDB disconnected.");
                        }
                    });
                } catch (ex) {
                    // according to doc after first connect this should not raised anymore
                    LogIt.Warning("Initial connection error: " + ex.name + ": " + ex.message);
                    if (StringUtils.ContainsIgnoreCase(ex.message, "ECONNREFUSED") && $autoReconnect) {
                        return connect();
                    } else {
                        return false;
                    }
                }
                return true;
            };
            return connect();
        }
        return true;
    }

    public async Close($force : boolean = false) : Promise<boolean> {
        this.isOpen = false;
        await this.driver.connection.close($force);
        return true;
    }

    public IsOpen() : boolean {
        return this.isOpen;
    }

    public getDb() : any {
        return this.driver;
    }

    public getModel($className : string) : any {
        return this.driver.model($className.replace(/[.]/g, ""));
    }

    public getContextFor($model : any) : MongoContext {
        return new MongoContext(this, $model);
    }

    private getConnectionString($dbConfig? : IDbConfiguration) : string {
        let connectionString : string = (ObjectValidator.IsSet($dbConfig.protocol) ? $dbConfig.protocol : "mongodb") + "://";
        connectionString += ObjectValidator.IsSet($dbConfig.location) ? $dbConfig.location : "localhost";
        connectionString += ObjectValidator.IsSet($dbConfig.port) && $dbConfig.port > 0 ? ":" + $dbConfig.port : "";
        connectionString += ObjectValidator.IsSet($dbConfig.dbName) ? "/" + $dbConfig.dbName : "";
        connectionString += ObjectValidator.IsSet($dbConfig.attributes) ? ("?" + $dbConfig.attributes) : "";
        return connectionString;
    }
}

export class MongoContext extends BaseDbContext {
    private mongoSchema : any;

    constructor($driver : BaseDbDriver, $model? : any) {
        super($driver, $model);
        this.mongoSchema = null;
    }

    public CreateDocument($className : string, $schema : any, $collection : string) : any {
        let model : any;
        const className : string = $className.replace(/[.]/g, "");
        if (!this.driver.getDb().modelNames().includes(className)) {
            model = this.driver.getDb().model(className, $schema, $collection);
        } else {
            model = this.driver.getModel($className);
        }
        return model();
    }

    /**
     * Low-level access directly into Mongoose model for this type which could be used for more complex requests like
     *     BaseModel.Model().find(...).where(...).[gt(...),...].exec()
     * Note: set https://mongoosejs.com/docs/models.html for more info and remember that NO type-checks or any advanced features
     *   will be applied on operation, so it is fully under your control.
     * @returns Returns underlying Mongoose model.
     */
    public getDocument() : any {
        return this.driver.getModel(this.Model.getClassName());
    }

    public async FindById<T>($id : string, $options? : IModelFindOptions) : Promise<T> {
        this.backendOnlyAssert();
        const object : any = this.getModelInstance();
        const value : any = await this.getDocument().findById($id).exec();
        if (!ObjectValidator.IsEmptyOrNull(value)) {
            object.document = value;
            await object.fromModel($options);
            return object;
        }
        return null;
    }

    public async Find<T>($query : any, $options? : IModelFindOptions) : Promise<T[]> {
        this.backendOnlyAssert();
        const objects : T[] = [];
        let context : any = this.getDocument().find($query);
        if (!ObjectValidator.IsEmptyOrNull($options)) {
            if (!ObjectValidator.IsEmptyOrNull($options.sort)) {
                context = context.sort($options.sort);
                if ($options.allowDiskUse === true) {
                    context = context.allowDiskUse();
                }
            }
            if (ObjectValidator.IsInteger($options.offset) && $options.offset > 0) {
                context = context.skip($options.offset);
            }
            if (ObjectValidator.IsInteger($options.limit) && $options.limit > 0) {
                context = context.limit($options.limit);
            }
        }
        const values : any[] = await context.exec();
        for await (const value of values) {
            const object : any = this.getModelInstance();
            object.document = value;
            await object.fromModel($options);
            objects.push(object);
        }
        return objects;
    }

    /**
     * Data aggregation is an efficient alternative to Find function. Since this method represents advanced access to db then it needs
     * also more effort to design proper aggregation query.
     * @param {any[] | IModelAggregateOptions} $chain Specify full aggregation or its partials. In the case of partials the aggregation
     * will use filterQuery as first step and after this part it adds also resultQuery
     * as "post-processing" (useful for data transformation and lookup).
     * @param {IModelFindOptions} [$options] Specify options for model conversions
     * @returns {Promise<any>} Returns object which contains two properties result:[<aggregation results>],
     * total count of found documents before limit/skip application
     */
    public async Aggregate<T>($chain : any[] | IModelAggregateOptions, $options? : IModelFindOptions) : Promise<any> {
        this.backendOnlyAssert();
        let isCustomQuery : boolean = false;
        let query : any[];
        if (ObjectValidator.IsArray($chain)) {
            isCustomQuery = true;
            query = <any[]>$chain;
        } else {
            const options : IModelAggregateOptions = <IModelAggregateOptions>$chain;
            if (ObjectValidator.IsEmptyOrNull(options.sortQuery)) {
                options.sortQuery = {
                    created: -1
                };
            }
            query = [
                {
                    $match: {
                        $and: options.filterQuery
                    }
                },
                {
                    $sort: options.sortQuery
                },
                {
                    $facet: {
                        results   : options.resultQuery,
                        totalCount: [
                            {
                                $count: "count"
                            }
                        ]
                    }
                },
                {
                    $project: {
                        results: 1,
                        total  : {$arrayElemAt: ["$totalCount.count", 0]}
                    }
                }
            ];
        }

        // const prototype : any = <any>this;
        const objects : T[] = [];
        const result : any[] = await this.getDocument().aggregate(query);
        let total : number = -1;
        let values : any[];
        if (result.length > 0 && result[0].hasOwnProperty("results") && result[0].hasOwnProperty("total")) {
            values = result[0].results;
            total = result[0].total;
        } else {
            values = result;
        }
        for await (const value of values) {
            const object : any = this.getModelInstance();
            object.document = value;
            await object.fromModel(JsonUtils.Extend({dataTranslation: true}, $options));
            objects.push(object);
        }
        if (isCustomQuery) {
            return objects;
        }
        return {results: objects, total};
    }

    public async Save() : Promise<void> {
        this.backendOnlyAssert();
        (<any>this.Model).document.parent = this.Model;
        await (<any>this.Model).document.save();
        delete (<any>this.Model).document.parent;
    }

    public async Update($property : string) : Promise<void> {
        this.backendOnlyAssert();
        for await (const property of this.Model.Properties) {
            if (property.name === $property || property.symbol === $property) {
                const setProperty : any = {};
                const value : any = this[property.fieldKey];
                if (ObjectValidator.IsObject(property) && Reflection.getInstance().Implements(value, IModel)) {
                    setProperty[property.symbol] = await property.converter.to(value, {
                        dataTranslation: false,
                        parent         : this.Model,
                        property
                    });
                } else {
                    setProperty[property.symbol] = value;
                }
                await this.Model.getDocument().updateOne({$set: setProperty}).exec();
            }
        }
    }

    public async Refresh($options? : IModelFindOptions) : Promise<void> {
        this.backendOnlyAssert();
        if (!ObjectValidator.IsEmptyOrNull(this.Model.Id)) {
            const value : any = await this.getDocument().findById(this.Model.Id).exec();
            if (!ObjectValidator.IsEmptyOrNull(value)) {
                (<any>this.Model).document = value;
                await (<any>this.Model).fromModel($options);
            }
        }
    }

    public async Remove($property? : string, $child? : string) : Promise<void> {
        this.backendOnlyAssert();
        if (ObjectValidator.IsEmptyOrNull($property)) {
            await this.getDocument().deleteOne({_id: this.Model.Id});
        } else {
            for await (const property of this.Model.Properties) {
                if (property.name === $property || property.symbol === $property) {
                    const unset : any = {};
                    let name : string = property.symbol;
                    if (!ObjectValidator.IsEmptyOrNull($child)) {
                        name += "." + $child;
                    }
                    unset[name] = "";
                    await this.Model.getDocument().updateOne({$unset: unset}).exec();
                }
            }
        }
    }

    public DocumentToObject($property? : any) : any {
        if (ObjectValidator.IsSet($property)) {
            if (ObjectValidator.IsString($property)) {
                return this.Model.getDocument()[$property].toObject();
            }
            return $property.toObject();
        }
        return this.Model.getDocument().toJSON();
    }

    public async Validate() : Promise<boolean> {
        return this.Model.getDocument().validate();
    }

    public getSchema() : any {
        if (!ObjectValidator.IsEmptyOrNull(this.mongoSchema)) {
            return this.mongoSchema;
        }
        this.backendOnlyAssert();
        this.mongoSchema = new (this.driver.getDb()).Schema(super.getSchema());
        this.mongoSchema.pre("save", function ($next : () => void) : void {
            if (this.isNew) {
                if ((<any>this).constructor.name !== "EmbeddedDocument" && (<any>this).constructor.name !== "SingleNested") {
                    this.created = new Date();
                    this.parent.Created = this.created;
                }
            }
            if (this.isModified()) {
                if ((<any>this).constructor.name !== "EmbeddedDocument" && (<any>this).constructor.name !== "SingleNested") {
                    this.lastModified = new Date();
                    this.parent.LastModified = this.lastModified;
                }
            }
            $next();
        });

        return this.mongoSchema;
    }
}

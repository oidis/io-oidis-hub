/*! ******************************************************************************************************** *
 *
 * Copyright 2022-2023 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { HttpStatusType } from "@io-oidis-commons/Io/Oidis/Commons/Enums/HttpStatusType.js";
import { LogIt } from "@io-oidis-commons/Io/Oidis/Commons/Utils/LogIt.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { StringUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/StringUtils.js";
import { StreamResponse } from "@io-oidis-localhost/Io/Oidis/Localhost/HttpProcessor/ResponseApi/Handlers/StreamResponse.js";
import { IHttpResponse } from "@io-oidis-localhost/Io/Oidis/Localhost/Interfaces/IRoute.js";
import { BaseConnector } from "@io-oidis-localhost/Io/Oidis/Localhost/Primitives/BaseConnector.js";
import { Route } from "@io-oidis-localhost/Io/Oidis/Localhost/Primitives/Decorators.js";
import { Loader } from "../../Loader.js";

export class LogsFetch extends BaseConnector {

    @Route("GET /logs?date=int(0)&format=txt", {responseHandler: new StreamResponse()})
    public async getLogs($date? : number, $format? : string) : Promise<IHttpResponse> {
        let date : Date = new Date();
        if (!ObjectValidator.IsEmptyOrNull($date) && $date > 0) {
            date = new Date($date);
        }
        $format = StringUtils.ToLowerCase(StringUtils.Remove($format, "."));
        if ($format !== "txt" && $format !== "json") {
            LogIt.Warning("Unsupported log format \"{0}\". Use \"txt\" or \"json\" instead.", $format);
        }
        const year : string = date.getUTCFullYear() + "";
        const day : string = date.getUTCDate() < 10 ? "0" + date.getUTCDate() : date.getUTCDate() + "";
        const monthNumber : number = date.getUTCMonth() + 1;
        const month : string = monthNumber < 10 ? "0" + monthNumber : monthNumber + "";
        const path : string = "./log/" + year + "/" + month + "/" + day + "_" + month + "_" + year;
        return {
            body   : Loader.getInstance().getFileSystemHandler().Read(path + "." + $format).toString("utf-8"),
            headers: {
                "content-type": $format === "txt" ? "text/plain; charset=UTF-8" : "application/json; charset=UTF-8"
            },
            status : HttpStatusType.SUCCESS
        };
    }
}

/*! ******************************************************************************************************** *
 *
 * Copyright 2022-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { LogLevel } from "@io-oidis-commons/Io/Oidis/Commons/Enums/LogLevel.js";
import { BaseAdapter } from "@io-oidis-commons/Io/Oidis/Commons/LogProcessor/Adapters/BaseAdapter.js";
import { ILoggerTrace } from "@io-oidis-commons/Io/Oidis/Commons/LogProcessor/Logger.js";
import { StringUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/StringUtils.js";
import { ITraceBackEntry } from "@io-oidis-commons/Io/Oidis/Commons/Utils/TraceBack.js";
import { Loader } from "../../Loader.js";

export class CrashAdapter extends BaseAdapter {
    private buffer : ILoggerTrace[];
    private lastMessage : string;
    private readonly ignoreList : string[];

    constructor() {
        super();
        this.buffer = [];
        this.ignoreList = ["None or multiple users has been found", "User pass not valid"];
    }

    public getIgnoreList() : string[] {
        return this.ignoreList;
    }

    protected timeToString($value : number) : string {
        return <any>$value;
    }

    protected traceBackToString($traceback : ITraceBackEntry) : any {
        return $traceback;
    }

    protected formatter($trace : ILoggerTrace) : ILoggerTrace {
        return $trace;
    }

    protected print($trace : ILoggerTrace) : void {
        if (this.allowPrintFor($trace)) {
            this.buffer.push($trace);
            if (this.buffer.length === 1) {
                this.printAsync();
            }
        }
    }

    protected allowPrintFor($trace : ILoggerTrace) : boolean {
        if ($trace.level === LogLevel.ERROR) {
            const thisMessage : string = StringUtils.Remove($trace.message, "> FE: ");
            /// TODO: this uses cases should be solved by throwing of custom errors and handled by error code instead of messages
            if (this.lastMessage !== thisMessage && !StringUtils.ContainsIgnoreCase(thisMessage, ...this.ignoreList)) {
                this.lastMessage = thisMessage;
                return true;
            }
        }
        return false;
    }

    private printAsync() : void {
        const startLength : number = this.buffer.length;
        if (this.buffer.length > 0) {
            // TODO: consider also ability to send error logs to slack or other system
            Loader.getInstance().getMailEngine().ReportFailure(this.buffer)
                .then(() : void => {
                    if (startLength === this.buffer.length) {
                        this.buffer = [];
                    } else {
                        /// TODO: is this good way how to create bulks?
                        //        > maybe it should be handle by print method directly
                        setTimeout(() : void => {
                            this.buffer = this.buffer.slice(startLength);
                            this.printAsync();
                        }, 5 * 60 * 1000); // 5 minutes
                    }
                })
                .catch((ex) : void => {
                    console.error(ex); // eslint-disable-line no-console
                });
        }
    }
}

/*! ******************************************************************************************************** *
 *
 * Copyright 2022-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { LogLevel } from "@io-oidis-commons/Io/Oidis/Commons/Enums/LogLevel.js";
import { Convert } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Convert.js";
import { JsonUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/JsonUtils.js";
import { LogIt } from "@io-oidis-commons/Io/Oidis/Commons/Utils/LogIt.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { ISystemLogFilter, ISystemLogTrace } from "@io-oidis-hub-gui/Io/Oidis/Hub/Gui/Connectors/SystemLogConnector.js";
import { BaseConnector } from "@io-oidis-localhost/Io/Oidis/Localhost/Primitives/BaseConnector.js";
import { Extern } from "@io-oidis-localhost/Io/Oidis/Localhost/Primitives/Decorators.js";
import { IModelListResult } from "@io-oidis-services/Io/Oidis/Services/Connectors/AuthManagerConnector.js";
import { LogEntry } from "../DAO/Models/LogEntry.js";
import { User } from "../DAO/Models/User.js";
import { ISysLogConfiguration } from "../Interfaces/IProject.js";
import { Loader } from "../Loader.js";

export class SystemLog extends BaseConnector {
    private config : ISysLogConfiguration;

    public static async Info($message : string) : Promise<void> {
        await this.getInstance<SystemLog>().Info($message);
    }

    public static async Warning($message : string) : Promise<void> {
        await this.getInstance<SystemLog>().Warning($message);
    }

    public static async Error($message : string) : Promise<void> {
        await this.getInstance<SystemLog>().Error($message);
    }

    public static async Trace($trace : ISystemLogTrace) : Promise<void> {
        await this.getInstance<SystemLog>().Trace($trace);
    }

    public static async Clean() : Promise<number> {
        return this.getInstance<SystemLog>().Clean();
    }

    constructor() {
        super();
        this.config = Loader.getInstance().getAppConfiguration().sysLog;
    }

    public async Info($message : string) : Promise<void> {
        await this.process({message: $message, level: LogLevel.INFO});
    }

    public async Warning($message : string) : Promise<void> {
        await this.process({message: $message, level: LogLevel.WARNING});
    }

    public async Error($message : string) : Promise<void> {
        await this.process({message: $message, level: LogLevel.ERROR});
    }

    public async Trace($trace : ISystemLogTrace) : Promise<void> {
        await this.process($trace);
    }

    @Extern()
    public async getLogsList($filter : ISystemLogFilter) : Promise<IModelListResult> {
        const models : LogEntry[] = [];
        let limit : number = -1;
        let offset : number = 0;
        let ascending : boolean = true;
        const query : any = {};
        if (!ObjectValidator.IsEmptyOrNull($filter.date)) {
            query.created = {};
            query.created.$gte = new Date($filter.date);
            const to : Date = new Date($filter.date);
            to.setDate(to.getDate() + 1);
            query.created.$lte = to;
        }
        if (!ObjectValidator.IsEmptyOrNull($filter.traceBack)) {
            query.traceBack = $filter.traceBack;
        }
        if (!ObjectValidator.IsEmptyOrNull($filter.tags)) {
            query.tags = {$all: $filter.tags};
        }
        if (!ObjectValidator.IsEmptyOrNull($filter.limit)) {
            limit = $filter.limit;
        }
        if (!ObjectValidator.IsEmptyOrNull($filter.offset)) {
            offset = $filter.offset;
        }
        if (!ObjectValidator.IsEmptyOrNull($filter.ascending)) {
            ascending = $filter.ascending;
        }
        // LogIt.Debug("Get syslogs for: {0}", query);
        const size : number = await LogEntry.Model().countDocuments(query);
        const entries : LogEntry[] = await LogEntry.Find(query, {
            limit,
            offset,
            sort: {created: ascending ? 1 : -1}
        });
        if (!ObjectValidator.IsEmptyOrNull(entries)) {
            for await (const entry of entries) {
                const serialized : any = await entry.ToJSON();
                models.push(serialized);
            }
        }
        return {
            data: models,
            limit,
            offset,
            size
        };
    }

    @Extern()
    public async Clean() : Promise<number> {
        if (this.config.enabled) {
            const trigger : Date = new Date();
            trigger.setDate(trigger.getDate() - this.config.maxAge);
            const logs : LogEntry[] = await LogEntry.Find({created: {$lte: trigger}});
            const toBeRemoved : number = ObjectValidator.IsEmptyOrNull(logs) ? 0 : logs.length;
            if (toBeRemoved > 0) {
                LogIt.Warning("Removing " + toBeRemoved + " system logs older than " + Convert.TimeToGMTformat(trigger));
                for await (const log of logs) {
                    await log.Remove();
                }
            } else {
                LogIt.Debug("System logs are already cleaned up");
            }
            return toBeRemoved;
        } else {
            LogIt.Warning("SystemLog clean has been skipped due to sysLog.enabled configuration");
        }
        return 0;
    }

    private async process($trace : ISystemLogTrace) : Promise<void> {
        if (this.config.enabled) {
            try {
                const trace : ISystemLogTrace = JsonUtils.Extend(<ISystemLogTrace>{
                    message  : "",
                    traceBack: this.getClassNameWithoutNamespace(),
                    level    : LogLevel.INFO,
                    tags     : [],
                    onBehalf : null,
                    token    : this.getCurrentToken()
                }, $trace);

                const model : LogEntry = new LogEntry();
                model.Message = trace.message;
                model.TraceBack = trace.traceBack;
                model.User = await this.getCurrentUser(trace.token);
                model.OnBehalf = trace.onBehalf;
                model.Tags = [LogLevel[trace.level]].concat(trace.tags);
                await model.Save();
            } catch (ex) {
                LogIt.Error(ex);
            }
        }
    }

    private async getCurrentUser($token : string) : Promise<string> {
        const user : User = await Loader.getInstance().getAuthManager().getUser($token);
        if (ObjectValidator.IsEmptyOrNull(user)) {
            return "SYSTEM";
        }
        /// TODO: user ID will be better but it can be issue when user will be deleted and it will slow down data fetch
        //        > maybe this part can be somehow configurable including anonymization option
        if (!ObjectValidator.IsEmptyOrNull(user.Firstname) && !ObjectValidator.IsEmptyOrNull(user.Lastname)) {
            return user.Lastname + " " + user.Firstname;
        }
        return user.UserName;
    }
}

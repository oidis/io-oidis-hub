/*! ******************************************************************************************************** *
 *
 * Copyright 2017-2019 NXP
 * Copyright 2019-2024 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { IndexPage } from "@io-oidis-gui/Io/Oidis/Gui/Pages/IndexPage.js";
import { DashboardPageViewer } from "@io-oidis-hub-gui/Io/Oidis/Hub/Gui/Viewers/Pages/DashboardPageViewer.js";
import { SSOPageViewer } from "@io-oidis-hub-gui/Io/Oidis/Hub/Gui/Viewers/Pages/SSOPageViewer.js";
import { AsyncBasePageController } from "@io-oidis-services/Io/Oidis/Services/HttpProcessor/Resolvers/AsyncBasePageController.js";
import { AgentsRegisterTest } from "./RuntimeTests/AgentsRegisterTest.js";
import { AuthorizationTest } from "./RuntimeTests/AuthorizationTest.js";
import { ConfigurationManagerTest } from "./RuntimeTests/ConfigurationManagerTest.js";
import { FileUploadTest } from "./RuntimeTests/FileUploadTest.js";
import { ModelOnFETest } from "./RuntimeTests/ModelOnFETest.js";
import { ReportAPITest } from "./RuntimeTests/ReportAPITest.js";
import { SelfupdateManagerTest } from "./RuntimeTests/SelfupdateManagerTest.js";
import { Snippets } from "./RuntimeTests/Snippets.js";

export class Index extends AsyncBasePageController {

    constructor() {
        super();
        this.setInstanceOwner(new IndexPage());
    }

    protected async onSuccess($instance : IndexPage) : Promise<void> {
        $instance.headerTitle.Content("Oidis Hub");
        $instance.headerInfo.Content("Oidis Framework synchronization hub");

        let content : string = "";
        /* dev:start */
        content += `
            <h3>Pages</h3>
            <a href="${DashboardPageViewer.CallbackLink(true)}">Dashboard page</a>
            <a href="${SSOPageViewer.CallbackLink(true)}">SSO page</a>
            <a href="${this.getRequest().getHostUrl() + "graphiql"}">GraphiQL</a>

            <h3>Runtime tests</h3>
            <a href="${Snippets.CallbackLink()}">Snippets</a>
            <a href="${AuthorizationTest.CallbackLink()}">Authorization test</a>
            <a href="${ConfigurationManagerTest.CallbackLink()}">Configuration Manager test</a>
            <a href="${SelfupdateManagerTest.CallbackLink()}">Selfupdate Manager test</a>
            <a href="${ReportAPITest.CallbackLink()}">Report test</a>
            <a href="${FileUploadTest.CallbackLink()}">File upload/download test</a>
            <a href="${AgentsRegisterTest.CallbackLink()}">AgentsRegisterTest</a>
            <a href="${ModelOnFETest.CallbackLink()}">Model on front-end test</a>
            `;
        /* dev:end */

        $instance.pageIndex.Content(content);
        $instance.footerInfo.Content("" +
            "version: " + this.getEnvironmentArgs().getProjectVersion() +
            ", build: " + this.getEnvironmentArgs().getBuildTime() +
            ", " + this.getEnvironmentArgs().getProjectConfig().target.copyright);
    }
}

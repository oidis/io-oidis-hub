/*! ******************************************************************************************************** *
 *
 * Copyright 2018 NXP
 * Copyright 2019-2024 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { Property } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Property.js";
import { StringUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/StringUtils.js";
import { ProgramArgs as Parent } from "@io-oidis-localhost/Io/Oidis/Localhost/Structures/ProgramArgs.js";
import { Threads } from "../Utils/Threads.js";

export class ProgramArgs extends Parent {
    private isAgent : boolean;
    private resetDB : boolean;

    constructor() {
        super();
        this.isAgent = false;
        this.resetDB = false;
    }

    public IsAgent($value? : boolean) : boolean {
        return this.isAgent = Property.Boolean(this.isAgent, $value);
    }

    public WithDBReset($value? : boolean) : boolean {
        return this.resetDB = Property.Boolean(this.resetDB, $value);
    }

    public PrintHelp() : void {
        this.getHelp(
            "" +
            "Oidis Framework Hub                                                          \n" +
            "   - Synchronization server for Oidis Framework services                     \n" +
            "                                                                             \n" +
            "   copyright:         Copyright 2017-2019 NXP                                \n" +
            "                      Copyright " + StringUtils.YearFrom(2019) + " Oidis                              \n" +
            "   author:            Oidis z.s., info@oidis.io                              \n" +
            "   license:           BSD-3-Clause License distributed with this material    \n",

            "" +
            "Basic options:                                                               \n" +
            "   -h [ --help ]      Prints application help description.                   \n" +
            "   -v [ --version ]   Prints application version message.                    \n" +
            "   -p [ --path ]      Print current Oidis Host location.                     \n" +
            "                                                                             \n" +
            "Server options:                                                              \n" +
            "   start              Start localhost service.                               \n" +
            "   stop               Stop localhost service.                                \n" +
            "   restart            Restart localhost service.                             \n" +
            "   list-routes        List available routes.                                 \n" +
            "                                                                             \n" +
            "Other options:                                                               \n" +
            "   --target=<path>    Specify target *.html file to host by localhost.       \n" +
            "   --agent            Specify if current instance should be in agent role.   \n" +
            "   --reset-db         Use this option for force synchronization of DB.       \n"
        );
    }

    public processArg($key : string, $value : string) : boolean {
        switch ($key) {
        case "--agent":
            this.IsAgent(true);
            break;
        case "--reset-db":
            this.WithDBReset(true);
            break;
        default:
            return super.processArg($key, $value);
        }
        return true;
    }

    protected processDefaultArgs() : void {
        if (!Threads.IsMainThread()) {
            this.IsHelp(false);
            this.StartServer(false);
        } else {
            super.processDefaultArgs();
        }
    }
}

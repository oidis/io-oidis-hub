/*! ******************************************************************************************************** *
 *
 * Copyright 2017-2019 NXP
 * Copyright 2019-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { BaseDbDriver } from "@io-oidis-commons/Io/Oidis/Commons/DAO/BaseDbContext.js";
import { SentryAdapter } from "@io-oidis-commons/Io/Oidis/Commons/LogProcessor/Adapters/SentryAdapter.js";
import { ILoggerTrace } from "@io-oidis-commons/Io/Oidis/Commons/LogProcessor/Logger.js";
import { isBrowser } from "@io-oidis-commons/Io/Oidis/Commons/Utils/EnvironmentHelper.js";
import { JsonUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/JsonUtils.js";
import { LogIt } from "@io-oidis-commons/Io/Oidis/Commons/Utils/LogIt.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { Reflection } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Reflection.js";
import { StringUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/StringUtils.js";
import { ForwardingAdapter } from "@io-oidis-hub-gui/Io/Oidis/Hub/Gui/LogProcessor/Adapters/ForwardingAdapter.js";
import { InMemoryAdapter } from "@io-oidis-hub-gui/Io/Oidis/Hub/Gui/LogProcessor/Adapters/InMemoryAdapter.js";
import { LogAdapterType } from "@io-oidis-localhost/Io/Oidis/Localhost/Enums/LogAdapterType.js";
import { EventsManager } from "@io-oidis-localhost/Io/Oidis/Localhost/Events/EventsManager.js";
import { Loader as Parent } from "@io-oidis-localhost/Io/Oidis/Localhost/Loader.js";
import { EnvironmentState } from "@io-oidis-services/Io/Oidis/Services/Enums/EnvironmentState.js";
import { GraphQL } from "./DAO/GraphQL/GraphQL.js";
import { AppSettings } from "./DAO/Models/AppSettings.js";
import { ConfigEntry } from "./DAO/Models/ConfigEntry.js";
import { FileEntry, FileEntryType } from "./DAO/Models/FileEntry.js";
import { Organization } from "./DAO/Models/Organization.js";
import { User } from "./DAO/Models/User.js";
import { MongoDriver } from "./DAO/Mongoose/MongoContext.js";
import { LoaderEventType } from "./Enums/LoaderEventType.js";
import "./ForceCompile.js";
import { HttpResolver } from "./HttpProcessor/HttpResolver.js";
import { HttpServer } from "./HttpProcessor/HttpServer.js";
import { ForwardingResolver } from "./HttpProcessor/Resolvers/ForwardingResolver.js";
import { ILoaderEvents } from "./Interfaces/ILoaderEvents.js";
import { IDbConfiguration, IDefaultUserConfiguration, IProject } from "./Interfaces/IProject.js";
import { CrashAdapter } from "./LogProcessor/Adapters/CrashAdapter.js";
import { SystemLog } from "./LogProcessor/SystemLog.js";
import { ProgramArgs } from "./Structures/ProgramArgs.js";
import { AdminInitHelper, AuthManager } from "./Utils/AuthManager.js";
import { EnvironmentHelper, EnvironmentStatePool } from "./Utils/EnvironmentHelper.js";
import { MailEngine } from "./Utils/MailEngine.js";
import { PushNotifications } from "./Utils/PushNotifications.js";
import { Threads } from "./Utils/Threads.js";
import { WebhooksManager } from "./Webhooks/WebhooksManager.js";

export class Loader extends Parent {
    private authManager : AuthManager;
    private webhooksManager : WebhooksManager;
    private mailEngine : MailEngine;
    private readonly events : EventsManager;
    private readonly memoryAdapter : InMemoryAdapter;
    private forwardingAdapter : ForwardingAdapter;
    private readonly state : EnvironmentStatePool;
    private crashAdapter : CrashAdapter;
    private readonly notifications : PushNotifications;
    private serviceUser : User;
    private environmentHelper : EnvironmentHelper;

    public static getInstance() : Loader {
        return <Loader>super.getInstance();
    }

    constructor() {
        super();

        this.events = new EventsManager();
        this.memoryAdapter = new InMemoryAdapter();
        if (isBrowser) {
            LogIt.getLogger().AddAdapter(this.memoryAdapter);
        } else {
            this.notifications = new PushNotifications();
        }
        this.state = this.initStatePool();
        this.state.EndState(EnvironmentState.LOADED);
    }

    public getAppConfiguration() : IProject {
        return <IProject>super.getAppConfiguration();
    }

    public getEventsManager() : EventsManager {
        return this.events;
    }

    public getHttpResolver() : HttpResolver {
        return <HttpResolver>super.getHttpResolver();
    }

    public getProgramArgs() : ProgramArgs {
        return <ProgramArgs>super.getProgramArgs();
    }

    public getAuthManager() : AuthManager {
        if (!ObjectValidator.IsSet(this.authManager)) {
            this.authManager = this.initAuthManager();
            this.authManager.setOnTokenExpireHandler(async ($token : string) : Promise<void> => {
                await this.getConnectorsRegister().Destroy($token);
            });
            this.getAuthManager = () : AuthManager => {
                return this.authManager;
            };
        }
        return this.authManager;
    }

    public getWebhooksManager() : WebhooksManager {
        if (!ObjectValidator.IsSet(this.webhooksManager)) {
            this.webhooksManager = this.initWebhooksManager();
            this.getWebhooksManager = () : WebhooksManager => {
                return this.webhooksManager;
            };
        }
        return this.webhooksManager;
    }

    public getMailEngine() : MailEngine {
        if (!ObjectValidator.IsSet(this.mailEngine)) {
            this.mailEngine = this.initMailEngine();
            this.getMailEngine = () : MailEngine => {
                return this.mailEngine;
            };
        }
        return this.mailEngine;
    }

    public getEnvironmentHelper() : EnvironmentHelper {
        if (!ObjectValidator.IsSet(this.environmentHelper)) {
            this.environmentHelper = this.initEnvironmentHelper();
            this.getEnvironmentHelper = () : EnvironmentHelper => {
                return this.environmentHelper;
            };
        }
        return this.environmentHelper;
    }

    public getEvents() : ILoaderEvents {
        return {
            OnAgentDisconnected: ($handler : any) : void => {
                this.getEventsManager().setEvent(this.getClassName(), LoaderEventType.ON_AGENT_DISCONNECTED, $handler);
            },
            OnDbConnected      : ($handler : any) : void => {
                this.events.setEvent(this.getClassName(), LoaderEventType.ON_DB_CONNECTED, $handler);
            },
            OnDbPrepared       : ($handler : any) : void => {
                this.events.setEvent(this.getClassName(), LoaderEventType.ON_DB_PREPARED, $handler);
            },
            OnNewAgentConnected: ($handler : any) : void => {
                this.getEventsManager().setEvent(this.getClassName(), LoaderEventType.ON_NEW_AGENT_CONNECTED, $handler);
            }
        };
    }

    public ApplicationLog() : ILoggerTrace[] {
        return this.memoryAdapter.getLogs();
    }

    public getStatePool() : EnvironmentStatePool {
        return this.state;
    }

    public setLogsContext($value : string) : void {
        if (!ObjectValidator.IsEmptyOrNull(this.forwardingAdapter)) {
            this.forwardingAdapter.setContext($value);
        }
        (<SentryAdapter>LogIt.getLogger().getAdapter(LogAdapterType.SENTRY)).setContext($value);
    }

    public getCrashReportIgnoreList() : string[] {
        if (!ObjectValidator.IsEmptyOrNull(this.crashAdapter)) {
            return this.crashAdapter.getIgnoreList();
        }
        return [];
    }

    public PushNotifications() : PushNotifications {
        return this.notifications;
    }

    public getServiceAccount() : User {
        return this.serviceUser;
    }

    public getGQLContext() : GraphQL {
        const instance : GraphQL = new GraphQL();
        this.getGQLContext = () : GraphQL => {
            return instance;
        };
        return instance;
    }

    public getAdminHelper() : AdminInitHelper {
        const instance : AdminInitHelper = new AdminInitHelper();
        this.getAdminHelper = () : AdminInitHelper => {
            return instance;
        };
        return instance;
    }

    protected initProgramArgs() : ProgramArgs {
        return new ProgramArgs();
    }

    protected initHttpServer() : HttpServer {
        return new HttpServer();
    }

    protected initResolver() : HttpResolver {
        return new HttpResolver("/");
    }

    protected initDbDriver() : BaseDbDriver {
        return new MongoDriver();
    }

    /**
     * It is recommended to returns singleton created directly from BaseConnector to share single instance
     * with one in LCP resolver. Using a new instance could lead to undefined behaviour based on new features in overridden
     * AuthManager child class.
     * @returns {AuthManager} Returns new AuthManager instance suitable for this Entry Point.
     */
    protected initAuthManager() : AuthManager {
        return AuthManager.getInstance();
    }

    protected initWebhooksManager() : WebhooksManager {
        return WebhooksManager.getSingleton();
    }

    protected initMailEngine() : MailEngine {
        return new MailEngine();
    }

    protected initStatePool() : EnvironmentStatePool {
        return new EnvironmentStatePool();
    }

    protected initEnvironmentHelper() : EnvironmentHelper {
        return EnvironmentHelper.getInstance();
    }

    protected processProgramArgs() : boolean {
        const isMainThread : boolean = Threads.IsMainThread();
        if (this.getProgramArgs().IsAgent()) {
            this.getProgramArgs().StartServer(false);
        }
        let processed : boolean = super.processProgramArgs();
        if (this.getProgramArgs().IsAgent() && !processed) {
            processed = true;
            process.stdout.write(this.printLogo());
            ForwardingResolver.CreateTunnels();
        }
        if (!isMainThread) {
            processed = true;
        }

        this.state.State(EnvironmentState.STARTING);
        this.getEvents().OnDbPrepared(async () : Promise<void> => {
            await this.onDbPrepared();
        });
        this.getEvents().OnDbConnected(async () : Promise<void> => {
            await this.onDbConnected();
        });
        this.state.State(EnvironmentState.DB_CONNECTING);
        const profile : string = !this.getEnvironmentArgs().IsProductionMode() ? "dev" : "prod";
        const config : IDbConfiguration = JsonUtils.Extend(this.getAppConfiguration().dbConfigs,
            this.getAppConfiguration().dbConfigs[profile]);

        LogIt.Debug("Database \"" + config.dbName + "\" location: " + config.location + ":" + config.port);
        (async () : Promise<void> => {
            if (!await this.getDBDriver().Open(config, true)) {
                LogIt.Warning("Can not connect to BE database. Some features will not be available.");
                this.state.State(EnvironmentState.DB_NOT_CONNECTED);
            } else {
                this.events.FireEvent(this.getClassName(), LoaderEventType.ON_DB_CONNECTED);
            }
        })().catch(($error : Error) : void => {
            LogIt.Warning("Failed to connect to BE database: {0}", $error.message);
            this.state.State(EnvironmentState.DB_CONNECTION_ERROR);
        });
        return processed;
    }

    protected async onDbConnected() : Promise<void> {
        this.state.State(EnvironmentState.DB_CONNECTED);

        try {
            this.getGQLContext().RegisterModels();
        } catch (ex) {
            LogIt.Error("Failed to init GraphQL interface.", ex);
            this.Exit(-1);
        }
        try {
            if (Threads.IsMainThread()) {
                const settings : AppSettings = await this.getEnvironmentHelper().getAppSettings();
                if (settings.Version !== this.getAppConfiguration().version ||
                    settings.LastModified.getTime() < this.getAppConfiguration().build.timestamp) {
                    LogIt.Debug("Old DB version has been detected. Processing DB Upgrade ...");
                    let upgraded : boolean = false;
                    try {
                        upgraded = await this.onDbUpgrade(settings.Version);
                    } catch (ex) {
                        LogIt.Error("Failed to process DB upgrade.", ex);
                    }
                    if (!upgraded) {
                        LogIt.Error("Failed to upgrade DB content");
                        this.Exit(-1);
                    }
                }

                const userManager : AuthManager = this.getAuthManager();
                await userManager.SyncSystemGroups();
                const userClass : any = Reflection.getInstance().getClass((<any>userManager).getUserInstance().getClassName());

                const defaultUser : IDefaultUserConfiguration | undefined = this.getAppConfiguration().defaultUser;
                if (!ObjectValidator.IsEmptyOrNull(defaultUser)) {
                    if (!ObjectValidator.IsEmptyOrNull(defaultUser.user)) {
                        const users : User[] = await userClass.Find({userName: defaultUser.user, deleted: {$ne: true}}, {partial: true});
                        if (!ObjectValidator.IsEmptyOrNull(users)) {
                            LogIt.Warning("Specification of default root user is deprecated. " +
                                "Marking all default users as Deleted in DB... " +
                                "Init root user from application GUI instead.");
                            for await (const user of users) {
                                await userManager.DeleteUser(user.Id);
                            }
                        }
                    }

                    if (!ObjectValidator.IsEmptyOrNull(defaultUser.defaultAccessibleMethods) ||
                        !ObjectValidator.IsEmptyOrNull(defaultUser.defaultGroupAccess)) {
                        LogIt.Warning("Specification of defaultAccessibleMethods and defaultGroupAccess as part of " +
                            "defaultUser is deprecated and is not applied to Authorized methods anymore. Removing from config...");
                        delete this.getAppConfiguration().defaultUser;
                    }
                }
                if (await this.getAdminHelper().CreateToken()) {
                    this.events.FireEvent(this.getClassName(), LoaderEventType.ON_DB_PREPARED);
                } else {
                    LogIt.Error("Failed to create root user");
                    this.Exit(-1);
                }
            } else {
                if (Threads.getData().isCron) {
                    /// TODO: enable LCP communication
                    Threads.OnMessage(($data : string) : void => {
                        Threads.Send($data);
                    });
                    if (this.getAppConfiguration().authManagerWorker.activationCheck.enabled) {
                        const checkExpiration : any = () : void => {
                            setTimeout(async () : Promise<void> => {
                                await this.getAuthManager().CheckActivation();
                                checkExpiration();
                            }, this.getAppConfiguration().authManagerWorker.activationCheck.interval * 1000);
                        };
                        checkExpiration();
                    }
                    if (this.getAppConfiguration().authManagerWorker.cleanUpService.enabled) {
                        const executeCleanUp : any = () : void => {
                            setTimeout(async () : Promise<void> => {
                                await this.getAuthManager().CheckResetTokens();
                                executeCleanUp();
                            }, this.getAppConfiguration().authManagerWorker.cleanUpService.interval * 1000);
                        };
                        executeCleanUp();
                    }
                }
                this.state.State(EnvironmentState.LOADED);
            }
        } catch (ex) {
            LogIt.Error("Failed to init auth system.", ex);
            this.Exit(-1);
        }
    }

    protected async onDbPrepared() : Promise<void> {
        if (this.getAppConfiguration().authManagerWorker.enabled) {
            try {
                const cronThread : Threads = new Threads();
                cronThread.Create({initData: {isCron: true}});
                cronThread.OnMessage(($data : string) : void => {
                    LogIt.Debug("> cron: {0}", $data);
                });
                /// TODO: enable LCP communication
                // cronThread.Send("some data from main thread");
            } catch (ex) {
                LogIt.Error("Failed to init auth manager worker", ex);
            }
        }
        this.state.State(EnvironmentState.DB_PREPARED);

        if (Threads.IsMainThread()) {
            if (this.getAppConfiguration().sysLog.enabled) {
                await SystemLog.Clean();
            } else {
                LogIt.Warning("System logs has been disabled for this instance by sysLog.enabled configuration");
            }
            await this.getEnvironmentHelper().SyncConfigsData();
            await this.getEnvironmentHelper().SyncAppSettings(this.getProgramArgs().WithDBReset());
            await this.agentsServiceInit();
            if (this.getAppConfiguration().puppeteer.enabled) {
                await this.getEnvironmentHelper().InitPDFPrint();
            }
        } else {
            this.state.State(EnvironmentState.LOADED);
        }

        this.getMailEngine().StartLoop();
        this.getWebhooksManager().StartLoop();
    }

    protected async onEnvironmentLoad() : Promise<void> {
        await super.onEnvironmentLoad();
        if (isBrowser) {
            this.forwardingAdapter = new ForwardingAdapter();
            LogIt.getLogger().AddAdapter(this.forwardingAdapter);
        } else {
            if (!this.getAppConfiguration().noReport) {
                this.crashAdapter = new CrashAdapter();
                LogIt.getLogger().AddAdapter(this.crashAdapter);
            }
            this.getConnectorsRegister().setSymbolMapping({
                symbol: ["*.RuntimeHandler"],
                value : EnvironmentHelper.ClassName()
            });
        }
    }

    protected async onDbUpgrade($oldVersion : string) : Promise<boolean> {
        /// TODO: refactor to only check DB consistency and if fails show FE page for DB migration
        if (StringUtils.VersionIsLower($oldVersion, "2024.0.1")) {
            const filesWithoutOwners : FileEntry[] = await FileEntry.Find({owner: null}, {partial: true});
            if (!ObjectValidator.IsEmptyOrNull(filesWithoutOwners)) {
                for await (const file of filesWithoutOwners) {
                    if (!ObjectValidator.IsEmptyOrNull(await ConfigEntry.Find({data: file.Id}, {partial: true}))) {
                        file.StorageType = FileEntryType.DATABASE;
                        file.Owner = ConfigEntry.ClassNameWithoutNamespace();
                        LogIt.Info("Update of file owner for: " + file.Id);
                        await file.Save();
                    } else {
                        if (ObjectValidator.IsEmptyOrNull(await file.Data())) {
                            if (ObjectValidator.IsEmptyOrNull(file.getAbsolutePath()) ||
                                !this.getFileSystemHandler().Exists(file.getAbsolutePath())) {
                                LogIt.Warning("Removing redundant FileEntry: " + file.Id);
                                await file.Remove();
                            }
                        }
                    }
                }
            }
            const users : User[] = await User.Find({image: {$ne: null}}, {partial: true});
            if (!ObjectValidator.IsEmptyOrNull(users)) {
                for await (const user of users) {
                    await user.Refresh({expanded: true});
                    LogIt.Info("Convert user buffer to FileEntry: " + user.Id);
                    if (ObjectValidator.IsEmptyOrNull(user.Image)) {
                        user.Image = new FileEntry();
                        user.Image.Owner = user.getClassNameWithoutNamespace();
                        await user.Save();
                    }
                    await user.Image.Data((<any>user).imageBuffer);
                    await user.Remove("imageBuffer");
                }
            }
            const orgs : Organization[] = await Organization.Find({image: {$ne: null}}, {partial: true});
            if (!ObjectValidator.IsEmptyOrNull(orgs)) {
                for await (const org of orgs) {
                    await org.Refresh({expanded: true});
                    LogIt.Info("Convert organization buffer to FileEntry: " + org.Id);
                    if (ObjectValidator.IsEmptyOrNull(org.Image)) {
                        org.Image = new FileEntry();
                        org.Image.Owner = org.getClassNameWithoutNamespace();
                        await org.Save();
                    }
                    await org.Image.Data((<any>org).imageBuffer);
                    await org.Remove("imageBuffer");
                }
            }

            const files : FileEntry[] = await FileEntry.Find({}, {partial: true});
            if (!ObjectValidator.IsEmptyOrNull(files)) {
                for await (const file of files) {
                    if (file.Owner !== "Standalone") {
                        if (ObjectValidator.IsEmptyOrNull(await ConfigEntry.Find({data: file.Id}, {partial: true})) &&
                            ObjectValidator.IsEmptyOrNull(await User.Find({imageData: file.Id}, {partial: true})) &&
                            ObjectValidator.IsEmptyOrNull(await Organization.Find({imageData: file.Id}, {partial: true}))
                        ) {
                            if (ObjectValidator.IsEmptyOrNull(await file.Data())) {
                                if (ObjectValidator.IsEmptyOrNull(file.getAbsolutePath()) ||
                                    !this.getFileSystemHandler().Exists(file.getAbsolutePath())) {
                                    LogIt.Warning("Removing redundant FileEntry: " + file.Id);
                                    await file.Remove();
                                }
                            }
                        }
                    }
                }
            }
        }
        return true;
    }

    private async agentsServiceInit() : Promise<void> {
        const agentsRegisterUser : string = this.getAppConfiguration().agentsRegisterUser.user;
        if (ObjectValidator.IsEmptyOrNull(agentsRegisterUser)) {
            LogIt.Warning("Missing agentsRegisterUser setup in project config, skipping services account creation.");
            this.state.State(!this.getAdminHelper().IsRequired() ? EnvironmentState.LOADED : EnvironmentState.ADMIN_INIT);
            return;
        }

        this.state.State(EnvironmentState.AGENTS_INIT);
        this.serviceUser = <any>(await this.getAuthManager().getServiceAccounts()).AgentsRegister;
        this.state.State(!this.getAdminHelper().IsRequired() ? EnvironmentState.LOADED : EnvironmentState.ADMIN_INIT);
    }
}

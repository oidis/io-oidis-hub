/*! ******************************************************************************************************** *
 *
 * Copyright 2024-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { LogLevel } from "@io-oidis-commons/Io/Oidis/Commons/Enums/LogLevel.js";
import { BaseObject } from "@io-oidis-commons/Io/Oidis/Commons/Primitives/BaseObject.js";
import { LogIt } from "@io-oidis-commons/Io/Oidis/Commons/Utils/LogIt.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { Reflection } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Reflection.js";
import { StringUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/StringUtils.js";
import { IWebhookLoopStatus, IWebhooksInfo } from "@io-oidis-hub-gui/Io/Oidis/Hub/Gui/Connectors/WebhooksManagerConnector.js";
import { EventsManager } from "@io-oidis-localhost/Io/Oidis/Localhost/Events/EventsManager.js";
import { BaseConnector } from "@io-oidis-localhost/Io/Oidis/Localhost/Primitives/BaseConnector.js";
import { Extern } from "@io-oidis-localhost/Io/Oidis/Localhost/Primitives/Decorators.js";
import { WebhookRecord } from "../DAO/Models/WebhookRecord.js";
import { IWebhooks } from "../Interfaces/IProject.js";
import { Loader } from "../Loader.js";
import { SystemLog } from "../LogProcessor/SystemLog.js";
import { IWebhookAdapterDescriptor } from "../Primitives/Decorators.js";
import { BaseWebhookAdapter } from "./Adapters/BaseWebhookAdapter.js";

export class WebhooksManagerConnector extends BaseConnector {
    private instance : WebhooksManager;

    constructor() {
        super();
        this.instance = Loader.getInstance().getWebhooksManager();
    }

    @Extern()
    public async getWebhooksInfo() : Promise<IWebhooksInfo> {
        return this.instance.getWebhooksInfo();
    }

    @Extern()
    public setAdapterState($type : string, $active : boolean) : void {
        this.instance.setAdapterState($type, $active);
    }

    @Extern()
    public async ReactivateAdapterRecords($type : string) : Promise<void> {
        return this.instance.ReactivateAdapterRecords($type);
    }

    @Extern()
    public StartLoop($tick? : number) : void {
        this.instance.StartLoop($tick);
    }

    @Extern()
    public StopLoop() : void {
        this.instance.StopLoop();
    }

    @Extern()
    public async MasterStop($active : boolean) : Promise<boolean> {
        return this.instance.MasterStop($active);
    }
}

export class WebhooksManager extends BaseObject {
    private static adapters : BaseWebhookAdapter[] = [];
    private loopThread : number;
    private config : IWebhooks;
    private readonly loopState : IWebhooksInfo;

    public static getSingleton() : WebhooksManager {
        const instance : WebhooksManager = new WebhooksManager();
        WebhooksManager.getSingleton = () : WebhooksManager => {
            return instance;
        };
        return instance;
    }

    constructor() {
        super();

        this.loopThread = null;
        this.config = Loader.getInstance().getAppConfiguration().webhooks;
        this.loopState = {
            adapters        : [],
            bucketSize      : this.config.bucketSize,
            enabled         : this.config.enabled,
            masterStopActive: false,
            status          : IWebhookLoopStatus.Stopped,
            tick            : this.config.tick
        };
    }

    public async getWebhooksInfo() : Promise<IWebhooksInfo> {
        return {
            adapters        : this.getAdapters().map(($adapter : BaseWebhookAdapter) => {
                return {
                    active    : $adapter.IsActive(),
                    name      : $adapter.getAdapterType(),
                    processed : $adapter.ProcessedHooks(),
                    registered: $adapter.RegisteredHooks()
                };
            }),
            bucketSize      : this.loopState.bucketSize,
            enabled         : this.IsEnabled(),
            masterStopActive: this.loopState.masterStopActive,
            status          : this.loopState.status,
            tick            : this.loopState.tick
        };
    }

    public setAdapterState($type : string, $active : boolean) : void {
        this.getAdapters().forEach(($adapter : BaseWebhookAdapter) => {
            if ($type === $adapter.getAdapterType()) {
                LogIt.Info(`Webhooks adapter ${$adapter.getAdapterType()} state changed: ${$active}`);
                $adapter.IsActive($active);
            }
        });
    }

    public async ReactivateAdapterRecords($type : string) : Promise<void> {
        const query : any = {$and: [{active: false}, {deleted: false}, {adapter: $type}]};
        await this.updateRecord(query, async ($record : WebhookRecord) => {
            $record.Active = true;
            await $record.Save();
        });
        if (this.loopState.status === IWebhookLoopStatus.Stopped) {
            this.StartLoop();
        }
    }

    public StartLoop($tick? : number) : void {
        if (!this.config?.enabled) {
            LogIt.Warning("Webhooks processor is disabled by config");
            return;
        }
        if (this.loopState.masterStopActive) {
            LogIt.Warning("Webhooks processing loop has been stopped by master stop event");
            return;
        }
        if (this.loopState.status !== IWebhookLoopStatus.Stopped) {
            // already running
            return;
        }
        let fetchInterval : number = this.config.tick;
        if (!ObjectValidator.IsEmptyOrNull($tick)) {
            fetchInterval = $tick;
            LogIt.Info("Using new fetchTick passed as argument to StartLoop method: " + fetchInterval);
        }
        this.loopState.tick = fetchInterval;
        if (fetchInterval > 0) {
            this.loopState.status = IWebhookLoopStatus.Idle;
            LogIt.Info("Starting webhooks processing loop. Tick: " + this.loopState.tick + "ms");
            const process : any = () : void => {
                if (!ObjectValidator.IsEmptyOrNull(this.loopThread)) {
                    clearTimeout(this.loopThread);
                }
                if (this.loopState.masterStopActive) {
                    return;
                }
                this.loopThread = EventsManager.getInstanceSingleton().FireAsynchronousMethod(async () : Promise<void> => {
                    try {
                        this.loopState.status = IWebhookLoopStatus.Running;
                        if (await this.processWebhookRecords()) {
                            this.loopState.status = IWebhookLoopStatus.Idle;
                            process();
                        } else {
                            LogIt.Info("No more webhooks records, stopping loop. ");
                            this.loopState.status = IWebhookLoopStatus.Stopped;
                        }
                    } catch (ex) {
                        LogIt.Error("processWebhookRecords error", ex);
                        await SystemLog.Trace({
                            level    : LogLevel.ERROR,
                            message  : "processWebhookRecords error: " + ex.trace,
                            traceBack: this.getClassNameWithoutNamespace() + ".processWebhookRecords"
                        });
                        process();
                    }
                }, fetchInterval);
            };
            process();
        } else {
            LogIt.Warning("Running without Shoptet synchronization due to shoptet.fetchTick <= 0");
        }
    }

    public StopLoop() : void {
        if (!ObjectValidator.IsEmptyOrNull(this.loopThread)) {
            clearTimeout(this.loopThread);
            this.loopThread = null;
        }
        this.loopState.status = IWebhookLoopStatus.Stopped;
        LogIt.Info("Webhooks loop has been stopped and automatic start is disabled.");
    }

    public async MasterStop($active : boolean) : Promise<boolean> {
        if (!$active) {
            LogIt.Info("Deactivated MASTER STOP!");
            this.loopState.masterStopActive = false;
        } else {
            LogIt.Warning("Executed MASTER STOP!");
            try {
                this.loopState.masterStopActive = true;
                this.StopLoop();
                await this.updateRecord({$and: [{active: true}, {deleted: false}]}, async ($record : WebhookRecord) => {
                    $record.Active = false;
                    await $record.Save();
                });
            } catch (ex) {
                LogIt.Error("Master stop failed.", ex);
                return false;
            }
        }
        return true;
    }

    public IsEnabled() : boolean {
        return this.config.enabled;
    }

    /**
     * @returns {boolean} Returns true if webhooks are enabled and are not forced into stop mode (master stop).
     */
    public IsActive() : boolean {
        return !this.loopState.masterStopActive && this.IsEnabled();
    }

    public NotifyWebhookReceived($adapter : string) : void {
        const adapter : BaseWebhookAdapter = this.getAdapters().find(($item) => $item.getAdapterType() === $adapter);
        if (!ObjectValidator.IsEmptyOrNull(adapter)) {
            adapter.RegisteredHooks(adapter.RegisteredHooks() + 1);
        }
        if (this.loopState.status === IWebhookLoopStatus.Stopped) {
            this.StartLoop();
        }
    }

    protected getAdapters() {
        if (ObjectValidator.IsEmptyOrNull(WebhooksManager.adapters)) {
            WebhooksManager.adapters = [];
            if (!globalThis.hasOwnProperty("oidisWebhookAdapters")) {
                globalThis.oidisWebhookAdapters = [];
            }
            for (const adapter of <IWebhookAdapterDescriptor[]>globalThis.oidisWebhookAdapters) {
                const instanceClass : any = adapter.instance;
                const instance : BaseWebhookAdapter = new instanceClass();
                if (instance.IsEnabled()) {
                    LogIt.Info(`webhook adapter '${adapter.options.type}' registration from ${adapter.ClassName()}`);
                    WebhooksManager.adapters.push(instance);
                }
            }
        }
        return WebhooksManager.adapters;
    }

    protected async processRecord($record : WebhookRecord) : Promise<void> {
        if (!ObjectValidator.IsEmptyOrNull($record.ClassName) && $record.ClassName !== $record.getClassName()) {
            $record = await Reflection.getInstance().getClass($record.ClassName).FindById($record.Id);
        }
        LogIt.Debug("Processing webhook record: " + $record.Id);
        const validAdapters : BaseWebhookAdapter[] = this.getAdapters().filter(
            ($adapter) => $adapter.getAdapterType() === $record.Adapter);
        if (validAdapters.length > 1) {
            throw new Error(`Multiple adapters found to process ${$record.Type}, this is not supported.`);
        } else if (validAdapters.length === 0) {
            throw new Error(`No adapter found to process ${$record.Type}.`);
        }
        if (!validAdapters[0].IsActive()) {
            throw new Error("Adapter is deactivated, skipping record processing.");
        }
        await validAdapters[0].Process($record);
        validAdapters[0].ProcessedHooks(validAdapters[0].ProcessedHooks() + 1);
    }

    private async updateRecord($query : any, $handler : ($model : WebhookRecord) => Promise<void>) : Promise<void> {
        const documents : number = await WebhookRecord.Model().countDocuments($query);
        const bucketIterator = async function* ($documents : number, $bucketSize : number) {
            let index : number = 0;
            while (index < $documents) {
                yield Array.from({length: Math.min($bucketSize, $documents - index)}, (_, i) => index + i);
                index += $bucketSize;
            }
        };
        const buckets : any = bucketIterator(documents, this.loopState.bucketSize);
        let counter : number = 0;
        for await(const bucket of buckets) {
            LogIt.Info(`Updating records: ${counter += bucket.length}/${documents}`);
            const records : WebhookRecord[] = await WebhookRecord.Find(
                $query,
                {limit: this.loopState.bucketSize});
            if (!ObjectValidator.IsEmptyOrNull(records)) {
                for await (const record of records) {
                    await $handler(record);
                }
            }
        }
    }

    private async processWebhookRecords() : Promise<boolean> {
        const records : WebhookRecord[] = await WebhookRecord.Find(
            {$and: [{active: true}, {deleted: false}]},
            {limit: this.loopState.bucketSize});
        let processed : number = 0;
        let inactive : number = 0;
        if (!ObjectValidator.IsEmptyOrNull(records)) {
            for await (const record of records) {
                try {
                    await this.processRecord(record);
                    await SystemLog.Trace({
                        message  : `Processed webhook for: ${record.OwnerId} with code ${record.Code}`,
                        tags     : [record.OwnerId + "", record.Code],
                        traceBack: this.getClassNameWithoutNamespace() + ".processWebhookRecords"
                    });
                    await record.Remove();
                    processed++;
                } catch (ex) {
                    if (!StringUtils.ContainsIgnoreCase(ex.message, "Adapter is deactivated")) {
                        LogIt.Error(ex);
                    } else {
                        inactive++;
                    }
                    record.Active = false;
                    await record.Save();
                }
            }
        }
        return (processed > 0) || (inactive > 0);
    }
}

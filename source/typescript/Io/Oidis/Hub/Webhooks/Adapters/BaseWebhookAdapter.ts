/*! ******************************************************************************************************** *
 *
 * Copyright 2024-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
import { HttpMethodType } from "@io-oidis-commons/Io/Oidis/Commons/Enums/HttpMethodType.js";
import { HttpStatusType } from "@io-oidis-commons/Io/Oidis/Commons/Enums/HttpStatusType.js";
import { LogLevel } from "@io-oidis-commons/Io/Oidis/Commons/Enums/LogLevel.js";
import { ArrayList } from "@io-oidis-commons/Io/Oidis/Commons/Primitives/ArrayList.js";
import { JsonUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/JsonUtils.js";
import { LogIt } from "@io-oidis-commons/Io/Oidis/Commons/Utils/LogIt.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { Property } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Property.js";
import { StringUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/StringUtils.js";
import { RESTResponse } from "@io-oidis-localhost/Io/Oidis/Localhost/HttpProcessor/ResponseApi/Handlers/RESTResponse.js";
import { BaseConnector } from "@io-oidis-localhost/Io/Oidis/Localhost/Primitives/BaseConnector.js";
import { WebhookRecord } from "../../DAO/Models/WebhookRecord.js";
import { IEndPointConfig } from "../../Interfaces/IProject.js";
import { Loader } from "../../Loader.js";
import { SystemLog } from "../../LogProcessor/SystemLog.js";
import { WebhooksManager } from "../WebhooksManager.js";

export abstract class BaseWebhookAdapter extends BaseConnector {
    protected readonly config : IEndPointConfig;
    protected readonly manager : WebhooksManager;
    private isActive : boolean;
    private processedHooks : number;
    private registeredHooks : number;

    constructor() {
        super();
        this.config = Loader.getInstance().getAppConfiguration()[this.getAdapterType()];
        this.manager = Loader.getInstance().getWebhooksManager();
        this.IsActive(true);
        this.ProcessedHooks(0);
        this.RegisteredHooks(0);
    }

    public IsEnabled() : boolean {
        return this.config.enabled;
    }

    public IsActive($value? : boolean) : boolean {
        return this.isActive = Property.Boolean(this.isActive, $value);
    }

    public ProcessedHooks($value? : number) : number {
        return this.processedHooks = Property.Integer(this.processedHooks, $value);
    }

    public RegisteredHooks($value? : number) : number {
        return this.registeredHooks = Property.Integer(this.registeredHooks, $value);
    }

    /**
     * @returns {string} TReturns adapter type string.
     */
    public getAdapterType() : string {
        if (this.hasOwnProperty("__webhookAdapterType")) {
            return (<any>this).__webhookAdapterType();
        }
        if (this.constructor.hasOwnProperty("__webhookAdapterType")) {
            return (<any>this).constructor.__webhookAdapterType();
        }
        throw new Error(`Webhook adapter type is not specified in ${this.getClassName()}`);
    }

    /**
     * This method is main entrypoint for REST API invoke. Create basic override and decorate with proper route definition
     * @example
     * ```typescript
     * @Route("POST /my-api", {responseType: ExternResponseType.FULL_CONTROL})
     * public async Webhook($data : any, $response : RESTResponse) : Promise<void> {
     *     return super.Webhook($data, $response);
     * }
     * ```
     *
     * If you need more variability then override one of protected methods.
     * @param {object} $data Webhook data (POST body).
     * @param {RESTResponse} $response REST response with to fully control output.
     * @returns {Promise<void>} Returns Async API.
     */
    public async Webhook($data : any, $response : RESTResponse) : Promise<void> {
        return this.processWebhookRequest($data, $response);
    }

    /**
     * Process method purpose is to process previously registered webhook request.
     * @param {WebhookRecord} $record Registered webhook record.
     * @returns {Promise<void>} Returns Async API.
     */
    public async Process($record : WebhookRecord) : Promise<void> {
        LogIt.Info(`Processing record: ${$record.Type}, ${$record.Code}`);
    }

    /**
     * Request method can send data back to webhook origin.
     * @param {object} $data Data for request construction.
     * @returns {Promise<any>} Returns Async API.
     */
    public async Request($data : any) : Promise<any> {
        LogIt.Info(`Processing request to ${this.getAdapterType()} origin`);
        return this.makeRequest({}, $data);
    }

    protected async processWebhookRequest($data : any, $response : RESTResponse) : Promise<void> {
        if (!Loader.getInstance().getWebhooksManager().IsActive() || !this.IsEnabled()) {
            $response.SendError("Out of service due to Stopped or Disabled state.", HttpStatusType.ERROR);
        } else {
            try {
                $response.Send(await this.registerWebhookRecord($data, this.deserializeHeaders($response.getRequestHeaders())));
            } catch (err) {
                LogIt.Warning("Error: " + err.stack);
                await SystemLog.Trace({
                    level    : LogLevel.WARNING,
                    message  : "WebhookChain error: " + err.message,
                    traceBack: this.getClassNameWithoutNamespace() + ".WebhookChain"
                });
                $response.SendError("Error", HttpStatusType.ERROR);
            }
        }
    }

    protected deserializeHeaders($headers : ArrayList<string>) : ArrayList<string> {
        try {
            if (!ObjectValidator.IsArray($headers)) {
                const unserialized : ArrayList<string> = new ArrayList<string>();
                (<any>unserialized).size = (<any>$headers).size;
                (<any>unserialized).keys = (<any>$headers).keys;
                (<any>unserialized).data = (<any>$headers).data;
                unserialized.Reindex();
                $headers = unserialized;
            }
        } catch (ex) {
            LogIt.Error("Failed to deserialize webhook headers.", ex);
        }
        return $headers;
    }

    protected async registerWebhookRecord($data : any, $headers : ArrayList<string>) : Promise<any> {
        LogIt.Debug("Webhook for:\n{0}\n{1}", $headers, JSON.stringify($data));
        let eventType : string = !ObjectValidator.IsEmptyOrNull($data.event) ? $data.event : $headers.getItem("x-wc-webhook-topic");
        eventType = StringUtils.ToLowerCase(eventType);
        if (eventType.match(/^.*:create/) ||
            eventType.match(/^.*:update/) ||
            eventType.match(/^.*:delete/)) {
            const record : WebhookRecord = new WebhookRecord();
            record.Adapter = this.getAdapterType();
            record.AdapterClassName = this.getClassName();
            record.OwnerId = $data.ownerId;
            record.Type = $data.event;
            record.Code = $data.eventInstance;
            await record.Save();

            this.manager.NotifyWebhookReceived(this.getAdapterType());
        } else {
            if (!ObjectValidator.IsEmptyOrNull($data.webhook_id)) {
                LogIt.Warning("Ignoring webhook request without event type for:\n" + JSON.stringify($headers));
            } else {
                LogIt.Error("Ignoring webhook request:\n" + JSON.stringify($headers) + "\n" + JSON.stringify($data));
            }
        }
        return null;
    }

    protected async makeRequest($options : any, $data : string = null) : Promise<any> {
        if (!this.IsEnabled()) {
            throw new Error(`Adapter ${this.getAdapterType()} is disabled.`);
        }
        $options = JsonUtils.Extend(this.prepareRequest(), $options);
        if (!ObjectValidator.IsEmptyOrNull($data)) {
            const dataBuffer : Buffer = Buffer.from($data);
            $data = dataBuffer.toString("utf-8");
            $options = JsonUtils.Extend($options, {
                headers: {
                    "Content-Length": dataBuffer.length
                }
            });
        }
        return new Promise(($resolve : ($protocol : any) => void) : void => {
            const req : any = ($options.port === 443 ? require("https") : require("http")).request($options, ($res : any) : void => {
                if ($res.statusCode === 200 || $res.statusCode === 201) {
                    let data : any = "";
                    $res.on("data", ($chunk : string) : void => {
                        data += $chunk;
                    });
                    $res.on("end", () : void => {
                        if (!ObjectValidator.IsEmptyOrNull(data)) {
                            try {
                                data = JSON.parse(data);
                                if (data.hasOwnProperty("errors") && !ObjectValidator.IsEmptyOrNull(data.errors)) {
                                    $resolve({status: false, data, errors: data.errors});
                                } else {
                                    $resolve({status: true, data, errors: []});
                                }
                            } catch (ex) {
                                LogIt.Warning(`Failed to parse data from ${this.getAdapterType()} origin: ${data}\n${ex.stack}`);
                                $resolve({
                                    data  : null,
                                    errors: [`Failed to parse data from ${this.getAdapterType()} origin`],
                                    status: false
                                });
                            }
                        } else {
                            $resolve({status: true, data, errors: []});
                        }
                    });
                } else if ($res.statusCode === 403 || $res.statusCode === 400) {
                    LogIt.Debug(`${this.getAdapterType()} origin unauthorized request detected.`);
                    $resolve({
                        data         : null,
                        errors       : [`Not authorized for ${this.getAdapterType()} origin request`],
                        notAuthorized: true,
                        status       : false
                    });
                } else if ($res.statusCode === 415) {
                    LogIt.Debug(`${this.getAdapterType()} origin: Unsupported Media Types.`);
                    $resolve({
                        data         : null,
                        errors       : [`Unsupported media types for ${this.getAdapterType()} request`],
                        notAuthorized: true,
                        status       : false
                    });
                } else {
                    LogIt.Warning(`Invalid response from ${this.getAdapterType()}: ${$res.statusCode}`);
                    $resolve({status: false, data: null, errors: [`Invalid response from ${this.getAdapterType()}`]});
                }
            });
            req.on("error", ($ex : Error) : void => {
                $resolve({status: false, data: null, errors: [$ex.message], outOfService: true});
            });
            if (!ObjectValidator.IsEmptyOrNull($data)) {
                req.write($data);
            }
            req.end();
        });
    }

    protected prepareRequest() : any {
        return {
            headers : {
                "Authorization": "Basic" + Buffer.from(
                    this.config.server.userName + ":" + this.config.server.token).toString("base64"),
                "Content-Type" : "application/json;charset=UTF-8",
                "User-Agent"   : this.config.server.userName
            },
            hostname: this.config.server.hostname,
            method  : HttpMethodType.GET,
            port    : 443
        };
    }
}

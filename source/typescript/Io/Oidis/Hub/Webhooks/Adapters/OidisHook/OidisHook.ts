/*! ******************************************************************************************************** *
 *
 * Copyright 2024-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { RESTResponse } from "@io-oidis-localhost/Io/Oidis/Localhost/HttpProcessor/ResponseApi/Handlers/RESTResponse.js";
import { ExternResponseType } from "@io-oidis-localhost/Io/Oidis/Localhost/Interfaces/IExternOptions.js";
import { Route } from "@io-oidis-localhost/Io/Oidis/Localhost/Primitives/Decorators.js";
import { WebhookAdapter } from "../../../Primitives/Decorators.js";
import { BaseWebhookAdapter } from "../BaseWebhookAdapter.js";

@WebhookAdapter({type: "oidishook"})
export class OidisHook extends BaseWebhookAdapter {

    @Route("POST /oidishook/notify", {responseType: ExternResponseType.FULL_CONTROL})
    @Route("POST /v1/oidishook/notify", {responseType: ExternResponseType.FULL_CONTROL})
    public async Webhook($data : any, $response : RESTResponse) : Promise<void> {
        return super.Webhook($data, $response);
    }
}

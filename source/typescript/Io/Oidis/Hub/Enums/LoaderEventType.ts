/*! ******************************************************************************************************** *
 *
 * Copyright 2022-2024 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { BaseEnum } from "@io-oidis-commons/Io/Oidis/Commons/Primitives/BaseEnum.js";

export class LoaderEventType extends BaseEnum {
    public static readonly ON_DB_CONNECTED : string = "ondbconnected";
    public static readonly ON_DB_PREPARED : string = "ondbprepared";
    public static readonly ON_NEW_AGENT_CONNECTED : string = "onnewagentconnected";
    public static readonly ON_AGENT_DISCONNECTED : string = "onagentdisconnected";
}

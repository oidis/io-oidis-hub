/*! ******************************************************************************************************** *
 *
 * Copyright 2018 NXP
 * Copyright 2019-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { JsonUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/JsonUtils.js";
import { LogIt } from "@io-oidis-commons/Io/Oidis/Commons/Utils/LogIt.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { StringUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/StringUtils.js";
import { ResponseFactory } from "@io-oidis-localhost/Io/Oidis/Localhost/HttpProcessor/ResponseApi/ResponseFactory.js";
import { ExternResponseType } from "@io-oidis-localhost/Io/Oidis/Localhost/Interfaces/IExternOptions.js";
import { IResponse } from "@io-oidis-localhost/Io/Oidis/Localhost/Interfaces/IResponse.js";
import { BaseConnector } from "@io-oidis-localhost/Io/Oidis/Localhost/Primitives/BaseConnector.js";
import { Extern } from "@io-oidis-localhost/Io/Oidis/Localhost/Primitives/Decorators.js";
import {
    IAgentCapabilities,
    IAgentInfo,
    IAgentMetadata,
    IAgentStatus
} from "@io-oidis-services/Io/Oidis/Services/Connectors/AgentsRegisterConnector.js";
import { LoaderEventType } from "../Enums/LoaderEventType.js";
import { Loader } from "../Loader.js";

// TODO(mkelnar) it will be good idea to have this class abstract, but it is used by LCP and instances are created by reflection,
//  so it will be complicated to suppress it directly in LCP
export class AgentsRegister extends BaseConnector {
    // TODO(mkelnar) tasks register for backpropagation of data from agent to client is completely isolated from agent instance/ID
    //  this is big issue if we needs to clean/handle anything because it is impossible to pair it again during running
    protected static tasks : IAgentTaskRegisterInfo[] = [];
    // TODO(mkelnar) previous static register has several problems which comes from limited extensibility and also as static then all
    //  agents were "shared" with all users and all agentsRegister derived classes. If we count with different agent registers then
    //  we can expect different api per register type and thus it needs different services connector so it will need more enhanced
    //  logic to filter proper agents for connector and access over generic AgentsRegisterConnector to i.e. builder agent still
    //  doesnt make sense. This leads to services connector per agent split and then it also doesnt make sense to be able to read all
    //  agents info from base class
    // TODO(mkelnar) next question is targeted on user-permission question, because agents are not persistent then they are not stored
    //  it db and finally agent could be registered by one user (i.e. service account) and used by different user. This also leads to
    //  motivation to change static register into instanced one. However, it is good question how to share agents between user
    //  event in scenario that this agent is not exists at the time of user access delegation and agent itself is then registered
    //  to register by i.e. swarm service?... This will need some special logic implemented between agent register and forwarding client
    //  observer
    // TODO(mkelnar) we can expect that agents will be registered only by person who knows behaviour and thus all agents will be global
    //  until this paradigm will be solved

    // this register could be static (could have performance issues in future) to allow agents register by anybody who is logedIn
    // as next step instances of AgentsRegister class should use this.getAgents() which will works with this static register and
    // apply filter internally to use only accessible ones by groupIDs
    protected static agents : IAgent[] = [];

    @Extern(ExternResponseType.FULL_CONTROL)
    public RegisterAgent($capabilities : IAgentCapabilities, $callback? : IResponse) : void {
        if (!ObjectValidator.IsEmptyOrNull($capabilities.credentials)) {
            (Loader.getInstance().getAuthManager()
                .IsAuthTokenValid($capabilities.credentials.authToken, $capabilities.credentials.userName))
                .then(($status : boolean) : void => {
                    if ($status) {
                        const agent : IAgent = this.register($capabilities, $callback);
                        agent.connection.Send(this.getRegisterResponseArgs(true, $capabilities, agent));
                    } else {
                        LogIt.Warning("Registration request rejected.");
                        ResponseFactory.getResponse($callback).Send(this.getRegisterResponseArgs(false, $capabilities, null));
                    }
                })
                .catch(($error : Error) : void => {
                    ResponseFactory.getResponse($callback).OnError($error);
                });
        } else {
            const agent : IAgent = this.register($capabilities, $callback);
            LogIt.Warning("Agent registration without credentials.");
            agent.connection.Send(this.getRegisterResponseArgs(true, $capabilities, agent));
        }
    }

    @Extern(ExternResponseType.FULL_CONTROL)
    public ForwardMessage($targetId : string, $data : any, $callback? : IResponse) : void {
        const agent : IAgent = this.getAgent($targetId);
        const task : IAgentTaskRegisterInfo = AgentsRegister.tasks.find(($item) => $item.id === $targetId);
        if (!ObjectValidator.IsEmptyOrNull(task)) {
            task.response.OnChange($data);
            ResponseFactory.getResponse($callback).Send(true);
        } else if (!ObjectValidator.IsEmptyOrNull(agent)) {
            agent.connection.OnChange($data);
            ResponseFactory.getResponse($callback).Send(true);
        } else {
            ResponseFactory.getResponse($callback).Send(false);
        }
    }

    @Extern()
    public getAgentsList() : IAgentInfo[] {
        const list : IAgentInfo[] = [];
        for (const item of AgentsRegister.agents) {
            list.push({
                domain   : item.domain,
                id       : item.id,
                name     : item.name,
                platform : item.platform,
                poolName : item.poolName,
                startTime: item.startTime,
                status   : item.status,
                tag      : item.tag,
                version  : item.version
            });
        }
        return list;
    }

    @Extern()
    public getAgentsMetadata($agentId) : IAgentMetadata {
        // TODO(mkelnar) add is authorized for agent/pool check once this rule will be ready
        const info : IAgentInfo = AgentsRegister.agents.find(($item) => $item.id === $agentId);
        return {
            configuration: info?.metadata?.configuration ?? {},
            environment  : info?.metadata?.environment ?? {}
        };
    }

    @Extern()
    public getAgentHandshakeToken($agentId : string) : string {
        const info : IAgent = this.getAgent($agentId);
        if (!ObjectValidator.IsEmptyOrNull(info)) {
            return info.handshakeToken;
        }
        return null;
    }

    @Extern()
    public async UpdateAgentStatus($status : any) : Promise<any> {
        // todo(mkelnar) write some statistics data and return: TBD
        return {restart: false};
    }

    @Extern()
    public async setAgentStatus($agentId : string, $status : IAgentStatus) : Promise<void> {
        const agent : IAgent = AgentsRegister.agents.find(($agent) => $agent.id === $agentId);
        if (ObjectValidator.IsEmptyOrNull(agent)) {
            throw new Error("Requested agent ID '" + $agentId + "' not found in register.");
        }
        agent.status = $status;
    }

    public getAgent($id : string) : IAgent {
        return AgentsRegister.agents.find(($item) => $item.id === $id);
    }

    protected createAgentId($agent : IAgentCapabilities) : string {
        return StringUtils.getSha1($agent.domain + $agent.platform + $agent.name);
    }

    protected register($capabilities : IAgentCapabilities, $callback : IResponse) : IAgent {
        const agent : IAgent = JsonUtils.Clone($capabilities);
        const infoSha : string = StringUtils.getSha1(JSON.stringify($capabilities.packageInfo) +
            $capabilities.poolName + $capabilities.tag);
        agent.id = this.createAgentId($capabilities);
        agent.owner = this;
        agent.handshakeToken = this.getUID();
        agent.startTime = new Date();
        agent.status = IAgentStatus.Running;
        agent.connection = ResponseFactory.getResponse($callback);
        agent.connection.AddAbortHandler(() : void => {
            LogIt.Warning("Agent [" + agent.id + "] has been disconnected. Removing from register ...");
            const index : number = AgentsRegister.agents.findIndex(($item) => $item.id === agent.id);
            if (index > -1) {
                AgentsRegister.agents.splice(index, 1);
                Loader.getInstance().getEventsManager()
                    .FireEvent(Loader.getInstance().getClassName(), LoaderEventType.ON_AGENT_DISCONNECTED, agent.id);
            }
        });
        const isNewAgentType = AgentsRegister.agents.findIndex(($item) => {
            return StringUtils.getSha1(JSON.stringify($item.packageInfo) + $item.poolName + $item.tag) === infoSha;
        }) === -1;

        LogIt.Info("Registered agent[" + agent.id + "](isNewType: " + isNewAgentType + ") with capabilities: " +
            JSON.stringify($capabilities, ($key : string, $value : any) => {
                if (["token", "authToken", "configuration", "environment", "metadata", "pass", "password"].includes($key)) {
                    return undefined;
                }
                return $value;
            }));
        AgentsRegister.agents.push(agent);
        if ((AgentsRegister.agents.length <= 1) || isNewAgentType) {
            LogIt.Info("New agent version registration request detected.");
            Loader.getInstance().getEventsManager()
                .FireEvent(Loader.getInstance().getClassName(), LoaderEventType.ON_NEW_AGENT_CONNECTED, agent);
        } else {
            LogIt.Info("Another instance of this agent exists already.");
        }
        return agent;
    }

    protected getRegisterResponseArgs($status : boolean, $capabilities : IAgentCapabilities, $agent : IAgent) : any[] {
        const args : any = [$status];
        if ($status && !ObjectValidator.IsEmptyOrNull($capabilities.packageInfo)) {
            const pkgInfo : IPackageInfo = $capabilities.packageInfo;
            args.push(Loader.getInstance().getAuthManager().getAppMethods({
                appName    : pkgInfo.name,
                releaseName: pkgInfo.release,
                platform   : pkgInfo.platform,
                version    : pkgInfo.version,
                buildTime  : pkgInfo.buildTime
            }));
        } else {
            args.push([]);
        }
        if ($capabilities.withProtectedAPI && !ObjectValidator.IsEmptyOrNull($agent)) {
            args.push($agent.handshakeToken);
        }
        return args;
    }
}

export interface IAgent extends IAgentInfo {
    connection : IResponse;
    owner : AgentsRegister;
    handshakeToken : string;
}

export interface IPackageInfo {
    name : string;
    release : string;
    platform : string;
    version : string;
    buildTime : number;
}

export interface IAgentTaskRegisterInfo {
    id : string;
    response : IResponse;
}

// generated-code-start
export const IAgent = globalThis.RegisterInterface(["connection", "owner", "handshakeToken"], <any>IAgentInfo);
export const IPackageInfo = globalThis.RegisterInterface(["name", "release", "platform", "version", "buildTime"]);
export const IAgentTaskRegisterInfo = globalThis.RegisterInterface(["id", "response"]);
// generated-code-end

/*! ******************************************************************************************************** *
 *
 * Copyright 2020-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { IClassName } from "@io-oidis-commons/Io/Oidis/Commons/Interfaces/Interface.js";
import {
    Entity as ParentEntity,
    IPropertyOptions, IResolverDescriptor, IResolverOptions,
    Property as ParentProperty
} from "@io-oidis-commons/Io/Oidis/Commons/Primitives/Decorators.js";
import { Convert } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Convert.js";
import { JsonUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/JsonUtils.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { IAuthPromise } from "@io-oidis-localhost/Io/Oidis/Localhost/Interfaces/IResponse.js";

/**
 * @deprecated Use Property decorator from Commons namespace
 * @param {IPropertyOptions} $options Define property options
 * @returns {object} Returns decorator wrapping for Model Property
 */
export function Property($options? : IPropertyOptions) : any {
    return ParentProperty($options);
}

/**
 * @deprecated Use Entity decorator from Commons namespace
 * @param {string} $shortcut Define name of entity
 * @returns {object} Returns decorator wrapping for Model Entity
 */
export function Entity($shortcut? : string) : any {
    return ParentEntity($shortcut);
}

export function WebhookAdapter($options : IWebhookAdapterOptions) : any {
    return function ($target : any, $propertyKey : string, $descriptor : PropertyDescriptor) : any {
        if (!globalThis.hasOwnProperty("oidisWebhookAdapters")) {
            globalThis.oidisWebhookAdapters = [];
        }
        $target.__webhookAdapterType = () : string => {
            return $options.type;
        };
        globalThis.oidisWebhookAdapters.push(<IWebhookAdapterDescriptor>{
            ClassName: () : string => {
                return $target.ClassName();
            },
            instance : $target,
            options  : $options
        });
    };
}

export enum GQLMethod {
    Query,
    Mutation
}

export interface IGQLArg {
    name : string;
    type : any;
}

export interface IGQLOptions {
    /**
     * Method name in graphQl schema
     */
    symbol? : string;
    description? : string;
    /**
     * Input arguments definition (position dependent). https://github.com/graphql-compose/graphql-compose
     */
    args? : IGQLArg[];
    /**
     * Return value type definition. https://github.com/graphql-compose/graphql-compose
     */
    returnType? : any;
}

export interface IGQLDescriptor {
    options : IGQLOptions;
    methodName : string;
    type : GQLMethod;
    method : (...$args : any[]) => IAuthPromise;
    ClassName : () => string;
}

export interface IWebhookAdapterOptions {
    type : string;
}

export interface IWebhookAdapterDescriptor {
    ClassName : () => string;
    instance : IClassName;
    options : IWebhookAdapterOptions;
}

// TODO(mkelnar) code injection could be removed once underlying object will be extended from baseObject - IF
//  since this i mostly for data interface then reflection and other stuff may not be optimal here at least BaseModel split - TBD
export function Model($name : string) : any {
    return function ($target : any) : any {
        $target.__model = () => {
            // dummy
        };
        // const proto = class extends $target{
        $target.prototype.getClassName = () : string => {
            return $name;
        };
        $target.prototype.getClassNameWithoutNamespace = () : string => {
            return this.getClassName();
        };
        $target.prototype.__defineGetter__("Properties", function () {
            return this?._modelProperties;
        });
        if (!globalThis.hasOwnProperty("oidisAPIGraphQLModels")) {
            globalThis.oidisAPIGraphQLModels = [];
        }
        globalThis.oidisAPIGraphQLModels.push(() : any => {
            return {
                prototype: $target
            };
        });
        // return proto;
        // const original = $target;

        // function replCtor(...args : any[]):any {
        //     const instance = new original(...args);
        //     instance.getClassName = () : string => {
        //         return $name;
        //     }
        //     instance.getClassNameWithoutNamespace = () : string => {
        //         return instance.getClassName();
        //     }
        //     instance.Properties = instance?._modelProperties;
        //     return instance;
        // }
        //
        // replCtor.toString = () : string => {
        //     return $name + " decorated by @Model";
        // }
        // replCtor.prototype = original.prototype;
        // replCtor.__model = () => {
        //     // dummy
        // }
        // if (!globalThis.hasOwnProperty("oidisAPIGraphQLModels")) {
        //     globalThis.oidisAPIGraphQLModels = [];
        // }
        // globalThis.oidisAPIGraphQLModels.push(() : any => {
        //     return {
        //         prototype: replCtor
        //     }
        // });
        // return new replCtor();
    };
}

export function GQL($type : GQLMethod, $options? : IGQLOptions) : any {
    return function ($target : any, $propertyKey : string, $descriptor : PropertyDescriptor) : any {
        const key : string = $propertyKey + "_GQL";
        $target[key] = () : any => {
            const options : IGQLOptions = <IGQLOptions>{
                // TODO(mkelnar) create default definition
            };

            let methodName : string = "." + $propertyKey;
            if (ObjectValidator.IsSet($target.getClassName)) {
                methodName = $target.getClassName() + methodName;
            } else {
                methodName = $target.ClassName() + methodName;
            }
            JsonUtils.Extend(options, $options);

            if (ObjectValidator.IsEmptyOrNull(options.symbol)) {
                // TODO(mkelnar) must be unique, check it somewhere
                options.symbol = $propertyKey;
                // options.symbol = methodName.replace(/\./g, "");
            }

            return {
                ClassName: () : string => {
                    if (ObjectValidator.IsSet($target.getClassName)) {
                        return $target.getClassName();
                    }
                    return $target.ClassName();
                },
                method   : (...$args : any[]) : IAuthPromise => {
                    return {
                        auth: Convert.ToType($target) + "." + $propertyKey,
                        async resolve($token : string, $version : string) : Promise<any> {
                            // TODO(mkelnar) enable authorization and connect with resolveResponse same as for @Extern, @Route
                            this.getCurrentToken = () => {
                                return $token;
                            };
                            this.getProtocolVersion = () => {
                                return $version;
                            };
                            const args : any[] = [...$args];
                            return $descriptor.value.apply(this, args);
                        }
                    };
                },
                methodName,
                options,
                type     : $type
            };
        };
        if (!globalThis.hasOwnProperty("oidisAPIGraphQL")) {
            globalThis.oidisAPIGraphQL = [];
        }
        globalThis.oidisAPIGraphQL.push($target[key]);
    };
}

export function Query($options? : IGQLOptions) : any {
    return GQL(GQLMethod.Query, $options);
}

export function Mutation($options? : IGQLOptions) : any {
    return GQL(GQLMethod.Mutation, $options);
}

// generated-code-start
export const IGQLArg = globalThis.RegisterInterface(["name", "type"]);
export const IGQLOptions = globalThis.RegisterInterface(["symbol", "description", "args", "returnType"]);
export const IGQLDescriptor = globalThis.RegisterInterface(["options", "methodName", "type", "method", "ClassName"]);
export const IWebhookAdapterOptions = globalThis.RegisterInterface(["type"]);
export const IWebhookAdapterDescriptor = globalThis.RegisterInterface(["ClassName", "instance", "options"]);
// generated-code-end

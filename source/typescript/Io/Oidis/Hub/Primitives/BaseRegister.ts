/*! ******************************************************************************************************** *
 *
 * Copyright 2018 NXP
 * Copyright 2019-2023 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { StringUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/StringUtils.js";
import { FileSystemHandler } from "@io-oidis-localhost/Io/Oidis/Localhost/Connectors/FileSystemHandler.js";
import { BaseConnector } from "@io-oidis-localhost/Io/Oidis/Localhost/Primitives/BaseConnector.js";
import { Loader } from "../Loader.js";

export class BaseRegister<TConfigItem> extends BaseConnector {

    public FileRegisterExists() : boolean {
        return Loader.getInstance().getFileSystemHandler().Exists(this.getRegisterPath());
    }

    protected getStoragePath($isAbsolute : boolean = true) : string {
        const rootPath : string = Loader.getInstance().getProgramArgs().AppDataPath();
        const path : string = "/resource/data/" + StringUtils.Replace(this.getClassName(), ".", "/");
        this.getStoragePath = ($isAbsolute : boolean = true) : string => {
            return ($isAbsolute ? rootPath : "") + path;
        };
        return ($isAbsolute ? rootPath : "") + path;
    }

    protected getRegisterPath() : string {
        const path : string = this.getStoragePath() + "/Register.json";
        this.getRegisterPath = () : string => {
            return path;
        };
        return path;
    }

    protected getRegister() : TConfigItem[] {
        const path : string = this.getRegisterPath();
        const fileSystem : FileSystemHandler = Loader.getInstance().getFileSystemHandler();
        if (fileSystem.Exists(path)) {
            return JSON.parse(<string>fileSystem.Read(path));
        }
        return [];
    }
}

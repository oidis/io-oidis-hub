/*! ******************************************************************************************************** *
 *
 * Copyright 2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { BaseAdapter } from "@io-oidis-commons/Io/Oidis/Commons/LogProcessor/Adapters/BaseAdapter.js";
import { ILoggerTrace } from "@io-oidis-commons/Io/Oidis/Commons/LogProcessor/Logger.js";
import { JsonUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/JsonUtils.js";
import { LogIt } from "@io-oidis-commons/Io/Oidis/Commons/Utils/LogIt.js";
import { TraceBack } from "@io-oidis-commons/Io/Oidis/Commons/Utils/TraceBack.js";
import { BaseConnector } from "@io-oidis-localhost/Io/Oidis/Localhost/Primitives/BaseConnector.js";
import { Extern } from "@io-oidis-localhost/Io/Oidis/Localhost/Primitives/Decorators.js";

export class LoggerConnector extends BaseConnector {

    @Extern()
    public async ProcessTrace($trace : ILoggerTrace) : Promise<void> {
        $trace = JsonUtils.Extend({
            entryPoint: {
                at  : this.getClassNameWithoutNamespace() + ".ProcessTrace",
                from: "[FE forwarding]"
            },
            time      : new Date().getTime(),
            trace     : TraceBack.getTrace()
        }, $trace);
        (<any>LogIt.getLogger()).adapters.forEach(($adapter : BaseAdapter) : void => {
            if ($adapter.Enabled()) {
                $adapter.Process(JsonUtils.Clone($trace));
            }
        });
    }
}

/*! ******************************************************************************************************** *
 *
 * Copyright 2019 NXP
 * Copyright 2019-2023 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { ResponseFactory } from "@io-oidis-localhost/Io/Oidis/Localhost/HttpProcessor/ResponseApi/ResponseFactory.js";
import { ExternResponseType } from "@io-oidis-localhost/Io/Oidis/Localhost/Interfaces/IExternOptions.js";
import { IResponse } from "@io-oidis-localhost/Io/Oidis/Localhost/Interfaces/IResponse.js";
import { BaseConnector } from "@io-oidis-localhost/Io/Oidis/Localhost/Primitives/BaseConnector.js";
import { Extern } from "@io-oidis-localhost/Io/Oidis/Localhost/Primitives/Decorators.js";
import { CertsManager } from "@io-oidis-localhost/Io/Oidis/Localhost/Utils/CertsManager.js";
import { Loader } from "../Loader.js";

export class CertsHandler extends BaseConnector {

    @Extern(ExternResponseType.FULL_CONTROL)
    public getCertsFor($domain : string, $callback : IResponse) : void {
        $callback = ResponseFactory.getResponse($callback);
        if (Loader.getInstance().getAppConfiguration().certs.whitelist.includes($domain)) {
            new CertsManager($domain).Create(($status : boolean, $cert : string, $key : string) : void => {
                if ($status) {
                    $callback.Send($cert, $key);
                } else {
                    $callback.Send("", "");
                }
            });
        } else {
            $callback.Send("", "");
        }
    }
}

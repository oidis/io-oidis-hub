/*! ******************************************************************************************************** *
 *
 * Copyright 2018-2019 NXP
 * Copyright 2019-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { LogLevel } from "@io-oidis-commons/Io/Oidis/Commons/Enums/LogLevel.js";
import { LogSeverity } from "@io-oidis-commons/Io/Oidis/Commons/Enums/LogSeverity.js";
import { ILoggerTrace } from "@io-oidis-commons/Io/Oidis/Commons/LogProcessor/Logger.js";
import { JsonUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/JsonUtils.js";
import { LogIt } from "@io-oidis-commons/Io/Oidis/Commons/Utils/LogIt.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { StringUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/StringUtils.js";
import { TraceBack } from "@io-oidis-commons/Io/Oidis/Commons/Utils/TraceBack.js";
import { ILogsRecords, ILogsRecordsCounts, ILogsSource } from "@io-oidis-hub-gui/Io/Oidis/Hub/Gui/Connectors/ReportsConnector.js";
import { BaseConnector } from "@io-oidis-localhost/Io/Oidis/Localhost/Primitives/BaseConnector.js";
import { Extern } from "@io-oidis-localhost/Io/Oidis/Localhost/Primitives/Decorators.js";
import { Loader } from "../Loader.js";
import { SystemLog } from "../LogProcessor/SystemLog.js";
import { ReportAPI } from "../Utils/ReportAPI.js";

export class ReportsConnector extends BaseConnector {
    private reportApi : ReportAPI;

    constructor() {
        super();
        this.reportApi = new ReportAPI();
    }

    @Extern()
    public getReportingApps() : string[] {
        return this.reportApi.getReportingApps();
    }

    @Extern()
    public getReports($appId : string) : string[] {
        return this.reportApi.getReports($appId);
    }

    @Extern()
    public getReport($id : string) : string {
        return this.reportApi.getContent($id);
    }

    @Extern()
    public ProcessLogs($options? : ILogsSource) : ILogsRecords {
        /// TODO: enable pagination by file seek
        //        > instead of pagination by parsed item it will request file chunk
        //        > file seek will be driven by meta info send with pagination data so logic will be driven by seek instead of record index
        $options = JsonUtils.Extend(<ILogsSource>{
            data       : null,
            filter     : null,
            from       : 0,
            isAscending: true,
            levels     : [LogLevel.ALL],
            limit      : -1,
            location   : "./log",
            offset     : 0,
            to         : -1
        }, $options);

        let traces : ILoggerTrace[] = [];
        let isJson : boolean = false;
        let data : any = null;
        if (!ObjectValidator.IsEmptyOrNull($options.data)) {
            data = <ILoggerTrace[]>$options.data;
            if (ObjectValidator.IsArray(data)) {
                traces = data;
                isJson = true;
            } else {
                try {
                    traces = JSON.parse(data);
                    isJson = true;
                } catch (ex) {
                    // not in JSON format let's try to parse data as file
                }
            }
        }
        if (ObjectValidator.IsEmptyOrNull(data)) {
            const fromDate : Date = new Date($options.from);
            const year : string = fromDate.getUTCFullYear() + "";
            const day : string = fromDate.getUTCDate() < 10 ? "0" + fromDate.getUTCDate() : fromDate.getUTCDate() + "";
            const monthNumber : number = fromDate.getUTCMonth() + 1;
            const month : string = monthNumber < 10 ? "0" + monthNumber : monthNumber + "";
            const path : string = $options.location + "/" + year + "/" + month + "/" + day + "_" + month + "_" + year;
            if (Loader.getInstance().getFileSystemHandler().Exists(path + ".json")) {
                data = Loader.getInstance().getFileSystemHandler().Read(path + ".json").toString("utf-8");
                if (!ObjectValidator.IsEmptyOrNull(data)) {
                    try {
                        traces = JSON.parse(data);
                        isJson = true;
                    } catch (ex) {
                        // not in JSON format let's try to parse file
                        data = null;
                    }
                }
            }
            if (ObjectValidator.IsEmptyOrNull(data)) {
                data = Loader.getInstance().getFileSystemHandler().Read(path + ".txt").toString("utf-8");
            }
        }

        /// TODO: enable offset and limit directly for raw data together with some kind of cache so reading performance can be increased
        try {
            if (!isJson && !ObjectValidator.IsEmptyOrNull(data)) {
                // eslint-disable-next-line @stylistic/max-len
                const header : RegExp = /^\[(?<date>\w+,\s\d+\s\w+\s\d{4}\s\d+:\d+:\d+\s\w+)\]\[(?<level>(?:ERROR)|(?:WARNING)|(?:INFO)|(?:DEBUG)|(?:VERBOSE))\][ ]/gm;
                let recordMatch : any = header.exec(data);
                const records : any[] = [];
                do {
                    if (!ObjectValidator.IsEmptyOrNull(recordMatch)) {
                        if (records.length > 0) {
                            records[records.length - 1].end = recordMatch.index;
                        }
                        records.push({
                            end  : data.length,
                            start: header.lastIndex,
                            trace: {
                                entryPoint: null,
                                level     : LogLevel[recordMatch.groups.level],
                                message   : "",
                                severity  : LogSeverity.HIGH,
                                time      : recordMatch.groups.date,
                                trace     : null
                            }
                        });
                    } else {
                        LogIt.Verbose("Failed to parse record: {0}", data);
                    }
                } while ((recordMatch = header.exec(data)) !== null);
                records.forEach(($record : any) : void => {
                    let message : string = data.substr($record.start, $record.end - $record.start);
                    message = message.substr(0, message.length - (StringUtils.EndsWith(message, "\r\n") ? 2 : 1));
                    const body : RegExp = /[ ]--[ ](?<at>.*)(?: from )(?<from>.*)$/gm;
                    const bodyMatch : any = body.exec(message);
                    if (bodyMatch !== null) {
                        $record.trace.message = message.substr(0, bodyMatch.index);
                        $record.trace.entryPoint = {
                            at  : bodyMatch.groups.at,
                            from: bodyMatch.groups.from
                        };
                    } else {
                        $record.trace.message = message;
                    }
                    if (StringUtils.Contains($record.trace.message, "Stack trace:")) {
                        $record.trace.trace = TraceBack.Parse(<any>{stack: $record.trace.message});
                        $record.trace.message = $record.trace.message.split("\n")[0];
                    }
                    traces.push($record.trace);
                });
            }
        } catch (ex) {
            LogIt.Error(ex);
            traces = [];
        }
        const output : ILoggerTrace[] = [];
        const counters : ILogsRecordsCounts = {
            debug  : 0,
            error  : 0,
            info   : 0,
            warning: 0
        };
        traces.forEach(($trace : ILoggerTrace, $index : number) : void => {
            switch ($trace.level) {
            case LogLevel.INFO:
                counters.info++;
                break;
            case LogLevel.WARNING:
                counters.warning++;
                break;
            case LogLevel.ERROR:
                counters.error++;
                break;
            default:
                counters.debug++;
                break;
            }
            if ($index >= $options.offset &&
                ($options.limit === -1 || output.length < $options.limit) &&
                ($options.levels.includes(LogLevel.ALL) || $options.levels.includes($trace.level))) {
                // TODO: enable also regex by detection of /syntax/options
                if (ObjectValidator.IsEmptyOrNull($options.filter) || StringUtils.ContainsIgnoreCase($trace.message, $options.filter)) {
                    output.push($trace);
                }
            }
        });
        if (!$options.isAscending) {
            output.reverse();
        }
        let size : number = 0;
        if ($options.levels.includes(LogLevel.ALL)) {
            size = traces.length;
        } else {
            if ($options.levels.includes(LogLevel.INFO)) {
                size += counters.info;
            }
            if ($options.levels.includes(LogLevel.WARNING)) {
                size += counters.warning;
            }
            if ($options.levels.includes(LogLevel.ERROR)) {
                size += counters.error;
            }
            if ($options.levels.includes(LogLevel.DEBUG)) {
                size += counters.debug;
            }
        }
        return {
            counters,
            limit : $options.limit,
            offset: $options.offset,
            size,
            traces: output
        };
    }

    @Extern()
    public async TestLogs() : Promise<void> {
        const dummy : number = new Date().getTime();
        LogIt.Warning("Test BE Warning - " + dummy);
        LogIt.Error("Test BE Error - " + dummy);
        await SystemLog.Warning("Test BE sys warning");
        await SystemLog.Error("Test BE sys error");
    }
}

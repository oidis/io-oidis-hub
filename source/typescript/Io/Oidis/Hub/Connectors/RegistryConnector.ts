/*! ******************************************************************************************************** *
 *
 * Copyright 2018-2019 NXP
 * Copyright 2019-2024 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { JsonUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/JsonUtils.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { StringUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/StringUtils.js";
import { IRemoteFileTransferProtocol } from "@io-oidis-gui/Io/Oidis/Gui/Interfaces/Components/IFileUpload.js";
import { IRegistryItem } from "@io-oidis-hub-gui/Io/Oidis/Hub/Gui/Interfaces/IRegistryContent.js";
import { FileSystemHandler } from "@io-oidis-localhost/Io/Oidis/Localhost/Connectors/FileSystemHandler.js";
import { HttpManager } from "@io-oidis-localhost/Io/Oidis/Localhost/HttpProcessor/HttpManager.js";
import { ExternResponseType } from "@io-oidis-localhost/Io/Oidis/Localhost/Interfaces/IExternOptions.js";
import { IResponse } from "@io-oidis-localhost/Io/Oidis/Localhost/Interfaces/IResponse.js";
import { BaseConnector } from "@io-oidis-localhost/Io/Oidis/Localhost/Primitives/BaseConnector.js";
import { Extern } from "@io-oidis-localhost/Io/Oidis/Localhost/Primitives/Decorators.js";
import { IModelListFilter, IModelListResult } from "@io-oidis-services/Io/Oidis/Services/Connectors/AuthManagerConnector.js";
import { ConfigEntry } from "../DAO/Models/ConfigEntry.js";
import { FileEntry } from "../DAO/Models/FileEntry.js";
import { FileInputHandler } from "../IOApi/Handlers/FileInputHandler.js";
import { Loader } from "../Loader.js";
import { ConfigurationsManager } from "../Utils/ConfigurationsManager.js";
import { SelfupdatesManager } from "../Utils/SelfupdatesManager.js";
import { FileTransferHandler } from "./FileTransferHandler.js";

export class RegistryConnector extends BaseConnector {
    private configs : ConfigurationsManager;
    private updates : SelfupdatesManager;
    private inputHandler : FileInputHandler;

    constructor() {
        super();
        this.configs = new ConfigurationsManager();
        this.updates = new SelfupdatesManager();
        this.inputHandler = new FileInputHandler();
    }

    @Extern()
    public async getConfigs() : Promise<IRegistryItem[]> {
        return this.configs.getRegistryItems();
    }

    @Extern()
    public async getUpdates() : Promise<IRegistryItem[]> {
        return this.updates.getRegistryItems();
    }

    @Extern()
    public getDownloads() : IRegistryItem[] {
        const manager : HttpManager = Loader.getInstance().getHttpManager();
        const cwd : string = FileTransferHandler.getInstance<FileTransferHandler>().getDefaultPath();

        return Loader.getInstance().getFileSystemHandler().Expand(cwd + "/**/*").map(($path : string) : IRegistryItem => {
            if (!StringUtils.Contains($path, ".htaccess")) {
                return {
                    link: manager.CreateLink(StringUtils.Replace(
                        StringUtils.Replace($path, "\\", "/"), cwd, "/Download/"))
                };
            }
        });
    }

    @Extern()
    public async getFileEntries($filter? : IModelListFilter) : Promise<IModelListResult<IRegistryItem>> {
        $filter = JsonUtils.Extend({
            limit : -1,
            offset: 0
        }, $filter);
        /// TODO: filter should be able to consider config entry filtering somehow
        // const size : number = await FileEntry.Model().countDocuments({});
        const files : FileEntry[] = await FileEntry.Find({}, {});
        const output : IModelListResult = {
            data  : [],
            limit : $filter.limit,
            offset: $filter.offset,
            size  : 0
        };
        if (!ObjectValidator.IsEmptyOrNull(files)) {
            output.size = 0;
            for await (const file of files) {
                if (!ObjectValidator.IsEmptyOrNull((<any>file).blobId)) {
                    if (ObjectValidator.IsEmptyOrNull(await ConfigEntry.Find({data: file.Id}, {partial: true}))) {
                        if (output.size >= $filter.offset && output.data.length < $filter.limit) {
                            output.data.push({
                                info: "Content owner: " + file.Owner,
                                link: file.getAbsolutePath(),
                                name: file.Id,
                                type: "Blob"
                            });
                        }
                        output.size++;
                    }
                } else {
                    await file.Data();
                    if (file.Size > 0) {
                        if (output.size >= $filter.offset && output.data.length < $filter.limit) {
                            output.data.push({
                                info: "Content owner: " + file.Owner,
                                link: file.getAbsolutePath(),
                                name: file.Id,
                                type: "FS Entry"
                            });
                        }
                        output.size++;
                    }
                }
            }
        }
        return output;
    }

    @Extern()
    public async FileEntryToFS(...$ids : string[]) : Promise<void> {
        const files : FileEntry[] = await FileEntry.Find({_id: $ids});
        if (!ObjectValidator.IsEmptyOrNull(files)) {
            for await (const file of files) {
                await file.ConvertToFS();
            }
        }
    }

    @Extern()
    public async FileEntryToDB(...$ids : string[]) : Promise<void> {
        const files : FileEntry[] = await FileEntry.Find({_id: $ids});
        if (!ObjectValidator.IsEmptyOrNull(files)) {
            for await (const file of files) {
                await file.ConvertToDB();
            }
        }
    }

    @Extern(ExternResponseType.FULL_CONTROL)
    public UploadFile($data : IRemoteFileTransferProtocol, $callback : ($status : boolean) => void | IResponse) : void {
        FileTransferHandler.getInstance<FileTransferHandler>().UploadFile($data, $callback);
    }

    @Extern()
    public RegisterFile($name : string) : void {
        const cwd : string = FileTransferHandler.getInstance<FileTransferHandler>().getDefaultPath();
        const fileSystem : FileSystemHandler = Loader.getInstance().getFileSystemHandler();
        const path : string = fileSystem.NormalizePath(cwd + "/" + $name);
        fileSystem.Write(path, this.inputHandler.getData("RegistryFile"));
    }

    @Extern()
    public IsInDatabase() : boolean {
        return !this.configs.FileRegisterExists() && !this.updates.FileRegisterExists();
    }

    @Extern()
    public async Reindex() : Promise<boolean> {
        return await this.configs.ToDatabase() && this.updates.ToDatabase();
    }

    @Extern()
    public async DeteleConfig($appName : string, $configName : string, $version : string) : Promise<void> {
        return this.configs.Delete($appName, $configName, $version);
    }
}

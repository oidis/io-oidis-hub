/*! ******************************************************************************************************** *
 *
 * Copyright 2019-2023 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { StringUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/StringUtils.js";
import { IRemoteFileTransferProtocol } from "@io-oidis-gui/Io/Oidis/Gui/Interfaces/Components/IFileUpload.js";
import { FileTransferHandler as Parent } from "@io-oidis-localhost/Io/Oidis/Localhost/Connectors/FileTransferHandler.js";
import { ExternResponseType } from "@io-oidis-localhost/Io/Oidis/Localhost/Interfaces/IExternOptions.js";
import { IFileInfo, IRemoteFileInfo } from "@io-oidis-localhost/Io/Oidis/Localhost/Interfaces/IFileInfo.js";
import { IResponse } from "@io-oidis-localhost/Io/Oidis/Localhost/Interfaces/IResponse.js";
import { Extern } from "@io-oidis-localhost/Io/Oidis/Localhost/Primitives/Decorators.js";
import { FileUploadResolver } from "../HttpProcessor/Resolvers/FileUploadResolver.js";
import { Loader } from "../Loader.js";

export class FileTransferHandler extends Parent {

    @Extern(ExternResponseType.FULL_CONTROL)
    public UploadFile($data : IRemoteFileTransferProtocol, $callback : ($status : boolean) => void | IResponse) : void {
        super.UploadFile(<IRemoteFileTransferProtocol>$data, $callback);
    }

    @Extern(ExternResponseType.FULL_CONTROL)
    public DownloadFile($fileInfo : IFileInfo, $callback : IResponse) : void {
        super.DownloadFile(<IRemoteFileInfo>$fileInfo, $callback);
    }

    @Extern()
    public AcknowledgeChunk($chunkId : string) : void {
        super.AcknowledgeChunk($chunkId);
    }

    public getDefaultPath() : string {
        return Loader.getInstance().getProgramArgs().AppDataPath() +
            "/resource/data/" + StringUtils.Replace(FileUploadResolver.ClassName(), ".", "/") + "/";
    }
}

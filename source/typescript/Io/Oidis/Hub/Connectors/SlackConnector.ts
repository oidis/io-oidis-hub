/*! ******************************************************************************************************** *
 *
 * Copyright 2019 NXP
 * Copyright 2019-2023 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { LogIt } from "@io-oidis-commons/Io/Oidis/Commons/Utils/LogIt.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { StringUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/StringUtils.js";
import { ResponseFactory } from "@io-oidis-localhost/Io/Oidis/Localhost/HttpProcessor/ResponseApi/ResponseFactory.js";
import { ExternResponseType } from "@io-oidis-localhost/Io/Oidis/Localhost/Interfaces/IExternOptions.js";
import { IResponse } from "@io-oidis-localhost/Io/Oidis/Localhost/Interfaces/IResponse.js";
import { BaseConnector } from "@io-oidis-localhost/Io/Oidis/Localhost/Primitives/BaseConnector.js";
import { Extern } from "@io-oidis-localhost/Io/Oidis/Localhost/Primitives/Decorators.js";
import { ISlackConfiguration } from "../Interfaces/IProject.js";
import { Loader } from "../Loader.js";

export class SlackConnector extends BaseConnector {

    @Extern(ExternResponseType.FULL_CONTROL)
    public Notify($chanel : string, $text : string, $callback? : IResponse) : void {
        this.NotifyAs($chanel, "", $text, $callback);
    }

    @Extern(ExternResponseType.FULL_CONTROL)
    public NotifyAs($chanel : string, $user : string, $text : string, $callback? : IResponse) : void {
        $callback = ResponseFactory.getResponse($callback);
        const config : ISlackConfiguration = Loader.getInstance().getAppConfiguration().slack;
        if (ObjectValidator.IsEmptyOrNull(config.token)) {
            $callback.Send(false, "Token must be specified.");
        } else {
            if (ObjectValidator.IsEmptyOrNull($chanel)) {
                $callback.Send(false, "Chanel must be specified.");
            } else {
                if (!StringUtils.StartsWith($chanel, "#")) {
                    $chanel = "#" + $chanel;
                }
                const message : any = {
                    channel: $chanel,
                    text   : $text
                };
                if (ObjectValidator.IsEmptyOrNull($user)) {
                    $user = config.name;
                }
                if (!ObjectValidator.IsEmptyOrNull($user)) {
                    message.as_user = false;
                    message.icon_url = config.icon;
                    message.username = $user;
                }
                LogIt.Debug(message);
                const Slack : any = require("slack");
                const connector : any = new Slack({token: config.token});
                connector.chat
                    .postMessage(message)
                    .then(($data : any) : void => {
                        if ($data.ok) {
                            LogIt.Debug($data);
                            $callback.Send(true);
                        } else {
                            LogIt.Warning($data.error);
                            $callback.Send(false, $data.error);
                        }
                    })
                    .catch(($error : Error) : void => {
                        LogIt.Warning($error.stack);
                        $callback.Send(false, $error.message);
                    });
            }
        }
    }
}

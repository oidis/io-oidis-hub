/*! ******************************************************************************************************** *
 *
 * Copyright 2019-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { IModelFindOptions as ParentFind } from "@io-oidis-commons/Io/Oidis/Commons/Primitives/BaseModel.js";

export interface IModelFindOptions extends ParentFind {
    /**
     * Enable this option in case of sort hits memory limits
     */
    allowDiskUse? : boolean;
}

// generated-code-start
export const IModelFindOptions = globalThis.RegisterInterface(["allowDiskUse"], <any>ParentFind);
// generated-code-end

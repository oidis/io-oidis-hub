/*! ******************************************************************************************************** *
 *
 * Copyright 2020-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { IContext as Parent } from "@io-oidis-commons/Io/Oidis/Commons/Interfaces/IBaseObject.js";

/* eslint-disable @typescript-eslint/no-empty-object-type */
/**
 * @deprecated Use IContext from Commons namespace
 */
export interface IContext extends Parent {
    // Fallback mapping
}

/* eslint-enable */

// generated-code-start
export const IContext = globalThis.RegisterInterface([], <any>Parent);
// generated-code-end

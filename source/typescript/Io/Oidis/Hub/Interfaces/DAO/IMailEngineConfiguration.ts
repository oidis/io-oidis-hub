/*! ******************************************************************************************************** *
 *
 * Copyright 2021-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { IBaseConfiguration } from "@io-oidis-services/Io/Oidis/Services/Interfaces/DAO/IBaseConfiguration.js";

export interface IMailEngineConfiguration extends IBaseConfiguration {
    template : string;
    logo : string;
    favicon : string;
    accountsAddress : string;
    contents : IMailType;
    footer : string;
}

export interface IMailType {
    register : IMailContent;
    activation : IMailContent;
    restored : IMailContent;
    deactivation : IMailContent;
    invitation : IMailContent;
    resetPassword : IMailContent;
    resetPasswordConfirm : IMailContent;
    removed : IMailContent;
}

export interface IMailContent {
    subject : string;
    title? : string;
    header : string;
    htmlBody : string;
    navigateText : string;
    plainBody : string;
    template? : string;
}

// generated-code-start
/* eslint-disable */
export const IMailEngineConfiguration = globalThis.RegisterInterface(["template", "logo", "favicon", "accountsAddress", "contents", "footer"], <any>IBaseConfiguration);
export const IMailType = globalThis.RegisterInterface(["register", "activation", "restored", "deactivation", "invitation", "resetPassword", "resetPasswordConfirm", "removed"]);
export const IMailContent = globalThis.RegisterInterface(["subject", "title", "header", "htmlBody", "navigateText", "plainBody", "template"]);
/* eslint-enable */
// generated-code-end

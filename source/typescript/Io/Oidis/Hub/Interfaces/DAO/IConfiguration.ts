/*! ******************************************************************************************************** *
 *
 * Copyright 2020-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { IBaseConfiguration } from "@io-oidis-services/Io/Oidis/Services/Interfaces/DAO/IBaseConfiguration.js";

export interface IConfiguration extends IBaseConfiguration {
    notifications : INotifications;
    recordSchema : any;
}

export interface INotifications {
    api_sample_response : string;
    authentication_is_required : string;
}

export interface ISampleResponse {
    newestApiVersion : number;
    documentationLink : string;
}

// generated-code-start
export const IConfiguration = globalThis.RegisterInterface(["notifications", "recordSchema"], <any>IBaseConfiguration);
export const INotifications = globalThis.RegisterInterface(["api_sample_response", "authentication_is_required"]);
export const ISampleResponse = globalThis.RegisterInterface(["newestApiVersion", "documentationLink"]);
// generated-code-end

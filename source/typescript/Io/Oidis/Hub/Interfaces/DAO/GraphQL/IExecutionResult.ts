/*! ******************************************************************************************************** *
 *
 * Copyright 2019 PASAJA Authors
 * Copyright 2019-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

export interface IExecutionResult {
    data? : { [key : string] : any } | null;
    errors? : any;
}

// generated-code-start
export const IExecutionResult = globalThis.RegisterInterface(["data"]);
// generated-code-end

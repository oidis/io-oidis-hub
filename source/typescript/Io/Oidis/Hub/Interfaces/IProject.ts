/*! ******************************************************************************************************** *
 *
 * Copyright 2017-2019 NXP
 * Copyright 2019-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { IDbConfiguration as ParentDbConfiguration } from "@io-oidis-commons/Io/Oidis/Commons/Interfaces/IProject.js";
import { ISendmailConfiguration } from "@io-oidis-hub-gui/Io/Oidis/Hub/Gui/Interfaces/ISendmailConfiguration.js";
import {
    ICertsConfiguration,
    IProject as Parent,
    IProjectTarget as ILocalhostProjectTarget,
    IServerConfigurationDomain as ILocalhostServerConfigurationDomain
} from "@io-oidis-localhost/Io/Oidis/Localhost/Interfaces/IProject.js";
import { EnvironmentType } from "@io-oidis-services/Io/Oidis/Services/Enums/EnvironmentType.js";

export interface IProject extends Parent {
    domain : IServerConfigurationDomain;
    sendmail : ISendmailConfiguration;
    slack : ISlackConfiguration;
    tunnels : ITunnelsConfiguration;
    certs : ICertsHubConfiguration;
    deploy : IDeployConfiguration;
    /**
     * @deprecated Default user hase been replaced by activation of admin user by Hub GUI
     */
    defaultUser? : IDefaultUserConfiguration;
    agentsRegisterUser : IAgentsRegisterUser;
    dbConfigs : IDbConfigurations | IDbConfiguration;
    authManagerWorker : IAuthManagerWorker;
    target : IProjectTarget;
    noReport : boolean;
    sysLog : ISysLogConfiguration;
    puppeteer : IPuppeteerConfiguration;
    apiConfig : IAPIConfig;
    webhooks : IWebhooks;
    oidishook : IEndPointConfig;
}

export interface IServerConfigurationDomain extends ILocalhostServerConfigurationDomain {
    localhostName : string;
}

export interface ITunnelsConfiguration {
    hubLocation : string;
    server : string[];
    agent : string[];
}

export interface ISlackConfiguration {
    token : string;
    icon : string;
    name : string;
}

export interface IDeployConfiguration {
    slackChannel : string;
}

export interface ICertsHubConfiguration extends ICertsConfiguration {
    whitelist : string[];
}

/**
 * @deprecated Default user hase been replaced by activation of admin user by Hub GUI
 */
export interface IDefaultUserConfiguration {
    enabled : boolean;
    initOnly : boolean;
    defaultAccessibleMethods : string[];
    defaultGroupAccess : string[];
    pass : string;
    user : string;
}

export interface IAgentsRegisterUser {
    pass : string;
    user : string;
}

export interface IDbConfigurations extends ParentDbConfiguration {
    dev : IDbConfiguration;
    prod : IDbConfiguration;
}

export interface IDbConfiguration extends ParentDbConfiguration {
    location? : string;
    port? : number;
    user? : string;
    pass? : string;
    protocol? : string;
    attributes? : string;
    authSource? : string;
    fileStorageLocation? : string;
}

export interface IAuthManagerWorker {
    enabled : boolean;
    activationCheck : IAuthManagerService;
    cleanUpService : IAuthManagerService;
    singInCheck : ILogInService;
    multiSessionEnabled : boolean;
    strictEmailCheck : boolean;
    strictSecureSSOcookies : boolean;
    twoFactorAuthentication : ITwoFAConfig;
}

export interface IAuthManagerService {
    enabled : boolean;
    interval : number;
}

export interface ILogInService extends IAuthManagerService {
    numberOfTries : number;
}

export interface IProjectTarget extends ILocalhostProjectTarget {
    environmentType : EnvironmentType;
    ga4ID : string;
    initPageEnabled : boolean;
    pwaEnabled : boolean;
    preferDBSettings : boolean;
}

export interface ISysLogConfiguration {
    enabled : boolean;
    maxAge : number;
}

export interface IPuppeteerConfiguration {
    enabled? : boolean;
    urlThreads? : number;
    maxWaitTime? : number;
    proxy? : IPuppeteerProxy;
    chromeBuildId? : string;
    cacheDir? : string;
}

export interface IPuppeteerProxy {
    enabled? : boolean;
    location? : string;
}

export interface IAPIConfig {
    maxPrintRequests : number;
    maxIdsPerPrintRequests : number;
}

export interface ITwoFAConfig {
    force : boolean;
    identityName : string;
}

export interface IWebhooks {
    enabled : boolean;
    tick : number;
    bucketSize : number;
}

export interface IEndPointConfig {
    enabled : boolean;
    server : IEndPointServerConfig;
    bidirectionalSync : boolean;
    strongValidation : boolean;
}

export interface IEndPointServerConfig {
    hostname : string;
    userName : string;
    token : string;
}

// generated-code-start
/* eslint-disable */
export const IProject = globalThis.RegisterInterface(["domain", "sendmail", "slack", "tunnels", "certs", "deploy", "defaultUser", "agentsRegisterUser", "dbConfigs", "authManagerWorker", "target", "noReport", "sysLog", "puppeteer", "apiConfig", "webhooks", "oidishook"], <any>Parent);
export const IServerConfigurationDomain = globalThis.RegisterInterface(["localhostName"], <any>ILocalhostServerConfigurationDomain);
export const ITunnelsConfiguration = globalThis.RegisterInterface(["hubLocation", "server", "agent"]);
export const ISlackConfiguration = globalThis.RegisterInterface(["token", "icon", "name"]);
export const IDeployConfiguration = globalThis.RegisterInterface(["slackChannel"]);
export const ICertsHubConfiguration = globalThis.RegisterInterface(["whitelist"], <any>ICertsConfiguration);
export const IDefaultUserConfiguration = globalThis.RegisterInterface(["enabled", "initOnly", "defaultAccessibleMethods", "defaultGroupAccess", "pass", "user"]);
export const IAgentsRegisterUser = globalThis.RegisterInterface(["pass", "user"]);
export const IDbConfigurations = globalThis.RegisterInterface(["dev", "prod"], <any>ParentDbConfiguration);
export const IDbConfiguration = globalThis.RegisterInterface(["location", "port", "user", "pass", "protocol", "attributes", "authSource", "fileStorageLocation"], <any>ParentDbConfiguration);
export const IAuthManagerWorker = globalThis.RegisterInterface(["enabled", "activationCheck", "cleanUpService", "singInCheck", "multiSessionEnabled", "strictEmailCheck", "strictSecureSSOcookies", "twoFactorAuthentication"]);
export const IAuthManagerService = globalThis.RegisterInterface(["enabled", "interval"]);
export const ILogInService = globalThis.RegisterInterface(["numberOfTries"], <any>IAuthManagerService);
export const IProjectTarget = globalThis.RegisterInterface(["environmentType", "ga4ID", "initPageEnabled", "pwaEnabled", "preferDBSettings"], <any>ILocalhostProjectTarget);
export const ISysLogConfiguration = globalThis.RegisterInterface(["enabled", "maxAge"]);
export const IPuppeteerConfiguration = globalThis.RegisterInterface(["enabled", "urlThreads", "maxWaitTime", "proxy", "chromeBuildId", "cacheDir"]);
export const IPuppeteerProxy = globalThis.RegisterInterface(["enabled", "location"]);
export const IAPIConfig = globalThis.RegisterInterface(["maxPrintRequests", "maxIdsPerPrintRequests"]);
export const ITwoFAConfig = globalThis.RegisterInterface(["force", "identityName"]);
export const IWebhooks = globalThis.RegisterInterface(["enabled", "tick", "bucketSize"]);
export const IEndPointConfig = globalThis.RegisterInterface(["enabled", "server", "bidirectionalSync", "strongValidation"]);
export const IEndPointServerConfig = globalThis.RegisterInterface(["hostname", "userName", "token"]);
/* eslint-enable */
// generated-code-end

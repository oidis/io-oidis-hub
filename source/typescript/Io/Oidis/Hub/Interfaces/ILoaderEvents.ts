/*! ******************************************************************************************************** *
 *
 * Copyright 2022-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

export interface ILoaderEvents {
    OnDbConnected($eventHandler : ($eventArgs? : any) => void) : void;

    OnDbPrepared($eventHandler : ($eventArgs? : any) => void) : void;

    OnNewAgentConnected($eventHandler : ($eventArgs? : any) => void) : void;

    OnAgentDisconnected($eventHandler : ($eventArgs? : any) => void) : void;
}

// generated-code-start
export const ILoaderEvents = globalThis.RegisterInterface(["OnDbConnected", "OnDbPrepared", "OnNewAgentConnected", "OnAgentDisconnected"]);
// generated-code-end

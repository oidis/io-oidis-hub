/*! ******************************************************************************************************** *
 *
 * Copyright 2019-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { IFileTransferProtocol, IRemoteFileTransferProtocol } from "@io-oidis-gui/Io/Oidis/Gui/Interfaces/Components/IFileUpload.js";
import { ResponseFactory } from "@io-oidis-localhost/Io/Oidis/Localhost/HttpProcessor/ResponseApi/ResponseFactory.js";
import { ExternResponseType } from "@io-oidis-localhost/Io/Oidis/Localhost/Interfaces/IExternOptions.js";
import { IResponse } from "@io-oidis-localhost/Io/Oidis/Localhost/Interfaces/IResponse.js";
import { BaseConnector } from "@io-oidis-localhost/Io/Oidis/Localhost/Primitives/BaseConnector.js";
import { Extern } from "@io-oidis-localhost/Io/Oidis/Localhost/Primitives/Decorators.js";
import { IReportProtocol } from "@io-oidis-services/Io/Oidis/Services/Connectors/ReportServiceConnector.js";
import { CertsHandler as BaseCertsHandler } from "./../../../Io/Oidis/Hub/Connectors/CertsHandler.js";
import { FileTransferHandler } from "./../../../Io/Oidis/Hub/Connectors/FileTransferHandler.js";
import { SlackConnector as BaseSlackConnector } from "./../../../Io/Oidis/Hub/Connectors/SlackConnector.js";
import { User } from "./../../../Io/Oidis/Hub/DAO/Models/User.js";
import { ForwardingConnector as BaseForwardingConnector } from "./../../../Io/Oidis/Hub/HttpProcessor/Resolvers/ForwardingResolver.js";
import { AuthManager } from "./../../../Io/Oidis/Hub/Utils/AuthManager.js";
import { ConfigurationsManager as BaseConfigurationsManager } from "./../../../Io/Oidis/Hub/Utils/ConfigurationsManager.js";
import { ReportAPI as BaseReportAPI } from "./../../../Io/Oidis/Hub/Utils/ReportAPI.js";
import { SelfupdatesManager as BaseSelfupdatesManager } from "./../../../Io/Oidis/Hub/Utils/SelfupdatesManager.js";
import { SendMail as BaseSendMail } from "./../../../Io/Oidis/Hub/Utils/SendMail.js";

namespace Com.Wui.Framework.Hub.Connectors { // eslint-disable-line @typescript-eslint/no-namespace
    "use strict";

    export class CertsHandler extends BaseCertsHandler {

        @Extern(ExternResponseType.FULL_CONTROL)
        public getCertsFor($domain : string, $callback : IResponse) : void {
            super.getCertsFor($domain, $callback);
        }
    }

    export class SlackConnector extends BaseSlackConnector {

        @Extern(ExternResponseType.FULL_CONTROL)
        public Notify($chanel : string, $text : string, $callback? : IResponse) : void {
            super.Notify($chanel, $text, $callback);
        }

        @Extern(ExternResponseType.FULL_CONTROL)
        public NotifyAs($chanel : string, $user : string, $text : string, $callback? : IResponse) : void {
            super.NotifyAs($chanel, $user, $text, $callback);
        }
    }
}

namespace Com.Wui.Framework.Hub.HttpProcessor.Resolvers { // eslint-disable-line @typescript-eslint/no-namespace
    "use strict";

    export class FileUploadHandler extends FileTransferHandler {

        @Extern(ExternResponseType.FULL_CONTROL)
        public Upload($data : IRemoteFileTransferProtocol, $callback? : ($status : boolean) => void | IResponse) : void {
            if (!ObjectValidator.IsEmptyOrNull((<any>$data).name)) {
                $data.path = (<any>$data).name;
            }
            this.UploadFile($data, $callback);
        }
    }

    export class ForwardingConnector extends BaseForwardingConnector {

        @Extern(ExternResponseType.FULL_CONTROL)
        public RegisterTunnel($capabilities : string[], $callback : IResponse) : void {
            super.RegisterTunnel($capabilities, $callback);
        }
    }
}

namespace Com.Wui.Framework.Hub.Utils { // eslint-disable-line @typescript-eslint/no-namespace
    "use strict";

    export class ConfigurationsManager extends BaseConfigurationsManager {

        @Extern()
        public async Upload($appName : string, $configName : string, $version : string, $data : string) : Promise<boolean> {
            return super.Upload($appName, $configName, $version, $data);
        }

        @Extern(ExternResponseType.FULL_CONTROL)
        public Download($appName : string, $configName : string, $version : string = null, $callback? : IResponse) : void {
            super.Download($appName, $configName, $version, $callback);
        }

        protected getStoragePath($isAbsolute : boolean = true) : string {
            return BaseConfigurationsManager.getInstance<any>().getStoragePath($isAbsolute);
        }
    }

    export class ReportAPI extends BaseReportAPI {

        @Extern()
        public LogIt($message : string) : boolean {
            return super.LogIt($message);
        }

        @Extern(ExternResponseType.FULL_CONTROL)
        public CreateReport($data : IReportProtocol, $sendEmail : boolean = true, $callback? : IResponse) : void {
            super.CreateReport($data, $sendEmail, $callback);
        }
    }

    export class SelfupdatesManager extends BaseSelfupdatesManager {

        @Extern(ExternResponseType.FULL_CONTROL)
        public static Register($appName : string, $releaseName : string, $platform : string, $version : string, $buildTime : number,
                               $fileName : string, $type : string, $callback? : IResponse) : void {
            const response : IResponse = ResponseFactory.getResponse($callback);
            SelfupdatesManager.getInstance<SelfupdatesManager>()
                .Register($appName, $releaseName, $platform, $version, $buildTime, $fileName, $type)
                .then(($status : boolean) : void => {
                    response.Send($status);
                })
                .catch(($error : Error) : void => {
                    response.OnError($error);
                });
        }

        @Extern(ExternResponseType.FULL_CONTROL)
        public static UpdateExists($appName : string, $releaseName : string, $platform : string, $version : string, $buildTime : number,
                                   $fixedVersion : boolean = false, $callback? : IResponse) : void {
            if (!ObjectValidator.IsBoolean($fixedVersion)) {
                $callback = <any>$fixedVersion;
                $fixedVersion = false;
            }
            const response : IResponse = ResponseFactory.getResponse($callback);
            SelfupdatesManager.getInstance<SelfupdatesManager>()
                .UpdateExists($appName, $releaseName, $platform, $version, $buildTime, $fixedVersion)
                .then(($status : boolean) : void => {
                    response.Send($status);
                })
                .catch(($error : Error) : void => {
                    response.OnError($error);
                });
        }

        @Extern(ExternResponseType.FULL_CONTROL)
        public static Upload($data : IFileTransferProtocol, $callback? : ($status : boolean) => void | IResponse) : void {
            $data.path = $data.id;
            this.getInstance<SelfupdatesManager>().Upload($data, $callback);
        }

        @Extern(ExternResponseType.FULL_CONTROL)
        public static Download($appName : string, $releaseName : string = "", $platform : string = "", $version : string = null,
                               $callback? : IResponse) : void {
            super.Download($appName, $releaseName, $platform, $version, $callback);
        }

        @Extern(ExternResponseType.FULL_CONTROL)
        public static DownloadRaw($appName : string, $releaseName : string = "", $platform : string = "", $version : string = null,
                                  $callback? : any) : void {
            super.DownloadRaw($appName, $releaseName, $platform, $version, $callback);
        }

        protected getStoragePath($isAbsolute : boolean = true) : string {
            return BaseSelfupdatesManager.getInstance<any>().getStoragePath($isAbsolute);
        }
    }

    export class SendMail extends BaseSendMail {

        @Extern(ExternResponseType.FULL_CONTROL)
        public Send($to : string | string[], $subject : string, $body : string, $attachments : string[] = null,
                    $plainText : string = "", $copyTo : string | string[] = "", $from : string = "",
                    $callback? : (($status : boolean) => void) | IResponse) : void {
            super.Send($to, $subject, $body, $attachments, $plainText, $copyTo, $from, $callback);
        }
    }

    export class UserManager extends BaseConnector {
        private authManager : AuthManager;

        @Extern(ExternResponseType.FULL_CONTROL)
        public static getAuthorizedMethods($token : string, $callback? : IResponse) : void {
            this.pipe<UserManager>($callback).getAuthorizedMethods($token);
        }

        @Extern(ExternResponseType.FULL_CONTROL)
        public static setAuthorizedMethods($userName : string, $authMethods : string[], $callback? : IResponse) : void {
            this.pipe<UserManager>($callback).setAuthorizedMethods($userName, $authMethods);
        }

        @Extern(ExternResponseType.FULL_CONTROL)
        public LogIn($userName : string, $password : string, $callback? : IResponse) : void {
            this.toSync(this.authManager.LogIn($userName, $password), $callback);
        }

        @Extern(ExternResponseType.FULL_CONTROL)
        public LogOut($token : string, $callback? : IResponse) : void {
            this.toSync(this.authManager.LogOut($token), $callback);
        }

        @Extern(ExternResponseType.FULL_CONTROL)
        public IsAuthenticated($token : string, $callback? : IResponse) : void {
            this.toSync(this.authManager.IsAuthenticated($token), $callback);
        }

        @Extern(ExternResponseType.FULL_CONTROL)
        public IsAuthorizedFor($token : string, $method : string, $callback? : IResponse) : void {
            this.toSync(this.authManager.IsAuthorizedFor($token, $method), $callback);
        }

        @Extern(ExternResponseType.FULL_CONTROL)
        public Register($userName : string, $password : string, $callback? : IResponse) : void {
            this.toSync(this.authManager.Register({
                name          : $userName,
                password      : $password,
                withActivation: true
            }), $callback);
        }

        @Extern(ExternResponseType.FULL_CONTROL)
        public Clear($userName : string, $callback? : IResponse) : void {
            this.toSync(this.authManager.RemoveUser($userName), $callback);
        }

        public getAuthorizedMethods($token : string, $callback? : IResponse) : any {
            this.toSync(this.authManager.getAuthorizedMethods($token), $callback);
        }

        public setAuthorizedMethods($username : string, $authMethods : string[], $callback? : IResponse) : any {
            this.toSync((async () : Promise<any> => {
                return this.authManager.setAuthorizedMethods(
                    (await this.authManager.getUser<User>($username)).DefaultGroup.Name, $authMethods);
            })(), $callback);
        }

        private toSync($asyncMethod : Promise<any>, $callback : IResponse) : void {
            const response : IResponse = ResponseFactory.getResponse($callback);
            $asyncMethod
                .then(($output : any) : void => {
                    response.Send($output);
                })
                .catch(($error : Error) : void => {
                    response.OnError($error);
                });
        }
    }
}

namespace Com.Wui.Framework.Rest.Services.Utils { // eslint-disable-line @typescript-eslint/no-namespace
    "use strict";

    /**
     * @deprecated Backwards compatibility API will to be removed soon
     */
    export class SelfupdatesManager extends BaseSelfupdatesManager {

        @Extern(ExternResponseType.FULL_CONTROL)
        public static Download($appName : string, $releaseName : string = "", $platform : string = "", $version : string = null,
                               $callback? : any) : void {
            super.DownloadRaw($appName, $releaseName, $platform, $version, $callback);
        }

        protected getStoragePath() : string {
            return BaseSelfupdatesManager.getInstance<any>().getStoragePath();
        }
    }
}

export { Com };
